/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.Evento;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListarArtigosRemovidosControllerTest {
    
    public ListarArtigosRemovidosControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setEscolhaEvento method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testSetEscolhaEvento() {
        System.out.println("setEscolhaEvento");
        Evento escolhaEvento = new Evento();
        ListarArtigosRemovidosController instance = new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        instance.setEscolhaEvento(escolhaEvento);
        assertEquals(escolhaEvento, instance.getEscolhaEvento());
    }

    /**
     * Test of setEscolhaSubmissivel method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testSetEscolhaSubmissivel() {
        System.out.println("setEscolhaSubmissivel");
        Submissivel escolhaSubmissivel = new SessaoTematica();
        ListarArtigosRemovidosController instance = new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        instance.setEscolhaSubmissivel(escolhaSubmissivel);
        assertEquals(escolhaSubmissivel, instance.getEscolhaSubmissivel());

    }

    /**
     * Test of getSessoesTematicas method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testGetSessoesTematicas() {
        System.out.println("getSessoesTematicas");
        String id = "";
        ListarArtigosRemovidosController instance = new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        instance.setEscolhaEvento(new Evento());
        List<Submissivel> expResult = new ArrayList<Submissivel>(){{
            add(new Evento());
        }};
        List<Submissivel> result = instance.getSessoesTematicas(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoesRemovidas method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testGetSubmissoesRemovidas() {
        System.out.println("getSubmissoesRemovidas");
        ListarArtigosRemovidosController instance = new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        Utilizador u = new Utilizador("goncal", "greis","pass123", "gonca@gmail.com",new NaoFazNada() );
        List<String> palavras = new ArrayList();
        palavras.add("artigo");
        palavras.add("cientifico");
        Evento e = new Evento();
        ListaAutores la = new ListaAutores();
        la.addAutor(new Autor(u,"ISEP"));
        Artigo a = new Artigo(la,"titulo", "resumo","ficheiro.pdf",new AutorCorrespondente(new Autor(u, "ISEP"), u) );
        a.setPalavrasChave(palavras);
        final Submissao s = new Submissao();
        s.setArtigoInicial(a);
        s.setSubmissaoRegistadaState();
        System.out.println(s.setRemovida());
        e.addSubmissao(s, a, new Utilizador());
        List<Submissao> expResult = new ArrayList(){{
            add(s);
        }};
        instance.setEscolhaEvento(e);
        instance.setEscolhaSubmissivel(e);
        List<Submissao> result = instance.getSubmissoesRemovidas();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListarArtigosRemovidosController instance =new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEventos method, of class ListarArtigosRemovidosController.
     */
    @Test
    public void testGetEventos() {
        System.out.println("getEventos");
        String id = "";
        ListarArtigosRemovidosController instance =new ListarArtigosRemovidosController(new RegistoEventos(new RegistoUtilizadores()));
        List<Evento> expResult = null;
        List<Evento> result = instance.getEventos(id);
        assertEquals(expResult, result);

    }
    
}
