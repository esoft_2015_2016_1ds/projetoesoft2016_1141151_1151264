/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import listas.ListaRevisoes;
import model.Artigo;
import model.Autor;
import model.CP;
import model.Evento;
import model.Organizador;
import model.ProcessoDistribuicao;
import model.Revisao;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import states.evento.EventoEmSubmissaoCameraReadyState;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class GerarEstatisticasEventosControllerTest {
    
    public GerarEstatisticasEventosControllerTest() {
    }
    private GerarEstatisticasEventosController instance;
    private registos.RegistoEventos regE;
    private registos.RegistoUtilizadores regU;
    private NaoFazNada tabela;
    private Utilizador u1,u2,u3;
    private List<Utilizador> lu;
    private Organizador o1,o2;
    private List<Organizador> lo;
    private Evento ev1,ev2,ev3;
    private SessaoTematica st1;
    private List<SessaoTematica> lst;
    private List<Evento> le;
    private Submissao sub;
    private Artigo a;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
          tabela = new NaoFazNada();
        
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", tabela);
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", tabela);
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", tabela);

        lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);

        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        o1 = new Organizador(u1);
        o2 = new Organizador(u2);
        lo = new ArrayList();
        lo.add(o1);
        lo.add(o2);
        
        sub=new Submissao();
        a=new Artigo();
        
        lst=new ArrayList();
        st1= new SessaoTematica();
        lst.add(st1);
        ev1 = new Evento();
        ev2 = new Evento(lo,lst);
        ev2.addSessaoTematica(st1);
        
        
        
        ev1.addSubmissao(sub, a, u1);
        ev2.setState(new EventoEmSubmissaoCameraReadyState(ev2));
        
        ev3 = new Evento();
        le=new ArrayList();
        le.add(ev1);
        le.add(ev2);
        le.add(ev3);
        regE = new RegistoEventos(regU,le);
        instance=new GerarEstatisticasEventosController(regE);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaEventosOrganizadorEmSubmissaoCameraReady method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testGetListaEventosOrganizadorEmSubmissaoCameraReady() {
        System.out.println("getListaEventosOrganizadorEmSubmissaoCameraReady");
        String id = u1.getEmail();
        List<Evento> expResult = new ArrayList();
        expResult.add(ev2);
        List<Evento> result = instance.getListaEventosOrganizadorEmSubmissaoCameraReady(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of selectEvento method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento instance=new Evento();
        Utilizador u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"Kanto Region"));
        a1.setAutorCorrespondente(new Autor(u1,"Kanto Region"),u1);
        Submissao sub1=new Submissao(a1,a1,null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2=new Submissao(a1,a1,null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3=new Submissao(a1,a1,null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4=new Submissao(a1,a1,null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls= new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        instance.addSubmissao(sub1, a1,new Utilizador());
        instance.addSubmissao(sub2, a1,new Utilizador());
        instance.addSubmissao(sub3, a1,new Utilizador());
        instance.addSubmissao(sub4, a1,new Utilizador());
        Revisao rev1=new Revisao();
        Revisao rev2=new Revisao();
        Revisao rev3=new Revisao();
        Revisao rev4=new Revisao();
        rev1.setClassificacaoAdequacao(2);
        rev2.setClassificacaoAdequacao(2);
        rev3.setClassificacaoAdequacao(2);
        rev4.setClassificacaoAdequacao(2);
        rev1.setClassificacaoApresentacao(2);
        rev2.setClassificacaoApresentacao(2);
        rev3.setClassificacaoApresentacao(2);
        rev4.setClassificacaoApresentacao(2);
        rev1.setClassificacaoConfianca(2);
        rev2.setClassificacaoConfianca(2);
        rev3.setClassificacaoConfianca(2);
        rev4.setClassificacaoConfianca(2);
        rev1.setClassificacaoOriginalidade(2);
        rev2.setClassificacaoOriginalidade(2);
        rev3.setClassificacaoOriginalidade(2);
        rev4.setClassificacaoOriginalidade(2);
        rev1.setClassificacaoRecomendacao(2);
        rev2.setClassificacaoRecomendacao(2);
        rev3.setClassificacaoRecomendacao(2);
        rev4.setClassificacaoRecomendacao(2);
        List<Revisao> listr=new ArrayList();
        listr.add(rev1);
        listr.add(rev2);
        listr.add(rev3);
        listr.add(rev4);
        ListaRevisoes lr=new ListaRevisoes(listr);
        double[]v=new double[5];
        v[0]=2.0;
        v[1]=2.0;
        v[2]=2.0;
        v[3]=2.0;
        v[4]=2.0;  
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        double[] expResult = v;
        double[] result = instance.calcularValoresMedios();
        boolean resultBol = result.length==expResult.length;
        if (resultBol){
        for (int i=0;i<result.length;i++)
        {
            resultBol=resultBol && (result[i]==expResult[i]);
        }
        }
        instance.novaCP();
        int sID = System.identityHashCode(instance);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[1];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
        assertEquals(true,resultBol);
    }

    /**
     * Test of getTaxaAceitacao method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testGetTaxaAceitacao() {
        System.out.println("getTaxaAceitacao");
        double expResult = 2.0;
        instance.setTaxaAceitacao(expResult);
        double result = instance.getTaxaAceitacao();
        assertEquals(expResult, result, 2.0);
    }

    /**
     * Test of getValoresMedios method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testGetValoresMedios() {
        System.out.println("getValoresMedios");
        double [] v=new double[5];
        v[0]=2.0;
        v[1]=2.0;
        v[2]=2.0;
        v[3]=2.0;
        v[4]=2.0;
        double[] expResult = v;
        instance.setValoresMedios(v);
        double[] result = instance.getValoresMedios();
        assertArrayEquals(expResult, result,2.0);
 
    }

    /**
     * Test of setTaxaAceitacao method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testSetTaxaAceitacao() {
        System.out.println("setTaxaAceitacao");
        double taxa = 2.0;
        instance.setTaxaAceitacao(taxa);
        assertEquals(taxa,instance.getTaxaAceitacao(),0.0);
    }

    /**
     * Test of setValoresMedios method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testSetValoresMedios() {
        System.out.println("setValoresMedios");
        double[] v=new double[5];
        v[0]=2.0;
        v[1]=2.0;
        v[2]=2.0;
        v[3]=2.0;
        v[4]=2.0;
        double[] valores = v;
        instance.setValoresMedios(valores);
        assertEquals(valores,instance.getValoresMedios());
    }   

    /**
     * Test of showData method, of class GerarEstatisticasEventosController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        GerarEstatisticasEventosController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
