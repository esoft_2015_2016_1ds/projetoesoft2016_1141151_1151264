/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jbraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({DefinirTipoConflitoControllerTest.class, LicitarArtigoControllerTest.class, CriarCPControllerTest.class, AnalisarEstatisticaControllerTest.class, DetetarConflitosControllerTest.class, DefinirCPControllerTest.class, GerarEstatisticasTopicosControllerTest.class, SubmeterVersaoFinalControllerTest.class, AlterarSubmissaoControllerTest.class, DecidirSubmissoesControllerTest.class, RemoverSubmissaoControllerTest.class, RegistarUtilizadorControllerTest.class, DefinirSTControllerTest.class, ReverArtigoControllerTest.class, SubmeterArtigoControllerTest.class, CriarEventoCientificoControllerTest.class, GerarEstatisticasEventosControllerTest.class, ListarArtigosRemovidosControllerTest.class, ListarTodasSubmissoesControllerTest.class, CarregarArtigosControllerTest.class, AlterarUtilizadorControllerTest.class, LoginUtilizadorControllerTest.class, DistribuiRevisoesControllerTest.class})
public class ControllersSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
