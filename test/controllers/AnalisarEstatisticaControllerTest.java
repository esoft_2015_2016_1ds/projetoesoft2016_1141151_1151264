/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import model.Evento;
import model.ProcessoDistribuicao;
import model.Revisao;
import model.Revisor;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author Diogo
 */
public class AnalisarEstatisticaControllerTest {
    
    public AnalisarEstatisticaControllerTest() {
    }
    AnalisarEstatisticaController controller;
    RegistoEventos reg;
    RegistoUtilizadores regU;
    Utilizador u1,u2;
    Revisor target;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        target = new Revisor(u1);
        List<Utilizador> lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        regU = new RegistoUtilizadores(lu);
        List<Evento> le = new ArrayList();
        Evento ev1,ev2;
        ev1 = new Evento();
        ev1.novaCP().addMembroCP(target);
        ev1.addOrganizador(u1);
        ev2 = new Evento();
        ev2.novaCP().addMembroCP(u2);
        ev2.addOrganizador(u2);
        le.add(ev1);
        le.add(ev2);
        reg = new RegistoEventos(regU,le);
        controller=new AnalisarEstatisticaController(reg,regU);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaTodosRevisores method, of class AnalisarEstatisticaController.
     */
    @Test
    public void testGetListaTodosRevisores() {
        System.out.println("getListaTodosRevisores");
        AnalisarEstatisticaController instance = controller;
        List<Revisor> expResult = new ArrayList();
        expResult.add(target);
        expResult.add(new Revisor(u2));
        List<Revisor> result = instance.getListaTodosRevisores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of selectRevisor method, of class AnalisarEstatisticaController.
     */
    @Test
    public void testSelectRevisor() {
        System.out.println("selectRevisor");
        Revisor r = target;
        AnalisarEstatisticaController instance = controller;
        instance.selectRevisor(r);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = controller.showData().split("\n");
        String compareData = chunk[0];
        String expResult = String.valueOf(System.identityHashCode(r));
        assertEquals(compareData,expResult);
    }

    /**
     * Test of getAnaliseResults method, of class AnalisarEstatisticaController.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testGetAnaliseResults() {
        System.out.println("getAnaliseResults");
        AnalisarEstatisticaController instance = controller;
        double expResult = 0.0;
        double result = instance.getAnaliseResults();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of getAnaliseResults method, of class AnalisarEstatisticaController.
     * In this case, there are at least 30 revisions.
     */
    @Test
    public void testGetAnaliseResultsFull() {
        System.out.println("getAnaliseResults");
        AnalisarEstatisticaController instance;
        List<Evento> le = new ArrayList();
        Evento ev1 = new Evento(),ev2=new Evento();
        ev1.novaCP().addMembroCP(target);
        ev2.novaCP().addMembroCP(u2);
        ProcessoDistribuicao pd = ev1.novoProcessoDistribuicao();
        ev1.setProcessoDistribuicao(pd);
        List<Revisao> lr = new ArrayList();
        int[] data = {1,2,2,-1,-1,-1,-1,-2,0,0,0,0,0,2,2,1,1,1,-1,1,-1,-1,-1,-2,-2,2,2,2,1,2,0,0,0,0,0};
        for (int i=0;i<data.length;i++)
        {
            lr.add(new Revisao(1,1,1,1,data[i]));
        }
        pd.setListRevisoes(lr);
        le.add(ev1);
        lr = new ArrayList();
        int[] data2 = {1,2,2,1,2,0,0,-2,1,1,0,1,1,2,2,1,1,1,-1,1,1,0,-1,-1,-2,2,2,2,1,2,0,1,2,0,0};
        for (int i=0;i<data2.length;i++)
        {
            lr.add(new Revisao(1,1,1,1,data2[i]));
        }
        ProcessoDistribuicao pd2 = ev2.novoProcessoDistribuicao();
        pd2.setListRevisoes(lr);
        ev2.setProcessoDistribuicao(pd2);
        le.add(ev2);
        RegistoEventos registoEventos = new RegistoEventos(regU,le);
        instance=new AnalisarEstatisticaController(registoEventos,regU);
        instance.selectRevisor(target);
        double expResult = 1.2330198290770804;
        double result = instance.getAnaliseResults();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of enviarAlerta method, of class AnalisarEstatisticaController.
     */
    @Test
    public void testEnviarAlerta() {
        System.out.println("enviarAlerta");
        AnalisarEstatisticaController instance = controller;
        controller.selectRevisor(target);
        boolean expResult = true;
        boolean result = instance.enviarAlerta();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class AnalisarEstatisticaController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        AnalisarEstatisticaController instance = controller;
        String expResult = "0\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
