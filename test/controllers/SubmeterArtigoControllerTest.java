/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import model.Autor;
import model.Evento;
import model.SessaoTematica;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class SubmeterArtigoControllerTest {
    
    public SubmeterArtigoControllerTest() {
    }
    RegistoEventos reg;
    Utilizador u;
    Utilizador other;
    RegistoUtilizadores regU;
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
        reg= new RegistoEventos(regU);
        other = new Utilizador("outro","OtherPancakes","ilovecandy224","random@gmail.com",new NaoFazNada());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setAutorCorrespondente method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetAutorCorrespondente() {
        System.out.println("setAutorCorrespondente");
        Autor author = new Autor("admin","adminFake@gmail.com","fakeLandia","adminA");
        Evento sub = new Evento();
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(sub);
        instance.addAutor(author);
        instance.setAutorCorrespondente(author);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[4];
        assertEquals(setFile,String.valueOf(System.identityHashCode(author)));
    }

    /**
     * Test of selectSubmissivel method, of class SubmeterArtigoController.
     */
    @Test
    public void testSelectSubmissivel() {
        System.out.println("selectSubmissivel");
        Submissivel sub = new Evento();
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(sub);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[3];
        assertEquals(setFile,String.valueOf(System.identityHashCode(sub)));
    }

    /**
     * Test of setArticleTitle method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetArticleTitle() {
        System.out.println("setArticleTitle");
        String title = "Experiment Title";
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.setArticleTitle(title);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[2];
        assertEquals(title,setFile);
    }

    /**
     * Test of setArticleSummary method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetArticleSummary() {
        System.out.println("setArticleSummary");
        String summary = "Summary example.";
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.setArticleSummary(summary);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[1];
        assertEquals(summary,setFile);
    }

    /**
     * Test of setFile method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.setFile(file);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[0];
        assertEquals(file,setFile);
    }

    /**
     * Test of registerSubmissao method, of class SubmeterArtigoController.
     * In this case, the submission is empty.
     */
    @Test
    public void testRegisterSubmissaoInvalid() {
        System.out.println("registerSubmissao (invalid submission)");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        boolean expResult = false;
        boolean result = instance.registerSubmissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of registerSubmissao method, of class SubmeterArtigoController.
     * In this case, the submission has valid data.
     */
    @Test
    public void testRegisterSubmissaoValidData() {
        System.out.println("registerSubmissao (valid submission)");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        List<String> k = new ArrayList();
        instance.setArtigoData("Abc2", "Yayayayayaya", k,"CandyLand");
        Autor autor = new Autor(u,"PotatoLand");
        instance.addAutor(autor);
        instance.setAutorCorrespondente(autor);
        boolean expResult = false;
        boolean result = instance.registerSubmissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        boolean expResult = true;
        boolean result = instance.addAutor(author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of addAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testAddAutorRepeated() {
        System.out.println("addAutorRepeated (trying to add the same author.)");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.addAutor(author);
        boolean expResult = false;
        boolean result = instance.addAutor(author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of removeAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testRemoveAutor() {
        System.out.println("removeAutor");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.addAutor(author);
        instance.removeAutor(author);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validateArticle method, of class SubmeterArtigoController.
     */
    @Test
    public void testValidateArticle() {
        System.out.println("validateArticle");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.addAutor(author);
        boolean expResult = false;
        boolean result = instance.validateArticle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoData method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetArtigoData() {
        System.out.println("setArtigoData");
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
                List<String> k = new ArrayList();
        k.add("Here's");
        k.add("Johnny!");
        instance.setArtigoData(title, summary, k,"Underworld");
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String setFile = chunk[0]+"/"+chunk[1]+"/"+chunk[2];
        assertEquals(file+"/"+summary+"/"+title,setFile);
    }

    /**
     * Test of getListaSubmissiveisEmEstadoAceitaSubmissoes method, of class SubmeterArtigoController.
     */
    @Test
    public void testGetListaSubmissiveisEmEstadoAceitaSubmissoes() {
        System.out.println("getListaSubmissiveisEmEstadoAceitaSubmissoes");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        Evento ev = new Evento();
        ev.addSessaoTematica(new SessaoTematica());
        instance.selectSubmissivel(ev);
        List<Submissivel> expResult = new ArrayList();
        List<Submissivel> result = instance.getListaSubmissiveisEmEstadoAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getPossiveisAutorCorrespondentes method, of class SubmeterArtigoController.
     */
    @Test
    public void testGetPossiveisAutorCorrespondentes() {
        System.out.println("getPossiveisAutorCorrespondentes (only email)");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author2 = new Autor("admin","admin@gmail.com","fakeLandia","notAUsername");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.addAutor(author);
        instance.addAutor(author2);
        List<Autor> expResult = new ArrayList();
        expResult.add(author2);
        List<Autor> result = instance.getPossiveisAutorCorrespondentes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of getPossiveisAutorCorrespondentes method, of class SubmeterArtigoController.
     */
    @Test
    public void testGetPossiveisAutorCorrespondentesOnlyUsername() {
        System.out.println("getPossiveisAutorCorrespondentes (only username)");
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author2 = new Autor("admin","adminFake@gmail.com","fakeLandia","adminA");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        instance.addAutor(author);
        instance.addAutor(author2);
        List<Autor> expResult = new ArrayList();
        expResult.add(author2);
        List<Autor> result = instance.getPossiveisAutorCorrespondentes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getInfoResumo method, of class SubmeterArtigoController.
     */
    @Test
    public void testGetInfoResumo() {
        System.out.println("getInfoResumo");
        Autor author = new Autor(u,"WhereAmI");
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        List<String> k = new ArrayList();
        k.add("Here's");
        k.add("Johnny!");
        instance.setArtigoData(title, summary, k,"WhereAmI");
        instance.addAutor(author);
        instance.setAutorCorrespondente(author);
        String expResult = "Título: "+title+";Resumo: "+summary+";Autor Correspondente: "
                +"Autor: "+author+";Utilizador: "+u+";Ficheir: "+k+";Lista de autores:"+author;
        String result = instance.getInfoResumo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testNovoAutor() {
        System.out.println("novoAutor");
        String name = "Abc";
        String email = "bcd@gmail.com";
        String institution = "Lololol";
        String username = "ILikeBeef";
        SubmeterArtigoController instance = new SubmeterArtigoController(reg,u,regU);
        instance.selectSubmissivel(new Evento());
        Autor expResult = new Autor(name,email,institution,username);
        Autor result = instance.novoAutor(name, email, institution, username);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of registerSubmissao method, of class SubmeterArtigoController.
     */
    @Test
    public void testRegisterSubmissao() {
        System.out.println("registerSubmissao");
        SubmeterArtigoController instance = null;
        boolean expResult = false;
        boolean result = instance.registerSubmissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showData method, of class SubmeterArtigoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SubmeterArtigoController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
