/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Licitavel;
import interfaces.MecanismoDetecao;
import java.util.ArrayList;
import java.util.List;
import model.Artigo;
import model.Evento;
import model.Licitacao;
import model.Revisor;
import model.Submissao;
import model.TipoConflito;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoTiposConflito;
import registos.RegistoUtilizadores;
import states.evento.EventoEmLicitacaoState;
import states.submissao.SubmissaoEmLicitacaoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class LicitarArtigoControllerTest {
    
    public LicitarArtigoControllerTest() {
    }
    RegistoEventos reg;
    Utilizador u;
    Utilizador other;
    RegistoUtilizadores regU;
    RegistoTiposConflito regTC;
    LicitarArtigoController controller;
    TipoConflito tc1,tc2;
    Evento ev1,ev2;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
        List<TipoConflito> ltc = new ArrayList();
        tc1=new TipoConflito("Autor do Artigo","É um dos autores do artigo.");
        tc2 = new TipoConflito("Revisor Apple","Um revisor que pertence à companhia Apple.");
        tc1.setMecanismoDetecao(new MecanismoDetecao() {

            @Override
            public boolean deteta(Revisor r, Submissao s) {
                return s.temAutorArtigoInicial(r.getUtilizador().getEmail());
            }
        });
        tc2.setMecanismoDetecao(new MecanismoDetecao() {

            @Override
            public boolean deteta(Revisor r, Submissao s) {
                return s.getArtigoFinal().getTitulo().contains("Apple");
            }
        });
        ev1 = new Evento();
        ev1.setTitulo("Pikachu!");
        List<Evento> le = new ArrayList();
        ev1.novaCP().addMembroCP(new Revisor(u));
        ev1.setState(new EventoEmLicitacaoState(ev1));
        ev2 = new Evento();
        ev2.setTitulo("Pika-pika!");
        ev2.setState(new EventoEmLicitacaoState(ev2));
        ev2.novaCP().addMembroCP(new Revisor(u));
        le.add(ev2);
        le.add(ev1);
        ltc.add(tc1);
        ltc.add(tc2);
        regTC = new RegistoTiposConflito(ltc);
        reg= new RegistoEventos(regU,le);
        controller= new LicitarArtigoController(reg,regTC);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of selecionaLicitavel method, of class LicitarArtigoController.
     */
    @Test
    public void testSelecionaLicitavel() {
        System.out.println("selecionaLicitavel");
        Licitavel sub = new Evento();
        LicitarArtigoController instance = new LicitarArtigoController(reg,regTC);
        instance.selecionaLicitavel(sub);
        String[] chunk = instance.showData().split("\n");
        String compareData = chunk[0];
        assertEquals(compareData,String.valueOf(System.identityHashCode(sub)));
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setInteresse method, of class LicitarArtigoController.
     */
    @Test
    public void testSetInteresse() {
        System.out.println("setInteresse");
        int interesse = 2;
        LicitarArtigoController instance = controller;
        controller.selecionaLicitavel(ev2);
        controller.selectSubmissao(new Submissao());
        controller.novaLicitacao(u.getEmail());
        instance.setInteresse(interesse);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaLicitacao method, of class LicitarArtigoController.
     */
    @Test
    public void testValidaLicitacao() {
        System.out.println("validaLicitacao");
        LicitarArtigoController instance = controller;
        controller.selecionaLicitavel(ev2);
        controller.selectSubmissao(new Submissao());
        controller.novaLicitacao(u.getEmail());
        controller.setInteresse(2);
        boolean expResult = true;
        boolean result = instance.validaLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of registaLicitacao method, of class LicitarArtigoController.
     */
    @Test
    public void testRegistaLicitacao() {
        System.out.println("registaLicitacao");
        LicitarArtigoController instance = controller;
        instance.selectSubmissao(new Submissao());
        instance.selecionaLicitavel(ev1);
        instance.novaLicitacao(u.getEmail());
        boolean result=instance.registaLicitacao();
        boolean expResult=true;
        assertEquals(expResult,result);
        
    }

    /**
     * Test of showData method, of class LicitarArtigoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        LicitarArtigoController instance = controller;
        String expResult = "0\n0\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaLicitaveisEmLicitacaoDe method, of class LicitarArtigoController.
     */
    @Test
    public void testGetListaLicitaveisEmLicitacaoDe() {
        System.out.println("getListaLicitaveisEmLicitacaoDe");
        String id = u.getEmail();
        LicitarArtigoController instance = controller;
        List<Licitavel> expResult = new ArrayList();
        expResult.add(ev2);
        expResult.add(ev1);
        List<Licitavel> result = instance.getListaLicitaveisEmLicitacaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of selectSubmissao method, of class LicitarArtigoController.
     */
    @Test
    public void testSelectSubmissao() {
        System.out.println("selectSubmissao");
        Submissao s = new Submissao();
        LicitarArtigoController instance = controller;
        instance.selectSubmissao(s);
        String[] chunk = instance.showData().split("\n");
        String compareData = chunk[1];
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(compareData,String.valueOf(System.identityHashCode(s)));
    }

    /**
     * Test of getSubmissoesEmLicitacao method, of class LicitarArtigoController.
     */
    @Test
    public void testGetSubmissoesEmLicitacao() {
        System.out.println("getSubmissoesEmLicitacao");
        LicitarArtigoController instance = controller;
        controller.selecionaLicitavel(ev1);
        Submissao s = new Submissao();
        s.setState(new SubmissaoEmLicitacaoState(s));
        ev1.addSubmissao(s, new Artigo(), u);
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getSubmissoesEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaTiposConflito method, of class LicitarArtigoController.
     */
    @Test
    public void testGetListaTiposConflito() {
        System.out.println("getListaTiposConflito");
        LicitarArtigoController instance = controller;
        List<TipoConflito> expResult = new ArrayList();
        expResult.add(tc1);
        expResult.add(tc2);
        List<TipoConflito> result = instance.getListaTiposConflito();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getInfo method, of class LicitarArtigoController.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Submissao s = new Submissao();
        LicitarArtigoController instance = controller;
        String expResult = s.toString();
        String result = instance.getInfo(s);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novaLicitacao method, of class LicitarArtigoController.
     */
    @Test
    public void testNovaLicitacao() {
        System.out.println("novaLicitacao");
        String id = u.getEmail();
        LicitarArtigoController instance = controller;
        instance.selectSubmissao(new Submissao());
        instance.selecionaLicitavel(ev1);
        Licitacao result = instance.novaLicitacao(id);
        boolean expResult = result!=null;
        assertEquals(expResult, true);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addTipoConflito method, of class LicitarArtigoController.
     */
    @Test
    public void testAddTipoConflito() {
        System.out.println("addTipoConflito");
        TipoConflito tc = tc1;
        LicitarArtigoController instance = controller;
        instance.selectSubmissao(new Submissao());
        instance.selecionaLicitavel(ev1);
        instance.novaLicitacao(u.getEmail());
        boolean expResult = true;
        boolean result = instance.addTipoConflito(tc);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
