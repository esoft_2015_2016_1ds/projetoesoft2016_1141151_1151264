/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import model.Empresa;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.Coder;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class RegistarUtilizadorControllerTest {
    
    public RegistarUtilizadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoUtilizador method, of class RegistarUtilizadorController.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        RegistarUtilizadorController instance = new RegistarUtilizadorController(new Empresa());
        instance.novoUtilizador();
    }

    /**
     * Test of setDados method, of class RegistarUtilizadorController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String strUsername = "greis";
        String strPassword = "password123";
        String strNome = "Gonçalo Reis";
        String strEmail = "greis@gmail.com";
        Empresa e = new Empresa();
        e.setTabelasFreq(new ArrayList<Coder>(){{
                add(new NaoFazNada());
        }});
        RegistarUtilizadorController instance = new RegistarUtilizadorController(e);
        instance.novoUtilizador();
        instance.setDados(strUsername,strPassword,strNome,strEmail);
        Utilizador expResult = new Utilizador("Gonçalo Reis","greis", "password123", "greis@gmail.com", new NaoFazNada());
    }
    
    /**
     * Test of setDados method, with invalid user atributes. As each atribute has already been tested
     * there is only need to test an invalid attribute and if that test passes then every invalid atribute should 
     * be covered
     */
    @Test (expected=IllegalArgumentException.class)
    public void testSetDadosInvalid() {
        System.out.println("setDadosInvalid");
        String strUsername = "g";
        String strPassword = "password123";
        String strNome = "Gonçalo Reis";
        String strEmail = "greis@gmail.com";
        Empresa e = new Empresa();
        e.setTabelasFreq(new ArrayList<Coder>(){{
                add(new NaoFazNada());
        }});
        RegistarUtilizadorController instance = new RegistarUtilizadorController(e);
        instance.novoUtilizador();
        instance.setDados(strUsername,strPassword,strNome,strEmail);
        Utilizador expResult = new Utilizador("Gonçalo Reis","greis", "password123", "greis@gmail.com", new NaoFazNada());
    }

    /**
     * Test of showData method, of class RegistarUtilizadorController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        RegistarUtilizadorController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of adicionarUtilizador method, of class RegistarUtilizadorController.
     */
    @Test
    public void testAdicionarUtilizador() {
        System.out.println("adicionarUtilizador");
        RegistarUtilizadorController instance = null;
        boolean expResult = false;
        boolean result = instance.adicionarUtilizador();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
