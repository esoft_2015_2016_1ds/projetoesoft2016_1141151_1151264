package controllers;

import java.util.ArrayList;
import java.util.List;
import model.CP;
import model.Evento;
import model.Organizador;
import model.Proponente;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.sessaotematica.SessaoTematicaRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class DefinirCPControllerTest {

    public DefinirCPControllerTest() {
    }
    private RegistoEventos regE;
    private RegistoUtilizadores regU;
    private Organizador o1, o2;
    private Utilizador u1, u2, u3;
    private List<Utilizador> lu;
    private List<Organizador> lo;
    private Submissao s1, s2;
    private List<Submissao> ls;
    private List<Evento> le;
    private Evento ev1, ev2, ev3;
    private NaoFazNada tabela;
    private DefinirCPController instance;
    private SessaoTematica st1,st2,st3;
    private List<SessaoTematica> lst;
    private Revisor r1,r2,r3;
    private List<Revisor>lr;
    private Proponente p1;
    private List<Proponente> lp;

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        tabela = new NaoFazNada();
        
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", tabela);
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", tabela);
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", tabela);

        p1=new Proponente(u2);
        lp=new ArrayList();
        lp.add(p1);
        
        
        lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);

        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        o1 = new Organizador(u1);
        o2 = new Organizador(u2);
        lo = new ArrayList();
        lo.add(o1);
        lo.add(o2);

        r1=new Revisor(u1);
        r2=new Revisor(u2);
        r3=new Revisor(u3);
        lr=new ArrayList();
        lr.add(r1);
        lr.add(r2);
        lr.add(r3);
        
        
    
        lst=new ArrayList();
        ev1 = new Evento();
        
        ev3 = new Evento();
        le=new ArrayList();
        le.add(ev1);
        
        le.add(ev3);
        st1 = new SessaoTematica();
        st1.setState(new SessaoTematicaRegistadoState(st1));
        st2=new SessaoTematica();
        st3= new SessaoTematica(lp);
        st3.setState(new SessaoTematicaRegistadoState(st3));
        lst.add(st1);
        
        lst.add(st1);
        lst.add(st2);
        lst.add(st3);
        ev1.addSessaoTematica(st1);
        ev2 = new Evento(lo, lst);
        le.add(ev2);
        s1 = new Submissao();
        s2 = new Submissao();
        ls = new ArrayList();
        ls.add(s1);
        ls.add(s2);
        regE = new RegistoEventos(regU,le);
        
        instance = new DefinirCPController(regE, regU);

    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of selectSessaoTematica method, of class DefinirCPController.
     */
    @Test
    public void testSelectSessaoTematica() {
        System.out.println("selectSessaoTematica");
        instance.selectSessaoTematica(st1);
        int sID = System.identityHashCode(st1);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[0];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
        
        
        
    }

    /**
     * Test of novaCP method, of class DefinirCPController.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");        
        instance.novaCP();
        String temp[] = instance.showData().split("\n");
        String compareData = temp[1];      
        assertEquals(compareData,"CP não nula");

    }

    /**
     * Test of addMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        instance.selectSessaoTematica(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.addMembroCP(r1);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testRegistaMembroCP() {
        System.out.println("registaMembroCP");
        CP cp=new CP(lr);
        instance.selectSessaoTematica(st1);
        boolean expResult = true;
        boolean result = instance.registaMembroCP(r1);
        assertEquals(expResult, result);
    }

    /**
     * Test of setCP method, of class DefinirCPController.
     */
    @Test
    public void testSetCP() {
        System.out.println("setCP");
        CP cp = new CP(lr);
        instance.selectSessaoTematica(st1);
        boolean result =instance.setCP();
        boolean expResult = true;
        assertEquals(expResult,result);
    }

    /**
     * Test of getSessoesTematicasProponenteEmEstadoRegistado method, of class DefinirCPController.
     */
    @Test
    public void testGetSessoesTematicasProponenteEmEstadoRegistado() {
        System.out.println("getSessoesTematicasProponenteEmEstadoRegistado");
        String id = u2.getEmail();
        List<SessaoTematica> expResult =new ArrayList();
        expResult.add(st3);
        List<SessaoTematica> result = instance.getSessoesTematicasProponenteEmEstadoRegistado(id);
        
        assertEquals(expResult, result);
    }

   /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP_Revisor() {
        System.out.println("addMembroCP");
        instance.selectSessaoTematica(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.addMembroCP(r1);
        assertEquals(expResult, result);
        
    }

       /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP_String() {
        System.out.println("addMembroCP");
        String strId = u1.getEmail();
        instance.selectSessaoTematica(st2);
        boolean expResult = true;
        boolean result = instance.addMembroCP(strId);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of novoMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testNovoMembroCP() {
        System.out.println("novoMembroCP");
        String id = u1.getEmail();
        instance.selectSessaoTematica(st1);
        instance.novoMembroCP(id);  
    }

    /**
     * Test of showData method, of class DefinirCPController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        DefinirCPController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
