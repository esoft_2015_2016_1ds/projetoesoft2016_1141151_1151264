/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import model.Empresa;
import model.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.evento.EventoRegistadoState;

/**
 *
 * @author jbraga
 */
public class DetetarConflitosControllerTest {
    
    public DetetarConflitosControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DetetarConflitosController.
     */
    @Test(expected=RuntimeException.class)
    public void testRun() {
        System.out.println("run");
        Empresa empresa = new Empresa();
        Evento evento = new Evento();
        evento.setState(new EventoRegistadoState(evento));
        DetetarConflitosController instance = new DetetarConflitosController(empresa,evento);
        instance.run();
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of showData method, of class DetetarConflitosController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Empresa empresa = new Empresa();
        Evento evento = new Evento();
        evento.setState(new EventoRegistadoState(evento));
        DetetarConflitosController instance = new DetetarConflitosController(empresa,evento);
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
