/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import model.CP;
import model.Empresa;
import model.Evento;
import model.Local;
import model.ProcessoDecisao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import static sun.misc.ClassFileTransformer.add;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class CriarEventoCientificoControllerTest {
    
    public CriarEventoCientificoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        Evento expResult = new Evento();
        instance.novoEvento();        
        Evento result = instance.getEvento();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEventoString method, of class CriarEventoCientificoController.
     */
    @Test
    public void testGetEventoString() {
        System.out.println("getEventoString");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        instance.novoEvento();
        String expResult = "Evento: " +null + "/" + null+ "\n";
        String result = instance.getEventoString();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTitulo method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "Titulo";
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        instance.setTitulo(strTitulo);
        String result = instance.getEvento().getTitle();
        assertEquals(strTitulo, result);
        
    }

    /**
     * Test of setDescricao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = "descricao";
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        instance.setDescricao(strDescricao);
        String result = instance.getEvento().getDescription();
        assertEquals(strDescricao, result);
    }

    /**
     * Test of setLocal method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String strLocal = "Local";
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        instance.novoEvento();
        instance.setLocal(strLocal);
        Local expResult =new Local("-1","Local");
        Local result = instance.getEvento().getLocal();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataInicio method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        instance.setDataInicio(inicio);
    }

    /**
     * Test of setDataFim method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data fim = new Data(3, 9 , 2015);
        instance.setDataInicio(inicio);
        instance.setDataFim(fim);
    }

    /**
     * Test of setDataInicioSubmissao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataInicioSubmissao() {
        System.out.println("setDataInicioSubmissao");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
    }

    /**
     * Test of setDataFimSubmissao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataFimSubmissao() {
        System.out.println("setDataFimSubmissao");
CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
 instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fimSub = new Data(21,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
        instance.setDataFimSubmissao(fimSub);
    }

    /**
     * Test of setDataInicioDistribuicao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataInicioDistribuicao() {
        System.out.println("setDataInicioDistribuicao");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
        instance.setDataFimSubmissao(fimSub);
        instance.setDataInicioDistribuicao(incioDist);
    }

    /**
     * Test of addOrganizador method, of class CriarEventoCientificoController.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        String strId = "greis";
        RegistoUtilizadores reg = new RegistoUtilizadores(new ArrayList<Utilizador>(){{
            add(new Utilizador("Gonçalo", "greis", "pass123", "goncaloinho@gmail.com", new NaoFazNada()));
        }});
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        boolean expResult = false;
        boolean result = instance.addOrganizador(strId);
        assertEquals(expResult, result);

    }

    /**
     * Test of registaEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        CriarEventoCientificoController instanceD = new CriarEventoCientificoController(new Empresa());
        Evento instance = new Evento();
         Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
        instance.setTitulo("titulo");
        instance.setDescricao("description");
        Local l = new Local();
        l.setLocal("Local");
        instance.setLocal(l);
        instance.setCP(new CP());
        instance.setProcessoDecisao(new ProcessoDecisao());
        boolean expResult = true;
        boolean result = instanceD.registaEvento();
        assertEquals(expResult, result);

    }

    /**
     * Test of showData method, of class CriarEventoCientificoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    /**
     * Test of setDataLimiteSubmissaoFinal method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
        instance.setDataFim(fim);
        instance.setDataFimSubmissao(fimSub);
        instance.setDataInicioDistribuicao(incioDist);
        instance.setDataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
    }

    /**
     * Test of setDataLimiteRevisao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
        instance.setDataFimSubmissao(fimSub);
        instance.setDataInicioDistribuicao(incioDist);
        instance.setDataLimiteRevisao(limRev);
    }

    /**
     * Test of createTimers method, of class CriarEventoCientificoController.
     */
    @Test
    public void testCreateTimers() {
        System.out.println("createTimers");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
         instance.novoEvento();
         Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        instance.setDataInicio(inicio);
        instance.setDataInicioSubmissao(inicioSub);
        instance.setDataFim(fim);
        instance.setDataFimSubmissao(fimSub);
        instance.setDataInicioDistribuicao(incioDist);
        instance.setDataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
            instance.createTimers();
    }

    /**
     * Test of getEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testGetEvento() {
        System.out.println("getEvento");
        CriarEventoCientificoController instance = new CriarEventoCientificoController(new Empresa());
        Evento expResult = new Evento();
        instance.novoEvento();
        Evento result = instance.getEvento();
        assertEquals(expResult, result);
    }
    
}
