/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import java.util.ArrayList;
import model.Empresa;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class AlterarUtilizadorControllerTest {
    
    public AlterarUtilizadorControllerTest() {
    }
    Utilizador u;
    Utilizador other;
    Utilizador outro;
    Utilizador uClone;
    RegistoUtilizadores regU;
    Empresa empresa;
        
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        u = new Utilizador("name","Username","Pass123","Email@gmail.com",new NaoFazNada());
        other=new Utilizador("newName","newUsername","newPass","nEmail@gmail.com",new NaoFazNada());
        outro=new Utilizador("outroName","outroUsername","outroPass","outroEmail@gmail.com",new NaoFazNada());
        uClone=new Utilizador("name","Username","Pass123","Email@gmail.com",new NaoFazNada());
        ArrayList<Utilizador> us = new ArrayList();
        us.add(u);
        us.add(other);
        us.add(outro);
        regU=new RegistoUtilizadores(us);
        empresa = new Empresa();
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     * This case tests a valid change
     */
    @Test
    public void testAlteraDados() {
        System.out.println("alteraDados");
        String username = "you";
        String password = "wee123";
        String email = "they@gmail.com";
        String nome = "neymar";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult=true;
        boolean newu = instance.alteraDados(username, password, email, nome);
        assertEquals(expResult, newu);
    }
    /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     * This case tests an invalid email
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAlteraDados1() 
    {
        System.out.println("alteraDados1");
        String username = "you";
        String password = "we123";
        String email = "trewertwert";
        String nome = "neymar";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult=false;
        boolean newu = instance.alteraDados(username, password, email, nome);
        assertEquals(expResult, newu);
    }
    /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     * This case tests an invalid name
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAlteraDados2() {
        System.out.println("alteraDados2");
        String username = "you";
        String password = "we123";
        String email = "they@gmail.com";
        String nome = "i";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult=false;
        boolean newu = instance.alteraDados(username, password, email, nome);
        assertEquals(expResult, newu);
    }
    /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     * This case tests an invalid password
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAlteraDados3() {
        System.out.println("alteraDados3");
        String username = "you";
        String password = "";
        String email = "they@gmail.com";
        String nome = "Neymar";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult=false;
        boolean newu = instance.alteraDados(username, password, email, nome);
        assertEquals(expResult, newu);
    }
     /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     * This case tests an invalid password
     */
    @Test(expected=IllegalArgumentException.class)
    public void testAlteraDados4() {
        System.out.println("alteraDados4");
        String username = "123123123";
        String password = "wee123";
        String email = "they@gmail.com";
        String nome = "Neymar";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult=false;
        boolean newu = instance.alteraDados(username, password, email, nome);
        assertEquals(expResult, newu);
    }
    /**
     * Test of registaAlteracao method, of class AlterarUtilizadorController.
     * This method tests a valid change
     */
    @Test
    public void testRegistaAlteracao() {
        System.out.println("registaAlteracao");
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult = true;
        boolean result = instance.registaAlteracao();
        assertEquals(expResult, result);

    }
    /**
     * Test of registaAlteracao method, of class AlterarUtilizadorController.
     * This method tests an invalid change
     */
    @Test
    public void testRegistaAlteracao1() {
        System.out.println("registaAlteracao1");
        Utilizador newUClone = new Utilizador("newName","newUsername","newPass","nEmail@gmail.com",new NaoFazNada());
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        boolean expResult = false;
        boolean result = instance.registaAlteracao();
        assertEquals(expResult, result);

    }
    /**
     * Test of showData method, of class AlterarUtilizadorController.
     */
    @Test
    public void testShowData() 
    {
        System.out.println("showData");
        empresa.setUtilizadorLogged(u);
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        String result = (instance.showData().split("\n"))[1];
        String  expResult = u.toString();
        assertEquals(result, expResult);
    }

    /**
     * Test of getUtilizadorAutenticado method, of class AlterarUtilizadorController.
     */
    @Test
    public void testGetUtilizadorAutenticado() {
        System.out.println("getUtilizadorAutenticado");
        empresa.setUtilizadorLogged(u);
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);
        instance.getUtilizadorAutenticado();
        String expResult = u.toString();
        String result = (instance.showData().split("\n"))[1];
        assertEquals(expResult,result);
    }

    /**
     * Test of criarClone method, of class AlterarUtilizadorController.
     */
    @Test
    public void testCriarClone() {
        System.out.println("criarClone");
        AlterarUtilizadorController instance = null;
        boolean expResult = false;
        boolean result = instance.criarClone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInfo method, of class AlterarUtilizadorController.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        empresa.setUtilizadorLogged(u);
        AlterarUtilizadorController instance = new AlterarUtilizadorController(empresa);             
        String expResult = u.toString();
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }
    
}
