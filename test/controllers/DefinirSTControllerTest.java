/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import model.Empresa;
import model.Evento;
import model.Organizador;
import model.SessaoTematica;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.evento.EventoRegistadoState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class DefinirSTControllerTest {
    
    public DefinirSTControllerTest() {
    }
    private Evento ev1,ev2;
    private List<Evento> lev;
    private Organizador o1,o2,o3;
    private List<Organizador> lorg;
    private List<SessaoTematica> lst;
    private SessaoTematica st1,st2;
    private RegistoEventos regE;
    private Utilizador u1,u2,u3;
    private RegistoUtilizadores regU;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", new NaoFazNada());
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", new NaoFazNada());
        
        o1=new Organizador(u1);
        o2=new Organizador(u2);
        o3=new Organizador(u3);
        lorg=new ArrayList();
        lorg.add(o1);
        lorg.add(o2);
        lorg.add(o3);
        
        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        st1=new SessaoTematica();
        st2=new SessaoTematica();
        lst=new ArrayList();
        lst.add(st1);
        lst.add(st2);
        
        
        ev1= new Evento();
        ev1.setdataInicio(new Data(27,6,2015));
        ev1.setdataFim(new Data(28,6,2015));
        ev1.setdataInicioSubmissao(new Data(20,6,2015));
        ev1.setdataFimSubmissao(new Data(21,6,2015));
        ev1.setdataInicioDistribuicao(new Data(22,6,2015));
        ev1.setDataLimiteSubmissaoFinal(new Data(23,6,2015));
        ev1.setdataLimiteRevisao(new Data(24,6,2015));
        ev2=new Evento();
        lev=new ArrayList();
        lev.add(ev1);
        lev.add(ev2);
        
        regE =new RegistoEventos(regU, lev);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novaSessaoTematica method, of class DefinirSTController.
     */
    @Test
    public void testNovaSessaoTematica() {
        System.out.println("novaSessaoTematica");
        DefinirSTController instance=new DefinirSTController(new Empresa());
        instance.novaSessaoTematica();
        String temp[] = instance.showData().split("\n");
        String compareData = temp[1];      
        assertEquals(compareData,"SessaoTematica não nula");
    }

    /**
     * Test of setCodigo method, of class DefinirSTController.
     */
    @Test
    public void testSetCodigo() {
        System.out.println("setCodigo");
        String codigo = "ola";
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.novaSessaoTematica(); 
        instance.setCodigo(codigo);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[2];     
        assertEquals(compareData, codigo);
    }

    /**
     * Test of setDescricao method, of class DefinirSTController.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String descricao = "adeus";
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.novaSessaoTematica(); 
        instance.setDescricao(descricao);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[3];     
        assertEquals(compareData, descricao);
    }

    /**
     * Test of addProponente method, of class DefinirSTController.
     */
    @Test
    public void testAddProponente() {
        System.out.println("addProponente");
        String id = u1.getEmail();
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.novaSessaoTematica();
        boolean expResult = true;
        boolean result = instance.addProponente(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSessaoTematicaString method, of class DefinirSTController.
     */
    @Test
    public void testToString() {
        System.out.println("getToString");
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.novaSessaoTematica();
        String temp[] = instance.showData().split("\n");
        String expResult = "SessaoTematica: " + temp[2] + "/" + temp[3];
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    /**
     * Test of escolheEvento method, of class DefinirSTController.
     */
    @Test
    public void testEscolheEvento() {
        System.out.println("escolheEvento");
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.escolheEvento(ev1);
    }

    /**
     * Test of getListaEventosRegistadosOrganizador method, of class DefinirSTController.
     */
    @Test
    public void testGetListaEventosRegistadosOrganizador() {
        System.out.println("getListaEventosRegistadosOrganizador");
        String id = u1.getEmail();
        ev1.addOrganizador(u1);
        ev1.setState(new EventoRegistadoState(ev1));
        DefinirSTController instance = new DefinirSTController(regU, regE);
        List<Evento> expResult = new ArrayList();
        expResult.add(ev1);
        List<Evento> result = instance.getListaEventosRegistadosOrganizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataInicio method, of class DefinirSTController.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        Data di = new Data(4,1,1996);
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.escolheEvento(ev1);
        instance.novaSessaoTematica();
        instance.setDataInicio(di);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[4];
        assertEquals(compareData, di);
    }
    
    /*
    String descricao = "adeus";
        DefinirSTController instance = new DefinirSTController(regU, regE);
        instance.novaSessaoTematica(); 
        instance.setDescricao(descricao);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[3];     
        assertEquals(compareData, descricao);
    */

    /**
     * Test of setDataFim method, of class DefinirSTController.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        Data df = null;
        DefinirSTController instance = null;
        instance.setDataFim(df);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataInicioSubmissao method, of class DefinirSTController.
     */
    @Test
    public void testSetDataInicioSubmissao() {
        System.out.println("setDataInicioSubmissao");
        Data dis = null;
        DefinirSTController instance = null;
        instance.setDataInicioSubmissao(dis);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataFimSubmissao method, of class DefinirSTController.
     */
    @Test
    public void testSetDataFimSubmissao() {
        System.out.println("setDataFimSubmissao");
        Data dif = null;
        DefinirSTController instance = null;
        instance.setDataFimSubmissao(dif);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataInicioDistribuicao method, of class DefinirSTController.
     */
    @Test
    public void testSetDataInicioDistribuicao() {
        System.out.println("setDataInicioDistribuicao");
        Data dd = null;
        DefinirSTController instance = null;
        instance.setDataInicioDistribuicao(dd);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataLimiteSubmissaoFinal method, of class DefinirSTController.
     */
    @Test
    public void testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        Data dlsf = null;
        DefinirSTController instance = null;
        instance.setDataLimiteSubmissaoFinal(dlsf);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataLimiteRevisao method, of class DefinirSTController.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        Data dlr = null;
        DefinirSTController instance = null;
        instance.setDataLimiteRevisao(dlr);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registaSessaoTematica method, of class DefinirSTController.
     */
    @Test
    public void testRegistaSessaoTematica() {
        System.out.println("registaSessaoTematica");
        DefinirSTController instance = null;
        boolean expResult = false;
        boolean result = instance.registaSessaoTematica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showData method, of class DefinirSTController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        DefinirSTController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createTimers method, of class DefinirSTController.
     */
    @Test
    public void testCreateTimers() {
        System.out.println("createTimers");
        DefinirSTController instance = null;
        instance.createTimers();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
