/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import model.Empresa;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import model.TipoConflito;
import registos.RegistoTiposConflito;

/**
 *
 * @author jbraga
 */
public class DefinirTipoConflitoControllerTest {
    
    public DefinirTipoConflitoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setDados method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String name = "Nome";
        String descricao = "Descricao";
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(new Empresa());
        instance.novoTipoConflito();
        instance.setDados(name, descricao);

    }

    /**
     * Test of novoTipoConflito method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testNovoTipoConflito() {
        System.out.println("novoTipoConflito");
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(new Empresa());
        instance.novoTipoConflito();
        assertEquals(true, instance.getConflito()!=null);
    }

    /**
     * Test of registarTipoConflito method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testRegistarTipoConflito() {
        System.out.println("registarTipoConflito");
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(new Empresa());
        String name = "Nome";
        String descricao = "Descricao";
        instance.novoTipoConflito();
        instance.setDados(name, descricao);
        boolean res = instance.registarTipoConflito();
        assertEquals(true, res);
    }

    /**
     * Test of getListaTipoConflitos method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testGetListaTipoConflitos() {
        System.out.println("getListaTipoConflitos");
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(new Empresa());
        final TipoConflito tc = new TipoConflito();
        tc.setDescricao("descricao");
        tc.setName("descricao");
        RegistoTiposConflito reg = new RegistoTiposConflito();
        reg.addTipoConflito(tc);
        
        List<TipoConflito> expResult = new ArrayList<TipoConflito>(){{
            add(tc);
        }};
        instance.novoTipoConflito();
        instance.setDados("descricao", "descricao");
        instance.registarTipoConflito();
        List<TipoConflito> result = instance.getListaTipoConflitos();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(new Empresa());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    
}
