/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import model.Artigo;
import model.Autor;
import model.Evento;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.evento.EventoCameraReadyState;
import states.evento.EventoRegistadoState;
import states.sessaotematica.SessaoTematicaCameraReadyState;
import states.sessaotematica.SessaoTematicaRegistadoState;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class RemoverSubmissaoControllerTest {
    
    public RemoverSubmissaoControllerTest() {
    }
    RegistoEventos regE;
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    Evento ev1,ev2,ev3;
    SessaoTematica st1,st2,st3,st4;
    Submissao s1;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        regU=new RegistoUtilizadores();
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
        List<Evento> les = new ArrayList();
        ev1=new Evento();
        st1=new SessaoTematica();
        st2=new SessaoTematica();
        st3 = new SessaoTematica();
        st4=new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"Kanto Region"));
        a1.setAutorCorrespondente(new Autor(u1,"Kanto Region"),u1);
        Artigo a2 = new Artigo();
        a2.addAutor(new Autor(u2,"Jhoto Region"));
        a2.setAutorCorrespondente(new Autor(u2,"Jhoto Region"),u2);
        ev1.addSessaoTematica(st1);
        s1 = new Submissao();
        s1.setArtigoInicial(a1);
        ev1.addSessaoTematica(st2);
        ev1.setState(new EventoCameraReadyState(ev1));
        st1.setState(new SessaoTematicaCameraReadyState(st1));
        st2.setState(new SessaoTematicaRegistadoState(st2));
        ev2 = new Evento();
        ev2.setTitulo("Pika-pikachu!");
        ev2.addSessaoTematica(st3);
        Submissao s = new Submissao();
        a2.setTitulo("Pikachu");
        a2.setResumo("Pikachu, the electric-mouse pokémon.");
        a2.setFicheiro("pikachu.pdf");
        s.setArtigoInicial(a2);
        ev2.addSubmissao(s1, a1, u1);
        ev2.setState(new EventoRegistadoState(ev2));
        st3.setState(new SessaoTematicaCameraReadyState(st3));
        ev3 = new Evento();
        ev3.addSubmissao(s,a2,u2);
        ev3.setState(new EventoRegistadoState(ev2));
        les.add(ev1);
        les.add(ev2);
        les.add(ev3);
        regE=new RegistoEventos(regU,les);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaEventosNaoCameraReadyDe method, of class RemoverSubmissaoController.
     */
    @Test
    public void testGetListaEventosNaoCameraReadyDe() {
        System.out.println("getListaEventosNaoCameraReadyDe");
        String id = u1.getEmail();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        List<Evento> expResult = new ArrayList();
        expResult.add(ev2);
        List<Evento> result = instance.getListaEventosNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaSubmissiveisNaoCameraReadyDe method, of class RemoverSubmissaoController.
     */
    @Test
    public void testGetListaSubmissiveisNaoCameraReadyDe() {
        System.out.println("getListaSubmissiveisNaoCameraReadyDe");
        String id = u1.getEmail();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        instance.selectEvento(ev2);
        List<Submissivel> expResult = new ArrayList();
        expResult.add(ev2);
        List<Submissivel> result = instance.getListaSubmissiveisNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesNaoCameraReadyDe method, of class RemoverSubmissaoController.
     */
    @Test
    public void testGetSubmissoesNaoCameraReadyDe() {
        System.out.println("getSubmissoesNaoCameraReadyDe");
        String id = u1.getUsername();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        instance.selectEvento(ev2);
        instance.selectSubmissivel(ev2);
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        List<Submissao> result = instance.getSubmissoesNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class RemoverSubmissaoController.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        Submissao s = new Submissao();
        instance.selectSubmissao(s);
        s.setState(new SubmissaoRegistadoState(s));
        boolean expResult = true;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of selectSubmissao method, of class RemoverSubmissaoController.
     */
    @Test
    public void testSelectSubmissao() {
        System.out.println("selectSubmissao");
        Submissao s = new Submissao();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        instance.selectSubmissao(s);
        int sID = System.identityHashCode(s);
        String chunk[] = instance.showData().split("\n");
        String compareData = chunk[0];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of selectEvento method, of class RemoverSubmissaoController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento ev = new Evento();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        instance.selectEvento(ev);
        int sID = System.identityHashCode(ev);
        String chunk[] = instance.showData().split("\n");
        String compareData = chunk[1];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of selectSubmissivel method, of class RemoverSubmissaoController.
     */
    @Test
    public void testSelectSubmissivel() {
        System.out.println("selectSubmissivel");
        Submissivel sub = new SessaoTematica();
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        instance.selectSubmissivel(sub);
        int sID = System.identityHashCode(sub);
        String chunk[] = instance.showData().split("\n");
        String compareData = chunk[2];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class RemoverSubmissaoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        RemoverSubmissaoController instance = new RemoverSubmissaoController(regE,regU);
        String expResult = "0\n0\n0\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
