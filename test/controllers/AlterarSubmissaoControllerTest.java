/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaConflitosDetetados;
import listas.ListaSessoesTematicas;
import listas.ListaSubmissoes;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.Evento;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.submissao.SubmissaoCriadoState;
import states.submissao.SubmissaoRegistadoState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class AlterarSubmissaoControllerTest {
    
    public AlterarSubmissaoControllerTest() {
    }
    RegistoEventos regE;
    Evento e1;
    Evento e2;
    ListaSubmissoes lsub1;
    ListaSubmissoes lsub2;
    ListaSessoesTematicas lst1;
    ListaSessoesTematicas lst2;
    SessaoTematica st1;
    SessaoTematica st2;
    Submissao s1;
    Submissao s2;
    Submissao s3;
    Submissao s4;
    Submissao s5;
    Submissao s6;
    RegistoUtilizadores regU;
    Autor author1;
    Autor author;
    Utilizador u1;
    Utilizador u2;
    String title;
    String summary;
    String file;
    AutorCorrespondente ac1;
    AutorCorrespondente ac2;
    ListaAutores a1;
    ListaAutores a2;
    Artigo artigo1;
    Artigo artigo2;
    Artigo artigo3;
    List<Autor> lautor;
      
     
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        Utilizador u1 = new Utilizador("Jon","Jonathan","wee123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        List<String> pc = new ArrayList();
        pc.add("Google");
        pc.add("Galaxy");
        artigo1 = new Artigo(a1,title,summary,file,ac1);
        artigo2 = new Artigo(a2,title, summary, file, ac2);
        artigo1.setPalavrasChave(pc);
        artigo1.setAutorCriador(u1);
        artigo1.setDataCriado(Data.getDataAtual());
        artigo2.setPalavrasChave(pc);
        artigo2.setAutorCriador(u1);
        artigo2.setDataCriado(Data.getDataAtual());
        s1=new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(s1), new ListaConflitosDetetados());
        s2=new Submissao(artigo1,artigo2,new SubmissaoRegistadoState(s2), new ListaConflitosDetetados());
        s3=new Submissao(artigo1,artigo2,new SubmissaoCriadoState(s3), new ListaConflitosDetetados());
        s4=new Submissao(artigo1,artigo2,new SubmissaoRegistadoState(s4), new ListaConflitosDetetados());
        s5=new Submissao(artigo1,artigo2,new SubmissaoCriadoState(s5), new ListaConflitosDetetados());
        ListaSubmissoes lsub1 = new ListaSubmissoes();
        lsub1.addSubmissao(s1);
        lsub1.addSubmissao(s2);
        ListaSubmissoes lsub2 = new ListaSubmissoes();
        lsub2.addSubmissao(s3);
        st1=new SessaoTematica(lsub1);
        st2=new SessaoTematica(lsub2);
        st1.setAceitaSubmissoes();
        st2.setAceitaSubmissoes();
        ListaSessoesTematicas lst1 = new ListaSessoesTematicas();
        lst1.add(st1);
        e1=new Evento();  
        e1.addSessaoTematica(st1);
        e1.addSessaoTematica(st2);
        e1.setAceitaSubmissoes();
        List<Evento> er = new ArrayList<Evento>();
        er.add(e1);   
        List<Utilizador> ur = new ArrayList<Utilizador>();
        ur.add(u1);
        ur.add(u2);
        RegistoUtilizadores regU = new RegistoUtilizadores(ur);
        
        regE=new RegistoEventos(regU,er);
        lautor = new ArrayList();
        lautor.add(author);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaSubmissoesUtilizador method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetListaSubmissoesUtilizador() {
        System.out.println("getListaSubmissoesUtilizador");
        String id = "Jonathan";
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE);
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        List<Submissao> result = instance.getListaSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDadosSubmissao method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetDadosSubmissao() {
        System.out.println("getDadosSubmissao");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,e1,artigo1);
        String expResult = s1.toString();
        String result = instance.getDadosSubmissao(s1);
        assertEquals(expResult, result);
    }

    /**
     * Test of selecionaSubmissao method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSelecionaSubmissao() {
        System.out.println("selecionaSubmissao");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,e1,artigo1);
        instance.selecionaSubmissao(s2);
        String expResult = String.valueOf(System.identityHashCode(s2));
        String[] chunk = instance.showData().split("\n");
        String compareData = chunk[0];
        assertEquals(expResult,compareData);        
    }

    /**
     * Test of criarClone method, of class AlterarSubmissaoController.
     * 
     */
    @Test
    public void testCriarClone() 
    {
        System.out.println("criarClone");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        Submissao result = instance.criarClone();
        Submissao expResult = s2;
        assertEquals(expResult, result);
    }
    /**
     * Test of registaAlteracao method, of class AlterarSubmissaoController.
     * This method tests an invalid change
     */
    @Test
    public void testRegistaAlteracao() {
        System.out.println("registaAlteracao");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,e1,artigo1);
        boolean expResult = true;
        boolean result = instance.registaAlteracao();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class AlterarSubmissaoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s1,e1,artigo1);
        String expResult = String.valueOf(System.identityHashCode(s1))+"\n";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoesRegistadasUtilizador method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetListaSubmissoesRegistadasUtilizador() {
        System.out.println("getListaSubmissoesRegistadasUtilizador");
        Evento event = new Evento();
        event.setAceitaSubmissoes();
        event.addSessaoTematica(st1);
        List<Evento> ev = new ArrayList();
        ev.add(event);
        RegistoEventos registo = new RegistoEventos(regU,ev);
        
        String id = "Jonathan";
        AlterarSubmissaoController instance = new AlterarSubmissaoController(registo,s3,s2,st2,artigo1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        expResult.add(s2);
        List<Submissao> result = instance.getListaSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador() {
        System.out.println("getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador");
        String id = "Jonathan";
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE, s3,s2,st2,artigo1);
        List<Submissivel> expResult = new ArrayList();
        expResult.add(st1);
        List<Submissivel> result = instance.getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of selecionaSubmissivel method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSelecionaSubmissivel() {
        System.out.println("selecionaSubmissivel");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        instance.selecionaSubmissivel(st2);
        Submissivel result = instance.getSubmissivel();
        Submissivel expResult = st2;
        assertEquals(result,expResult);

    }

    /**
     * Test of getsClone method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetsClone() {
        System.out.println("getsClone");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        Submissao expResult = s1;
        Submissao result = instance.getsClone();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissivel method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetSubmissivel() 
    {
        System.out.println("getSubmissivel");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        Submissivel expResult = st1;
        Submissivel result = instance.getSubmissivel();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoAutor method, of class AlterarSubmissaoController.
     */
    @Test
    public void testNovoAutor() 
    {
        System.out.println("novoAutor");
        String username = "Dark Souls";
        String email = "souls@gmail.com";
        String nome = "Gwen";
        String afiliacao = "Lordran";
        Autor test = new Autor(username,email,afiliacao,nome);
        //Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        Autor result = instance.novoAutor(username, email, afiliacao, nome);
        assertEquals(test, result);
    }

    /**
     * Test of addAutor method, of class AlterarSubmissaoController.
     */
    @Test
    public void testAddAutor() 
    {
        System.out.println("addAutor");
        //Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s2,s2,st1,artigo1);
        boolean expResult = true;
        boolean result = instance.addAutor(author);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeAutor method, of class AlterarSubmissaoController.
     */
    @Test
    public void testRemoveAutor() 
    {
        System.out.println("removeAutor");
        Autor test = new Autor("Venusaur","ganja@gmail.com","Pokedex","Bulbasaur");
        artigo1.addAutor(test);
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,artigo1);
        instance.removeAutor(author);
        List<Autor> result = artigo1.getListaAutores();
        List<Autor> expResult = new ArrayList();
        expResult.add(author);
        assertEquals(result, expResult);
    }

    /**
     * Test of alteraDados method, of class AlterarSubmissaoController.
     */
    @Test
    public void testAlteraDados() {
        System.out.println("alteraDados");
        String titulo = "ancient";
        String resumo = "branch";
        String ficheiro = "yore.pdf";
        Artigo art = new Artigo(a1,titulo,resumo,ficheiro,ac1);
        String expResult = art.toString();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(regE,s1,s2,st1,art);
        instance.alteraDados(titulo, resumo, ficheiro);
        String result = instance.getsClone().getArtigoFinal().toString();
        assertEquals(expResult, result);
    }
    
}
