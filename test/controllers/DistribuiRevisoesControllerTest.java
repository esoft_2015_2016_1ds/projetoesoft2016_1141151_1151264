/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Distribuivel;
import interfaces.MecanismoDistribuicao;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaConflitosDetetados;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.Empresa;
import model.Evento;
import model.ProcessoDistribuicao;
import model.Revisao;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class DistribuiRevisoesControllerTest {
    
    public DistribuiRevisoesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMecanismosDistribuicao method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetMecanismosDistribuicao() {
        System.out.println("getMecanismosDistribuicao");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(new Empresa());
        List<MecanismoDistribuicao> expResult = new ArrayList<MecanismoDistribuicao>();
        List<MecanismoDistribuicao> result = instance.getMecanismosDistribuicao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMecanismoDistribuicao method, of class DistribuiRevisoesController.
     */
    @Test
    public void testSetMecanismoDistribuicao()
    {
        System.out.println("setMecanismoDistribuicao");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        MecanismoDistribuicao m = null;
        ProcessoDistribuicao temp = instance.novoProcessoDistribuicao();
        instance.setMecanismoDistribuicao(m);
        MecanismoDistribuicao result = temp.getMecanismoDistribuicao();
        assertEquals(result,m);
        
    }

    /**
     * Test of registaDistribuicao method, of class DistribuiRevisoesController.
     */
    @Test
    public void testRegistaDistribuicao() {
        System.out.println("registaDistribuicao");
        DistribuiRevisoesController instance = null;
        boolean expResult = false;
        boolean result = instance.registaDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showData method, of class DistribuiRevisoesController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of selectEvento method, of class DistribuiRevisoesController.
     */
    @Test
    public void testSelectEvento() 
    {
        System.out.println("selectEvento");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectEvento(e1);
        Evento result = instance.getEvento();
        Evento expResult = e1;
        assertEquals(result, expResult);

    }

    /**
     * Test of selectDistribuivel method, of class DistribuiRevisoesController.
     */
    @Test
    public void testSelectDistribuivel() {
        System.out.println("selectDistribuivel");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectDistribuivel(e1);
        Distribuivel result = instance.getDistribuivel();
        Distribuivel expResult = e1;
        assertEquals(expResult,result);
    }

    /**
     * Test of getEventosDistribuiveis method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetEventosDistribuiveis() {
        System.out.println("getEventosDistribuiveis");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        e1.addOrganizador(u1);
        e1.setEmLicitacao();
        List<Evento> list = new ArrayList();
        list.add(e1);
        RegistoUtilizadores regU = new RegistoUtilizadores();
        RegistoEventos re = new RegistoEventos(regU,list);
        regU.addUtilizador(u1);
        Empresa empresa = new Empresa();
        empresa.setRegistoEventos(re);
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        List<Evento> expResult = list;
        List<Evento> result = instance.getEventosDistribuiveis("Jonathan");
        assertEquals(expResult,result);
    }

    /**
     * Test of getListaDistribuiveisEvento method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetListaDistribuiveisEvento() {
        System.out.println("getListaDistribuiveisEvento");
        String id = "";
        DistribuiRevisoesController instance = null;
        List<Distribuivel> expResult = null;
        List<Distribuivel> result = instance.getListaDistribuiveisEvento(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoProcessoDistribuicao method, of class DistribuiRevisoesController.
     */
    @Test
    public void testNovoProcessoDistribuicao() 
    {
        System.out.println("novoProcessoDistribuicao");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        ProcessoDistribuicao result = instance.novoProcessoDistribuicao();
        ProcessoDistribuicao expResult = instance.getProcessoDistribuicao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoes method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetSubmissoes() 
    {
        System.out.println("getSubmissoes");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectDistribuivel(e1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        instance.getSubmissoes();
    }

    /**
     * Test of distribui method, of class DistribuiRevisoesController.
     */
    @Test
    public void testDistribui() 
    {
        System.out.println("distribui");
        DistribuiRevisoesController instance = null;
        List<Revisao> expResult = null;
        List<Revisao> result = instance.distribui();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEvento method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetEvento() 
    {
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        System.out.println("getEvento");
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectEvento(e1);
        Evento expResult = e1;
        Evento result = instance.getEvento();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDsitribuivel method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetDsitribuivel() 
    {
        System.out.println("getDsitribuivel");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectDistribuivel(e1);
        Distribuivel expResult = e1;
        Distribuivel result = instance.getDistribuivel();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProcessoDistribuicao method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetProcessoDistribuicao() 
    {
        System.out.println("getProcessoDistribuicao");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.novoProcessoDistribuicao();      
        ProcessoDistribuicao expResult = instance.getProcessoDistribuicao();
        ProcessoDistribuicao result = instance.getProcessoDistribuicao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoes method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetListaSubmissoes() 
    {
        System.out.println("getListaSubmissoes");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.setListaSubmissoes(ls);
        List<Submissao> expResult = ls;
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDistribuivel method, of class DistribuiRevisoesController.
     */
    @Test
    public void testGetDistribuivel() 
    {
        System.out.println("getDistribuivel");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.selectDistribuivel(e1);
        Distribuivel expResult = e1;
        Distribuivel result = instance.getDistribuivel();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaSubmissoes method, of class DistribuiRevisoesController.
     */
    @Test
    public void testSetListaSubmissoes() {
        System.out.println("setListaSubmissoes");
                                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Evento e1 = new Evento();
        e1.addAllSubmissoes(ls);
        Empresa empresa = new Empresa();
        DistribuiRevisoesController instance = new DistribuiRevisoesController(empresa);
        instance.setListaSubmissoes(ls);
        List<Submissao> result = instance.getListaSubmissoes();
        List<Submissao> expResult = ls;
        assertEquals(result, expResult);
    }
    
}
