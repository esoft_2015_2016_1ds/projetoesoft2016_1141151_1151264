package controllers;

import java.util.ArrayList;
import java.util.List;
import model.CP;
import model.Evento;
import model.Organizador;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.evento.EventoSessaoTematicaDefinidaState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class CriarCPControllerTest {

    public CriarCPControllerTest() {
    }
    private RegistoEventos regE;
    private RegistoUtilizadores regU;
    private Organizador o1, o2;
    private Utilizador u1, u2, u3;
    private List<Utilizador> lu;
    private List<Organizador> lo;
    private Submissao s1, s2;
    private List<Submissao> ls;
    private List<Evento> le;
    private Evento ev1, ev2, ev3;
    private NaoFazNada tabela;
    private CriarCPController instance;
    private SessaoTematica st1;
    private List<SessaoTematica> lst;
    private Revisor r1,r2,r3;
    private List<Revisor>lr;

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        tabela = new NaoFazNada();
        
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", tabela);
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", tabela);
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", tabela);

        lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);

        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        o1 = new Organizador(u1);
        o2 = new Organizador(u2);
        lo = new ArrayList();
        lo.add(o1);
        lo.add(o2);

        r1=new Revisor(u1);
        r2=new Revisor(u2);
        r3=new Revisor(u3);
        lr=new ArrayList();
        lr.add(r1);
        lr.add(r2);
        lr.add(r3);
        
        
        st1=new SessaoTematica();
        lst=new ArrayList();
        lst.add(st1);
        ev1 = new Evento();
        ev2 = new Evento(lo, lst);
        ev3 = new Evento();
        le=new ArrayList();
        le.add(ev1);
        le.add(ev2);
        le.add(ev3);
        st1 = new SessaoTematica();
        ev2.addSessaoTematica(st1);
        ev2.setState(new EventoSessaoTematicaDefinidaState(ev2));
        s1 = new Submissao();
        s2 = new Submissao();
        ls = new ArrayList();
        ls.add(s1);
        ls.add(s2);
        
        regE = new RegistoEventos(regU,le);
        
        instance = new CriarCPController(regE, regU);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizadorEmEstadoSessaoTematicaDefinida method, of
     * class CriarCPController.
     */
    @Test
    public void testGetEventosOrganizadorEmEstadoSessaoTematicaDefinida() {
        System.out.println("getEventosOrganizadorEmEstadoSessaoTematicaDefinida");
        String strId = u1.getEmail();
        List<Evento> expResult = new ArrayList();
        expResult.add(ev2);
        List<Evento> result = instance.getEventosOrganizadorEmEstadoSessaoTematicaDefinida(strId);
        assertEquals(expResult, result);

    }

    /**
     * Test of selectEvento method, of class CriarCPController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        instance.selectEvento(ev1);
        int sID = System.identityHashCode(ev1);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[0];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
       
       
    }
    /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP_Revisor() {
        System.out.println("addMembroCP");
        instance.selectEvento(new Evento());
        boolean expResult = true;
        boolean result = instance.addMembroCP(r1);
        assertEquals(expResult, result);
        
    }

       /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP_String() {
        System.out.println("addMembroCP");
        String strId = u1.getEmail();
        instance.selectEvento(new Evento());
        boolean expResult = true;
        boolean result = instance.addMembroCP(strId);
        assertEquals(expResult, result);
      
    }
    
    /**
     * Test of registaMembroCP method, of class CriarCPController.
     */
    @Test
    public void testRegistaMembroCP() {
        System.out.println("registaMembroCP");
        CP cp=new CP(lr);
        instance.selectEvento(new Evento());
        boolean expResult = true;
        boolean result = instance.registaMembroCP(r1);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of novoMembroCP method, of class CriarCPController.
     */
    @Test
    public void testNovoMembroCP() {
        System.out.println("novoMembroCP");
        String id = u1.getEmail();
        instance.selectEvento(ev1);
        instance.novoMembroCP(id);     
    }

    /**
     * Test of setCP method, of class CriarCPController.
     */
    @Test
    public void testSetCP() {
        System.out.println("setCP");
        CP cp = new CP(lr);
        instance.selectEvento(ev1);
        instance.setCP();
    }

    /**
     * Test of showData method, of class CriarCPController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        CriarCPController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


 

   
}
