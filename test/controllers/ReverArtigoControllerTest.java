/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Revisivel;
import java.util.ArrayList;
import java.util.List;
import listas.ListaRevisoes;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.Evento;
import model.Organizador;
import model.ProcessoDistribuicao;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.evento.EventoEmRevisaoState;
import states.sessaotematica.SessaoTematicaEmRevisaoState;
import states.submissao.SubmissaoCriadoState;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ReverArtigoControllerTest {
    
    public ReverArtigoControllerTest() 
    {
        
    }
    private Utilizador u1,u2,u3;
    private List<Utilizador> lu;
    private Evento ev1,ev2,ev3;
    private List<Evento> le;
    private Organizador o1,o2,o3;
    private List<Organizador> lo;
    private SessaoTematica st1,st2,st3;
    private List<SessaoTematica> lst;
    private NaoFazNada tabela;
    private RegistoUtilizadores regU;
    private RegistoEventos regE;
    private Revisao revisao1,revisao2;
    private Revisor revisor1,revisor2;
    private List<Revisor> listaRevisor;
    private List<Revisao> listaRevisao;
    
    

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    
    @Before
    public void setUp() 
    {
        tabela= new NaoFazNada();
        
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", tabela);
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", tabela);
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", tabela);

        lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);

        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        o1 = new Organizador(u1);
        o2 = new Organizador(u2);
        o3 = new Organizador(u3);
        lo = new ArrayList();
        lo.add(o1);
        lo.add(o2);
        lo.add(o3);
        
        st1=new SessaoTematica();
        st1.addProponente(u1);
        st1.setState(new SessaoTematicaEmRevisaoState(st1));
        st2=new SessaoTematica();
        st3=new SessaoTematica();
        lst=new ArrayList();
        lst.add(st1);
        lst.add(st2);
        lst.add(st3);
        
        ev1=new Evento(lo, lst);
        ev1.setState(new EventoEmRevisaoState(ev1));
        ev2=new Evento();
        ev3=new Evento();
        le=new ArrayList();
        le.add(ev1);
        le.add(ev2);
        le.add(ev3);
        
        revisor1=new Revisor(u1);
        revisor2=new Revisor(u2);
        listaRevisor=new ArrayList();
        listaRevisor.add(revisor1);
        listaRevisor.add(revisor2);
        
        
        revisao1=new Revisao();
        revisao1.setRevisor(revisor1);
        revisao2=new Revisao();
        listaRevisao=new ArrayList();
        listaRevisao.add(revisao1);
        
        
        
        regE= new RegistoEventos(regU, le);
    }
    @After
    public void tearDown() {
    }




    /**
     * Test of getRevisoes method, of class ReverArtigoController.
     */
    @Test
    public void testGetRevisoes() {
        System.out.println("getRevisoes");
        String id = u1.getEmail();
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisivel(ev1);
        List<Revisao> expResult = new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        List<Autor> la=new ArrayList();
        la.add(aut);
        AutorCorrespondente au = new AutorCorrespondente(aut,u1);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao1.setSubmissao(sub);
        instance.selectRevisao(revisao1);
        expResult.add(revisao1);
        ListaRevisoes lista=new ListaRevisoes(listaRevisao);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        ev1.setProcessoDistribuicao(pdis);
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of setClassificacaoConfianca method, of class ReverArtigoController.
     */
    @Test
    public void testSetClassificacaoConfianca() {
        System.out.println("setClassificacaoConfianca");
        int classificacao = 2;
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setClassificacaoConfianca(classificacao);
    }

    /**
     * Test of setClassificacaoAdequacao method, of class ReverArtigoController.
     */
    @Test
    public void testSetClassificacaoAdequacao() {
        System.out.println("setClassificacaoAdequacao");
        int classificacao = 2;
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setClassificacaoAdequacao(classificacao);
    }

    /**
     * Test of setClassificacaoOriginalidade method, of class ReverArtigoController.
     */
    @Test
    public void testSetClassificacaoOriginalidade() {
        System.out.println("setClassificacaoOriginalidade");
       int classificacao = 2;
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setClassificacaoOriginalidade(classificacao);
    }

    /**
     * Test of setClassificacaoApresentacao method, of class ReverArtigoController.
     */
    @Test
    public void testSetClassificacaoApresentacao() {
        System.out.println("setClassificacaoApresentacao");
        int classificacao = 2;
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setClassificacaoApresentacao(classificacao);
    }

    /**
     * Test of setClassificacaoRecomendacao method, of class ReverArtigoController.
     */
    @Test
    public void testSetClassificacaoRecomendacao() {
        System.out.println("setClassificacaoRecomendacao");
        int classificacao = 2;
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setClassificacaoRecomendacao(classificacao);
    }

    /**
     * Test of setTextoExplicativo method, of class ReverArtigoController.
     */
    @Test
    public void testSetTextoExplicativo() {
        System.out.println("setTextoExplicativo");
        String texto = "TestasOuChumbas";
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(new Revisao());
        instance.setTextoExplicativo(texto);
        
    }

    /**
     * Test of saveRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testSaveRevisao() {
        System.out.println("saveRevisao");
        ReverArtigoController instance = new ReverArtigoController(regE);
        boolean expResult = true;
        instance.selectRevisivel(ev1);
        instance.selectRevisao(revisao1);
        ListaRevisoes lista= new ListaRevisoes(listaRevisao);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao1.setSubmissao(sub);
        ev1.setProcessoDistribuicao(pdis);
        boolean result = instance.saveRevisao();
        assertEquals(expResult, result);

    }
    /**
     * Test of getListaRevisivelUtilizadorEmEstadoRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testGetListaRevisivelUtilizadorEmEstadoRevisao() {
        System.out.println("getListaRevisivelUtilizadorEmEstadoRevisao");
        String id = u1.getEmail();
        ReverArtigoController instance = new ReverArtigoController(regE);
        List<Revisivel> expResult = new ArrayList();
        expResult.add(ev1);
        expResult.add(st1);
        List<Revisivel> result = instance.getListaRevisivelUtilizadorEmEstadoRevisao(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of selectRevisivel method, of class ReverArtigoController.
     */
    @Test
    public void testSelectRevisivel() {
        System.out.println("selectRevisivel");
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisivel(ev1);
        instance.selectRevisao(revisao1);
        int sID = System.identityHashCode(ev1);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[0];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
    }

    /**
     * Test of selectRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testSelectRevisao() {
        System.out.println("selectRevisao");
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(revisao1);
        instance.selectRevisivel(ev1);
        int sID = System.identityHashCode(revisao1);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[1];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);
    }

   
    /**
     * Test of setPreRevistoState method, of class ReverArtigoController.
     */
    @Test
    public void testSetPreRevistoState() {
        System.out.println("setPreRevistoState");
        ReverArtigoController instance = new ReverArtigoController(regE);
        instance.selectRevisao(revisao1);
        instance.selectRevisivel(ev1);
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao1.setSubmissao(sub);
        boolean expResult = true;
        boolean result = instance.setPreRevistoState();
        assertEquals(expResult, result);

    }

    /**
     * Test of setEmDecisaoState method, of class ReverArtigoController.
     */
    @Test
    public void testSetEmDecisaoState() {
        System.out.println("setEmDecisaoState");
        ReverArtigoController instance= new ReverArtigoController(regE);
        instance.selectRevisao(revisao1);
        instance.selectRevisivel(ev1);
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao1.setSubmissao(sub);
        boolean expResult = true;
        boolean result = instance.setEmDecisaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class ReverArtigoController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ReverArtigoController instance = null;
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
