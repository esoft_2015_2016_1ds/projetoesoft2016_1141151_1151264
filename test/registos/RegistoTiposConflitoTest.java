/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registos;

import java.util.ArrayList;
import java.util.List;
import model.TipoConflito;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class RegistoTiposConflitoTest {
    
    public RegistoTiposConflitoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of showData method, of class RegistoTiposConflito.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        RegistoTiposConflito instance = new RegistoTiposConflito();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaTiposConflito method, of class RegistoTiposConflito.
     */
    @Test
    public void testGetListaTiposConflito() {
        System.out.println("getListaTiposConflito");
        RegistoTiposConflito instance = new RegistoTiposConflito();
        List<TipoConflito> expResult = new ArrayList();
        List<TipoConflito> result = instance.getListaTiposConflito();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addTipoConflito method, of class RegistoTiposConflito.
     */
    @Test
    public void testAddTipoConflito() {
        System.out.println("addTipoConflito");
        TipoConflito tipoConflito = new TipoConflito();
        RegistoTiposConflito instance = new RegistoTiposConflito();
        boolean expResult = true;
        boolean result = instance.addTipoConflito(tipoConflito);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class RegistoTiposConflito.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        TipoConflito tipoConflito = new TipoConflito();
        RegistoTiposConflito instance = new RegistoTiposConflito();
        boolean expResult = true;
        boolean result = instance.valida(tipoConflito);
        assertEquals(expResult, result);
    }
    
}
