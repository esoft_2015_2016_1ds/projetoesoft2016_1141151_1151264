/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registos;

import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import listas.ListaRevisoes;
import listas.ListaSubmissoes;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.CP;
import model.Evento;
import model.Local;
import model.Organizador;
import model.ProcessoDecisao;
import model.ProcessoDistribuicao;
import model.Proponente;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.evento.EventoAceitaSubmissoesState;
import states.evento.EventoCameraReadyState;
import states.evento.EventoEmDecisaoState;
import states.evento.EventoEmLicitacaoState;
import states.evento.EventoEmRevisaoState;
import states.evento.EventoEmSubmissaoCameraReadyState;
import states.evento.EventoRegistadoState;
import states.evento.EventoSessaoTematicaDefinidaState;
import states.sessaotematica.SessaoTematicaRegistadoState;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoEmCameraReadyState;
import states.submissao.SubmissaoRegistadoState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class RegistoEventosTest {
    
    public RegistoEventosTest() {
    }
    private NaoFazNada tabela;
    private Utilizador u1,u2,u3;
    private List<Utilizador> lu;
    private Evento ev1,ev2,ev3,ev4,evento;
    private List<Evento> le;
    private RegistoUtilizadores regU;
    private Organizador o1,o2,o3;
    private List<Organizador> lo;
    private SessaoTematica st1,st2;
    private List<SessaoTematica> lst;
    private RegistoEventos instance;
    private Proponente p1;
    private List<Proponente> lp;
    private List<String> palavra;
    private Artigo a;
    private Autor au;
    Submissao s1,s2;
    Revisor target;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        tabela=new NaoFazNada(); 
        
        u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", tabela);
        u2 = new Utilizador("Luis Ferreira", "ferreira", "abc123", "luisferreira@hotmail.com", tabela);
        u3 = new Utilizador("Hidden User", "adminHidden", "123easyHide", "adminHidden@gmail.com", tabela);

        lu = new ArrayList();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);

        regU = new registos.RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(u3);  
        
        o1 = new Organizador(u1);
        o2 = new Organizador(u2);
        o3 = new Organizador(u3);
        lo = new ArrayList();
        lo.add(o1);
        lo.add(o2);
        lo.add(o3);

        st1=new SessaoTematica();
        st1.addProponente(u1);
        lst=new ArrayList();
        lst.add(st1);
        
        ev1 = new Evento(lo,lst);
        ev1.setState(new EventoEmRevisaoState(ev1));
        ev2 = new Evento(lo, lst);
        s2=new Submissao();
         au = new Autor(u1,"StarkTower");
        a = new Artigo();
        a.setTitulo("Avengers - Age of Ultron");
        a.setResumo("A battle where our heroes fight against Artificial Intelligence.");
        a.setFicheiro("avengers.pdf");
        List<Autor> la = new ArrayList();
        palavra = new ArrayList();
        palavra.add("heroes");
        palavra.add("batalha");
        la.add(au);
        a.setPalavrasChave(palavra);
        a.setAutores(la);
        target=new Revisor(u1);
        a.setAutorCorrespondente(new AutorCorrespondente(au,u1));
        s2.setArtigoInicial(a);
        ev2.addSubmissao(s2, a, u1);
        ev2.setState(new EventoSessaoTematicaDefinidaState(ev2));
        ev3 = new Evento();
        ev3.addSubmissao(new Submissao(), new Artigo(), u2);
        ev3.setState(new EventoAceitaSubmissoesState(ev3));
        le=new ArrayList();
        le.add(ev1);
        le.add(ev2);
        le.add(ev3);
        ev1.novaCP().addMembroCP(target);
        ev2.novaCP().addMembroCP(target);
        ev3.novaCP();
        s1=new Submissao();
       
        
        evento = new Evento();
         Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        evento.setdataInicio(inicio);
        evento.setdataInicioSubmissao(inicioSub);
        evento.setdataFim(fim);
        evento.setdataFimSubmissao(fimSub);
        evento.setdataInicioDistribuicao(incioDist);
        evento.setdataLimiteRevisao(limRev);
        evento.setDataLimiteSubmissaoFinal(limSubFin);
        evento.setTitulo("titulo");
        evento.setDescricao("description");
        evento.setCP(new CP());
        evento.setProcessoDecisao(new ProcessoDecisao());
        Local lec = new Local();
        lec.setLocal("Local");
        evento.setLocal(lec);
        evento.setCP(new CP());
        evento.setProcessoDecisao(new ProcessoDecisao());
        evento.addSubmissao(s1, a, u1);
                
        instance= new RegistoEventos(regU,le);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of registaEvento method, of class RegistoEventos.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        boolean expResult = true;
        boolean result = instance.registaEvento(evento);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of novoEvento method, of class RegistoEventos.
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        Evento result = instance.novoEvento();
        boolean expResult = result!=null;
        assertEquals(expResult, true);
        
    }

    /**
     * Test of getEventosDe method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosDe() {
        System.out.println("getEventosDe");
        String strId = u1.getEmail();
        List<Evento> expResult = new ArrayList();
        expResult.add(ev1);
        List<Evento> result = instance.getEventosOrganizadorEmEstadoSessaoTematicaDefinida(strId);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getSubmissoesUtilizador method, of class RegistoEventos.
     */
    @Test
    public void testGetSubmissoesUtilizador() {
        System.out.println("getSubmissoesUtilizador");
        String id = u1.getEmail();
        List<Submissao> expResult = new ArrayList();
        expResult.add(s2);
        List<Submissao> result = instance.getSubmissoesUtilizador(id);
        assertEquals(expResult, result);
    }
    /**
     * Test of getEventosOrganizadorEmEstadoSessaoTematicaDefinida method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosOrganizadorEmEstadoSessaoTematicaDefinida() {
        System.out.println("getEventosOrganizadorEmEstadoSessaoTematicaDefinida");
        String strId = u1.getEmail();
        List<Evento> expResult = new ArrayList();
        expResult.add(ev2);
        List<Evento> result = instance.getEventosOrganizadorEmEstadoSessaoTematicaDefinida(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSessoesTematicasProponenteEmEstadoRegistado method, of class RegistoEventos.
     */
    @Test
    public void testGetSessoesTematicasProponenteEmEstadoRegistado() {
        System.out.println("getSessoesTematicasProponenteEmEstadoRegistado");
        String id = u3.getEmail();
        p1= new Proponente(u3);
        lp=new ArrayList();
        lp.add(p1);
        List<SessaoTematica> expResult = new ArrayList();
        st2=new SessaoTematica(lp);
        st2.setState(new SessaoTematicaRegistadoState(st2));
        lst.add(st2);
        ev4=new Evento(lo, lst);
        le.add(ev4);
        RegistoEventos instance=new RegistoEventos(regU, le);
        expResult.add(st2);
        List<SessaoTematica> result = instance.getSessoesTematicasProponenteEmEstadoRegistado(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getEventosDeOrganizador method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosDeOrganizador() {
        System.out.println("getEventosDeOrganizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        List<SessaoTematica>lst1=new ArrayList();
        lst1.add(st1);
        ev4=new Evento();
        ev4.addSessaoTematica(st1);
        ev4.addOrganizador(u4);
        List<Evento> expResult = new ArrayList();
        le.add(ev4);
        expResult.add(ev4);
        RegistoEventos instance=new RegistoEventos(regU, le);
        List<Evento> result = instance.getEventosDeOrganizador(id);
        assertEquals(expResult, result);
  
        
    }

    /**
     * Test of getEventosComSTDeProponente method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosComSTDeProponente() {
        System.out.println("getEventosComSTDeProponente");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st3 = new SessaoTematica();
        st3.addProponente(u4);
        ev4=new Evento();
        le.add(ev4);
        ev4.addSessaoTematica(st3);
        List<Evento> expResult = new ArrayList();
        expResult.add(ev4);
        List<Evento> result = new RegistoEventos(regU,le).getEventosComSTDeProponente(id);
        assertEquals(expResult, result);
       
        
    }

    /**
     * Test of getListaEventos method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventos() {
        System.out.println("getListaEventos");
        List<Evento> expResult = new ArrayList();
        expResult.add(ev1);
        expResult.add(ev2);
        expResult.add(ev3);
        List<Evento> result = instance.getListaEventos();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRegistoUtilizadores method, of class RegistoEventos.
     */
    @Test
    public void testGetRegistoUtilizadores() {
        System.out.println("getRegistoUtilizadores");
        RegistoUtilizadores expResult = regU;
        RegistoUtilizadores result = instance.getRegistoUtilizadores();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getListaLicitaveisEmLicitacaoDe method, of class RegistoEventos.
     */
    @Test
    public void testGetListaLicitaveisEmLicitacaoDe() {
        System.out.println("getListaLicitaveisEmLicitacaoDe");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev5=new Evento();
        CP cp = new CP();
        Revisor r4=new Revisor(u4);
        cp.addMembroCP(r4);
        ev5.setCP(cp);
        ev5.addOrganizador(u4);
        le.add(ev5);
        ev5.setState(new EventoEmLicitacaoState(ev5));
        List<Licitavel> expResult=new ArrayList();
        expResult.add(ev5);
        List<Licitavel> result = new RegistoEventos(regU, le).getListaLicitaveisEmLicitacaoDe(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaEventosNaoCameraReadyDe method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosNaoCameraReadyDe() {
        System.out.println("getListaEventosNaoCameraReadyDe");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getUsername();
        Evento ev5=new Evento();
        ev5.addOrganizador(u4);
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setPalavrasChave(palavra);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        le.add(ev5);
        ev5.addSubmissao(sub, a1, u4);
        ev5.setState(new EventoEmDecisaoState(ev5));
        List<Evento> expResult = new ArrayList();
        expResult.add(ev5);
        List<Evento> result = new RegistoEventos(regU, le).getListaEventosNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaSubmissiveisEmEstadoAceitaSubmissoes method, of class RegistoEventos.
     */
    @Test
    public void testGetListaSubmissiveisEmEstadoAceitaSubmissoes() {
        System.out.println("getListaSubmissiveisEmEstadoAceitaSubmissoes");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Evento ev6= new Evento();
        ev6.setState(new EventoAceitaSubmissoesState(ev6));
        le.add(ev6);    
        List<Submissivel> expResult = new ArrayList();
        expResult.add(ev6);
        expResult.add(ev6);
        List<Submissivel> result = new RegistoEventos(regU, le).getListaSubmissiveisEmEstadoAceitaSubmissoes();
        assertEquals(expResult, result);

        
    }

    /**
     * Test of getListaEventosOrganizadorEmSubmissaoCameraReady method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosOrganizadorEmSubmissaoCameraReady() {
        System.out.println("getListaEventosOrganizadorEmSubmissaoCameraReady");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getUsername();
        Evento ev7= new Evento();
        le.add(ev7);
        ev7.addOrganizador(u4);
        ev7.setState(new EventoEmSubmissaoCameraReadyState(ev7));
        List<Evento> expResult = new ArrayList();
        expResult.add(ev7);
        List<Evento> result = new RegistoEventos(regU, le).getListaEventosOrganizadorEmSubmissaoCameraReady(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaEventosComRevisor method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosComRevisor() {
        System.out.println("getListaEventosComRevisor");
        Revisor r = target;
        List<Evento> expResult = new ArrayList();
        expResult.add(ev1);
        expResult.add(ev2);
        List<Evento> result = instance.getListaEventosComRevisor(r);
        assertEquals(expResult, result);
  
        
    }

    /**
     * Test of getListaTodasRevisoes method, of class RegistoEventos.
     */
    @Test
    public void testGetListaTodasRevisoes() {
        System.out.println("getListaTodasRevisoes");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Evento ev8 = new Evento();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        a1.setPalavrasChave(palavra);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        ev8.addSubmissao(sub, a1, u4);
        List<Evento> lev=new ArrayList();
        Revisor r=new Revisor(u4);
        Revisao rev=new Revisao();
        rev.setSubmissao(sub);
        rev.setRevisor(r);
        List<Revisao> lrev= new ArrayList();
        lrev.add(rev);
        ListaRevisoes listaRevisoes= new ListaRevisoes(lrev);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(listaRevisoes);
        ev8.setProcessoDistribuicao(pdis);
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        lev.add(ev8);
        List<Revisao> result = new RegistoEventos(regU, lev).getListaTodasRevisoes();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getSubmissoesRegistadasUtilizador method, of class RegistoEventos.
     */
    @Test
    public void testGetSubmissoesRegistadasUtilizador() {
        System.out.println("getSubmissoesRegistadasUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8 = new Evento();
        ev8.setState(new EventoAceitaSubmissoesState(ev8));
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setPalavrasChave(palavra);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setState(new SubmissaoRegistadoState(sub));
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        ev8.addSubmissao(sub, a1, u4);
        List<Evento> lev=new ArrayList();
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        lev.add(ev8);
        List<Submissao> result = new RegistoEventos(regU, lev).getSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador method, of class RegistoEventos.
     */
    @Test
    public void testGetListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador() {
        System.out.println("getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8=new Evento();
        ev8.setState(new EventoAceitaSubmissoesState(ev8));
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.setPalavrasChave(palavra);
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setState(new SubmissaoRegistadoState(sub));
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        ev8.addSubmissao(sub, a1, u4);
        List<Submissivel> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento>lev=new ArrayList();
        lev.add(ev8);
        List<Submissivel> result = new RegistoEventos(regU, lev).getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaEventosRegistadosOrganizador method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosRegistadosOrganizador() {
        System.out.println("getListaEventosRegistadosOrganizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8= new Evento();
        ev8.setState(new EventoRegistadoState(ev8));
        ev8.addOrganizador(u4);
        List<Evento> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento> lev=new ArrayList();
        lev.add(ev8);
        List<Evento> result = new RegistoEventos(regU, lev).getListaEventosRegistadosOrganizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaEventosEmSubmissaoCR method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosEmSubmissaoCR() {
        System.out.println("getListaEventosEmSubmissaoCR");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8= new Evento();
        ev8.setState(new EventoEmSubmissaoCameraReadyState(ev8));
        ev8.addOrganizador(u4);
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setPalavrasChave(palavra);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setState(new SubmissaoAceiteState(sub));
        sub.setArtigoInicial(a1);
        
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        
        ev8.addSubmissao(sub, a1, u4);
        List<Evento> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento> lev=new ArrayList();
        lev.add(ev8);
        List<Evento> result = new RegistoEventos(regU, lev).getListaEventosEmSubmissaoCR(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaRevisivelUtilizadorEmEstadoRevisao method, of class RegistoEventos.
     */
    @Test
    public void testGetListaRevisivelUtilizadorEmEstadoRevisao() {
        System.out.println("getListaRevisivelUtilizadorEmEstadoRevisao");
        String id = u1.getEmail();
        List<Revisivel> expResult = new ArrayList();
        Evento ev3=new Evento(lo,lst);
        ev3.setState(new EventoEmRevisaoState(ev3));
        expResult.add(ev3);
        List<Revisivel> result = instance.getListaRevisivelUtilizadorEmEstadoRevisao(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of getListaEventosDecididos method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosDecididos() {
        System.out.println("getListaEventosDecididos");
        Evento ev8= new Evento();
        ev8.setState(new EventoCameraReadyState(ev8));
        ArrayList<Evento> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento> lev = new ArrayList();
        lev.add(ev8);
        ArrayList<Evento> result = new RegistoEventos(regU, lev).getListaEventosDecididos();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEventosDistribuiveis method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosDistribuiveis() {
        System.out.println("getEventosDistribuiveis");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8= new Evento();
        ev8.addOrganizador(u4);
        ev8.setState(new EventoEmLicitacaoState(ev8));
        List<Evento> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento> lev = new ArrayList();
        lev.add(ev8);
        List<Evento> result = new RegistoEventos(regU, lev).getEventosDistribuiveis(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaEventosEmDecisao method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventosEmDecisao() {
        System.out.println("getListaEventosEmDecisao");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        Evento ev8= new Evento();
        ev8.addOrganizador(u4);
        ev8.setState(new EventoEmDecisaoState(ev8));
        List<Evento> expResult = new ArrayList();
        expResult.add(ev8);
        List<Evento> lev = new ArrayList();
        lev.add(ev8);
        List<Evento> result = new RegistoEventos(regU, lev).getListaEventosEmDecisao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisor method, of class RegistoEventos.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Evento ev8= new Evento();
        ev8.addOrganizador(u4);
        CP cp=new CP();
        cp.addMembroCP(u4);
        ev8.setCP(cp);
        Revisor expResult = new Revisor(u4);
        List<Evento> lista=new ArrayList();
        lista.add(ev8);
        Revisor result = new RegistoEventos(regU, lista).getRevisor(u4);
        assertEquals(expResult, result);
    }


    /**
     * Test of showData method, of class RegistoEventos.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        String expResult = "";
        String result = new RegistoEventos(regU, le).showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEventosAceitaSubmissoes method, of class RegistoEventos.
     */
    @Test
    public void testGetEventosAceitaSubmissoes() {
        System.out.println("getEventosAceitaSubmissoes");
        evento.setState(new EventoAceitaSubmissoesState(evento));
        instance= new RegistoEventos(regU,new ArrayList<Evento>(){{
            add(evento);
        }});
      
        List<Evento> expResult = new ArrayList();
        expResult.add(evento);
        List<Evento> result = instance.getEventosAceitaSubmissoes();
        assertEquals(expResult, result);
    }



  
}
