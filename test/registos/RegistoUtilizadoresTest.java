package registos;

import java.util.ArrayList;
import java.util.List;
import model.CP;
import model.Evento;
import model.Revisor;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author Gonçalo
 */
public class RegistoUtilizadoresTest {
    
    public RegistoUtilizadoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of novoUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        Utilizador expResult = new Utilizador();
        Utilizador result = instance.novoUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of validaUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testValidaUtilizador() {
        System.out.println("validaUtilizador");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com",new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = true;
        boolean result = instance.validaUtilizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validaUtilizador method with an already existing user
     */
    @Test
    public void testValidaUtilizadorExisting() {
        System.out.println("validaUtilizadorExisting");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com",
                new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores(new ArrayList<Utilizador>(){{
         add(new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com",
                new NaoFazNada()));       
        }});
        boolean expResult = false;
        boolean result = instance.validaUtilizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getLista method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetLista() {
        System.out.println("getLista");
        Utilizador u1=new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com",new NaoFazNada());
        List<Utilizador> expResult = new ArrayList();
        expResult.add(u1);
        List<Utilizador >lista=new ArrayList();
        lista.add(u1);
        List<Utilizador> result = new RegistoUtilizadores(lista).getLista();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testRegistaUtilizador() {
        System.out.println("registaUtilizador");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com", new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = true;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of registaUtilizador method, with an already existing email.
     */
    @Test
    public void testRegistaUtilizadorExstingEmail() {
        System.out.println("registaUtilizadorEmail");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com", new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores(new ArrayList<Utilizador>(){{
            add(new Utilizador("tiago", "tgeis", "pass123", "g.reis@email.com", new NaoFazNada()));
        }});
        boolean expResult = false;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of registaUtilizador method, with an already existing username.
     */
    @Test
    public void testRegistaUtilizadorExstingUsername() {
        System.out.println("registaUtilizadorExistingUsername");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com", new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores(new ArrayList<Utilizador>(){{
            add(new Utilizador("tiago", "greis", "pass123", "tiagos@email.com", new NaoFazNada()));
        }});
        boolean expResult = false;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of registaUtilizador method, with an invalid User.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testRegistaUtilizadorInvalid() {
        System.out.println("registaUtilizadorInvalid");
        Utilizador u = new Utilizador("o", "greis", "pass123", "g.reis@email.com", new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = false;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of getUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Utilizador u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        List<Utilizador> lu=new ArrayList();
        lu.add(u1);
        String strId = u1.getEmail();
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        Utilizador expResult = u1;
        Utilizador result = instance.getUtilizador(strId);
        assertEquals(expResult, result);
    }




    /**
     * Test of getPosition method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        Utilizador u1 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        List<Utilizador> lista= new ArrayList();
        lista.add(u1);
        int expResult = 0;
        int result =  new RegistoUtilizadores(lista).getPosition(u1);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaAlteracaoUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testValidaAlteracaoUtilizador() {
        System.out.println("validaAlteracaoUtilizador");
        Utilizador uClone= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        List<Utilizador> lista=new ArrayList();
        lista.add(uClone);
        int i = 0;
        boolean expResult = true;
        boolean result = new RegistoUtilizadores(lista).validaAlteracaoUtilizador(uClone, i);
        assertEquals(expResult, result);
    }

    /**
     * Test of temUtilizadorEmail method, of class RegistoUtilizadores.
     */
    @Test
    public void testTemUtilizadorEmail() {
        System.out.println("temUtilizadorEmail");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String email = "andrevieira_1996@hotmail.com";
        List<Utilizador> lista = new ArrayList();
        lista.add(u1);
        boolean expResult = true;
        boolean result = new RegistoUtilizadores(lista).temUtilizadorEmail(email);
        assertEquals(expResult, result);
    }

    /**
     * Test of temUtilizadorUsername method, of class RegistoUtilizadores.
     */
    @Test
    public void testTemUtilizadorUsername() {
        System.out.println("temUtilizadorUsername");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String username = "vieira";
        List<Utilizador> lista= new ArrayList();
        lista.add(u1);
        boolean expResult = true;
        boolean result = new RegistoUtilizadores(lista).temUtilizadorUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of addUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testAddUtilizador() {
        System.out.println("addUtilizador");
        Utilizador u = new Utilizador("Gonçalo", "greis", "pass123", "g.reis@email.com", new NaoFazNada());
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = true;
        boolean result = instance.addUtilizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class RegistoUtilizadores.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        List<Utilizador> lista=new ArrayList();
        String expResult = lista.toString();
        String result =  new RegistoUtilizadores(lista).toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class RegistoUtilizadores.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        List<Utilizador> lista=new ArrayList();
        lista.add(u1);
        boolean expResult = true;
        boolean result = new RegistoUtilizadores(lista).equals(new RegistoUtilizadores(lista));
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaTodosRevisores method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetListaTodosRevisores() {
        System.out.println("getListaTodosRevisores");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Evento ev=new Evento(); 
        List<Evento> listaev=new ArrayList();
        List<Utilizador> lista=new ArrayList();
        Revisor r=new Revisor(u1);
        Revisor r1=new Revisor();
        List<Revisor>listar= new ArrayList();
        CP cp= new CP();
        cp.addMembroCP(u1);
        ev.setCP(cp);
        lista.add(u1);
        listaev.add(ev);
        listar.add(r);
        listar.add(r1);
        RegistoEventos reg = new RegistoEventos(new RegistoUtilizadores(lista),listaev);
        RegistoUtilizadores instance = new RegistoUtilizadores(lista);
        List<Revisor> expResult = new ArrayList();
        expResult.add(r);
        List<Revisor> result = instance.getListaTodosRevisores(reg);
        assertEquals(expResult, result);

    }

    /**
     * Test of login method, of class RegistoUtilizadores.
     */
    @Test
    public void testLogin() {
        System.out.println("login");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String username = u1.getUsername();
        String password = u1.getPassword();
        Utilizador expResult = u1;
        List<Utilizador> lista=new ArrayList();
        lista.add(u1);
        Utilizador result = new RegistoUtilizadores(lista).login(username, password);
        assertEquals(expResult, result);

    }   
    /**
     * Test of showData method, of class RegistoUtilizadores.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRegisto method, of class RegistoUtilizadores.
     */
    @Test
    public void testSetRegisto() {
        System.out.println("setRegisto");
        ArrayList<Utilizador> registo = new ArrayList<Utilizador>();
        RegistoUtilizadores instance = new RegistoUtilizadores();
        instance.setRegisto(registo);
    }
}
