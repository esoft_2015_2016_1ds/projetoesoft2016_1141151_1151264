/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SubmissaoRejeitadaStateTest {
    
    public SubmissaoRejeitadaStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPreRevista method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetPreRevista() {
        System.out.println("setPreRevista");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setPreRevista();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNaoRevisto method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetNaoRevisto() {
        System.out.println("setNaoRevisto");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setNaoRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRejeitada method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetRejeitada() {
        System.out.println("setRejeitada");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRevisto method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetRevisto() {
        System.out.println("setRevisto");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceite method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetAceite() {
        System.out.println("setAceite");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setAceite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceiteNaoFinal method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetAceiteNaoFinal() {
        System.out.println("setAceiteNaoFinal");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setAceiteNaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmCameraReady method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaRemover method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testValidaRemover() {
        System.out.println("validaRemover");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = true;
        boolean result = instance.validaRemover();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRemovidaState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInRemovidaState() {
        System.out.println("isInRemovidaState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRemovidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInAceiteState() {
        System.out.println("isInAceiteState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmCameraReadyState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInEmCameraReadyState() {
        System.out.println("isInEmCameraReadyState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInNaoRevistoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInNaoRevistoState() {
        System.out.println("isInNaoRevistoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInNaoRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInPreRevistaState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInPreRevistaState() {
        System.out.println("isInPreRevistaState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInPreRevistaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRejeitadaState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInRejeitadaState() {
        System.out.println("isInRejeitadaState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = true;
        boolean result = instance.isInRejeitadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRevistoState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInRevistoState() {
        System.out.println("isInRevistoState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteNaoFinalState method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testIsInAceiteNaoFinalState() {
        System.out.println("isInAceiteNaoFinalState");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteNaoFinalState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SubmissaoRejeitadaState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SubmissaoRejeitadaState instance = new SubmissaoRejeitadaState(new Submissao());
        String expResult = "SubmissaoStateRejeitada";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
