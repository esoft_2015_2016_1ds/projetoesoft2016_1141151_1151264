/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SubmissaoAceiteStateTest {
    
    public SubmissaoAceiteStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoAceiteState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPreRevista method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetPreRevista() {
        System.out.println("setPreRevista");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setPreRevista();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNaoRevisto method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetNaoRevisto() {
        System.out.println("setNaoRevisto");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setNaoRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRejeitada method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetRejeitada() {
        System.out.println("setRejeitada");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRevisto method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetRevisto() {
        System.out.println("setRevisto");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceite method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetAceite() {
        System.out.println("setAceite");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setAceite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceiteNaoFinal method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetAceiteNaoFinal() {
        System.out.println("setAceiteNaoFinal");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setAceiteNaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmCameraReady method, of class SubmissaoAceiteState.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SubmissaoAceiteState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaRemover method, of class SubmissaoAceiteState.
     */
    @Test
    public void testValidaRemover() {
        System.out.println("validaRemover");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = true;
        boolean result = instance.validaRemover();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRemovidaState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInRemovidaState() {
        System.out.println("isInRemovidaState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRemovidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInAceiteState() {
        System.out.println("isInAceiteState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = true;
        boolean result = instance.isInAceiteState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmCameraReadyState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInEmCameraReadyState() {
        System.out.println("isInEmCameraReadyState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInNaoRevistoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInNaoRevistoState() {
        System.out.println("isInNaoRevistoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInNaoRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInPreRevistaState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInPreRevistaState() {
        System.out.println("isInPreRevistaState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInPreRevistaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRejeitadaState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInRejeitadaState() {
        System.out.println("isInRejeitadaState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRejeitadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRevistoState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInRevistoState() {
        System.out.println("isInRevistoState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteNaoFinalState method, of class SubmissaoAceiteState.
     */
    @Test
    public void testIsInAceiteNaoFinalState() {
        System.out.println("isInAceiteNaoFinalState");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteNaoFinalState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaNaoFinal method, of class SubmissaoAceiteState.
     */
    @Test
    public void testValidaNaoFinal() {
        System.out.println("validaNaoFinal");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());;
        boolean expResult = true;
        boolean result = instance.validaNaoFinal();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class SubmissaoAceiteState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SubmissaoAceiteState instance = new SubmissaoAceiteState(new Submissao());
        String expResult = "SubmissaoStateAceite";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
