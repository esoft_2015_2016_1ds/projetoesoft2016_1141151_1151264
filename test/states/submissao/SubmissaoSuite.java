/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jbraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({SubmissaoPreRevistaStateTest.class, SubmissaoRejeitadaStateTest.class, SubmissaoCriadoStateTest.class, SubmissaoRevistoStateTest.class, SubmissaoEmCameraReadyStateTest.class, SubmissaoRemovidaStateTest.class, SubmissaoEmLicitacaoStateTest.class, SubmissaoRegistadoStateTest.class, SubmissaoAceiteNaoFinalStateTest.class, SubmissaoEmRevisaoStateTest.class, SubmissaoNaoRevistoStateTest.class, SubmissaoAceiteStateTest.class})
public class SubmissaoSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
