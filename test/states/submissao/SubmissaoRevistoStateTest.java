/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SubmissaoRevistoStateTest {
    
    public SubmissaoRevistoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoRevistoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPreRevista method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetPreRevista() {
        System.out.println("setPreRevista");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setPreRevista();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNaoRevisto method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetNaoRevisto() {
        System.out.println("setNaoRevisto");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setNaoRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRejeitada method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetRejeitada() {
        System.out.println("setRejeitada");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRevisto method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetRevisto() {
        System.out.println("setRevisto");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceite method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetAceite() {
        System.out.println("setAceite");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setAceite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceiteNaoFinal method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetAceiteNaoFinal() {
        System.out.println("setAceiteNaoFinal");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setAceiteNaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmCameraReady method, of class SubmissaoRevistoState.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SubmissaoRevistoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaRejeitada method, of class SubmissaoRevistoState.
     */
    @Test
    public void testValidaRejeitada() {
        System.out.println("validaRejeitada");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.validaRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaRemover method, of class SubmissaoRevistoState.
     */
    @Test
    public void testValidaRemover() {
        System.out.println("validaRemover");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.validaRemover();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRemovidaState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInRemovidaState() {
        System.out.println("isInRemovidaState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRemovidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInAceiteState() {
        System.out.println("isInAceiteState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmCameraReadyState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInEmCameraReadyState() {
        System.out.println("isInEmCameraReadyState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInNaoRevistoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInNaoRevistoState() {
        System.out.println("isInNaoRevistoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInNaoRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInPreRevistaState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInPreRevistaState() {
        System.out.println("isInPreRevistaState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInPreRevistaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRejeitadaState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInRejeitadaState() {
        System.out.println("isInRejeitadaState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRejeitadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRevistoState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInRevistoState() {
        System.out.println("isInRevistoState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = true;
        boolean result = instance.isInRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteNaoFinalState method, of class SubmissaoRevistoState.
     */
    @Test
    public void testIsInAceiteNaoFinalState() {
        System.out.println("isInAceiteNaoFinalState");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteNaoFinalState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SubmissaoRevistoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SubmissaoRevistoState instance = new SubmissaoRevistoState(new Submissao());
        String expResult = "SubmissaoStateRevisto";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
