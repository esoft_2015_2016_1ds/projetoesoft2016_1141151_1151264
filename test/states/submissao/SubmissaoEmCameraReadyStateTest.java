/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import model.Submissao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SubmissaoEmCameraReadyStateTest {
    
    public SubmissaoEmCameraReadyStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setPreRevista method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetPreRevista() {
        System.out.println("setPreRevista");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setPreRevista();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setNaoRevisto method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetNaoRevisto() {
        System.out.println("setNaoRevisto");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setNaoRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRejeitada method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetRejeitada() {
        System.out.println("setRejeitada");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRevisto method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetRevisto() {
        System.out.println("setRevisto");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setRevisto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceite method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetAceite() {
        System.out.println("setAceite");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setAceite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceiteNaoFinal method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetAceiteNaoFinal() {
        System.out.println("setAceiteNaoFinal");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.setAceiteNaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmCameraReady method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = true;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaRemover method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testValidaRemover() {
        System.out.println("validaRemover");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.validaRemover();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRemovidaState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInRemovidaState() {
        System.out.println("isInRemovidaState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRemovidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInAceiteState() {
        System.out.println("isInAceiteState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmCameraReadyState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInEmCameraReadyState() {
        System.out.println("isInEmCameraReadyState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = true;
        boolean result = instance.isInEmCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInNaoRevistoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInNaoRevistoState() {
        System.out.println("isInNaoRevistoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInNaoRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInPreRevistaState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInPreRevistaState() {
        System.out.println("isInPreRevistaState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInPreRevistaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRejeitadaState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInRejeitadaState() {
        System.out.println("isInRejeitadaState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRejeitadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRevistoState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInRevistoState() {
        System.out.println("isInRevistoState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteNaoFinalState method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testIsInAceiteNaoFinalState() {
        System.out.println("isInAceiteNaoFinalState");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        boolean expResult = false;
        boolean result = instance.isInAceiteNaoFinalState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SubmissaoEmCameraReadyState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SubmissaoEmCameraReadyState instance = new SubmissaoEmCameraReadyState(new Submissao());
        String expResult = "SubmissaoStateEmCameraReady";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
