/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import model.CP;
import model.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class EventoSessaoTematicaDefinidaStateTest {
    
    public EventoSessaoTematicaDefinidaStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testValida() {
        System.out.println("valida(sem CP)");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of valida method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testValidaCase2() {
        System.out.println("valida(com CP)");
        Evento ev = new Evento();
        ev.setCP(new CP());
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(ev);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of setCriado method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        Evento ev = new Evento();
        ev.setCP(new CP());
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(ev);
        boolean expResult = true;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSessaoTematicaDefinida method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetSessaoTematicaDefinida() {
        System.out.println("setSessaoTematicaDefinida");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = true;
        boolean result = instance.setSessaoTematicaDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        EventoSessaoTematicaDefinidaState instance =  new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInSessaoTematicaDefinidaState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInSessaoTematicaDefinidaState() {
        System.out.println("isInSessaoTematicaDefinidaState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = true;
        boolean result = instance.isInSessaoTematicaDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class EventoSessaoTematicaDefinidaState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        EventoSessaoTematicaDefinidaState instance = new EventoSessaoTematicaDefinidaState(new Evento());
        String expResult = "EventoStateSessaoTematicaDefinida";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
