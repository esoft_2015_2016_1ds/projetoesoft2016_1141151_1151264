/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import model.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class EventoCriadoStateTest {
    
    public EventoCriadoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoCriadoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCriado method, of class EventoCriadoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = true;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setRegistado method, of class EventoCriadoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = true;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCPDefinida method, of class EventoCriadoState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class EventoCriadoState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmDetecao method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmLicitacao method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmDistribuicao method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmRevisao method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmDecisao method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class EventoCriadoState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSessaoTematicaDefinida method, of class EventoCriadoState.
     */
    @Test
    public void testSetSessaoTematicaDefinida() {
        System.out.println("setSessaoTematicaDefinida");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setSessaoTematicaDefinida();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class EventoCriadoState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCameraReady method, of class EventoCriadoState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        EventoCriadoState instance =  new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of showData method, of class EventoCriadoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        EventoCriadoState instance = new EventoCriadoState(new Evento());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCriadoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());
        boolean expResult = true;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInRegistadoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInSessaoTematicaDefinidaState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInSessaoTematicaDefinidaState() {
        System.out.println("isInSessaoTematicaDefinidaState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInSessaoTematicaDefinidaState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCPDefinidaState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDetecaoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmLicitacaoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDistribuicaoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmRevisaoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDecisaoState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());;
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCameraReadyState method, of class EventoCriadoState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        EventoCriadoState instance = new EventoCriadoState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class EventoCriadoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        EventoCriadoState instance = new EventoCriadoState(new Evento());
        String expResult = "EventoStateCriado";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
