/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import java.util.ArrayList;
import model.CP;
import model.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.Data;

/**
 *
 * @author jbraga
 */
public class EventoCPDefinidaStateTest {
    
    public EventoCPDefinidaStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoCPDefinidaState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Evento ev = new Evento("Pika","Pikachu",Data.getDataAtual(),Data.getDataAtual(),new Data(Data.getDataAtual().getAno(),Data.getDataAtual().getMes(),Data.getDataAtual().getDia()-1),Data.getDataAtual(),Data.getDataAtual(),new CP(),new ArrayList(),new ArrayList());
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(ev);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = true;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        Evento ev = new Evento("Pika","Pikachu",Data.getDataAtual(),Data.getDataAtual(),new Data(Data.getDataAtual().getAno(),Data.getDataAtual().getMes(),Data.getDataAtual().getDia()-1),Data.getDataAtual(),Data.getDataAtual(),new CP(),new ArrayList(),new ArrayList());
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(ev);
        boolean expResult = true;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSessaoTematicaDefinida method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetSessaoTematicaDefinida() {
        System.out.println("setSessaoTematicaDefinida");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setSessaoTematicaDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class EventoCPDefinidaState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        EventoCPDefinidaState instance =  new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class EventoCPDefinidaState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInSessaoTematicaDefinidaState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInSessaoTematicaDefinidaState() {
        System.out.println("isInSessaoTematicaDefinidaState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInSessaoTematicaDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = true;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class EventoCPDefinidaState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class EventoCPDefinidaState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        EventoCPDefinidaState instance = new EventoCPDefinidaState(new Evento());
        String expResult = "EventoStateCPDefinida";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
