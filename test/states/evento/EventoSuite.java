/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author jbraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({EventoEmSubmissaoCameraReadyStateTest.class, EventoEmDecisaoStateTest.class, EventoRegistadoStateTest.class, EventoSessaoTematicaDefinidaStateTest.class, EventoEmLicitacaoStateTest.class, EventoEmDistribuicaoStateTest.class, EventoCPDefinidaStateTest.class, EventoCriadoStateTest.class, EventoEmRevisaoStateTest.class, EventoCameraReadyStateTest.class, EventoAceitaSubmissoesStateTest.class, EventoEmDetecaoStateTest.class})
public class EventoSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
