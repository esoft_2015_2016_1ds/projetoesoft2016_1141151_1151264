/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import model.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class EventoEmSubmissaoCameraReadyStateTest {
    
    public EventoEmSubmissaoCameraReadyStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSessaoTematicaDefinida method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetSessaoTematicaDefinida() {
        System.out.println("setSessaoTematicaDefinida");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setSessaoTematicaDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = true;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        EventoEmSubmissaoCameraReadyState instance =  new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInSessaoTematicaDefinidaState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInSessaoTematicaDefinidaState() {
        System.out.println("isInSessaoTematicaDefinidaState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInSessaoTematicaDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = true;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class EventoEmSubmissaoCameraReadyState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        EventoEmSubmissaoCameraReadyState instance = new EventoEmSubmissaoCameraReadyState(new Evento());
        String expResult = "EventoStateEmSubmissaoCameraReady";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
