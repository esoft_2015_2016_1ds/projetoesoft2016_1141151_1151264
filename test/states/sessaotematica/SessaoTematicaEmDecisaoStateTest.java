/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.sessaotematica;

import model.SessaoTematica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SessaoTematicaEmDecisaoStateTest {
    
    public SessaoTematicaEmDecisaoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SessaoTematicaEmDecisaoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SessaoTematicaEmDecisaoState instance = new SessaoTematicaEmDecisaoState(new SessaoTematica());
        String expResult = "STStateEmDecisao";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
