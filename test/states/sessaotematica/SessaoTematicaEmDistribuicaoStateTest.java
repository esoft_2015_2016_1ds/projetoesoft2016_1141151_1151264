/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.sessaotematica;

import model.ProcessoDistribuicao;
import model.SessaoTematica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SessaoTematicaEmDistribuicaoStateTest {
    
    public SessaoTematicaEmDistribuicaoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SessaoTematica st = new SessaoTematica();
        st.setProcessoDistribuicao(new ProcessoDistribuicao());
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(st);
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SessaoTematica st = new SessaoTematica();
        st.setProcessoDistribuicao(new ProcessoDistribuicao());
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(st);
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SessaoTematicaEmDistribuicaoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SessaoTematicaEmDistribuicaoState instance = new SessaoTematicaEmDistribuicaoState(new SessaoTematica());
        String expResult = "STStateEmDistribuicao";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
