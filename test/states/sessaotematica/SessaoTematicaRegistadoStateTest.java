/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.sessaotematica;

import model.SessaoTematica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SessaoTematicaRegistadoStateTest {
    
    public SessaoTematicaRegistadoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SessaoTematicaRegistadoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SessaoTematicaRegistadoState instance = new SessaoTematicaRegistadoState(new SessaoTematica());
        String expResult = "STStateRegistado";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
