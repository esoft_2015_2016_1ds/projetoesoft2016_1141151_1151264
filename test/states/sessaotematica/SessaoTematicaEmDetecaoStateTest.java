/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.sessaotematica;

import model.SessaoTematica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class SessaoTematicaEmDetecaoStateTest {
    
    public SessaoTematicaEmDetecaoStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCriado method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRegistado method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCPDefinida method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmSubmissaoCameraReady method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetEmSubmissaoCameraReady() {
        System.out.println("setEmSubmissaoCameraReady");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setEmSubmissaoCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setCameraReady method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCPDefinidaState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = true;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCameraReadyState method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class SessaoTematicaEmDetecaoState.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SessaoTematicaEmDetecaoState instance = new SessaoTematicaEmDetecaoState(new SessaoTematica());
        String expResult = "STStateEmDetecao";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
