/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import java.util.ArrayList;
import java.util.List;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.CP;
import model.Evento;
import model.ProcessoDistribuicao;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.sessaotematica.SessaoTematicaAceitaSubmissoesState;
import states.sessaotematica.SessaoTematicaCameraReadyState;
import states.sessaotematica.SessaoTematicaEmDecisaoState;
import states.sessaotematica.SessaoTematicaEmLicitacaoState;
import states.sessaotematica.SessaoTematicaEmRevisaoState;
import states.sessaotematica.SessaoTematicaEmSubmissaoCameraReadyState;
import states.sessaotematica.SessaoTematicaRegistadoState;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaSessoesTematicasTest {

    public ListaSessoesTematicasTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }
    ListaSessoesTematicas lst;
    RegistoEventos regE;
    RegistoUtilizadores regU;
    Utilizador u1;
    Evento ev1, ev2, ev3;
    SessaoTematica st1, st2, st3, st4;
    Submissao s1;

    @Before
    public void setUp() {
        List<SessaoTematica> ls = new ArrayList();
        u1 = new Utilizador("admin", "adminA", "123easy", "admin@gmail.com", new NaoFazNada());
        st1 = new SessaoTematica();
        st1.setState(new SessaoTematicaAceitaSubmissoesState(st1));
        st2 = new SessaoTematica();
        st2.setState(new SessaoTematicaRegistadoState(st2));
        st3 = new SessaoTematica();
        st3.setState(new SessaoTematicaRegistadoState(st3));
        st4 = new SessaoTematica();
        st4.setState(new SessaoTematicaCameraReadyState(st4));
        ls.add(st1);
        ls.add(st2);
        ls.add(st3);
        ls.add(st4);
        lst = new ListaSessoesTematicas(ls);
    }

    @After
    public void tearDown() {
    }
    
    /**
     * Test of getSessoesTematicasProponenteEmEstadoRegistado method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetSessoesTematicasProponenteEmEstadoRegistado() {
        System.out.println("getSessoesTematicasProponenteEmEstadoRegistado");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaRegistadoState(st5));
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast=new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getSessoesTematicasProponenteEmEstadoRegistado(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of getSubmissoesSessoesUtilizador method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetSubmissoesSessoesUtilizador() {
        System.out.println("getSubmissoesSessoesUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut);
        Submissao sub=new Submissao();
        List<String> strings= new ArrayList();
        strings.add("olmd");
        a1.setPalavrasChave(strings);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        sub.setArtigoInicial(a1);
     
        st5.addSubmissao(sub, a1, u4);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<SessaoTematica> listast=new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<Submissao> result = new ListaSessoesTematicas(listast).getSubmissoesSessoesUtilizador(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of getSessoesTematicasComProponente method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetSessoesTematicasComProponente() {
        System.out.println("getSessoesTematicasComProponente");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaRegistadoState(st5));
        st5.addProponente(u4);
        SessaoTematica st6 = new SessaoTematica();
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast=new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getSessoesTematicasComProponenteRegistadoState(id);
        assertEquals(expResult, result);
 
    }

    /**
     * Test of add method, of class ListaSessoesTematicas.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        SessaoTematica st = new SessaoTematica();
        ListaSessoesTematicas instance = new ListaSessoesTematicas();
        boolean expResult = true;
        boolean result = instance.add(st);
        assertEquals(expResult, result);

    }

    /**
     * Test of temSessaoTematica method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSessaoTematicaV2() {
        System.out.println("temSessaoTematicaV2(com um objeto null na lista)");
        ListaSessoesTematicas instance = new ListaSessoesTematicas();
        instance.add(null);
        boolean expResult = false;
        boolean result = instance.temSessaoTematica();
        assertEquals(expResult, result);
    }

    /**
     * Test of temSessaoTematica method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSessaoTematica() {
        System.out.println("temSessaoTematica");
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
        boolean expResult = true;
        List<SessaoTematica> listast=new ArrayList();
        listast.add(st5);
        listast.add(st6);
        boolean result = new ListaSessoesTematicas(listast).temSessaoTematica();
        assertEquals(expResult, result);

    }

    /**
     * Test of temSessaoComProponente method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSessaoComProponente() {
        System.out.println("temSessaoComProponente");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        SessaoTematica st6 = new SessaoTematica();
        boolean expResult = true;
        List<SessaoTematica> listast=new ArrayList();
        listast.add(st5);
        listast.add(st6);
        boolean result = new ListaSessoesTematicas(listast).temSessaoComProponente(id);
        assertEquals(expResult, result);

    }
    /**
     * Test of getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes method, of
     * class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes() {
        System.out.println("getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes");
        ListaSessoesTematicas instance = lst;
        List<Submissivel> expResult = new ArrayList();
        expResult.add(st1);
        List<Submissivel> result = instance.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaLicitaveisEmLicitacaoDe method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetListaLicitaveisEmLicitacaoDe() {
        System.out.println("getListaLicitaveisEmLicitacaoDe");
        String id = u1.getEmail();
        SessaoTematica st3 = new SessaoTematica();
        st3.setState(new SessaoTematicaEmLicitacaoState(st3));
        st3.novaCP().addMembroCP(new Revisor(u1));
        ListaSessoesTematicas instance = lst;
        lst.add(st3);
        List<Licitavel> expResult = new ArrayList();
        expResult.add(st3);
        List<Licitavel> result = instance.getListaLicitaveisEmLicitacaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getListaSubmissiveisNaoCameraReadyDe method, of class
     * ListaSessoesTematicas. In this case, no thematic session has a
     * submmission of the author, eventhough there are several that are not in
     * the CameraReady state.
     */
    @Test
    public void testGetListaSubmissiveisNaoCameraReadyDe() {
        System.out.println("getListaSubmissiveisNaoCameraReadyDe");
        String id = u1.getUsername();
        ListaSessoesTematicas instance = lst;
        List<Submissivel> expResult = new ArrayList();
        List<Submissivel> result = instance.getListaSubmissiveisNaoCameraReadyDe(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of temSubmissoesAutor method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSubmissoesAutor() {
        System.out.println("temSubmissoesAutor");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
         Submissao sub=new Submissao();
         List<String> strings= new ArrayList();
         strings.add("kndkc");
         a1.setPalavrasChave(strings);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        st5.addSubmissao(sub, a1, u4);
        boolean expResult = true;
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        boolean result = new ListaSessoesTematicas(listast).temSubmissoesAutor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getConfianca method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetConfianca() {
        System.out.println("getConfianca");
        SessaoTematica st=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(lst);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoConfianca(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        st.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getConfianca();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAdequacao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetAdequacao() {
        System.out.println("getAdequacao");
        SessaoTematica st=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(lst);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoAdequacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        st.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getAdequacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalidade method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetOriginalidade() {
        System.out.println("getOriginalidade");
        SessaoTematica st=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(lst);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoOriginalidade(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        st.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getOriginalidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getQualidade method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetQualidade() {
          System.out.println("getQualidade");
        SessaoTematica st=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(lst);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoApresentacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        st.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getQualidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRecomendacao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetRecomendacao() {
        System.out.println("getRecomendacao");
        SessaoTematica st=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(lst);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoRecomendacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        st.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getRecomendacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroSubmissoesAceites method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetNumeroSubmissoesAceites() {
        System.out.println("getNumeroSubmissoesAceites");
        List<Autor> la = new ArrayList();
        Autor a=new Autor(new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada()),"ISEP");
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        List<String> strings= new ArrayList();
        strings.add("nkid");
        a1.setPalavrasChave(strings);
        Submissao sub1 = new Submissao(a1, a1, null);
        
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2 = new Submissao(a1, a1, null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3 = new Submissao(a1, a1, null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4 = new Submissao(a1, a1, null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        Submissao sub5 = new Submissao(a1, a1, null);
        sub5.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls = new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        ls.add(sub5);
        st1.setState(new SessaoTematicaAceitaSubmissoesState(st1));
        List<SessaoTematica> sst=new ArrayList();
        st1.addSubmissao(sub1, a1, u1);
        st1.addSubmissao(sub2, a1, u1);
        st1.addSubmissao(sub3, a1, u1);
        st1.addSubmissao(sub4, a1, u1);
        st1.addSubmissao(sub5, a1, u1);
        sst.add(st1);
        ListaSessoesTematicas instance=new ListaSessoesTematicas(sst);
        int expResult = 3;
        int result = instance.getNumeroSubmissoesAceites();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroTotalSubmissoes method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetNumeroTotalSubmissoes() {
        System.out.println("getNumeroTotalSubmissoes");
        List<Autor> la = new ArrayList();
        Autor a=new Autor(new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada()),"ISEP");
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        List<String> strings= new ArrayList();
        strings.add("nkid");
        a1.setPalavrasChave(strings);
        Submissao sub1 = new Submissao(a1, a1, null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2 = new Submissao(a1, a1, null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3 = new Submissao(a1, a1, null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4 = new Submissao(a1, a1, null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls = new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        
        List<SessaoTematica> sst=new ArrayList();
        st1.addSubmissao(sub1, a1, u1);
        st1.addSubmissao(sub2, a1, u1);
        st1.addSubmissao(sub3, a1, u1);
        st1.addSubmissao(sub4, a1, u1);
        
        sst.add(st1);
        ListaSessoesTematicas instance=new ListaSessoesTematicas(sst);
        int expResult = 4;
        int result = instance.getNumeroTotalSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoesRegistadasUtilizador method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSubmissoesRegistadasUtilizador() {
        System.out.println("getListaSubmissoesRegistadasUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaAceitaSubmissoesState(st5));
        SessaoTematica st6 = new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        Submissao sub=new Submissao();
        List<String> strings= new ArrayList();
        strings.add("kndvk");
        a1.setPalavrasChave(strings);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        sub.setState(new SubmissaoRegistadoState(sub));
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        st5.addSubmissao(sub, a1, u4);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<Submissao> result = new ListaSessoesTematicas(listast).getListaSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getListaTodasRevisoes method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaTodasRevisoes() {
        System.out.println("getListaTodasRevisoes");
        SessaoTematica st5 = new SessaoTematica();
         Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Revisor r= new Revisor(u4);
        Revisao rev= new Revisao();
        List<Revisao> list=new ArrayList();
        list.add(rev);
        ListaRevisoes lista =new ListaRevisoes(list);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lista);
        st5.setProcessoDistribuicao(pdis);
        CP cp= new CP();
        cp.addMembroCP(r);
        st5.setCP(cp);
         Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        Submissao sub=new Submissao(); 
        rev.setRevisor(new Revisor(u1));
        List<String> strings= new ArrayList();
        strings.add("olmd");
        a1.setPalavrasChave(strings);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        sub.setState(new SubmissaoRegistadoState(sub));
        sub.setArtigoInicial(a1);
        rev.setSubmissao(sub);
        st5.saveRevisao(rev);
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        List<Revisao> result = new ListaSessoesTematicas(listast).getListaTodasRevisoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador
     * method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador() {
        System.out.println("getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaAceitaSubmissoesState(st5));
        SessaoTematica st6 = new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        Submissao sub=new Submissao(); 
        List<String> strings= new ArrayList();
        strings.add("olmd");
        a1.setPalavrasChave(strings);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        sub.setState(new SubmissaoRegistadoState(sub));
        sub.setArtigoInicial(a1);
        st5.addSubmissao(sub, a1, u4);
        List<Submissivel> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<Submissivel> result = new ListaSessoesTematicas(listast).getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisiveisEmEstadoDeRevisaoDe method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetRevisiveisEmEstadoDeRevisaoDe() {
        System.out.println("getRevisiveisEmEstadoDeRevisaoDe");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaRegistadoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        List<Revisivel> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<Revisivel> result = new ListaSessoesTematicas(listast).getRevisiveisEmEstadoDeRevisaoDe(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of temSubmissoesAceitesUtilizador method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSubmissoesAceitesUtilizador() {
        System.out.println("temSubmissoesAceitesUtilizador");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
         Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        Submissao sub=new Submissao();
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        List<String> strings= new ArrayList();
        strings.add("ola");
        strings.add("knkdnckd");
        a1.setPalavrasChave(strings);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        
        sub.setState(new SubmissaoAceiteState(sub));
        sub.setArtigoInicial(a1);
      
       
        st5.addSubmissao(sub, a1, u4);
        boolean expResult = true;
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        boolean result = new ListaSessoesTematicas(listast).temSubmissoesAceitesUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessoesEmSubmissaoCR method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessoesEmSubmissaoCR() {
        System.out.println("getListaSessoesEmSubmissaoCR");
         Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaEmSubmissaoCameraReadyState(st5));
        SessaoTematica st6 = new SessaoTematica();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        Autor aut=new Autor(u4,"Kanto Region");
        a1.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a1.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        List<String> strings= new ArrayList();
        strings.add("ola");
        a1.setPalavrasChave(strings);
        Submissao sub=new Submissao();
        sub.setState(new SubmissaoAceiteState(sub));
        sub.setArtigoInicial(a1);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        st5.addSubmissao(sub, a1, u4);
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getListaSessoesEmSubmissaoCR(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of temSessoesEmDecisao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSessoesEmDecisao() {
        System.out.println("temSessoesEmDecisao");
         Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaEmDecisaoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        st5.addProponente(u4);
        boolean expResult = true;
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        boolean result = new ListaSessoesTematicas(listast).temSessoesEmDecisao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessoesEmDecisao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessoesEmDecisao() {
        System.out.println("getListaSessoesEmDecisao");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.setState(new SessaoTematicaEmDecisaoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        st5.addProponente(u4);
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);        
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getListaSessoesEmDecisao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessoesTematicas method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessoesTematicas() {
        System.out.println("getListaSessoesTematicas");
        SessaoTematica st5 = new SessaoTematica();
        SessaoTematica st6 = new SessaoTematica();
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        expResult.add(st6);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);   
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getListaSessoesTematicas();
        assertEquals(expResult, result);
    }

    /**
     * Test of temSessoesDistribuiveis method, of class ListaSessoesTematicas.
     */
    @Test
    public void testTemSessoesDistribuiveis() {
        System.out.println("temSessoesDistribuiveis");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaEmLicitacaoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        boolean expResult = true;
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);  
        boolean result = new ListaSessoesTematicas(listast).temSessoesDistribuiveis(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSessoesTematicasEmLicitacao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetSessoesTematicasEmLicitacao() {
        System.out.println("getSessoesTematicasEmLicitacao");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaEmLicitacaoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getSessoesTematicasEmLicitacao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaRevisivelUtilizadorEmEstadoRevisao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaRevisivelUtilizadorEmEstadoRevisao() {
        System.out.println("getListaRevisivelUtilizadorEmEstadoRevisao");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaEmRevisaoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        List<Revisivel> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<Revisivel> result = new ListaSessoesTematicas(listast).getListaRevisivelUtilizadorEmEstadoRevisao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSessoesTematicasComProponenteRegistadoState method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetSessoesTematicasComProponenteRegistadoState() {
        System.out.println("getSessoesTematicasComProponenteRegistadoState");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaRegistadoState(st5));
        SessaoTematica st6 = new SessaoTematica();
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getSessoesTematicasComProponenteRegistadoState(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessoesDecididas method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessoesDecididas() {
        System.out.println("getListaSessoesDecididas");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        String id = u4.getEmail();
        SessaoTematica st5 = new SessaoTematica();
        st5.addProponente(u4);
        st5.setState(new SessaoTematicaCameraReadyState(st5));
        SessaoTematica st6 = new SessaoTematica();
        List<SessaoTematica> expResult = new ArrayList();
        expResult.add(st5);
        List<SessaoTematica> listast= new ArrayList();
        listast.add(st5);
        listast.add(st6);
        List<SessaoTematica> result = new ListaSessoesTematicas(listast).getListaSessoesDecididas();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaTodosRevisores method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaTodosRevisores() {
        System.out.println("getListaTodosRevisores");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Revisor r= new Revisor(u4);
        SessaoTematica st= new SessaoTematica();
        CP cp= new CP();
        cp.addMembroCP(u4);
        st.setCP(cp);
        List<Revisor> expResult = new ArrayList();
        expResult.add(r);
        List<SessaoTematica> lista = new ArrayList();
        lista.add(st);
        List<Revisor> result = new ListaSessoesTematicas(lista).getListaTodosRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisor method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        Revisor r= new Revisor(u4);
        SessaoTematica st= new SessaoTematica();
        CP cp= new CP();
        cp.addMembroCP(u4);
        st.setCP(cp);
        List<SessaoTematica> lista = new ArrayList();
        lista.add(st);
        Revisor expResult = r;
        Revisor result = new ListaSessoesTematicas(lista).getRevisor(u4);
        assertEquals(expResult, result);

    }

    /**
     * Test of showData method, of class ListaSessoesTematicas.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        String expResult = "";
        String result = new ListaSessoesTematicas().showData();
        assertEquals(expResult, result);

    }

    /**
     * Test of getSessaoTematicaCodigo method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetSessaoTematicaCodigo() {
        System.out.println("getSessaoTematicaCodigo");
        SessaoTematica st= new SessaoTematica();
        st.setCodigo("ola");
        List<SessaoTematica> list= new ArrayList();
        list.add(st);
        String code = "ola";
        ListaSessoesTematicas instance = new ListaSessoesTematicas(list);
        SessaoTematica expResult = st;
        SessaoTematica result = instance.getSessaoTematicaCodigo(code);
        assertEquals(expResult, result);

    }

    /**
     * Test of addAllSessoesTematicas method, of class ListaSessoesTematicas.
     */
    @Test
    public void testAddAllSessoesTematicas() {
        System.out.println("addAllSessoesTematicas");
        SessaoTematica st= new SessaoTematica();
        List<SessaoTematica> list= new ArrayList();
        list.add(st1);
        ListaSessoesTematicas instance = new ListaSessoesTematicas();
        instance.addAllSessoesTematicas(list);
    }

    /**
     * Test of registaSessaoTematica method, of class ListaSessoesTematicas.
     */
    @Test
    public void testRegistaSessaoTematica() {
        System.out.println("registaSessaoTematica");
        Utilizador u4=new Utilizador("Diogo","Braga","123abc","dogo@hotmail.com",new NaoFazNada());
        SessaoTematica st = new SessaoTematica();
        st.addProponente(u4);
        CP cp= new CP();
        cp.addMembroCP(u4);
        st.setCP(cp);
        List<SessaoTematica> list= new ArrayList();
        list.add(st);
        ListaSessoesTematicas instance = new ListaSessoesTematicas(list);
        instance.add(st);
        boolean expResult = true;
        boolean result = instance.registaSessaoTematica(st);
        assertEquals(expResult, result);
        fail("ver validast ciclo for");
    }

    /**
     * Test of validaST method, of class ListaSessoesTematicas.
     */
    @Test
    public void testValidaST() {
        System.out.println("validaST");
        SessaoTematica st = new SessaoTematica();
        List<SessaoTematica> list= new ArrayList();
        ListaSessoesTematicas instance = new ListaSessoesTematicas(list);
        boolean expResult = true;
        boolean result = instance.validaST(st);
        assertEquals(expResult, result);
    }

}
