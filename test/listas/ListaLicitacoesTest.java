/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;
import model.Licitacao;
import model.Revisor;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaLicitacoesTest {
    
    public ListaLicitacoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setListLicitacoes method, of class ListaLicitacoes.
     */
    @Test
    public void testSetListLicitacoes() {
        System.out.println("setListLicitacoes");
        List<Licitacao> lc = null;
        ListaLicitacoes instance = new ListaLicitacoes();
        instance.setListLicitacoes(lc);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getLicitacaoDe method, of class ListaLicitacoes.
     */
    @Test
    public void testGetLicitacaoDe() {
        System.out.println("getLicitacaoDe");
        Utilizador  u = new Utilizador("admin","admin","admin1","admin@gmail.com",new NaoFazNada());
        Licitacao l = new Licitacao(new Submissao(),new Revisor(u),new ArrayList());
        String id = u.getEmail();
        ListaLicitacoes instance = new ListaLicitacoes();
        instance.registaLicitacao(l);
        Licitacao expResult = l;
        Licitacao result = instance.getLicitacaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of registaLicitacao method, of class ListaLicitacoes.
     */
    @Test
    public void testRegistaLicitacaoNull() {
        System.out.println("registaLicitacao(null object)");
        Licitacao l = null;
        ListaLicitacoes instance = new ListaLicitacoes();
        boolean expResult = false;
        boolean result = instance.registaLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of registaLicitacao method, of class ListaLicitacoes.
     */
    @Test
    public void testRegistaLicitacao() {
        System.out.println("registaLicitacao(null object)");
        Licitacao l = new Licitacao();
        ListaLicitacoes instance = new ListaLicitacoes();
        boolean expResult = true;
        boolean result = instance.registaLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class ListaLicitacoes.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListaLicitacoes instance = new ListaLicitacoes();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaLicitacao method, of class ListaLicitacoes.
     */
    @Test
    public void testValidaLicitacao() {
        System.out.println("validaLicitacao");
        Licitacao l = new Licitacao(new Submissao(),new Revisor(),new ArrayList());
        l.setInteresse(2);
        ListaLicitacoes instance = new ListaLicitacoes();
        boolean expResult = false;
        boolean result = instance.validaLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getLicitacoes method, of class ListaLicitacoes.
     */
    @Test
    public void testGetLicitacoes() {
        System.out.println("getLicitacoes");
        Licitacao l = new Licitacao(new Submissao(),new Revisor(),new ArrayList());
        List<Licitacao> expResult = new ArrayList();
        expResult.add(l);
        List<Licitacao> list= new ArrayList();
        list.add(l);
        List<Licitacao> result = new ListaLicitacoes(list).getLicitacoes();
        assertEquals(expResult, result);

    }
}
