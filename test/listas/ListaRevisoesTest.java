/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaRevisoesTest {
    
    public ListaRevisoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testGetRevisoes() {
        System.out.println("getRevisoes");
        Utilizador u = new Utilizador("admin","admin","admin1","admin@gmail.com",new NaoFazNada());
        String id = u.getEmail();
        Submissao sub =new Submissao();
        Revisao rev = new Revisao(new Revisor(u));
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ListaRevisoes instance = new ListaRevisoes(lr);
        List<Revisao> expResult = lr;
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setListRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testSetListRevisoes() {
        System.out.println("setListRevisoes");
        List<Revisao> lr = null;
        ListaRevisoes instance = new ListaRevisoes();
        instance.setListRevisoes(lr);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of saveRevisao method, of class ListaRevisoes.
     */
    @Test
    public void testSaveRevisao() {
        System.out.println("saveRevisao");
        Revisao rev = null;
        ListaRevisoes instance = new ListaRevisoes();
        boolean expResult = false;
        boolean result = instance.saveRevisao(rev);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getConfianca method, of class ListaRevisoes.
     */
    @Test
    public void testGetConfianca() {
        System.out.println("getConfianca");
        SessaoTematica st1=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st1); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoConfianca(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes instance=new ListaRevisoes(listr);      
        listr.add(rev1);
        int expResult = 2;
        int result = instance.getConfianca();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAdequacao method, of class ListaRevisoes.
     */
    @Test
    public void testGetAdequacao() {
        System.out.println("getAdequacao");
        SessaoTematica st1=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st1); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoAdequacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes instance=new ListaRevisoes(listr);      
        listr.add(rev1);
        int expResult = 2;
        int result = instance.getAdequacao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getOriginalidade method, of class ListaRevisoes.
     */
    @Test
    public void testGetOriginalidade() {
        System.out.println("getOriginalidade");
          SessaoTematica st1=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st1); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoOriginalidade(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes instance=new ListaRevisoes(listr);      
        listr.add(rev1);
        int expResult = 2;
        int result = instance.getOriginalidade();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getQualidade method, of class ListaRevisoes.
     */
    @Test
    public void testGetQualidade() {
        System.out.println("getQualidade");
          SessaoTematica st1=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st1); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoApresentacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes instance=new ListaRevisoes(listr);      
        listr.add(rev1);
        int expResult = 2;
        int result = instance.getQualidade();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRecomendacao method, of class ListaRevisoes.
     */
    @Test
    public void testGetRecomendacao() {
        System.out.println("getRecomendacao");
          SessaoTematica st1=new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(st1); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoRecomendacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes instance=new ListaRevisoes(listr);      
        listr.add(rev1);
        int expResult = 2;
        int result = instance.getRecomendacao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaTodasRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testGetListaTodasRevisoes() {
        System.out.println("getListaTodasRevisoes");
        Revisao rev1 = new Revisao();
        Revisao rev2 = new Revisao();
        ListaRevisoes instance = new ListaRevisoes();
        List<Revisao> lr = new ArrayList();
        lr.add(rev1);
        lr.add(rev2);
        instance.setListRevisoes(lr);
        List<Revisao> expResult = lr;
        List<Revisao> result = instance.getListaTodasRevisoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getMediaDeRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testGetMediaDeRevisoes() {
        System.out.println("getMediaDeRevisoes");
        Revisao rev1 = new Revisao();
        Revisao rev2 = new Revisao();
        rev1.setClassificacaoRecomendacao(2);
        rev2.setClassificacaoRecomendacao(1);
        List<Revisao> lr = new ArrayList();
        lr.add(rev1);
        lr.add(rev2);
        ListaRevisoes instance = new ListaRevisoes(lr);
        double expResult = 1.5;
        double result = instance.getMediaDeRevisoes();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getStatsTopicos method, of class ListaRevisoes.
     */
    @Test
    public void testGetStatsTopicos() {
        System.out.println("getStatsTopicos");
        ArrayList ls = null;
        ListaRevisoes instance = new ListaRevisoes();
        ArrayList expResult = null;
        ArrayList result = instance.getStatsTopicos(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temTodasRevisoesFeitas method, of class ListaRevisoes.
     */
    @Test
    public void testTemTodasRevisoesFeitas() {
        System.out.println("temTodasRevisoesFeitas");
        Revisao rev1=new Revisao();
        rev1.setTextoExplicativo("Ola");
        List<Revisao> list=new ArrayList();
        list.add(rev1);
        ListaRevisoes instance = new ListaRevisoes(list);
        boolean expResult = true;
        boolean result = instance.temTodasRevisoesFeitas();
        assertEquals(expResult, result);

    }

    /**
     * Test of showData method, of class ListaRevisoes.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListaRevisoes instance = new ListaRevisoes();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of addListaRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testAddListaRevisoes() {
        System.out.println("addListaRevisoes");
        Revisao rev= new Revisao();
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ListaRevisoes instance = new ListaRevisoes(lr);
        instance.addListaRevisoes(lr);
    }
    /**
     * Test of temRevisoes method, of class ListaRevisoes.
     */
    @Test
    public void testTemRevisoes() {
        System.out.println("temRevisoes");
        Revisao rev= new Revisao();
        List<Revisao> list= new ArrayList();
        list.add(rev);
        ListaRevisoes instance = new ListaRevisoes(list);
        boolean expResult = true;
        boolean result = instance.temRevisoes();
        assertEquals(expResult, result);
    }
    
}
