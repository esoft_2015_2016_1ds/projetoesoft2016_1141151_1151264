/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;
import model.Artigo;
import model.Autor;
import model.AutorCorrespondente;
import model.Evento;
import model.Licitacao;
import model.Revisor;
import model.Submissao;
import model.TipoConflito;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoEmCameraReadyState;
import states.submissao.SubmissaoRegistadoState;
import states.submissao.SubmissaoRemovidaState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaSubmissoesTest {
    
    public ListaSubmissoesTest() {
    }
    ListaSubmissoes list;
    Submissao s1,s2;
    Utilizador u1;
    ListaAutores la;
    Artigo a,a2;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        list = new ListaSubmissoes();
        ListaAutores la = new ListaAutores();
        la.addAutor(new Autor(u1,"UnknownPlace"));
        s1= new Submissao();
        a = new Artigo(la,"Android","Android and the future","Android.pdf",new AutorCorrespondente(new Autor(u1,"UnknownPlace"),u1));
        a2 = new Artigo(la,"iOS","iOS and the future","IOS.pdf",new AutorCorrespondente(new Autor(u1,"UnknownPlace"),u1));
        List<String> pc = new ArrayList();
        pc.add("Google");
        pc.add("Galaxy");
        a.setPalavrasChave(pc);
        a.setAutorCriador(u1);
        a.setDataCriado(Data.getDataAtual());
        s1.setArtigoInicial(a);
        s2 = new Submissao();
        a2.setPalavrasChave(pc);
        a2.setAutorCriador(u1);
        a.setDataCriado(Data.getDataAtual());
        la = new ListaAutores();
        la.addAutor(new Autor(u1,"UnknownPlace"));
        s2.setArtigoInicial(a2);
        list.addSubmissao(s1);
        list.addSubmissao(s2);
        s2.setState(new SubmissaoRemovidaState(s2));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSubmissoesUtilizador method, of class ListaSubmissoes.
     */
    @Test
    public void testGetSubmissoesUtilizador() {
        System.out.println("getSubmissoesUtilizador");
        String id = u1.getEmail();
        ListaSubmissoes instance = new ListaSubmissoes();
        instance.addSubmissao(s1);
        instance.addSubmissao(s2);
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        expResult.add(s2);
        List<Submissao> result = instance.getSubmissoesUtilizador(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesRemovidas method, of class ListaSubmissoes.
     */
    @Test
    public void testGetSubmissoesRemovidas() {
        System.out.println("getSubmissoesRemovidas");
        ListaSubmissoes instance = list;
        List<Submissao> expResult = new ArrayList();
        expResult.add(s2);
        List<Submissao> result = instance.getSubmissoesRemovidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class ListaSubmissoes.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListaSubmissoes instance = new ListaSubmissoes();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of valida method, of class ListaSubmissoes.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Submissao sClone = s1;
        int i = 0;
        ListaSubmissoes instance = new ListaSubmissoes();
        instance.addSubmissao(s1);
        instance.addSubmissao(s2);
        boolean expResult = true;
        boolean result = instance.valida(sClone, i);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesNaoCameraReadyDe method, of class ListaSubmissoes.
     */
    @Test
    public void testGetSubmissoesNaoCameraReadyDe() {
        System.out.println("getSubmissoesNaoCameraReadyDe");
        String id = u1.getEmail();
        ListaSubmissoes instance = list;
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        expResult.add(s2);
        List<Submissao> result = instance.getSubmissoesNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesEmLicitacao method, of class ListaSubmissoes.
     */
    @Test
    public void testGetSubmissoesEmLicitacao() {
        System.out.println("getSubmissoesEmLicitacao");
        ListaSubmissoes instance = list;
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getSubmissoesEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addSubmissao method, of class ListaSubmissoes.
     */
    @Test
    public void testAddSubmissao() {
        System.out.println("addSubmissao");
        Evento ev5=new Evento();
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        ev5.addSubmissao(sub, a, u4);
        List<Submissao> lista=new ArrayList();
        boolean expResult = true;
        boolean result = new ListaSubmissoes(lista).addSubmissao(sub);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of novaSubmissao method, of class ListaSubmissoes.
     */
    @Test
    public void testNovaSubmissao() {
        System.out.println("novaSubmissao");
        ListaSubmissoes instance = list;
        boolean expResult = true;
        boolean result = instance.novaSubmissao() != null;
        assertEquals(expResult, result);
        
    }

    /**
     * Test of validaSubmissao method, of class ListaSubmissoes.
     */
    @Test
    public void testValidaSubmissao() {
        System.out.println("validaSubmissao");
        Submissao s = s1;
        ListaSubmissoes instance = list;
        boolean expResult = false;
        boolean result = instance.validaSubmissao(s);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temSubmissoesAutor method, of class ListaSubmissoes.
     */
    @Test
    public void testTemSubmissoesAutor() {
        System.out.println("temSubmissoesAutor");
        String id = u1.getEmail();
        ListaSubmissoes instance = list;
        boolean expResult = true;
        boolean result = instance.temSubmissoesAutor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getNumeroSubmissoesAceites method, of class ListaSubmissoes.
     */
    @Test
    public void testGetNumeroSubmissoesAceites() {
        System.out.println("getNumeroSubmissoesAceites");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Autor aut=new Autor(u4,"Kanto Region");
        List<Autor> list=new ArrayList();
        list.add(aut);
        la=new ListaAutores(list);
        /*Artigo a1=new Artigo();   
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        a.setAutores(list);
        a.setAutorCorrespondente(aut,u4);
        Submissao sub1=new Submissao(a,a,null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2=new Submissao(a,a,null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3=new Submissao(a,a,null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4=new Submissao(a,a,null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao>ls=new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4); 
        int expResult = 2;
        int result = new ListaSubmissoes(ls).getNumeroSubmissoesAceites();
        assertEquals(expResult, result);

    }

    /**
     * Test of getNumeroTotalSubmissoes method, of class ListaSubmissoes.
     */
    @Test
    public void testGetNumeroTotalSubmissoes() {
        System.out.println("getNumeroTotalSubmissoes");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Autor aut=new Autor(u4,"Kanto Region");
        List<Autor> list=new ArrayList();
        list.add(aut);
        la=new ListaAutores(list);
        /*Artigo a1=new Artigo();   
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        a.setAutores(list);
        a.setAutorCorrespondente(aut,u4);
        Submissao sub1=new Submissao(a,a,null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2=new Submissao(a,a,null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3=new Submissao(a,a,null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4=new Submissao(a,a,null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao>ls=new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        ListaSubmissoes instance = new ListaSubmissoes(ls);
        int expResult = 4;
        int result = instance.getNumeroTotalSubmissoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of getSubmissoes method, of class ListaSubmissoes.
     */
    @Test
    public void testGetSubmissoes() {
        System.out.println("getSubmissoes");
        ListaSubmissoes instance = list;
        List<Submissao> expResult = new ArrayList();
        expResult.add(s1);
        expResult.add(s2);
        List<Submissao> result = instance.getSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getListaSubmissoesRegistadasUtilizador method, of class ListaSubmissoes.
     */
    @Test
    public void testGetListaSubmissoesRegistadasUtilizador() {
        System.out.println("getListaSubmissoesRegistadasUtilizador");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id= u4.getEmail();
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoRegistadoState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        ListaSubmissoes instance = new ListaSubmissoes();
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getListaSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPosition method, of class ListaSubmissoes.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoRegistadoState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub); 
        int expResult = 0;
        int result = new ListaSubmissoes(ls).getPosition(sub);
        assertEquals(expResult, result);

    }

    /**
     * Test of getListaAutores method, of class ListaSubmissoes.
     */
    @Test
    public void testGetListaAutores() {
        System.out.println("getListaAutores");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        a.setAutores(la);
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoRegistadoState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub); 
        List<Autor> expResult = new ArrayList();
        expResult.add(aut);
        List<Autor> result =  new ListaSubmissoes(ls).getListaAutores();
        assertEquals(expResult, result);

    }

    /**
     * Test of temTodasSubmissoesCameraReady method, of class ListaSubmissoes.
     */
    @Test
    public void testTemTodasSubmissoesCameraReady() {
        System.out.println("temTodasSubmissoesCameraReady");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoEmCameraReadyState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        boolean expResult = true;
        boolean result = new ListaSubmissoes(ls).temTodasSubmissoesCameraReady();
        assertEquals(expResult, result);

    }

    /**
     * Test of temSubmissoesAceitesUtilizador method, of class ListaSubmissoes.
     */
    @Test
    public void testTemSubmissoesAceitesUtilizador() {
        System.out.println("temSubmissoesAceitesUtilizador");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u4.getEmail();
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoAceiteState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        boolean expResult = true;
        boolean result = new ListaSubmissoes(ls).temSubmissoesAceitesUtilizador(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of getLicitacoes method, of class ListaSubmissoes.
     */
    @Test
    public void testGetLicitacoes() {
        System.out.println("getLicitacoes");        
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor rev=new Revisor(u4);
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoAceiteState(sub));
        List<Submissao> ls=new ArrayList();
        TipoConflito conf= new TipoConflito("Andre","Andre");
        List<TipoConflito> lconf=new ArrayList();
        lconf.add(conf);
        Licitacao licitacao=sub.novaLicitacao(rev, lconf);
        sub.registaLicitacao(licitacao);
        ls.add(sub);
        List<Licitacao> expResult = new ArrayList();
        expResult.add(licitacao);
        List<Licitacao> result = new ListaSubmissoes(ls).getLicitacoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAcceptedTopics method, of class ListaSubmissoes.
     */
    @Test
    public void testGetAcceptedTopics() {
        System.out.println("getAcceptedTopics");
        ArrayList<String> ls = null;
        ListaSubmissoes instance = new ListaSubmissoes();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getAcceptedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getRejectedTopics method, of class ListaSubmissoes.
     */
    @Test
    public void testGetRejectedTopics() {
        System.out.println("getRejectedTopics");
        ArrayList<String> ls = null;
        ListaSubmissoes instance = new ListaSubmissoes();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getRejectedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getListaSubmissoesAceites method, of class ListaSubmissoes.
     */
    @Test
    public void testGetListaSubmissoesAceites() {
        System.out.println("getListaSubmissoesAceites");
        Utilizador u4 = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id= u4.getEmail();
        Revisor rev=new Revisor(u4);
        /*Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");*/
        Autor aut=new Autor(u4,"Kanto Region");
        a.addAutor(aut);
        List<Autor>la=new ArrayList();
        la.add(aut); 
        a.setAutorCorrespondente(new Autor(u4,"Kanto Region"),u4);
        Submissao sub=new Submissao();
        sub.setArtigoInicial(a);
        sub.setState(new SubmissaoAceiteState(sub));
        List<Submissao> ls=new ArrayList();
        ls.add(sub);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = new ListaSubmissoes(ls).getListaSubmissoesAceites(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of addAllSubmissoes method, of class ListaSubmissoes.
     */
    @Test
    public void testAddAllSubmissoes() {
        System.out.println("addAllSubmissoes");
        List<Submissao> ls = new ArrayList();
        ls.add(s1);
        ListaSubmissoes instance = new ListaSubmissoes();
        instance.addAllSubmissoes(ls);
        assertEquals(ls,instance.getSubmissoes());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of temSubmissoes method, of class ListaSubmissoes.
     */
    @Test
    public void testTemSubmissoes() {
        System.out.println("temSubmissoes");
        List<Submissao> ls = new ArrayList();
        ls.add(new Submissao());
        ListaSubmissoes instance = new ListaSubmissoes(ls);
        boolean expResult = true;
        boolean result = instance.temSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
