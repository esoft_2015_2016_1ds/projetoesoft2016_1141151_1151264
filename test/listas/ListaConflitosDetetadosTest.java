/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;
import model.ConflitoDetetado;
import model.Revisor;
import model.Submissao;
import model.TipoConflito;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaConflitosDetetadosTest {
    
    public ListaConflitosDetetadosTest() {
    }
    Revisor r;
    Utilizador u;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u=new Utilizador("administrador","admin","admin1","admin@gmail.com",new NaoFazNada());
        r=new Revisor(u);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addConflitoDetetado method, of class ListaConflitosDetetados.
     */
    @Test
    public void testAddConflitoDetetado() {
        System.out.println("addConflitoDetetado(null object addition)");
        ConflitoDetetado c = null;
        ListaConflitosDetetados instance = new ListaConflitosDetetados();
        boolean expResult = false;
        boolean result = instance.addConflitoDetetado(c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class ListaConflitosDetetados.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListaConflitosDetetados instance = new ListaConflitosDetetados();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getTiposConflito method, of class ListaConflitosDetetados.
     */
    @Test
    public void testGetTiposConflito() {
        System.out.println("getTiposConflito");
        String id = u.getEmail();
        ListaConflitosDetetados instance = new ListaConflitosDetetados();
        ConflitoDetetado cd = new ConflitoDetetado(r,new Submissao());
        instance.addConflitoDetetado(cd);
        List<TipoConflito> expResult = new ArrayList();
        expResult.addAll(cd.getTiposConflito());
        List<TipoConflito> result = instance.getTiposConflito(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
