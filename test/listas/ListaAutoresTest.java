/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;
import model.Autor;
import model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ListaAutoresTest {
    
    public ListaAutoresTest() {
    }
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaAutores method, of class ListaAutores.
     */
    @Test
    public void testGetListaAutores() {
        System.out.println("getListaAutores");
        ListaAutores instance = new ListaAutores();
        instance.addAutor(new Autor("Abc","Easy@gmail.com","As123","Ohnonono"));
        instance.addAutor(new Autor("Asus","asus@tech.support.com","South Korea","Android"));
        ArrayList<Autor> expResult = new ArrayList();
        expResult.add(new Autor("Abc","Easy@gmail.com","As123","Ohnonono"));
        expResult.add(new Autor("Asus","asus@tech.support.com","South Korea","Android"));
        ArrayList<Autor> result = instance.getListaAutores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of addAutor method, of class ListaAutores.
     * In this test, an author that is null is attempted to be added.
     */
    @Test
    public void testAddAutorNull() {
        System.out.println("addAutorNull (null author)");
        Autor autor = null;
        ListaAutores instance = new ListaAutores();
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of addAutor method, of class ListaAutores.
     * In this test, an author that has no fields is attempted to be added.
     */
    @Test
    public void testAddAutorEmpty() {
        System.out.println("addAutorEmpty (empty author)");
        Autor autor = new Autor();
        ListaAutores instance = new ListaAutores();
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of addAutor method, of class ListaAutores.
     * In this test, an author that valid fields is attempted to be added.
     */
    @Test
    public void testAddAutorValid() {
        System.out.println("addAutorValid (valid author)");
        Autor autor = new Autor("Potato","potato@gmail.com","McDonalds","HotFries");
        ListaAutores instance = new ListaAutores();
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of removeAutor method, of class ListaAutores.
     * In this test, an author that does not belong to the list is provided.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveAutor() {
        System.out.println("removeAutor (author doesn't belong to list)");
        Autor author = new Autor();
        ListaAutores instance = new ListaAutores();
        instance.removeAutor(author);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of validaAutor method, of class ListaAutores.
     * In this test, a null author is used.
     */
    @Test
    public void testValidaAutor() {
        System.out.println("validaAutor (null author)");
        Autor author = null;
        ListaAutores instance = new ListaAutores();
        boolean expResult = false;
        boolean result = instance.validaAutor(author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isEmpty method, of class ListaAutores.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        ListaAutores instance = new ListaAutores();
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class ListaAutores.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ListaAutores instance = new ListaAutores();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class ListaAutores.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ListaAutores instance = new ListaAutores();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class ListaAutores.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        ListaAutores instance = new ListaAutores();
        instance.addAutor(new Autor("Abc","Easy@gmail.com","As123","Ohnonono"));
        instance.addAutor(new Autor("Asus","asus@tech.support.com","South Korea","Android"));
        instance.addAutor(new Autor("adminFunny","adminB@gmail.com","1e44a5sy","adminBs"));
        List<Autor> expResult = new ArrayList();
        expResult.add(new Autor("adminFunny","adminB@gmail.com","1e44a5sy","adminBs"));
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes(regU);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temAutor method, of class ListaAutores.
     */
    @Test
    public void testTemAutor() {
        System.out.println("temAutor");
        String id = "Bananas";
        ListaAutores instance = new ListaAutores();
        boolean expResult = false;
        boolean result = instance.temAutor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addAutor method, of class ListaAutores.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor(u1,"ISEP");
        ListaAutores instance = new ListaAutores();
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAutor method, of class ListaAutores.
     */
    @Test
    public void testGet() {
        System.out.println("getAutor");
        Autor autor = new Autor(u1,"ISEP");
        String id = autor.getEmail();
        ListaAutores instance = new ListaAutores();
        instance.addAutor(autor);
        Autor expResult = autor;
        Autor result = instance.getAutor(id);
        assertEquals(expResult, result);
    }
}
