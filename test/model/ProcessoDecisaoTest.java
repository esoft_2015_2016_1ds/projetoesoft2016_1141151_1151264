/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDecisao;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class ProcessoDecisaoTest {
    
    public ProcessoDecisaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setMecanismoDecisao method, of class ProcessoDecisao.
     */
    @Test
    public void testSetMecanismoDecisao() {
        System.out.println("setMecanismoDecisao");
        MecanismoDecisao md = null;
        ProcessoDecisao instance = new ProcessoDecisao();
        instance.setMecanismoDecisao(md);
        MecanismoDecisao expResult = md;
        MecanismoDecisao result = instance.getMecanismoDecisao();
        assertEquals(result, expResult);
    }

    /**
     * Test of getInfo method, of class ProcessoDecisao.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        ProcessoDecisao instance = new ProcessoDecisao();
        String expResult = "";
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }


    /**
     * Test of showData method, of class ProcessoDecisao.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ProcessoDecisao instance = new ProcessoDecisao();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);

    }

    /**
     * Test of addDecisaoUnchecked method, of class ProcessoDecisao.
     */
    @Test
    public void testAddDecisaoUnchecked() {
        System.out.println("addDecisaoUnchecked");
        Decisao d = new Decisao();
        ProcessoDecisao instance = new ProcessoDecisao();
        boolean expResult = true;
        boolean result = instance.addDecisaoUnchecked(d);
        assertEquals(expResult, result);
    }

    /**
     * Test of decide method, of class ProcessoDecisao.
     */
    @Test
    public void testDecide() {
        System.out.println("decide");
        List<Revisao> lr = new ArrayList();
        ProcessoDecisao instance = new ProcessoDecisao();
        List<Decisao> expResult = null;
        List<Decisao> result = instance.decide(lr);
        assertEquals(expResult, result);
    }

    /**
     * Test of temSubmissoesAceites method, of class ProcessoDecisao.
     */
    @Test
    public void testTemSubmissoesAceites() {
        System.out.println("temSubmissoesAceites");
        ProcessoDecisao instance = new ProcessoDecisao();
        Decisao d1 = new Decisao();
        d1.setVeredito(true);
        Decisao d2 = new Decisao();
        d2.setVeredito(false);
        Decisao d3 = new Decisao();
        d3.setVeredito(false);
        instance.addDecisaoUnchecked(d1);
        instance.addDecisaoUnchecked(d2);
        instance.addDecisaoUnchecked(d3);
        boolean expResult = true;
        boolean result = instance.temSubmissoesAceites();
        assertEquals(expResult, result);
    }
    
}
