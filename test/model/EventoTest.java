/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.EventoState;
import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaRevisoes;
import listas.ListaSubmissoes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import states.evento.EventoEmRevisaoState;
import states.evento.EventoEmSubmissaoCameraReadyState;
import states.evento.EventoRegistadoState;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoRegistadoState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class EventoTest {
    
    public EventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDescription method, of class Evento.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Evento instance = new Evento();
        instance.setDescricao("Analise de Sabonetes");
        String expResult = "Analise de Sabonetes";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitle method, of class Evento.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Evento instance = new Evento();
        instance.setTitulo("Digimon");
        String expResult = "Digimon";
        String result = instance.getTitle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of setTitulo method, of class Evento.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strtitulo = "Evento 1";
        Evento instance = new Evento();
        instance.setTitulo(strtitulo);
        String res = instance.getTitle();
        assertEquals(strtitulo, res);

    }

    /**
     * Test of setDescricao method, of class Evento.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strdescricao = "descricao";
        Evento instance = new Evento();
        instance.setDescricao(strdescricao);
        String res = instance.getDescription();
        assertEquals(res, strdescricao);
        
    }

    /**
     * Test of setDataInicio method, of class Evento.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        Data strDataInicio = new Data(27,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(strDataInicio);
        
    }
    
    /**
     * Test of setDataInicio method, of class Evento.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testSetDataInicioAntesDaAtual() {
        System.out.println("setDataInicioAntesDaAtual");
        Data strDataInicio = new Data(27,5,2015);
        Evento instance = new Evento();
        instance.setdataInicio(strDataInicio);
        
    }

    /**
     * Test of setDataFim method, of class Evento.
     */
    @Test
    public void  testSetDataFim() {
        System.out.println("setDataFim");
        Data strDataInicio = new Data(26,7,2015);
        Data strDataFim = new Data(27,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(strDataInicio);
        instance.setdataFim(strDataFim);
    }
    

    /**
     * Test of setDataInicioSubmissao method, of class Evento.
     */
    @Test
    public void  testSetDataInicioSubmissao() {
        System.out.println("setDataInicioSubmissao");
        Data inicio = new Data(30,8,2015);
        Data data = new Data(27,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(data);
        
    }
    

    /**
     * Test of setDataFimSubmissao method, of class Evento.
     */
    @Test
    public void  testSetDataFimSubmissao() {
        System.out.println("setDataFimSubmissao");
        Data inicio = new Data(30,8,2015);
        Data inicoSubmissao = new Data(28,8,2015);
        Data data = new Data(29,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicoSubmissao);
        instance.setdataFimSubmissao(data);
        
    }

    /**
     * Test of setDataInicioDistribuicao method, of class Evento.
     */
    @Test
    public void  testSetDataInicioDistribuicao() {
        System.out.println("setDataInicioDistribuicao");

        
    }

    /**
     * Test of setLocal method, of class Evento.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        Local strLocal = new Local("5","Porto");
        Evento instance = new Evento();
        instance.setLocal(strLocal);
        assertEquals(strLocal,instance.getLocal());
    }

    /**
     * Test of novaCP method, of class Evento.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        Evento instance = new Evento();
        CP result = instance.novaCP();
        String temp[] = instance.showData().split("\n");
        String compareData=temp[0];
         assertEquals(compareData,"CP não nula");
        
   
        
    }

    /**
     * Test of getListaOrganizadores method, of class Evento.
     */
    @Test
    public void testGetListaOrganizadores() {
        System.out.println("getListaOrganizadores");
        Evento instance = new Evento();
        List<Organizador> expResult = new ArrayList<Organizador>();
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of addOrganizador method com parametro organizador, of class Evento.
     */
    @Test
    public void  testAddOrganizador() {
        System.out.println("addOrganizador");
        Utilizador u = new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addOrganizador(u);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addOrganizador method com with an already existing email
     */
    @Test
    public void  testAddOrganizadorExistingEmail() {
        System.out.println("addOrganizadorExistingEmail");
        Utilizador u = new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        Evento instance = new Evento();
        instance.addOrganizador(u);
        Utilizador a = new Utilizador("Joao" , "jlol", "pass123", "greis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.addOrganizador(a);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addOrganizador method com with an already existing username
     */
    @Test
    public void  testAddOrganizadorExistingUsername() {
        System.out.println("addOrganizadorExistingEmail");
        Utilizador u = new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        Evento instance = new Evento();
        instance.addOrganizador(u);
        Utilizador a = new Utilizador("Joao" , "greis", "pass123", "gareis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.addOrganizador(a);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validaOrganizador() of the evento classe.
     */
    @Test
    public void  testValidaOrganizador(){
        System.out.println("validaOrganizador");
        Organizador o = new Organizador(new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada()));
        Evento instance = new Evento();
        instance.addOrganizador(new Utilizador("Rui Freitas", "rfreitas", "BABAAB1", "greis@email.com", new NaoFazNada()));
        boolean expResult = false;
        boolean result = instance.validaOrganizador(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Evento.
     */
    @Test
    public void  testValida() {
        System.out.println("valida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCP method, of class Evento.
     */
    @Test
    public void testSetCP() {
        System.out.println("setCP");
        CP cp = new CP();
        Evento instance = new Evento();
        instance.setCP(cp);        
    }

    /**
     * Test of getSubmissoesUtilizador method, of class Evento.
     */
    @Test
    public void testGetSubmissoesUtilizador() {
        System.out.println("getSubmissoesUtilizador");
        String id = "gpmgpmgpmg@gmail.com";
        Utilizador U = new Utilizador("Gonçalo","greis","pass","gpmgpmgpmg@gmail.com", new NaoFazNada());
        ListaAutores la = new ListaAutores();
        la.addAutor(new Autor("Gonçalo","gpmgpmgpmg@gmail.com", "ISEP","greis"));
        Artigo a = new Artigo(la,"resumo","titutlo","artigo.pdf", new AutorCorrespondente(new Autor("Gonçalo","gpmgpmgpmg@gmail.com", "ISEP","greis"),U));
        a.addAutor(new Autor("Gonçalo","gpmgpmgpmg@gmail.com", "ISEP","greis"));
        Evento instance = new Evento();
        
        Submissao s = new Submissao();
        s.setArtigoInicial(a);
        List<Submissao> expResult = new ArrayList();
        expResult.add(s);
        instance.addSubmissao(s, a, U);
        List<Submissao> result = instance.getSubmissoesUtilizador(id);
        assertEquals(expResult, result);  
    }

    /**
     * Test of toString method, of class Evento.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of novaSubmissao method, of class Evento.
     */
    @Test
    public void testNovaSubmissao() {
        System.out.println("novaSubmissao");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.novaSubmissao()!=null;
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisoes method, of class Evento.
     */
    @Test
    public void testGetRevisoes() {
        System.out.println("getRevisoes");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Evento instance = new Evento();
        Revisao revisao=new Revisao();
        Revisor revisor=new Revisor();
        revisao.setRevisor(revisor);
        List<Revisao> listaRevisao=new ArrayList();
        listaRevisao=new ArrayList();
        listaRevisao.add(revisao);
        List<Revisao> expResult = new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        List<Autor> la=new ArrayList();
        la.add(aut);
        AutorCorrespondente au = new AutorCorrespondente(aut,u1);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao.setSubmissao(sub);
        expResult.add(revisao);
        ListaRevisoes lista=new ListaRevisoes(listaRevisao);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of saveRevisao method, of class Evento.
     */
    @Test
    public void testSaveRevisao() {
        System.out.println("saveRevisao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        Artigo a =new Artigo();
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        Revisao rev = new Revisao();
        rev.setSubmissao(sub);
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.saveRevisao(rev);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of novoProcessoDistribuicao method, of class Evento.
     */
    @Test
    public void testNovoProcessoDistribuicao() {
        System.out.println("novoProcessoDistribuicao");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.novoProcessoDistribuicao() != null;
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setProcessoDistribuicao method, of class Evento.
     */
    @Test
    public void testSetProcessoDistribuicao() {
        System.out.println("setProcessoDistribuicao");
        ProcessoDistribuicao pd = new ProcessoDistribuicao();
        Evento instance = new Evento();
        instance.setProcessoDistribuicao(pd);  
    }


    /**
     * Test of setCriado method, of class Evento.
     */
    @Test
    public void  testSetCriado() {
        System.out.println("setCriado");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setRegistado method, of class Evento.
     */
    @Test
    public void  testSetRegistado() {
        System.out.println("setRegistado");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCPDefinida method, of class Evento.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAceitaSubmissoes method, of class Evento.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDetecao method, of class Evento.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmLicitacao method, of class Evento.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDistribuicao method, of class Evento.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmRevisao method, of class Evento.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setEmDecisao method, of class Evento.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissoesDecididas method, of class Evento.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setSubmissoesDecididas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setState method, of class Evento.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        EventoState state = null;
        Evento instance = new Evento();
        instance.setState(state);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getListaRevisoresCP method, of class Evento.
     */
    @Test
    public void testGetListaRevisoresCP() {
        System.out.println("getListaRevisoresCP");
        Evento instance = new Evento();
        List<Revisor> expResult = null;
        List<Revisor> result = instance.getListaRevisoresCP();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novoProcessoDecisao method, of class Evento.
     */
    @Test
    public void testNovoProcessoDecisao() {
        System.out.println("novoProcessoDecisao");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.novoProcessoDecisao()!=null;
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setProcessoDecisao method, of class Evento.
     */
    @Test
    public void testSetProcessoDecisao() {
        System.out.println("setProcessoDecisao");
        ProcessoDecisao pd = null;
        Evento instance = new Evento();
        instance.setProcessoDecisao(pd);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissiveisDe method, of class Evento.
     */
    @Test
    public void testGetSubmissiveisDe() {
        System.out.println("getSubmissiveisDe");
        String id = "";
        Evento instance = new Evento();
        List<Submissivel> expResult = null;
        //GABRIEL COMENTEI DUAS LINHAS ABAIXO
        //List<Submissivel> result = instance.getSubmissiveisDe(id);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temRevisor method, of class Evento.
     */
    @Test
    public void testTemRevisor() {
        System.out.println("temRevisor");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temOrganizador method, of class Evento.
     */
    @Test
    public void testTemOrganizador() {
        System.out.println("temOrganizador");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temOrganizadores();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getSessoesTematicasProponenteEmEstadoRegistado method, of class Evento.
     */
    @Test
    public void testGetSessoesTematicasProponenteEmEstadoRegistado() {
        System.out.println("getSessoesTematicasProponenteEmEstadoRegistado");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getSessoesTematicasProponenteEmEstadoRegistado(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getState method, of class Evento.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Evento instance = new Evento();
        EventoRegistadoState cs = new EventoRegistadoState(instance);
        instance.setState(cs);
        String expResult = cs.toString();
        String result = instance.getState();
        assertEquals(expResult, result);
    }

    /**
     * Test of temCP method, of class Evento.
     */
    @Test
    public void testTemCP() {
        System.out.println("temCP");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temCP();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSessoesTematicasProponenteEmEstadoRegistado method, of class Evento.
     */
    @Test
    public void testGetSessoesTematicasProponenteEmEstadoRegistado_String() {
        System.out.println("getSessoesTematicasProponenteEmEstadoRegistado");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getSessoesTematicasProponenteEmEstadoRegistado(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesSessoesUtilizador method, of class Evento.
     */
    @Test
    public void testGetSubmissoesSessoesUtilizador() {
        System.out.println("getSubmissoesSessoesUtilizador");
        String id = "";
        Evento instance = new Evento();
        List<Submissao> expResult = null;
        List<Submissao> result = instance.getSubmissoesSessoesUtilizador(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temSessaoTematica method, of class Evento.
     */
    @Test
    public void testTemSessaoTematica() {
        System.out.println("temSessaoTematica");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSessaoTematica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of registaSessaoTematica method, of class Evento.
     */
    @Test
    public void testRegistaSessaoTematica() {
        System.out.println("registaSessaoTematica");
        SessaoTematica st = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.registaSessaoTematica(st);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addSessaoTematica method, of class Evento.
     */
    @Test
    public void testAddSessaoTematica() {
        System.out.println("addSessaoTematica");
        SessaoTematica st = new SessaoTematica();
        st.setCodigo("CD-001");
        st.setDescricao("My description");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addSessaoTematica(st);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of temSessaoComProponente method, of class Evento.
     */
    @Test
    public void testTemSessaoComProponente() {
        System.out.println("temSessaoComProponente");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSessaoComProponente(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temOrganizador method, of class Evento.
     */
    @Test
    public void testTemOrganizador_0args() {
        System.out.println("temOrganizador");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temOrganizadores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesRemovidas method, of class Evento.
     */
    @Test
    public void testGetSubmissoesRemovidas() {
        System.out.println("getSubmissoesRemovidas");
        Evento instance = new Evento();
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getSubmissoesRemovidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temOrganizador method, of class Evento.
     */
    @Test
    public void testTemOrganizador_String() {
        System.out.println("temOrganizador");
        Utilizador u1 = new Utilizador("admin", "adminA", "123easy", "admin@gmail.com", new NaoFazNada());
        String id = u1.getEmail();
        List<Organizador> lo=new ArrayList();
        List<SessaoTematica> lst = new ArrayList();
        lo.add(new Organizador(u1));
        Evento instance = new Evento(lo,lst);
        boolean expResult = true;
        boolean result = instance.temOrganizador(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getDataInicioSubmissao method, of class Evento.
     */
    @Test
    public void  testGetDataInicioSubmissao() 
    {
        System.out.println("getDataInicioSubmissao");
        Evento instance = new Evento();
        Data inicio = new Data(30,8,2015);
        Data inicoSubmissao = new Data(28,8,2015);
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicoSubmissao);
        Data expResult = inicoSubmissao;
        Data result = instance.getDataInicioSubmissao();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getDataFimSubmissao method, of class Evento.
     */
    @Test
    public void testGetDataFimSubmissao() {
        System.out.println("getDataFimSubmissao");
        Data inicio = new Data(30,8,2015);
        Data fimSub = new Data(26,8,2015);
        Data data = new Data(27,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(data);
        assertEquals(data, instance.getDataFimSubmissao());
        
    }

    /**
     * Test of setdataInicio method, of class Evento.
     */
    @Test
    public void  testSetdataInicio() {
        System.out.println("setdataInicio");
        Data strdataInicio = new Data(27,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(strdataInicio);
        
    }

    /**
     * Test of setdataFim method, of class Evento.
     */
    @Test
    public void  testSetdataFim() {
        System.out.println("setdataFim");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
        
    }

    /**
     * Test of setdataInicioSubmissao method, of class Evento.
     */
    @Test
    public void  testSetdataInicioSubmissao() {
        System.out.println("setdataInicioSubmissao");
        Data data = new Data(27,8,2015);
        Data incio = new Data(29,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(incio);
        instance.setdataInicioSubmissao(data);
    }

    /**
     * Test of setdataFimSubmissao method, of class Evento.
     */
    @Test
    public void  testSetdataFimSubmissao() {
        System.out.println("setdataFimSubmissao");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
        
    }

    /**
     * Test of setdataInicioDistribuicao method, of class Evento.
     */
    @Test
    public void  testSetdataInicioDistribuicao() {
        System.out.println("setdataInicioDistribuicao");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
    }

    /**
     * Test of getSessoesTematicasComProponente method, of class Evento.
     */
    @Test
    public void testGetSessoesTematicasComProponente() {
        System.out.println("getSessoesTematicasComProponente");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getSessoesTematicasComProponenteRegistadoState(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class Evento.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class Evento.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of isInSessaoTematicaDefinidaState method, of class Evento.
     */
    @Test
    public void testIsInSessaoTematicaDefinidaState() {
        System.out.println("isInSessaoTematicaDefinidaState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInSessaoTematicaDefinidaState();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isInCPDefinidaState method, of class Evento.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class Evento.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDetecaoState method, of class Evento.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class Evento.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDistribuicaoState method, of class Evento.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmDecisaoState method, of class Evento.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class Evento.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        Evento instance = new Evento();
        instance.setState(new EventoEmSubmissaoCameraReadyState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isInCameraReadyState method, of class Evento.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class Evento.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes method, of class Evento.
     */
    @Test
    public void testGetListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes() {
        System.out.println("getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes");
        Evento instance = new Evento();
        List<Submissivel> expResult = null;
        List<Submissivel> result = instance.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaLicitaveisEmLicitacaoDe method, of class Evento.
     */
    @Test
    public void testGetListaLicitaveisEmLicitacaoDe() {
        System.out.println("getListaLicitaveisEmLicitacaoDe");
        String id = "";
        Evento instance = new Evento();
        List<Licitavel> expResult = null;
        List<Licitavel> result = instance.getListaLicitaveisEmLicitacaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaSubmissiveisNaoCameraReadyDe method, of class Evento.
     */
    @Test
    public void testGetListaSubmissiveisNaoCameraReadyDe() {
        System.out.println("getListaSubmissiveisNaoCameraReadyDe");
        String id = "";
        Evento instance = new Evento();
        List<Submissivel> expResult = null;
        List<Submissivel> result = instance.getListaSubmissiveisNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temSubmissoesAutor method, of class Evento.
     */
    @Test
    public void testTemSubmissoesAutor() {
        System.out.println("temSubmissoesAutor");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSubmissoesAutor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temSubmissoesAutorExclusivo method, of class Evento.
     */
    @Test
    public void testTemSubmissoesAutorExclusivo() {
        System.out.println("temSubmissoesAutorExclusivo");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSubmissoesAutorExclusivo(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addSubmissao method, of class Evento.
     */
    @Test
    public void testAddSubmissao() {
        System.out.println("addSubmissao");
        Submissao submissao = null;
        Artigo a = null;
        Utilizador u = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.addSubmissao(submissao, a, u);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getRevisor method, of class Evento.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        String id = "";
        Evento instance = new Evento();
        Revisor expResult = null;
        Revisor result = instance.getRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesNaoCameraReadyDe method, of class Evento.
     */
    @Test
    public void testGetSubmissoesNaoCameraReadyDe() {
        System.out.println("getSubmissoesNaoCameraReadyDe");
        String id = "";
        Evento instance = new Evento();
        List<Submissao> expResult = null;
        List<Submissao> result = instance.getSubmissoesNaoCameraReadyDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Evento.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getSubmissoesEmLicitacao method, of class Evento.
     */
    @Test
    public void testGetSubmissoesEmLicitacao() {
        System.out.println("getSubmissoesEmLicitacao");
        Evento instance = new Evento();
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getSubmissoesEmLicitacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temOrganizadores method, of class Evento.
     */
    @Test
    public void testTemOrganizadores() {
        System.out.println("temOrganizadores");
         Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        Evento instance = new Evento();
        instance.addOrganizador(u1);
        boolean expResult = true;
        boolean result = instance.temOrganizadores();
        assertEquals(expResult, result);

    }

    /**
     * Test of calcularTaxaAceitacao method, of class Evento.
     */
    @Test
    public void testCalcularTaxaAceitacao() {
        System.out.println("calcularTaxaAceitacao");
        Evento instance=new Evento();
        Utilizador u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Submissao sub1=new Submissao(a1,a1,null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2=new Submissao(a1,a1,null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3=new Submissao(a1,a1,null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4=new Submissao(a1,a1,null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls= new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        instance.addSubmissao(sub1, a1,new Utilizador());
        instance.addSubmissao(sub2, a1,new Utilizador());
        instance.addSubmissao(sub3, a1,new Utilizador());
        instance.addSubmissao(sub4, a1,new Utilizador());
        double expResult = 2.0;
        double result = instance.calcularTaxaAceitacao();
        assertEquals(expResult, result, 2.0);
    }

    /**
     * Test of calcularValoresMedios method, of class Evento.
     */
    @Test
    public void testCalcularValoresMedios() {
        System.out.println("calcularValoresMedios");
        Evento instance=new Evento();
        Utilizador u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Submissao sub1=new Submissao(a1,a1,null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2=new Submissao(a1,a1,null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3=new Submissao(a1,a1,null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4=new Submissao(a1,a1,null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls= new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        instance.addSubmissao(sub1, a1,new Utilizador());
        instance.addSubmissao(sub2, a1,new Utilizador());
        instance.addSubmissao(sub3, a1,new Utilizador());
        instance.addSubmissao(sub4, a1,new Utilizador());
        
        Revisao rev1=new Revisao();
        Revisao rev2=new Revisao();
        Revisao rev3=new Revisao();
        Revisao rev4=new Revisao();
        rev1.setClassificacaoAdequacao(2);
        rev2.setClassificacaoAdequacao(2);
        rev3.setClassificacaoAdequacao(2);
        rev4.setClassificacaoAdequacao(2);
        rev1.setClassificacaoApresentacao(2);
        rev2.setClassificacaoApresentacao(2);
        rev3.setClassificacaoApresentacao(2);
        rev4.setClassificacaoApresentacao(2);
        rev1.setClassificacaoConfianca(2);
        rev2.setClassificacaoConfianca(2);
        rev3.setClassificacaoConfianca(2);
        rev4.setClassificacaoConfianca(2);
        rev1.setClassificacaoOriginalidade(2);
        rev2.setClassificacaoOriginalidade(2);
        rev3.setClassificacaoOriginalidade(2);
        rev4.setClassificacaoOriginalidade(2);
        rev1.setClassificacaoRecomendacao(2);
        rev2.setClassificacaoRecomendacao(2);
        rev3.setClassificacaoRecomendacao(2);
        rev4.setClassificacaoRecomendacao(2);
        List<Revisao> listr=new ArrayList();
        listr.add(rev1);
        listr.add(rev2);
        listr.add(rev3);
        listr.add(rev4);
        ListaRevisoes lr=new ListaRevisoes(listr);
        double[]v=new double[5];
        v[0]=2.0;
        v[1]=2.0;
        v[2]=2.0;
        v[3]=2.0;
        v[4]=2.0;  
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        double[] expResult = v;
        double[] result = instance.calcularValoresMedios();
        boolean resultBol = result.length==expResult.length;
        if (resultBol){
        for (int i=0;i<result.length;i++)
        {
            resultBol=resultBol && (result[i]==expResult[i]);
        }
        }
        assertEquals(true, resultBol);
    }

    /**
     * Test of getSubmissoesRegistadasUtilizador method, of class Evento.
     */
    @Test
    public void testGetSubmissoesRegistadasUtilizador() {
        System.out.println("getSubmissoesRegistadasUtilizador");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Evento instance = new Evento();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoRegistadoState(sub));
        instance.addSubmissao(sub, a1, u1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
        

    }

    /**
     * Test of detetarConflitos method, of class Evento.
     */
    @Test
    public void testDetetarConflitos() {
        System.out.println("detetarConflitos");
        List<TipoConflito> lmc = null;
        Evento instance = new Evento();
        instance.detetarConflitos(lmc);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSubmissoes method, of class Evento.
     */
    @Test
    public void testGetListaSubmissoes() {
        System.out.println("getListaSubmissoes");
        Evento instance = new Evento();
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Artigo a1 = new Artigo();
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoRegistadoState(sub));
        instance.addSubmissao(sub, a1, u1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of getListaTodosRevisores method, of class Evento.
     */
    @Test
    public void testGetListaTodosRevisores() {
        System.out.println("getListaTodosRevisores");
        Evento instance = new Evento();
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
        Revisor r= new Revisor(u1);
        List<Revisor> list= new ArrayList();
        list.add(r);
        CP cp= new CP(list);
        instance.setCP(cp);
        List<Revisor> expResult = new ArrayList();
        expResult.add(r);
        List<Revisor> result = instance.getListaTodosRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaTodasRevisoes method, of class Evento.
     */
    @Test
    public void testGetListaTodasRevisoes() {
        System.out.println("getListaTodasRevisoes");
        Evento instance = new Evento();
         Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_2015@hotmail.com", new NaoFazNada());
         Artigo a1 = new Artigo();
         Revisor r= new Revisor(u1);
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Submissao sub= new Submissao(a1,a1,null);
        Revisao rev= new Revisao();
        rev.setRevisor(r);
        rev.setSubmissao(sub);
        List<Revisao> list= new ArrayList();
        list.add(rev);
        ListaRevisoes lista= new ListaRevisoes(list);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<Revisao> result = instance.getListaTodasRevisoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of alertaOrganizadores method, of class Evento.
     */
    @Test
    public void testAlertaOrganizadores() {
        System.out.println("alertaOrganizadores");
        Revisor r = new Revisor(new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada()));
        String message = "MYMESSAGE";
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.alertaOrganizadores(r, message);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataInicio method, of class Evento.
     */
    @Test
    public void testGetDataInicio() {
        System.out.println("getDataInicio");
        Evento instance = new Evento();
        Data expResult = new Data(30,6,2015);
        instance.setdataInicio(expResult);
        Data result = instance.getDataInicio();
        assertEquals(expResult, result);

    }
/*
         instance.setdataInicio(new Data(30, 6,2015));
        instance.setdataInicioSubmissao(new Data(22,6,2015));
        instance.setdataFimSubmissao(new Data(23,6,2015));
        instance.setdataInicioDistribuicao(new Data(24, 6, 2015));
        instance.setdataLimiteRevisao(new Data(26,6,2015));
        instance.setdataFim(new Data(2,7,2015));
    */
    /**
     * Test of getDataFim method, of class Evento.
     */
    @Test
    public void testGetDataFim() {
        System.out.println("getDataFim");
        Evento instance = new Evento();
        Data expResult = new Data(2,7,2015);
        instance.setdataInicio(new Data(30, 6,2015));
        instance.setdataInicioSubmissao(new Data(22,6,2015));
        instance.setdataFimSubmissao(new Data(23,6,2015));
        instance.setdataInicioDistribuicao(new Data(24, 6, 2015));
        instance.setdataLimiteRevisao(new Data(26,6,2015));
        instance.setdataFim(expResult);
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        Data result = instance.getDataFim();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDataInicioDistribuicao method, of class Evento.
     */
    @Test
    public void testGetDataInicioDistribuicao() {
        System.out.println("getDataInicioDistribuicao");
        Evento instance = new Evento();
        Data expResult = new Data(24, 6, 2015);
        instance.setdataInicio(new Data(30, 6,2015));
        instance.setdataInicioSubmissao(new Data(22,6,2015));
        instance.setdataFimSubmissao(new Data(23,6,2015));
        instance.setdataInicioDistribuicao(expResult);
        instance.setdataLimiteRevisao(new Data(26,6,2015));
        instance.setdataFim(new Data(2,7,2015));
        Data result = instance.getDataInicioDistribuicao();
        assertEquals(expResult, result);
    }
    /**
     * Test of setDataLimiteSubmissaoFinal method, of class Evento.
     */
    @Test
    public void  testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
    }

    /**
     * Test of getRevisiveisEmEstadoDeRevisaoDe method, of class Evento.
     */
    @Test
    public void testGetRevisiveisEmEstadoDeRevisaoDe() {
        System.out.println("getRevisiveisEmEstadoDeRevisaoDe");
        String id = "";
        Evento instance = new Evento();
        List<Revisivel> expResult = null;
        List<Revisivel> result = instance.getRevisiveisEmEstadoDeRevisaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador method, of class Evento.
     */
    @Test
    public void testGetListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador() {
        System.out.println("getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador");
        String id = "";
        Evento instance = new Evento();
        List<Submissivel> expResult = null;
        List<Submissivel> result = instance.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSubmissoesEvento method, of class Evento.
     */
    @Test
    public void testGetListaSubmissoesEvento() {
        System.out.println("getListaSubmissoesEvento");
        Evento instance = new Evento();
        ListaSubmissoes expResult = null;
        ListaSubmissoes result = instance.getListaSubmissoesEvento();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSubmissiveisEmEstadoDeRevisaoDe method, of class Evento.
     */
    @Test
    public void testGetListaSubmissiveisEmEstadoDeRevisaoDe() {
        System.out.println("getListaSubmissiveisEmEstadoDeRevisaoDe");
        String id = "";
        Evento instance = new Evento();
        List<Revisivel> expResult = null;
        List<Revisivel> result = instance.getListaSubmissiveisEmEstadoDeRevisaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isInEmRevisao method, of class Evento.
     */
    @Test
    public void testIsInEmRevisao() {
        System.out.println("isInEmRevisao");
        Evento instance = new Evento();
        instance.setState(new EventoEmRevisaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmRevisao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataLimiteSubmissaoFinal method, of class Evento.
     */
    @Test
    public void testGetDataLimiteSubmissaoFinal() {
        System.out.println("getDataLimiteSubmissaoFinal");
        Evento instance = new Evento();
        Data expResult = null;
        Data result = instance.getDataLimiteSubmissaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataInicioST method, of class Evento.
     */
    @Test
    public void testValidaDataInicioST() {
        System.out.println("validaDataInicioST");
        Data di = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataInicioST(di);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataFimST method, of class Evento.
     */
    @Test
    public void testValidaDataFimST() {
        System.out.println("validaDataFimST");
        Data df = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataFimST(df);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataInicioDistribuicaoST method, of class Evento.
     */
    @Test
    public void testValidaDataInicioDistribuicaoST() {
        System.out.println("validaDataInicioDistribuicaoST");
        Data dd = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataInicioDistribuicaoST(dd);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataInicioSubmissaoST method, of class Evento.
     */
    @Test
    public void testValidaDataInicioSubmissaoST() {
        System.out.println("validaDataInicioSubmissaoST");
        Data dis = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataInicioSubmissaoST(dis);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataFimSubmissaoST method, of class Evento.
     */
    @Test
    public void testValidaDataFimSubmissaoST() {
        System.out.println("validaDataFimSubmissaoST");
        Data dis = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataFimSubmissaoST(dis);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataLimiteSubmissaoFinalST method, of class Evento.
     */
    @Test
    public void testValidaDataLimiteSubmissaoFinalST() {
        System.out.println("validaDataLimiteSubmissaoFinalST");
        Data dis = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataLimiteSubmissaoFinalST(dis);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of validaDataLimiteRevisaoST method, of class Evento.
     */
    @Test
    public void testValidaDataLimiteRevisaoST() {
        System.out.println("validaDataLimiteRevisaoST");
        Data dis = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDataLimiteRevisaoST(dis);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDataLimiteRevisao method, of class Evento.
     */
    @Test
    public void  testGetDataLimiteRevisao() {
        System.out.println("getDataLimiteRevisao");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
        Data result = instance.getDataLimiteRevisao();
        assertEquals(limRev, result);
    }

    /**
     * Test of setdataLimiteRevisao method, of class Evento.
     */
    @Test
    public void  testSetdataLimiteRevisao() {
        System.out.println("setdataLimiteRevisao");
        Data inicio = new Data(30,8,2015);
        Data inicioSub = new Data(20,8,2015);
        Data fim = new Data(3, 9 , 2015);
        Data fimSub = new Data(21,8,2015);
        Data incioDist = new Data(22,8,2015);
        Data limRev = new Data(23,8,2015);
        Data limSubFin = new Data(24,8,2015);
        Evento instance = new Evento();
        instance.setdataInicio(inicio);
        instance.setdataInicioSubmissao(inicioSub);
        instance.setdataFim(fim);
        instance.setdataFimSubmissao(fimSub);
        instance.setdataInicioDistribuicao(incioDist);
        instance.setdataLimiteRevisao(limRev);
        instance.setDataLimiteSubmissaoFinal(limSubFin);
    }

    /**
     * Test of getListaSubmissoesAceites method, of class Evento.
     */
    @Test
    public void testGetListaSubmissoesAceites() {
        System.out.println("getListaSubmissoesAceites");
        String id = "";
        Evento instance = new Evento();
        List<Submissao> expResult = null;
        List<Submissao> result = instance.getListaSubmissoesAceites(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSessoesEmDecisao method, of class Evento.
     */
    @Test
    public void testGetListaSessoesEmDecisao() {
        System.out.println("getListaSessoesEmDecisao");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = new ArrayList();
        List<SessaoTematica> result = instance.getListaSessoesEmDecisao(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of decide method, of class Evento.
     */
    @Test
    public void testDecide() {
        System.out.println("decide");
        List<Revisao> lr = null;
        Evento instance = new Evento();
        List<Decisao> expResult = null;
        List<Decisao> result = instance.decide(lr);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRevisoes method, of class Evento.
     */
    @Test
    public void testGetRevisoes_0args() {
        System.out.println("getRevisoes");
        Evento instance = new Evento();
        List<Revisao> expResult = null;
        List<Revisao> result = instance.getRevisoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSessoesEmDecisao method, of class Evento.
     */
    @Test
    public void testTemSessoesEmDecisao() {
        System.out.println("temSessoesEmDecisao");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSessoesEmDecisao(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSubmissoesAceitesUtilizadorEventoPrincipal method, of class Evento.
     */
    @Test
    public void testTemSubmissoesAceitesUtilizadorEventoPrincipal() {
        System.out.println("temSubmissoesAceitesUtilizadorEventoPrincipal");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSubmissoesAceitesUtilizadorEventoPrincipal(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSessoesEmSubmissaoCR method, of class Evento.
     */
    @Test
    public void testGetListaSessoesEmSubmissaoCR() {
        System.out.println("getListaSessoesEmSubmissaoCR");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getListaSessoesEmSubmissaoCR(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addAllSubmissoes method, of class Evento.
     */
    @Test
    public void testAddAllSubmissoes() {
        System.out.println("addAllSubmissoes");
        List<Submissao> ls = null;
        Evento instance = new Evento();
        instance.addAllSubmissoes(ls);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temTodasRevisoesFeitas method, of class Evento.
     */
    @Test
    public void testTemTodasRevisoesFeitas() {
        System.out.println("temTodasRevisoesFeitas");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temTodasRevisoesFeitas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSessoesTematicasComProponenteRegistadoState method, of class Evento.
     */
    @Test
    public void testGetSessoesTematicasComProponenteRegistadoState() {
        System.out.println("getSessoesTematicasComProponenteRegistadoState");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getSessoesTematicasComProponenteRegistadoState(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addAllSessoesTematicas method, of class Evento.
     */
    @Test
    public void testAddAllSessoesTematicas() {
        System.out.println("addAllSessoesTematicas");
        List<SessaoTematica> st = null;
        Evento instance = new Evento();
        instance.addAllSessoesTematicas(st);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of valida method, of class Evento.
     */
    @Test
    public void testValida_0args() {
        System.out.println("valida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of geraSubmissoesDeFicheiro method, of class Evento.
     */
    @Test
    public void testGeraSubmissoesDeFicheiro() throws Exception {
        System.out.println("geraSubmissoesDeFicheiro");
        File stdIn = null;
        File stdOut = null;
        RegistoUtilizadores regU = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.geraSubmissoesDeFicheiro(stdIn, stdOut, regU);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSessoesDistribuiveis method, of class Evento.
     */
    @Test
    public void testTemSessoesDistribuiveis() {
        System.out.println("temSessoesDistribuiveis");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSessoesDistribuiveis(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSessoesTematicasEmLicitacao method, of class Evento.
     */
    @Test
    public void testGetSessoesTematicasEmLicitacao() {
        System.out.println("getSessoesTematicasEmLicitacao");
        String id = "";
        Evento instance = new Evento();
        List<SessaoTematica> expResult = null;
        List<SessaoTematica> result = instance.getSessoesTematicasEmLicitacao(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPosition method, of class Evento.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        Submissao sub = null;
        Evento instance = new Evento();
        int expResult = 0;
        int result = instance.getPosition(sub);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of valida method, of class Evento.
     */
    @Test
    public void testValida_Submissao_int() {
        System.out.println("valida");
        Submissao sub = null;
        int i = 0;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.valida(sub, i);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSubmissoesAceitesUtilizador method, of class Evento.
     */
    @Test
    public void testTemSubmissoesAceitesUtilizador() {
        System.out.println("temSubmissoesAceitesUtilizador");
        String id = "";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temSubmissoesAceitesUtilizador(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temTodasSubmissoesCameraReady method, of class Evento.
     */
    @Test
    public void testTemTodasSubmissoesCameraReady() {
        System.out.println("temTodasSubmissoesCameraReady");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.temTodasSubmissoesCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaRevisivelUtilizadorEmEstadoRevisao method, of class Evento.
     */
    @Test
    public void testGetListaRevisivelUtilizadorEmEstadoRevisao() {
        System.out.println("getListaRevisivelUtilizadorEmEstadoRevisao");
        String id = "";
        Evento instance = new Evento();
        List<Revisivel> expResult = null;
        List<Revisivel> result = instance.getListaRevisivelUtilizadorEmEstadoRevisao(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRevisoes method, of class Evento.
     */
    @Test
    public void testGetRevisoes_String() {
        System.out.println("getRevisoes");
        String id = "";
        Evento instance = new Evento();
        List<Revisao> expResult = null;
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRevisor method, of class Evento.
     */
    @Test
    public void testGetRevisor_String() {
        System.out.println("getRevisor");
        String id = "";
        Evento instance = new Evento();
        Revisor expResult = null;
        Revisor result = instance.getRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRevisor method, of class Evento.
     */
    @Test
    public void testGetRevisor_Utilizador() {
        System.out.println("getRevisor");
        Utilizador u = null;
        Evento instance = new Evento();
        Revisor expResult = null;
        Revisor result = instance.getRevisor(u);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLicitacoes method, of class Evento.
     */
    @Test
    public void testGetLicitacoes() {
        System.out.println("getLicitacoes");
        Evento instance = new Evento();
        List<Licitacao> expResult = null;
        List<Licitacao> result = instance.getLicitacoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSessaoTematicaDefinida method, of class Evento.
     */
    @Test
    public void testSetSessaoTematicaDefinida() {
        System.out.println("setSessaoTematicaDefinida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setSessaoTematicaDefinida();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmCameraReady method, of class Evento.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaAlteracao method, of class Evento.
     */
    @Test
    public void testRegistaAlteracao() {
        System.out.println("registaAlteracao");
        Submissao sub = null;
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.registaAlteracao(sub);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSubmissoes method, of class Evento.
     */
    @Test
    public void testGetSubmissoes() {
        System.out.println("getSubmissoes");
        Evento instance = new Evento();
        List<Submissao> expResult = null;
        List<Submissao> result = instance.getSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAcceptedTopics method, of class Evento.
     */
    @Test
    public void testGetAcceptedTopics() {
        System.out.println("getAcceptedTopics");
        ArrayList<String> ls = null;
        Evento instance = new Evento();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getAcceptedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRejectedTopics method, of class Evento.
     */
    @Test
    public void testGetRejectedTopics() {
        System.out.println("getRejectedTopics");
        ArrayList<String> ls = null;
        Evento instance = new Evento();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getRejectedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStatsTopics method, of class Evento.
     */
    @Test
    public void testGetStatsTopics() {
        System.out.println("getStatsTopics");
        ArrayList ls = null;
        Evento instance = new Evento();
        ArrayList expResult = null;
        ArrayList result = instance.getStatsTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaSessoesDecididas method, of class Evento.
     */
    @Test
    public void testGetListaSessoesDecididas() {
        System.out.println("getListaSessoesDecididas");
        Evento instance = new Evento();
        ArrayList<SessaoTematica> expResult = null;
        ArrayList<SessaoTematica> result = instance.getListaSessoesDecididas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    
}
