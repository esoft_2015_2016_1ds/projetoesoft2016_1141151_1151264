/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.Coder;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class UtilizadorTest {
    
    public UtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Utilizador.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Utilizador instance = new Utilizador("Goncalo Reis", "greis", "pass123", "greis@gmail.com", new NaoFazNada());
        String expResult = "Goncalo Reis";
        String result = instance.getName();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getName method with no name atribute
     */
    @Test
    public void testGetNameNull() {
        System.out.println("getNameNull");
        Utilizador instance = new Utilizador();
        String result = instance.getName();
        String expResult= null;
        assertEquals(expResult, result);
    }

    /**
     * Test of getUsername method, of class Utilizador.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        Utilizador instance = new Utilizador("Goncalo Reis", "greis", "pass123", "greis@gmail.com", new NaoFazNada());
        String expResult = "greis";
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsername method with no Username atribute
     */
    @Test
    public void testGetUsernameNull() {
        System.out.println("getUsernameNull");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassword method, of class Utilizador.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        Utilizador instance = new Utilizador("Goncalo Reis", "greis", "pass123", "greis@gmail.com", new NaoFazNada());
        String expResult = "pass123";
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPassword method with no password atribute.
     */
    @Test
    public void testGetPasswordNull() {
        System.out.println("getPasswordNUll");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Utilizador instance = new Utilizador("Goncalo Reis", "greis", "pass123", "greis@gmail.com", new NaoFazNada());
        String expResult = "greis@gmail.com";
        String result = instance.getEmail();
        assertEquals(result, expResult);
    }
    
    /**
     * Test of getEmail method with no email atribute
     */
    @Test
    public void testGetEmailNull() {
        System.out.println("getEmailNull");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getEmail();
        assertEquals(result, expResult);
    }

    /**
     * Test of setName method, of class Utilizador.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String strNome = "Goncalo";
        Utilizador instance = new Utilizador();
        instance.setName(strNome);
        assertEquals(strNome, instance.getName());
    }
    
    /**
     * Test of setName method with a name with less than two letters
     */
    @Test (expected=IllegalArgumentException.class)
    public void testSetNameLessThanTwo() {
        System.out.println("setNameLessThanTwo");
        String strNome = "g";
        Utilizador instance = new Utilizador();
        instance.setName(strNome);
    }
    
    /**
     * Test of setName method with a name without letters in it
     */
    @Test (expected=IllegalArgumentException.class)
    public void testSetNameWithoutLetters() {
        System.out.println("setNameWithNumbers");
        String strNome = "241241241";
        Utilizador instance = new Utilizador();
        instance.setName(strNome);
    }
    
    
    /**
     * Test of setUsername method, of class Utilizador.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String strUtilizadorname = "greis";
        Utilizador instance = new Utilizador();
        instance.setUsername(strUtilizadorname);
        String strRes = instance.getUsername();
        assertEquals(strRes, strUtilizadorname);
    }
    
    /**
     * Test of setUsername method, of class Utilizador with less than three characters.
     * 
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetUsernameLessThanThreeCharacters() {
        System.out.println("setUsername < 3 characters");
        String strUtilizadorname = "gr";
        Utilizador instance = new Utilizador();
        instance.setUsername(strUtilizadorname);
    }

    /**
     * Test of setPassword method,.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String strPassword = "BABAAB1";
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        instance.setPassword(strPassword);
        assertEquals(strPassword, instance.getPassword());
    }
    
    /**
     * Test of setPassword method with less than six characters.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetPasswordLessThanSixChars() {
        System.out.println("setPasswordLessThanSixChars");
        String strPassword = "BAAB1";
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        instance.setPassword(strPassword);
        assertEquals(strPassword, instance.getPassword());
    }
    
    /**
     * Test of setPassword method with less than three letters.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetPasswordLessThanThreeLetters() {
        System.out.println("setPasswordLessThanThreeLetters");
        String strPassword = "BA111111111";
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        instance.setPassword(strPassword);
        assertEquals(strPassword, instance.getPassword());
    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String strEmail = "goncalo@gmail.com";
        Utilizador instance = new Utilizador();
        instance.setEmail(strEmail);

    }
    
    /**
     * Test of setEmail method with an invalid email
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetEmailInvalid1() {
        System.out.println("setEmailInvalid1");
        String strEmail = "@gmail.com";
        Utilizador instance = new Utilizador();
        instance.setEmail(strEmail);
    }
    
    /**
     * Test of setEmail method with an invalid email
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetEmailInvalid2() {
        System.out.println("setEmailInvalid1");
        String strEmail = "goncalo";
        Utilizador instance = new Utilizador();
        instance.setEmail(strEmail);
    }

    /**
     * Test of setDados method, of class Utilizador.
     * As setDados is dependent on all the sets for the class Utilizador
     * there is no need to check different combinations of valid and invalid atributes
     * as each invalid atribute is handled by it's respective set method
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        Utilizador u = new Utilizador("Goncalo", "greis", "password12341", "greis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador();
        instance.setDados(u);
        assertEquals(u,instance);
    }

    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador instance =  new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador with an invalid name.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testValidaInvalidName() {
        System.out.println("validaInvalidName");
        Utilizador instance =  new Utilizador("G", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador with an invalid username.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testValidaInvalidUserName() {
        System.out.println("validaInvalidUserName");
        Utilizador instance =  new Utilizador("Goncalo", "s", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador with an invalid password.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testValidaInvalidPassword() {
        System.out.println("validaInvalidPassword");
        Utilizador instance =  new Utilizador("Goncalo", "s", "21", "greis@email.com", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador with an invalid Email.
     */
    @Test (expected=IllegalArgumentException.class)
    public void testValidaInvalidEmail() {
        System.out.println("validaInvalidEmail");
        Utilizador instance =  new Utilizador("Goncalo", "s", "2basgaag1", "greis", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    

    /**
     * Test of toString method, of class Utilizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        String expResult = "Goncalo/greis/greis@email.com/BABAAB1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of equals method, of class Utilizador with a different name.
     */
    @Test
    public void testEqualsDifferentName() {
        System.out.println("equalsDifferentName");
        Object other = new Utilizador("Tiago", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of equals method, of class Utilizador with a different username.
     */
    @Test
    public void testEqualsDifferentUsername() {
        System.out.println("equalsDifferentUsername");
        Object other = new Utilizador("Goncalo", "reisG", "BABAAB1", "greis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of equals method, of class Utilizador with a different password.
     */
    @Test
    public void testEqualsDifferentPassword() {
        System.out.println("equalsDifferentPassword");
        Object other = new Utilizador("Goncalo", "greis", "BABfAAB1", "greis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of equals method, of class Utilizador with a different email.
     */
    @Test
    public void testEqualsDifferentEmail() {
        System.out.println("equalsDifferentEmail");
        Object other = new Utilizador("Goncalo", "greis", "BABAAB1", "rrreis@email.com", new NaoFazNada());
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTabela method, of class Utilizador.
     */
    @Test
    public void testGetTabela() {
        System.out.println("getTabela");
        Utilizador instance = new Utilizador("Goncalo", "greis", "pass123", "g.reis@email.com",
                new NaoFazNada());
        Coder expResult = new NaoFazNada();
        Coder result = instance.getTabela();
        assertEquals(expResult, result);
    }
    
     /**
     * Test of getTabela method, of class Utilizador with no Coder.
     */
    @Test
    public void testGetTabelaNull() {
        System.out.println("getTabelaNull");
        Utilizador instance = new Utilizador();
        Coder expResult = null;
        Coder result = instance.getTabela();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTabela method, of class Utilizador.
     */
    @Test
    public void testSetTabela() {
        System.out.println("setTabela");
        Utilizador instance = new Utilizador();
        instance.setTabela(new NaoFazNada());
    }

    /**
     * Test of showData method, of class Utilizador.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Utilizador instance = new Utilizador("Goncalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        String expResult = "Nome Goncalo Mail greis@email.com Username greis Password BABAAB1";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    
}
