/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class LicitacaoTest {
    
    public LicitacaoTest() {
    }
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setInteresse method, of class Licitacao.
     * In this case, an interest of -1 is passed as an argument.
     * The interest can only be 0-3.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testSetInteresseInvalid() {
        System.out.println("setInteresse (-1 interesse)");
        int interesse = -1;
        Licitacao instance = new Licitacao();
        instance.setInteresse(interesse);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of setInteresse method, of class Licitacao.
     * In this case, an interest of 2 is passed as an argument.
     * The interest can only be 0-3.
     */
    @Test
    public void testSetInteresseValid() {
        System.out.println("setInteresse (valid interesse)");
        int interesse = 2;
        Licitacao instance = new Licitacao();
        instance.setInteresse(interesse);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String compareData = chunk[0];
        assertEquals(compareData,String.valueOf(interesse));
    }

    /**
     * Test of valida method, of class Licitacao.
     * In this test, a bid (Licitacao) with no data is tested.
     */
    @Test
    public void testValidaNullData() {
        System.out.println("valida (Licitacao with null data)");
        Licitacao instance = new Licitacao();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of valida method, of class Licitacao.
     * In this test, a bid (Licitacao) with valid data is.
     */
    @Test
    public void testValidaNormalCase() {
        System.out.println("valida (Licitacao with valid data)");
        Licitacao instance = new Licitacao(new Submissao(),new Revisor(u1),new ArrayList());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class Licitacao.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Licitacao instance = new Licitacao();
        String expResult = "0\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addTipoConflito method, of class Licitacao.
     * In this test, a null TipoConflito is being added.
     */
    @Test
    public void testAddTipoConflitoNullTipoConflito() {
        System.out.println("addTipoConflito (null TipoConflito)");
        TipoConflito tc = null;
        Licitacao instance = new Licitacao();
        boolean expResult = false;
        boolean result = instance.addTipoConflito(tc);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of addTipoConflito method, of class Licitacao.
     * In this test, a valid TipoConflito is being added.
     */
    @Test
    public void testAddTipoConflitoValidTipoConflito() {
        System.out.println("addTipoConflito (valid TipoConflito)");
        TipoConflito tc = new TipoConflito("Férias","Deteta se alguéme está de férias.");
        Licitacao instance = new Licitacao();
        boolean expResult = true;
        boolean result = instance.addTipoConflito(tc);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of addTipoConflito method, of class Licitacao.
     * In this test, a repeated TipoConflito is being added.
     */
    @Test
    public void testAddTipoConflitoRepeatedTipoConflito() {
        System.out.println("addTipoConflito (valid TipoConflito)");
        TipoConflito tc = new TipoConflito("Férias","Deteta se alguém está de férias.");
        Licitacao instance = new Licitacao();
        instance.addTipoConflito(new TipoConflito("Férias","Deteta se alguém está de férias."));
        boolean expResult = false;
        boolean result = instance.addTipoConflito(tc);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temRevisor method, of class Licitacao.
     * In this case, the revisor does not exist in the bid, meaning the bid does not
     * belong to the revisor.
     */
    @Test
    public void testTemRevisorNaoExiste() {
        System.out.println("temRevisor (doesnt' exist)");
        String id = "EuNaoExistoLogoNaoPenso";
        Licitacao instance = new Licitacao(new Submissao(), new Revisor(u1),new ArrayList());
        boolean expResult = false;
        boolean result = instance.temRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of temRevisor method, of class Licitacao.
     * In this case, the revisorexists in the bid, meaning the bid belongs
     * to the revisor.
     */
    @Test
    public void testTemRevisorExiste() {
        System.out.println("temRevisor (valid revisor)");
        String id = u1.getEmail();
        Licitacao instance = new Licitacao(new Submissao(), new Revisor(u1),new ArrayList());
        boolean expResult = true;
        boolean result = instance.temRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setInteresse method, of class Licitacao.
     */
    @Test
    public void testSetInteresse() {
        System.out.println("setInteresse");
        int interesse = 1;
        Licitacao instance = new Licitacao();
        instance.setInteresse(interesse);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getInteresse(),interesse);
    }

    /**
     * Test of addTipoConflito method, of class Licitacao.
     */
    @Test
    public void testAddTipoConflito() {
        System.out.println("addTipoConflito");
        TipoConflito tc = null;
        Licitacao instance = new Licitacao();
        boolean expResult = false;
        boolean result = instance.addTipoConflito(tc);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of valida method, of class Licitacao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Licitacao instance = new Licitacao();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of temRevisor method, of class Licitacao.
     */
    @Test
    public void testTemRevisor() {
        System.out.println("temRevisor");
        String id = u1.getEmail();
        Licitacao instance = new Licitacao(new Submissao(), new Revisor(u1),new ArrayList());
        boolean expResult = true;
        boolean result = instance.temRevisor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getInteresse method, of class Licitacao.
     */
    @Test
    public void testGetInteresse() {
        System.out.println("getInteresse");
        Licitacao instance = new Licitacao();
        int expResult = 1;
        instance.setInteresse(1);
        int result = instance.getInteresse();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
