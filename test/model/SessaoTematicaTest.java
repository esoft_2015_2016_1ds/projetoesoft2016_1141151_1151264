/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.SessaoTematicaState;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaConflitosDetetados;
import listas.ListaRevisoes;
import listas.ListaSubmissoes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.sessaotematica.SessaoTematicaAceitaSubmissoesState;
import states.sessaotematica.SessaoTematicaCPDefinidaState;
import states.sessaotematica.SessaoTematicaCameraReadyState;
import states.sessaotematica.SessaoTematicaCriadoState;
import states.sessaotematica.SessaoTematicaEmDecisaoState;
import states.sessaotematica.SessaoTematicaEmDetecaoState;
import states.sessaotematica.SessaoTematicaEmDistribuicaoState;
import states.sessaotematica.SessaoTematicaEmLicitacaoState;
import states.sessaotematica.SessaoTematicaEmRevisaoState;
import states.sessaotematica.SessaoTematicaEmSubmissaoCameraReadyState;
import states.sessaotematica.SessaoTematicaRegistadoState;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoEmCameraReadyState;
import states.submissao.SubmissaoEmLicitacaoState;
import states.submissao.SubmissaoRegistadoState;
import states.submissao.SubmissaoRemovidaState;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class SessaoTematicaTest {
    
    public SessaoTematicaTest() {
    }
    Submissao sub;
    List<String> k;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCodigo method, of class SessaoTematica.
     */
    @Test
    public void testGetCodigo() {
        System.out.println("getCodigo");
        SessaoTematica instance = new SessaoTematica();
        instance.setCodigo("Ola");
        String expResult = "Ola";
        String result = instance.getCodigo();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDescricao method, of class SessaoTematica.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        SessaoTematica instance = new SessaoTematica();
        String expResult = "Pika-pika!";
        instance.setDescricao(expResult);
        String result = instance.getDescricao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setCP method, of class SessaoTematica.
     */
    @Test
    public void testSetCP() {
        System.out.println("setCP");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        CP cp = new CP();
        cp.addMembroCP(u1);
        SessaoTematica instance = new SessaoTematica();
        instance.setCP(cp);
        String temp[] = instance.showData().split("\n");
        String compareData=temp[0];
        assertEquals(compareData,"CP não nula");
    }

    /**
     * Test of setCodigo method, of class SessaoTematica.
     */
    @Test
    public void testSetCodigo() {
        System.out.println("setCodigo");
        String value = "Ola";
        SessaoTematica instance = new SessaoTematica();
        instance.setCodigo(value);
        assertEquals(value,instance.getCodigo());
    }

    /**
     * Test of setDescricao method, of class SessaoTematica.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String value = "Ola";
        SessaoTematica instance = new SessaoTematica();
        instance.setDescricao(value);
        assertEquals(value,instance.getDescricao());
    }

    /**
     * Test of addProponente method, of class SessaoTematica.
     */
    @Test
    public void testAddProponente_String_Utilizador() {
        System.out.println("addProponente");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador utilizador = u1;
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.addProponente(utilizador);
        assertEquals(expResult, result);
    }

    /**
     * Test of addProponente method, of class SessaoTematica.
     */
    @Test
    public void testAddProponente_Proponente() {
        System.out.println("addProponente");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente proponente = new Proponente(u1);
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.addProponente(proponente);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class SessaoTematica.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        SessaoTematica instance = new SessaoTematica();
        Utilizador u = new Utilizador("Luis","LuisF","wee123","Goku@gmail.com",new NaoFazNada());
        Revisor r = new Revisor(u);
        instance.addProponente(u);
        List<Revisor> lr = new ArrayList();
        lr.add(r);
        instance.setCP(new CP(lr));
        boolean result = instance.valida();
        boolean expResult = true;
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoesUtilizador method, of class SessaoTematica.
     */
    @Test
    public void testGetSubmissoesUtilizador() {
        System.out.println("getSubmissoesUtilizador");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);
        instance.addSubmissao(sub,a1, u1);   
        List<Autor>laut=new ArrayList();
        laut.add(aut);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getSubmissoesUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisoes method, of class SessaoTematica.
     */
    @Test
    public void testGetRevisoes() {
        System.out.println("getRevisoes");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Evento instance = new Evento();
        Revisao revisao=new Revisao();
        Revisor revisor=new Revisor();
        revisao.setRevisor(revisor);
        List<Revisao> listaRevisao=new ArrayList();
        listaRevisao=new ArrayList();
        listaRevisao.add(revisao);
        List<Revisao> expResult = new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        List<Autor> la=new ArrayList();
        la.add(aut);
        AutorCorrespondente au = new AutorCorrespondente(aut,u1);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        revisao.setSubmissao(sub);
        expResult.add(revisao);
        ListaRevisoes lista=new ListaRevisoes(listaRevisao);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
    }

    

    /**
     * Test of saveRevisao method, of class SessaoTematica.
     */
    @Test
    public void testSaveRevisao() {
        System.out.println("saveRevisao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Artigo a =new Artigo();
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        Revisao rev = new Revisao();
        rev.setSubmissao(sub);
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.saveRevisao(rev);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class SessaoTematica.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SessaoTematica instance = new SessaoTematica();
        String temp[] = instance.showData().split("\n");
        String expResult = "SessaoTematica: " + temp[1] + "/" + temp[2];
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoProcessoDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testNovoProcessoDistribuicao() {
        System.out.println("novoProcessoDistribuicao");
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.novoProcessoDistribuicao() != null ;
        assertEquals(expResult, result);
    }

    /**
     * Test of setProcessoDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testSetProcessoDistribuicao() {
        System.out.println("setProcessoDistribuicao");
        SessaoTematica instance = new SessaoTematica();
        ProcessoDistribuicao pd = instance.novoProcessoDistribuicao();
        instance.setProcessoDistribuicao(pd);
        int sID = System.identityHashCode(pd);
        String temp[] = instance.showData().split("\n");
        String compareData = temp[3];
        int controllerId = Integer.parseInt(compareData);
        assertEquals(sID,controllerId);

    }

    /**
     * Test of novaSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testNovaSubmissao() {
        System.out.println("novaSubmissao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        Artigo a =new Artigo();
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        Revisao rev = new Revisao();
        rev.setSubmissao(sub);
        boolean expResult = true;
        boolean result = instance.novaSubmissao() != null;
        assertEquals(expResult, result);
    }


    /**
     * Test of setCriado method, of class SessaoTematica.
     */
    @Test
    public void testSetCriado() {
        System.out.println("setCriado");
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.setCriado();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRegistado method, of class SessaoTematica.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaRegistadoState(instance));
        boolean expResult = true;
        boolean result = instance.setRegistado();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCPDefinida method, of class SessaoTematica.
     */
    @Test
    public void testSetCPDefinida() {
        System.out.println("setCPDefinida");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaCPDefinidaState(instance));
        boolean expResult = true;
        boolean result = instance.setCPDefinida();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAceitaSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testSetAceitaSubmissoes() {
        System.out.println("setAceitaSubmissoes");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaAceitaSubmissoesState(instance));
        boolean expResult = true;
        boolean result = instance.setAceitaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmDetecao method, of class SessaoTematica.
     */
    @Test
    public void testSetEmDetecao() {
        System.out.println("setEmDetecao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDetecaoState(instance));
        boolean expResult = true;
        boolean result = instance.setEmDetecao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmLicitacao method, of class SessaoTematica.
     */
    @Test
    public void testSetEmLicitacao() {
        System.out.println("setEmLicitacao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmLicitacaoState(instance));
        boolean expResult = true;
        boolean result = instance.setEmLicitacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testSetEmDistribuicao() {
        System.out.println("setEmDistribuicao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDistribuicaoState(instance));
        boolean expResult = true;
        boolean result = instance.setEmDistribuicao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmRevisao method, of class SessaoTematica.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmRevisaoState(instance));
        boolean expResult = true;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmDecisao method, of class SessaoTematica.
     */
    @Test
    public void testSetEmDecisao() {
        System.out.println("setEmDecisao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDecisaoState(instance));
        boolean expResult = true;
        boolean result = instance.setEmDecisao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSubmissoesDecididas method, of class SessaoTematica.
     */
    @Test
    public void testSetSubmissoesDecididas() {
        System.out.println("setSubmissoesDecididas");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmSubmissaoCameraReadyState(instance));
        boolean expResult = true;
        boolean result = instance.setSubmissoesDecididas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setState method, of class SessaoTematica.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        SessaoTematica instance = new SessaoTematica();
        SessaoTematicaState state = new SessaoTematicaCPDefinidaState(instance);
        instance.setState(state);
    }

    /**
     * Test of getListaRevisoresCP method, of class SessaoTematica.
     */
    @Test
    public void testGetListaRevisoresCP() {
        System.out.println("getListaRevisoresCP");
        SessaoTematica instance = new SessaoTematica();
        List<Revisor> expResult = null;
        List<Revisor> result = instance.getListaRevisoresCP();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("Luis faz isto!!!");
    }


    /**
     * Test of novoProcessoDecisao method, of class SessaoTematica.
     */
    @Test
    public void testNovoProcessoDecisao() {
        System.out.println("novoProcessoDecisao");
        SessaoTematica instance = new SessaoTematica();
        ProcessoDecisao result = instance.novoProcessoDecisao();
    }

    /**
     * Test of setProcessoDecisao method, of class SessaoTematica.
     */
    @Test
    public void testSetProcessoDecisao() {
        System.out.println("setProcessoDecisao");
        ProcessoDecisao pd = new ProcessoDecisao();
        SessaoTematica instance = new SessaoTematica();
        instance.setProcessoDecisao(pd);
    }


    /**
     * Test of temRevisor method, of class SessaoTematica.
     */
    @Test
    public void testTemRevisor() {
        System.out.println("temRevisor");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        CP cp= new CP();
        cp.addMembroCP(u1);
        instance.setCP(cp);
        boolean expResult = true;
        boolean result = instance.temRevisor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of temProponente method, of class SessaoTematica.
     */
    @Test
    public void testTemProponente() {
        System.out.println("temProponente");
        Utilizador u = new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        instance.addProponente(u);
        boolean expResult = true;
        boolean result = instance.temProponente(u.getEmail());
        assertEquals(expResult, result);
    }
    /**
     * Test of novaCP method, of class SessaoTematica.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        SessaoTematica instance = new SessaoTematica();
        CP result = instance.novaCP();
        String temp[] = instance.showData().split("\n");
        String compareData = temp[0];      
        assertEquals(compareData,"CP não nula");
    }

    /**
     * Test of temCP method, of class SessaoTematica.
     */
    @Test
    public void testTemCP() {
        System.out.println("temCP");
        SessaoTematica instance = new SessaoTematica();
        CP cp= new CP();
        instance.setCP(cp);
        boolean expResult = true;
        boolean result = instance.temCP();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoesRemovidas method, of class SessaoTematica.
     */
    @Test
    public void testGetSubmissoesRemovidas() {
        System.out.println("getSubmissoesRemovidas");
        SessaoTematica instance = new SessaoTematica();
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Autor aut= new Autor(u1,"ISEP");
        ListaAutores lar= new ListaAutores();
        lar.addAutor(aut);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoRemovidaState(sub));
        List<Submissao> lista= new ArrayList();
        lista.add(sub);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        instance.addAllSubmissoes(lista);
        List<Submissao> result = instance.getSubmissoesRemovidas();
        assertEquals(expResult, result);
    }

    /**
     * Test of addProponente method, of class SessaoTematica.
     */
    @Test
    public void testAddProponente_Utilizador() {
        System.out.println("addProponente");
         Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador utilizador = u1;
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.addProponente(utilizador);
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCriadoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaCriadoState(instance));
        boolean expResult = true;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);

    }

    /**
     * Test of isInRegistadoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaRegistadoState(instance));
        boolean expResult = true;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCPDefinidaState method, of class SessaoTematica.
     */
    @Test
    public void testIsInCPDefinidaState() {
        System.out.println("isInCPDefinidaState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaCPDefinidaState(instance));
        boolean expResult = true;
        boolean result = instance.isInCPDefinidaState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInAceitaSubmissoesState method, of class SessaoTematica.
     */
    @Test
    public void testIsInAceitaSubmissoesState() {
        System.out.println("isInAceitaSubmissoesState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaAceitaSubmissoesState(instance));
        boolean expResult = true;
        boolean result = instance.isInAceitaSubmissoesState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDetecaoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmDetecaoState() {
        System.out.println("isInEmDetecaoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDetecaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmDetecaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmLicitacaoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmLicitacaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDistribuicaoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmDistribuicaoState() {
        System.out.println("isInEmDistribuicaoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDistribuicaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmDistribuicaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmDecisaoState method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmDecisaoState() {
        System.out.println("isInEmDecisaoState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmDecisaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmDecisaoState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmSubmissaoCameraReadyState method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmSubmissaoCameraReadyState() {
        System.out.println("isInEmSubmissaoCameraReadyState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmSubmissaoCameraReadyState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmSubmissaoCameraReadyState();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInCameraReadyState method, of class SessaoTematica.
     */
    @Test
    public void testIsInCameraReadyState() {
        System.out.println("isInCameraReadyState");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaCameraReadyState(instance));
        boolean expResult = true;
        boolean result = instance.isInCameraReadyState();
        assertEquals(expResult, result);
    }

    /**
     * Test of temSubmissoesAutor method, of class SessaoTematica.
     */
    @Test
    public void testTemSubmissoesAutor() {
        System.out.println("temSubmissoesAutor");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Autor aut= new Autor(u1,"ISEP");
        ListaAutores lar= new ListaAutores();
        lar.addAutor(aut);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(aut, u1));
        Submissao sub= new Submissao(a1,a1,null);
        List<Submissao> lista= new ArrayList();
        lista.add(sub);
        SessaoTematica instance = new SessaoTematica();
        instance.addAllSubmissoes(lista);
        boolean expResult = true;
        boolean result = instance.temSubmissoesAutor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoesNaoCameraReadyDe method, of class SessaoTematica.
     */
    @Test
    public void testGetSubmissoesNaoCameraReadyDe() {
        System.out.println("getSubmissoesNaoCameraReadyDe");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        Autor aut= new Autor(u1,"ISEP");
        ListaAutores lar= new ListaAutores();
        lar.addAutor(aut);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(aut, u1));
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoEmLicitacaoState(sub));
        instance.addSubmissao(sub, a1, u1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getSubmissoesNaoCameraReadyDe(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoesEmLicitacao method, of class SessaoTematica.
     */
    @Test
    public void testGetSubmissoesEmLicitacao() {
        System.out.println("getSubmissoesEmLicitacao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        Autor aut= new Autor(u1,"ISEP");
        ListaAutores lar= new ListaAutores();
        lar.addAutor(aut);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(aut, u1));
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoEmLicitacaoState(sub));
        instance.addSubmissao(sub, a1, u1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getSubmissoesEmLicitacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisor method, of class SessaoTematica.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        CP cp= new CP();
        cp.addMembroCP(u1);
        instance.setCP(cp);
        Revisor expResult = new Revisor(u1);
        Revisor result = instance.getRevisor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of addSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testAddSubmissao() {
        System.out.println("addSubmissao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Autor aut= new Autor(u1,"ISEP");
        ListaAutores lar= new ListaAutores();
        lar.addAutor(aut);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(aut, u1));
        Submissao sub= new Submissao(a1,a1,null);
        sub.setState(new SubmissaoRegistadoState(sub));
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.addSubmissao(sub, a1, u1);
        assertEquals(expResult, result);
    }

     /**
     * Test of getConfianca method, of class SessaoTematica.
     */
    @Test
    public void testGetConfianca() {
        System.out.println("getConfianca");
        SessaoTematica instance = new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(instance); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoConfianca(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getConfianca();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAdequacao method, of class SessaoTematica.
     */
    @Test
    public void testGetAdequacao() {
        System.out.println("getAdequacao");
        SessaoTematica instance = new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(instance); 
        Revisao rev1=new Revisao();
        rev1.setClassificacaoAdequacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getAdequacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalidade method, of class SessaoTematica.
     */
    @Test
    public void testGetOriginalidade() {
        System.out.println("getOriginalidade");
        SessaoTematica instance = new SessaoTematica();        
        List<SessaoTematica> lst=new ArrayList();
        lst.add(instance);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoOriginalidade(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getOriginalidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getQualidade method, of class SessaoTematica.
     */
    @Test
    public void testGetQualidade() {
         System.out.println("getQualidade");
        SessaoTematica instance = new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(instance);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoApresentacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getQualidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRecomendacao method, of class SessaoTematica.
     */
    @Test
    public void testGetRecomendacao() {
        System.out.println("getRecomendacao");
        SessaoTematica instance = new SessaoTematica();
        List<SessaoTematica> lst=new ArrayList();
        lst.add(instance);
        Revisao rev1=new Revisao();
        rev1.setClassificacaoRecomendacao(2);
        List<Revisao> listr=new ArrayList();
        ListaRevisoes lr=new ListaRevisoes(listr);      
        listr.add(rev1);
        ProcessoDistribuicao pdis=new ProcessoDistribuicao(lr);
        instance.setProcessoDistribuicao(pdis);
        int expResult = 2;
        int result = instance.getRecomendacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroSubmissoesAceites method, of class
     * SessaoTematica.
     */
    @Test
    public void testGetNumeroSubmissoesAceites() {
        System.out.println("getNumeroSubmissoesAceites");
        List<Autor> la = new ArrayList();
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Autor a=new Autor(u1,"ISEP");
        SessaoTematica st1=new SessaoTematica();
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        Submissao sub1 = new Submissao(a1, a1, null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2 = new Submissao(a1, a1, null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3 = new Submissao(a1, a1, null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4 = new Submissao(a1, a1, null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        Submissao sub5 = new Submissao(a1, a1, null);
        sub5.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls = new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        ls.add(sub5);
        ListaSubmissoes lsub=new ListaSubmissoes(ls);
        st1.setState(new SessaoTematicaAceitaSubmissoesState(st1));
        List<SessaoTematica> sst=new ArrayList();
        st1.addSubmissao(sub1, a1, u1);
        st1.addSubmissao(sub2, a1, u1);
        st1.addSubmissao(sub3, a1, u1);
        st1.addSubmissao(sub4, a1, u1);
        st1.addSubmissao(sub5, a1, u1);
        sst.add(st1);
        SessaoTematica instance = new SessaoTematica(lsub);
        int expResult = 3;
        int result = instance.getNumeroSubmissoesAceites();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroTotalSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testGetNumeroTotalSubmissoes() {
        System.out.println("getNumeroTotalSubmissoes");
        List<Autor> la = new ArrayList();
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Autor a=new Autor(u1,"ISEP");
        SessaoTematica st1=new SessaoTematica();
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        Submissao sub1 = new Submissao(a1, a1, null);
        sub1.setState(new SubmissaoAceiteState(sub1));
        Submissao sub2 = new Submissao(a1, a1, null);
        sub2.setState(new SubmissaoRegistadoState(sub2));
        Submissao sub3 = new Submissao(a1, a1, null);
        sub3.setState(new SubmissaoRegistadoState(sub3));
        Submissao sub4 = new Submissao(a1, a1, null);
        sub4.setState(new SubmissaoAceiteState(sub4));
        Submissao sub5 = new Submissao(a1, a1, null);
        sub5.setState(new SubmissaoAceiteState(sub4));
        List<Submissao> ls = new ArrayList();
        ls.add(sub1);
        ls.add(sub2);
        ls.add(sub3);
        ls.add(sub4);
        ls.add(sub5);
        ListaSubmissoes lsub=new ListaSubmissoes(ls);
        st1.setState(new SessaoTematicaAceitaSubmissoesState(st1));
        List<SessaoTematica> sst=new ArrayList();
        st1.addSubmissao(sub1, a1, u1);
        st1.addSubmissao(sub2, a1, u1);
        st1.addSubmissao(sub3, a1, u1);
        st1.addSubmissao(sub4, a1, u1);
        st1.addSubmissao(sub5, a1, u1);
        sst.add(st1);
        SessaoTematica instance = new SessaoTematica(lsub);
        int expResult = 5;
        int result = instance.getNumeroTotalSubmissoes();
        assertEquals(expResult, result);
    }
    /**
     * Test of getListaSubmissoesRegistadasUtilizador method, of class SessaoTematica.
     */
    @Test
    public void testGetListaSubmissoesRegistadasUtilizador() {
        System.out.println("getListaSubmissoesRegistadasUtilizador");
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        String id= u1.getEmail();
        Autor a=new Autor(u1,"ISEP");
        List<Autor> la = new ArrayList();
        SessaoTematica st1=new SessaoTematica();
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf", new AutorCorrespondente(new Autor(u1,"ISEP"), u1));
        Submissao sub = new Submissao(a1, a1, null);
        sub.setState(new SubmissaoRegistadoState(sub));
        SessaoTematica instance = new SessaoTematica();
        instance.addSubmissao(sub, a1, u1);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getListaSubmissoesRegistadasUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of detetarConflitos method, of class SessaoTematica.
     */
    @Test
    public void testDetetarConflitos() {
        System.out.println("detetarConflitos");
        List<TipoConflito> lmc = new ArrayList();
        SessaoTematica instance = new SessaoTematica();
        instance.detetarConflitos(lmc);
        fail("Luis faz isto");
    }

    /**
     * Test of getListaSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testGetListaSubmissoes() {
        System.out.println("getListaSubmissoes");
        SessaoTematica instance = new SessaoTematica();
        List<Submissao> expResult = new ArrayList();
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaTodasRevisoes method, of class SessaoTematica.
     */
    @Test
    public void testGetListaTodasRevisoes() {
        System.out.println("getListaTodasRevisoes");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        Autor a=new Autor(u1,"ISEP");
        List<Autor> la = new ArrayList();
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf");
        a1.setAutorCorrespondente(a,u1); 
        Submissao sub = new Submissao();
        sub.setArtigoFinal(a1);
        Revisao rev= new Revisao();
        rev.setRevisor(new Revisor(u1));
        rev.setSubmissao(sub);
        List<Revisao> list= new ArrayList();
        list.add(rev);
        ListaRevisoes lista= new ListaRevisoes(list);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<Revisao> result = instance.getListaTodasRevisoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of getListaTodosRevisores method, of class SessaoTematica.
     */
    @Test
    public void testGetListaTodosRevisores() {
        System.out.println("getListaTodosRevisores");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        CP cp= new CP();
        cp.addMembroCP(u1);
        instance.setCP(cp);
        List<Revisor> expResult = new ArrayList();
        expResult.add(new Revisor(u1));
        List<Revisor> result = instance.getListaTodosRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoesSessao method, of class SessaoTematica.
     */
    @Test
    public void testGetListaSubmissoesSessao() {
        System.out.println("getListaSubmissoesSessao");
        Utilizador u1= new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        Autor a=new Autor(u1,"ISEP");
        List<Autor> la = new ArrayList();
        la.add(a);
        ListaAutores lar = new ListaAutores(la);
        Artigo a1 = new Artigo(lar, "iOS", "iOS and the future", "IOS.pdf");
        a1.setAutorCorrespondente(a,u1); 
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);
        sub.setAutorCorrespondente(a, u1);
        List<Submissao> lista= new ArrayList();
        lista.add(sub);
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        instance.addAllSubmissoes(lista);
        List<Submissao> result = instance.getListaSubmissoesSessao();
        assertEquals(expResult, result);
    }

    /**
     * Test of isInEmRevisao method, of class SessaoTematica.
     */
    @Test
    public void testIsInEmRevisao() {
        System.out.println("isInEmRevisao");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmRevisaoState(instance));
        boolean expResult = true;
        boolean result = instance.isInEmRevisao();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDataInicio method, of class SessaoTematica.
     */
    @Test
    public void testGetDataInicio() {
        System.out.println("getDataInicio");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(30,6,2015);
        instance.setDataInicio(expResult);
        Data result = instance.getDataInicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataFim method, of class SessaoTematica.
     */
    @Test
    public void testGetDataFim() {
        System.out.println("getDataFim");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(30,6,2015);
        instance.setDataInicio(new Data(29, 6, 2015));
        instance.setDataFim(expResult);
        Data result = instance.getDataFim();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataInicio method, of class SessaoTematica.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        Data di = new Data(30,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(di);
        assertEquals(di,instance.getDataInicio());
    }

    /**
     * Test of setDataFim method, of class SessaoTematica.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        Data df = new Data(31,6,2015);
        Data di = new Data(30,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(di);
        instance.setDataFim(df);
        Data result = instance.getDataFim();
        Data expResult = df;
        assertEquals(expResult,result);
    }

    /**
     * Test of getDataInicioSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataInicioSubmissao() {
        System.out.println("getDataInicioSubmissao");
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        Data expResult = new Data(29,6,2015);
        instance.setDataInicioSubmissao(expResult);
        Data result = instance.getDataInicioSubmissao();
        assertEquals(expResult, result);

    }

    /**
     * Test of setDataInicioSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testSetDataInicioSubmissao() {
        System.out.println("setDataInicioSubmissao");
        Data dis = new Data(29, 6, 2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataInicioSubmissao(dis);
        assertEquals(dis, instance.getDataInicioSubmissao());
    }

    /**
     * Test of getDataFimSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataFimSubmissao() {
        System.out.println("getDataFimSubmissao");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(23,6,2015);
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(expResult);
        instance.setDataInicioDistribuicao(new Data(24, 6, 2015));
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        Data result = instance.getDataFimSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataFimSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testSetDataFimSubmissao() {
        System.out.println("setDataFimSubmissao");
        Data dif = new Data(23,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(dif);
        instance.setDataInicioDistribuicao(new Data(24, 6, 2015));
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        assertEquals(dif,instance.getDataFimSubmissao());
    }

    /**
     * Test of getDataInicioDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataInicioDistribuicao() {
        System.out.println("getDataInicioDistribuicao");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(24,6,2015);
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(expResult);
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        Data result = instance.getDataInicioDistribuicao();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of getListaSubmissoesAceites method, of class SessaoTematica.
     */
    @Test
    public void testGetListaSubmissoesAceites() {
        System.out.println("getListaSubmissoesAceites");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setState(new SubmissaoAceiteState(sub));
        sub.setArtigoInicial(a1);
        instance.addSubmissao(sub,a1, u1);   
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getListaSubmissoesAceites(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of setDataInicioDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testSetDataInicioDistribuicao() {
        System.out.println("setDataInicioDistribuicao");
        Data dd = new Data(24,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(dd);
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        assertEquals(dd, instance.getDataInicioDistribuicao());
    }

    /**
     * Test of getDataLimiteRevisao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataLimiteRevisao() {
        System.out.println("getDataLimiteRevisao");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(26,6,2015);
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(new Data(24,6,2015));
        instance.setDataLimiteRevisao(expResult);
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        Data result = instance.getDataLimiteRevisao();
        assertEquals(expResult, result);

    }

    /**
     * Test of temSubmissoesAceitesUtilizador method, of class SessaoTematica.
     */
    @Test
    public void testTemSubmissoesAceitesUtilizador() {
        System.out.println("temSubmissoesAceitesUtilizador");
         Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        SessaoTematica instance = new SessaoTematica();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setState(new SubmissaoAceiteState(sub));
        sub.setArtigoInicial(a1);
        instance.addSubmissao(sub,a1, u1); 
        boolean expResult = true;
        boolean result = instance.temSubmissoesAceitesUtilizador(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of temTodasRevisoesFeitas method, of class SessaoTematica.
     */
    @Test
    public void testTemTodasRevisoesFeitas() {
        System.out.println("temTodasRevisoesFeitas");
        SessaoTematica instance = new SessaoTematica();
        Revisao rev= new Revisao();
        rev.setTextoExplicativo("Ola");
        List<Revisao> list= new ArrayList();
        ListaRevisoes lista= new ListaRevisoes(list);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        boolean expResult = true;
        boolean result = instance.temTodasRevisoesFeitas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteRevisao method, of class SessaoTematica.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        Data dlr = new Data(26,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(new Data(24,6,2015));
        instance.setDataLimiteRevisao(dlr);
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(new Data(27,6,2015));
        assertEquals(dlr,instance.getDataLimiteRevisao());
    }

    /**
     * Test of getDataLimiteSubmissaoFinal method, of class SessaoTematica.
     */
    @Test
    public void testGetDataLimiteSubmissaoFinal() {
        System.out.println("getDataLimiteSubmissaoFinal");
        SessaoTematica instance = new SessaoTematica();
        Data expResult = new Data(27,6,2015);
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(new Data(24,6,2015));
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(expResult);
        Data result = instance.getDataLimiteSubmissaoFinal();
        assertEquals(expResult, result);

    }

    /**
     * Test of decide method, of class SessaoTematica.
     */
    @Test
    public void testDecide() {
        System.out.println("decide");
                Utilizador u1 = new Utilizador("Jon","Jonathan","wee123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        List<Revisao> lr = new ArrayList();
        SessaoTematica instance = new SessaoTematica();
        instance.addAllSubmissoes(ls);
        instance.setProcessoDecisao(null);
        List<Decisao> expResult = new ArrayList();
        List<Decisao> result = instance.decide(lr);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getRevisoes method, of class SessaoTematica.
     */
    @Test
    public void testGetRevisoes_0args() {
        System.out.println("getRevisoes");
        SessaoTematica instance = new SessaoTematica();
        Revisao rev= new Revisao();
        rev.setTextoExplicativo("Ola");
        List<Revisao> list= new ArrayList();
        ListaRevisoes lista= new ListaRevisoes(list);
        ProcessoDistribuicao pdis= new ProcessoDistribuicao(lista);
        instance.setProcessoDistribuicao(pdis);
        List<Revisao> expResult = list;
        List<Revisao> result = instance.getRevisoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of temTodasSubmissoesCameraReady method, of class SessaoTematica.
     */
    @Test
    public void testTemTodasSubmissoesCameraReady() {
        System.out.println("temTodasSubmissoesCameraReady");
        SessaoTematica instance = new SessaoTematica();
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setState(new SubmissaoEmCameraReadyState(sub));
        sub.setArtigoInicial(a1);
        instance.addSubmissao(sub,a1, u1); 
        boolean expResult = true;
        boolean result = instance.temTodasSubmissoesCameraReady();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteSubmissaoFinal method, of class SessaoTematica.
     */
    @Test
    public void testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        Data dlsf = new Data(27,6,2015);
        SessaoTematica instance = new SessaoTematica();
        instance.setDataInicio(new Data(30, 6,2015));
        instance.setDataInicioSubmissao(new Data(22,6,2015));
        instance.setDataFimSubmissao(new Data(23,6,2015));
        instance.setDataInicioDistribuicao(new Data(24,6,2015));
        instance.setDataLimiteRevisao(new Data(26,6,2015));
        instance.setDataFim(new Data(2,7,2015));
        instance.setDataLimiteSubmissaoFinal(dlsf);
        assertEquals(dlsf,instance.getDataLimiteSubmissaoFinal());
    }

    /**
     * Test of validaProponente method, of class SessaoTematica.
     */
    @Test
    public void testValidaProponente() {
        System.out.println("validaProponente");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador u = u1;
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = true;
        boolean result = instance.validaProponente(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testGetSubmissoes() {
        System.out.println("getSubmissoes");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setState(new SubmissaoEmCameraReadyState(sub));
        sub.setArtigoInicial(a1);
        instance.addSubmissao(sub,a1, u1); 
        List<Submissao> expResult = new ArrayList();
        expResult.add(sub);
        List<Submissao> result = instance.getSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class SessaoTematica.
     */
    @Test
    public void testValida_0args() {
        System.out.println("valida");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        SessaoTematica instance = new SessaoTematica();
        instance.addProponente(u1);
        List<Revisor> lista= new ArrayList();
        lista.add(new Revisor(u1));
        instance.setCP(new CP(lista));
        boolean expResult = true;
        boolean result = instance.valida();
       
        assertEquals(expResult, result);
    }

    /**
     * Test of getPosition method, of class SessaoTematica.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setState(new SubmissaoEmCameraReadyState(sub));
        sub.setArtigoInicial(a1);
        SessaoTematica instance = new SessaoTematica();
        instance.addSubmissao(sub,a1, u1);     
        int expResult = 0;
        int result = instance.getPosition(sub);
        assertEquals(expResult, result);

    }

    /**
     * Test of valida method, of class SessaoTematica.
     */
    @Test
    public void testValida_Submissao_int() {
        System.out.println("valida");
        Utilizador u1 = new Utilizador("Jon","Jonathan","wee123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        int i = 0;
        SessaoTematica instance = new SessaoTematica();
        instance.addAllSubmissoes(ls);
        boolean expResult = false;
        boolean result = instance.valida(sub1, i);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisoes method, of class SessaoTematica.
     */
    @Test
    public void testGetRevisoes_String() {
        System.out.println("getRevisoes");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        String id = u1.getEmail();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);  
        SessaoTematica instance = new SessaoTematica();
        instance.addSubmissao(sub,a1, u1); 
        Revisao rev= new Revisao();
        rev.setSubmissao(sub);
        rev.setRevisor(new Revisor(u1));
        List<Revisao> list= new ArrayList();
        list.add(rev);
        ListaRevisoes lista= new ListaRevisoes(list);
        instance.setProcessoDistribuicao(new ProcessoDistribuicao(lista));
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<Revisao> result = instance.getRevisoes(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getLicitacoes method, of class SessaoTematica.
     */
    @Test
    public void testGetLicitacoes() {
        System.out.println("getLicitacoes");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor r= new Revisor(u1);
        String id = u1.getEmail();
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);  
        SessaoTematica instance = new SessaoTematica();
        List<Submissao> list= new ArrayList();
        Licitacao lic= new Licitacao(sub, r, null);
        sub.novaLicitacao(r, null);
        sub.registaLicitacao(lic);
        instance.addSubmissao(sub,a1, u1); 
        list.add(sub);
        List<Licitacao> expResult = new ArrayList();
        expResult.add(lic);
        List<Licitacao> result = instance.getLicitacoes();
        assertEquals(expResult, result);

    }

    /**
     * Test of setEmCameraReady method, of class SessaoTematica.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        SessaoTematica instance = new SessaoTematica();
        instance.setState(new SessaoTematicaEmSubmissaoCameraReadyState(instance));
        boolean expResult = true;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaAlteracao method, of class SessaoTematica.
     */
    @Test
    public void testRegistaAlteracao() {
        System.out.println("registaAlteracao");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);  
        SessaoTematica instance = new SessaoTematica();
        instance.addSubmissao(sub, a1, u1);
        boolean expResult = true;
        boolean result = instance.registaAlteracao(sub);
        assertEquals(expResult, result);

    }

    /**
     * Test of getAcceptedTopics method, of class SessaoTematica.
     */
    @Test
    public void testGetAcceptedTopics() {
        System.out.println("getAcceptedTopics");
        ArrayList<String> ls = null;
        SessaoTematica instance = new SessaoTematica();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getAcceptedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRejectedTopics method, of class SessaoTematica.
     */
    @Test
    public void testGetRejectedTopics() {
        System.out.println("getRejectedTopics");
        ArrayList<String> ls = null;
        SessaoTematica instance = new SessaoTematica();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getRejectedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStatsTopics method, of class SessaoTematica.
     */
    @Test
    public void testGetStatsTopics() {
        System.out.println("getStatsTopics");
        ArrayList ls = null;
        SessaoTematica instance = new SessaoTematica();
        ArrayList expResult = null;
        ArrayList result = instance.getStatsTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addAllSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testAddAllSubmissoes() {
        System.out.println("addAllSubmissoes");
        Artigo a1 = new Artigo();
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1); 
        
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        SessaoTematica instance = new SessaoTematica();
        instance.addSubmissao(sub, a1, u1);
        instance.addAllSubmissoes(ls);
    }

    /**
     * Test of addGeneratedSubmission method, of class SessaoTematica.
     */
    @Test
    public void testAddGeneratedSubmission() {
        System.out.println("addGeneratedSubmission");
        Submissao s = null;
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = false;
        boolean result = instance.addGeneratedSubmission(s);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showData method, of class SessaoTematica.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        SessaoTematica instance = new SessaoTematica();
        String expResult = "CP nula\nN/A\nN/A\n0\n";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaProponentes method, of class SessaoTematica.
     */
    @Test
    public void testGetListaProponentes() {
        System.out.println("getListaProponentes");
        SessaoTematica instance = new SessaoTematica();
        List<Proponente> expResult = null;
        List<Proponente> result = instance.getListaProponentes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getState method, of class SessaoTematica.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        SessaoTematica instance = new SessaoTematica();
        instance.setAceitaSubmissoes();
        String result = instance.getState();
        String expResult = new SessaoTematicaAceitaSubmissoesState(instance).toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of temRevisoes method, of class SessaoTematica.
     */
    @Test
    public void testTemRevisoes() {
        System.out.println("temRevisoes");
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = false;
        boolean result = instance.temRevisoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSubmissoesAceites method, of class SessaoTematica.
     */
    @Test
    public void testTemSubmissoesAceites() {
        System.out.println("temSubmissoesAceites");
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = false;
        boolean result = instance.temSubmissoesAceites();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of temSubmissoes method, of class SessaoTematica.
     */
    @Test
    public void testTemSubmissoes() {
        System.out.println("temSubmissoes");
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = false;
        boolean result = instance.temSubmissoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class SessaoTematica.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        SessaoTematica instance = new SessaoTematica();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
