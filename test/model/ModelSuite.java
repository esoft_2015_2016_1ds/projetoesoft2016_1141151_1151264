/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author AndreFilipeRamosViei
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({LocalTest.class, CPTest.class, SessaoTematicaTest.class, ProcessoDetecaoTest.class, TipoConflitoTest.class, UtilizadorTest.class, LicitacaoTest.class, DecisaoTest.class, AutorTest.class, ProcessoDistribuicaoTest.class, AutorCorrespondenteTest.class, SubmissaoTest.class, AlertaTest.class, EventoTest.class, AdministradorTest.class, EmpresaTest.class, OrganizadorTest.class, ConflitoDetetadoTest.class, ArtigoTest.class, ProcessoDecisaoTest.class, ProponenteTest.class, RevisorTest.class, RevisaoTest.class})
public class ModelSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
