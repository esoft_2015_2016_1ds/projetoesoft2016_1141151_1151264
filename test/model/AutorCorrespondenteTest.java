/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class AutorCorrespondenteTest {
    
    public AutorCorrespondenteTest() {
    }
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setUtilizador method, of class AutorCorrespondente.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador u = u1;
        AutorCorrespondente instance = new AutorCorrespondente();
        instance.setUtilizador(u);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(u1,instance.getUtilizador());
    }

    /**
     * Test of getAutor method, of class AutorCorrespondente.
     */
    @Test
    public void testGetAutor() {
        System.out.println("getAutor");
        AutorCorrespondente instance = new AutorCorrespondente(new Autor(u1,"UnknownPlace"),u1);
        Autor expResult = new Autor(u1,"UnknownPlace");
        Autor result = instance.getAutor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getUtilizador method, of class AutorCorrespondente.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Autor autor = new Autor(u2,"UnknownPlace");
        AutorCorrespondente instance = new AutorCorrespondente(autor,u2);
        Utilizador expResult = u2;
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class AutorCorrespondente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor autor = new Autor(u2,"UnknownPlace");
        AutorCorrespondente instance = new AutorCorrespondente(autor,u2);
        String expResult = "Autor: adminFunny - adminB@gmail.com - UnknownPlace - adminB;Utilizador: adminFunny/adminB/adminB@gmail.com/1e44a5sy";
        String result = instance.toString();
        System.out.println(instance.toString());
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class AutorCorrespondente.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        AutorCorrespondente instance = new AutorCorrespondente();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class AutorCorrespondente.
     * In this case, the other object is null.
     */
    @Test
    public void testEqualsNullOther() {
        System.out.println("equals");
        Object other = null;
        AutorCorrespondente instance = new AutorCorrespondente();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    /**
     * Test of equals method, of class AutorCorrespondente.
     * In this case, the other is equal.
     */
    @Test
    public void testEqualsValid() {
        System.out.println("equals");
        Autor au = new Autor("José","marcos@gmail.com","Porto","Josezinator");
        Object other = new AutorCorrespondente(au,new Utilizador("Marcos","MarcosMendes","123passsecure","marcos@gmail.com",new NaoFazNada()));
        AutorCorrespondente instance = new AutorCorrespondente(au,new Utilizador("Marcos","MarcosMendes","123passsecure","marcos@gmail.com",new NaoFazNada()));
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of equals method, of class AutorCorrespondente.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador u1=new Utilizador("Marcos","MarcosMendes","123passsecure","marcos@gmail.com",new NaoFazNada());
        Object other = new AutorCorrespondente(u1);
        AutorCorrespondente instance = new AutorCorrespondente(u1);
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);

    }
}
