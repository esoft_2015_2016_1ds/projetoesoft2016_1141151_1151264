/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaConflitosDetetados;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class DecisaoTest {
    
    public DecisaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setClassificacao method, of class Decisao.
     */
    @Test
    public void testSetClassificacao() {
        System.out.println("setClassificacao");
        int value = 2;
        Decisao instance = new Decisao();
        instance.setClassificacao(value);
        int result = instance.getClassificacao();
        assertEquals(value,result);
    }

    /**
     * Test of showData method, of class Decisao.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Decisao instance = new Decisao();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getClassificacao method, of class Decisao.
     */
    @Test
    public void testGetClassificacao() {
        System.out.println("getClassificacao");
        Decisao instance = new Decisao();
        int expResult = 50;
        instance.setClassificacao(50);
        int result = instance.getClassificacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSubmissao method, of class Decisao.
     */
    @Test
    public void testSetSubmissao() {
        System.out.println("setSubmissao");
                        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        Decisao instance = new Decisao();
        instance.setSubmissao(sub);
        Submissao expResult = sub;
        Submissao result = instance.getSubmissao();
        assertEquals(expResult,result);
    }

    /**
     * Test of setVeredito method, of class Decisao.
     */
    @Test
    public void testSetVeredito() {
        System.out.println("setVeredito");
        boolean ver = true;
        Decisao instance = new Decisao();
        instance.setVeredito(ver);
        boolean result = instance.getVeredito();
        boolean expResult = ver;
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getVeredito method, of class Decisao.
     */
    @Test
    public void testGetVeredito() {
        System.out.println("getVeredito");
        Decisao instance = new Decisao();
        boolean expResult = false;
        boolean result = instance.getVeredito();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getSubmissao method, of class Decisao.
     */
    @Test
    public void testGetSubmissao() 
    {
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("getSubmissao");
        Decisao instance = new Decisao();
        instance.setSubmissao(sub);
        Submissao expResult = sub;
        Submissao result = instance.getSubmissao();
        assertEquals(expResult, result);
        
    }
    
}
