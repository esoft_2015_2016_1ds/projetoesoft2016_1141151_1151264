/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbraga
 */
public class LocalTest {
    
    public LocalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setLocal method, of class Local.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String strLocal = "Rua Padua Correia";
        Local instance = new Local();
        instance.setLocal(strLocal);
    }

    /**
     * Test of toString method, of class Local.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Local instance = new Local("Rua Padua Correia");
        String expResult = "null/Rua Padua Correia";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocal method, of class Local.
     */
    @Test
    public void testGetLocal() {
        System.out.println("getLocal");
        Local instance = new Local("Rua Padua Correia");
        String expResult = "Rua Padua Correia";
        String result = instance.getLocal();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Local.
     */
    @Test
    public void testValidaComLocal() {
        System.out.println("valida");
        Local instance = new Local("Rua Padua Correia");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Teste do metodo valida da classe Local com o local vazio
     */
    @Test
    public void testValidaSemLocaal(){
        System.out.println("valida");
        Local instance = new Local();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }


    /**
     * Test of showData method, of class Local.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Local instance = new Local("Rua Padua Correia");
        String expResult = "null/Rua Padua Correia";
        String result = instance.showData();
        assertEquals(expResult, result);

    }
    
    /**
     * Test of showData method, with an empty location
     */
    @Test
    public void testShowDataEmpty() {
        System.out.println("showDataEmpty");
        Local instance = new Local();
        String expResult = "null/null";
        String result = instance.showData();
        assertEquals(expResult, result);

    }

    /**
     * Test of valida method, of class Local.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Local instance = new Local();
        instance.setLocal("My Local");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, with a null local atribute
     */
    @Test
    public void testValidaNull() {
        System.out.println("valida");
        Local instance = new Local();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Local.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Local instance = new Local();
        String expResult = "123";
        instance.setId("123");
        String result = instance.getId();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getId method, of class Local.
     */
    @Test
    public void testGetIdNulll() {
        System.out.println("getId");
        Local instance = new Local();
        String expResult = null;
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Local.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        String id = "124";
        Local instance = new Local();
        instance.setId(id);
    }
    
}
