/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.SubmissaoState;
import java.util.ArrayList;
import java.util.List;
import listas.ListaConflitosDetetados;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.submissao.SubmissaoAceiteState;
import states.submissao.SubmissaoRegistadoState;

/**
 *
 * @author jbraga
 */
public class SubmissaoTest {
    
    public SubmissaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of alteraDados method, of class Submissao.
     */
    @Test
    public void testAlteraDados() {
        System.out.println("alteraDados");
        String titulo = "";
        String resumo = "";
        List<Autor> autores = null;
        String ficheiro = null;
        Submissao instance = new Submissao();
        instance.alteraDados(titulo, resumo, autores, ficheiro);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novoArtigo method, of class Submissao.
     */
    @Test
    public void testNovoArtigo() {
        System.out.println("novoArtigo");
        Submissao instance = new Submissao();
        boolean expResult =true;
        Artigo data = instance.novoArtigo();
        boolean result = data!=null;
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getArtigo method, of class Submissao.
     */
    @Test
    public void testGetArtigo() {
        System.out.println("getArtigo");
        Submissao instance = new Submissao();
        Artigo expResult = null;
        //Artigo result = instance.getArtigo();
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getInfo method, of class Submissao.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Submissao instance = new Submissao();
        String expResult = "";
        String result = instance.getInfo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigo method, of class Submissao.
     */
    @Test
    public void testSetArtigo() {
        System.out.println("setArtigo");
        Artigo artigo = null;
        Submissao instance = new Submissao();
        //instance.setArtigo(artigo);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoTitle method, of class Submissao.
     */
    @Test
    public void testSetArtigoTitle() {
        System.out.println("setArtigoTitle");
        String title = "Polaroid";
        Submissao instance = new Submissao();
        instance.setArtigoTitle(title);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoSummary method, of class Submissao.
     */
    @Test
    public void testSetArtigoSummary() {
        System.out.println("setArtigoSummary");
        String summary = "Sunwise";
        Submissao instance = new Submissao();
        instance.setArtigoSummary(summary);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoData method, of class Submissao.
     */
    @Test
    public void testSetArtigoData() {
        System.out.println("setArtigoData");
        String title = "ABC123";
        String summary = "Kindergarten.";
        String file = "kids.pdf";
        Submissao instance = new Submissao();
        instance.setArtigoData(title, summary, file);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setFile method, of class Submissao.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        String file = null;
        Submissao instance = new Submissao();
        instance.setFile(file);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAutorCorrespondente method, of class Submissao.
     */
    @Test
    public void testSetAutorCorrespondente() {
        System.out.println("setAutorCorrespondente");
        Autor author = null;
        Utilizador user = null;
        Submissao instance = new Submissao();
        instance.setAutorCorrespondente(author, user);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of newAutor method, of class Submissao.
     */
    @Test
    public void testNewAutor() {
        System.out.println("newAutor");
        String name = "";
        String email = "";
        String institution = "";
        Submissao instance = new Submissao();
        Autor expResult = null;
        //Autor result = instance.newAutor(name, email, institution);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addAutor method, of class Submissao.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor author = null;
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.addAutor(author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }



    /**
     * Test of removeAutor method, of class Submissao.
     */
    @Test
    public void testRemoveAutor() {
        System.out.println("removeAutor");
        Autor author = null;
        Submissao instance = new Submissao();
        instance.removeAutor(author);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of valida method, of class Submissao.
     */
    @Test
    public void testValidaInicial() {
        System.out.println("validaInicial");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.validaInicial();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class Submissao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Submissao instance = new Submissao();
        String expResult = "Submissão:\n" +
        "ArtigoInicial:Título: null;Resumo: null;Autor Correspondente: null;Ficheiro: null;Lista de autores:\n" +
        "ArtigoFinal:Título: null;Resumo: null;Autor Correspondente: null;Ficheiro: null;Lista de autores:";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setState method, of class Submissao.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        SubmissaoState state = null;
        Submissao instance = new Submissao();
        instance.setState(state);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getState method, of class Submissao.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Submissao instance = new Submissao();
        String expResult = "SubmissaoStateCriado";
        String result = instance.getState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addConflitoDetetado method, of class Submissao.
     */
    @Test
    public void testAddConflitoDetetado() {
        System.out.println("addConflitoDetetado");
        ConflitoDetetado c = null;
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.addConflitoDetetado(c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInCriadoState method, of class Submissao.
     */
    @Test
    public void testIsInCriadoState() {
        System.out.println("isInCriadoState");
        Submissao instance = new Submissao();
        boolean expResult = true;
        boolean result = instance.isInCriadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRegistadoState method, of class Submissao.
     */
    @Test
    public void testIsInRegistadoState() {
        System.out.println("isInRegistadoState");
        Submissao instance = new Submissao();
        instance.setState(new SubmissaoRegistadoState(instance));
        boolean expResult = true;
        boolean result = instance.isInRegistadoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of isInAceiteState method, of class Submissao.
     */
    @Test
    public void testIsInAceiteState() {
        System.out.println("isInAceiteState");
        Submissao instance = new Submissao();
        instance.setState(new SubmissaoAceiteState(instance));
        boolean expResult = true;
        boolean result = instance.isInAceiteState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmCameraReadyState method, of class Submissao.
     */
    @Test
    public void testIsInEmCameraReadyState() {
        System.out.println("isInEmCameraReadyState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInEmCameraReadyState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmLicitacaoState method, of class Submissao.
     */
    @Test
    public void testIsInEmLicitacaoState() {
        System.out.println("isInEmLicitacaoState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInEmLicitacaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInNaoRevistoState method, of class Submissao.
     */
    @Test
    public void testIsInNaoRevistoState() {
        System.out.println("isInNaoRevistoState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInNaoRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInPreRevistaState method, of class Submissao.
     */
    @Test
    public void testIsInPreRevistaState() {
        System.out.println("isInPreRevistaState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInPreRevistaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInEmRevisaoState method, of class Submissao.
     */
    @Test
    public void testIsInEmRevisaoState() {
        System.out.println("isInEmRevisaoState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInEmRevisaoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRejeitadaState method, of class Submissao.
     */
    @Test
    public void testIsInRejeitadaState() {
        System.out.println("isInRejeitadaState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInRejeitadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRevistoState method, of class Submissao.
     */
    @Test
    public void testIsInRevistoState() {
        System.out.println("isInRevistoState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class Submissao.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Submissao instance = new Submissao();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getArtigoInicial method, of class Submissao.
     */
    @Test
    public void testGetArtigoInicial() {
        System.out.println("getArtigoInicial");
        Submissao instance = new Submissao();
        Artigo expResult = null;
        Artigo result = instance.getArtigoInicial();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getArtigoFinal method, of class Submissao.
     */
    @Test
    public void testGetArtigoFinal() {
        System.out.println("getArtigoFinal");
        Submissao instance = new Submissao();
        Artigo expResult = null;
        Artigo result = instance.getArtigoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoInicial method, of class Submissao.
     */
    @Test
    public void testSetArtigoInicial() {
        System.out.println("setArtigoInicial");
        Artigo artigo = null;
        Submissao instance = new Submissao();
        instance.setArtigoInicial(artigo);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setArtigoFinal method, of class Submissao.
     */
    @Test
    public void testSetArtigoFinal() {
        System.out.println("setArtigoFinal");
        Artigo artigo = null;
        Submissao instance = new Submissao();
        instance.setArtigoFinal(artigo);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getLicitacaoDe method, of class Submissao.
     */
    @Test
    public void testGetLicitacaoDe() {
        System.out.println("getLicitacaoDe");
        String id = "";
        Submissao instance = new Submissao();
        Licitacao expResult = null;
        Licitacao result = instance.getLicitacaoDe(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getTiposConflito method, of class Submissao.
     */
    @Test
    public void testGetTiposConflito() {
        System.out.println("getTiposConflito");
        String id = "";
        Submissao instance = new Submissao();
        List<TipoConflito> expResult = null;
        List<TipoConflito> result = instance.getTiposConflito(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setSubmissaoRegistadaState method, of class Submissao.
     */
    @Test
    public void testSetSubmissaoRegistadaState() {
        System.out.println("setSubmissaoRegistadaState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setSubmissaoRegistadaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novaLicitacao method, of class Submissao.
     */
    @Test
    public void testNovaLicitacao() {
        System.out.println("novaLicitacao");
        Revisor r = new Revisor();
        List<TipoConflito> ltc = new ArrayList();
        Submissao instance = new Submissao();
        boolean expResult = true;
        Licitacao data = instance.novaLicitacao(r, ltc);
        boolean result = data!=null;
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of registaLicitacao method, of class Submissao.
     */
    @Test
    public void testRegistaLicitacao() {
        System.out.println("registaLicitacao");
        Licitacao l = null;
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.registaLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temAutorArtigoInicial method, of class Submissao.
     */
    @Test
    public void testTemAutorArtigoInicial() {
        System.out.println("temAutorArtigoInicial");
        String id = "";
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.temAutorArtigoInicial(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setRemovida method, of class Submissao.
     */
    @Test
    public void testSetRemovida() {
        System.out.println("setRemovida");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setRemovida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRemovidaState method, of class Submissao.
     */
    @Test
    public void testIsInRemovidaState() {
        System.out.println("isInRemovidaState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInRemovidaState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInAceiteNaoFinal method, of class Submissao.
     */
    @Test
    public void testIsInAceiteNaoFinal() {
        System.out.println("isInAceiteNaoFinal");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.isInAceiteNaoFinal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novaListaConflitos method, of class Submissao.
     */
    @Test
    public void testNovaListaConflitos() {
        System.out.println("novaListaConflitos");
        Submissao instance = new Submissao();
        boolean expResult = true;
        ListaConflitosDetetados data = instance.novaListaConflitos();
        boolean result = data!=null;
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of criarClone method, of class Submissao.
     */
    @Test
    public void testCriarClone() {
        System.out.println("criarClone");
        Submissao instance = new Submissao();
        Submissao expResult = null;
        Submissao result = instance.criarClone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of alteraDados method, of class Submissao.
     */
    @Test
    public void testAlteraDados_4args() {
        System.out.println("alteraDados");
        String titulo = "";
        String resumo = "";
        List<Autor> autores = null;
        String ficheiro = "";
        Submissao instance = new Submissao();
        instance.alteraDados(titulo, resumo, autores, ficheiro);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of alteraDados method, of class Submissao.
     */
    @Test
    public void testAlteraDados_3args() {
        System.out.println("alteraDados");
        String titulo = "";
        String resumo = "";
        String ficheiro = "";
        Submissao instance = new Submissao();
        instance.alteraDados(titulo, resumo, ficheiro);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLicitacoes method, of class Submissao.
     */
    @Test
    public void testGetLicitacoes() {
        System.out.println("getLicitacoes");
        Submissao instance = new Submissao();
        List<Licitacao> expResult = null;
        List<Licitacao> result = instance.getLicitacoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoAutor method, of class Submissao.
     */
    @Test
    public void testNovoAutor() {
        System.out.println("novoAutor");
        String username = "";
        String email = "";
        String nome = "";
        String afiliacao = "";
        Submissao instance = new Submissao();
        Autor expResult = null;
        Autor result = instance.novoAutor(username, email, nome, afiliacao);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmCameraReady method, of class Submissao.
     */
    @Test
    public void testSetEmCameraReady() {
        System.out.println("setEmCameraReady");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setEmCameraReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAceite method, of class Submissao.
     */
    @Test
    public void testSetAceite() {
        System.out.println("setAceite");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setAceite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRejeitada method, of class Submissao.
     */
    @Test
    public void testSetRejeitada() {
        System.out.println("setRejeitada");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setRejeitada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPreRevistoState method, of class Submissao.
     */
    @Test
    public void testSetPreRevistoState() {
        System.out.println("setPreRevistoState");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setPreRevistoState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmRevisao method, of class Submissao.
     */
    @Test
    public void testSetEmRevisao() {
        System.out.println("setEmRevisao");
        Submissao instance = new Submissao();
        boolean expResult = false;
        boolean result = instance.setEmRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAceptedTopics method, of class Submissao.
     */
    @Test
    public void testGetAceptedTopics() {
        System.out.println("getAceptedTopics");
        ArrayList<String> ls = null;
        Submissao instance = new Submissao();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getAceptedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRejectedTopics method, of class Submissao.
     */
    @Test
    public void testGetRejectedTopics() {
        System.out.println("getRejectedTopics");
        ArrayList<String> ls = null;
        Submissao instance = new Submissao();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getRejectedTopics(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaAutores method, of class Submissao.
     */
    @Test
    public void testGetListaAutores() {
        System.out.println("getListaAutores");
        Submissao instance = new Submissao();
        List<Autor> expResult = null;
        List<Autor> result = instance.getListaAutores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
