/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.Data;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ArtigoTest {
    
    public ArtigoTest() {
    }
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setTitulo method, of class Artigo.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "Pokémon";
        Artigo instance = new Artigo();
        instance.setTitulo(strTitulo);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getTitulo(),strTitulo);
    }

    /**
     * Test of setResumo method, of class Artigo.
     */
    @Test
    public void testSetResumo() {
        System.out.println("setResumo");
        String strResumo = "Pika-pikachu.";
        Artigo instance = new Artigo();
        instance.setResumo(strResumo);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(strResumo,instance.getResumo());
    }

    /**
     * Test of setAutores method, of class Artigo.
     */
    @Test
    public void testSetAutores() {
        System.out.println("setAutores");
        List<Autor> autores = new ArrayList();
        autores.add(new Autor(u1,"UnknownPlace"));
        Artigo instance = new Artigo();
        instance.setAutores(autores);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getListaAutores(),autores);
    }

    /**
     * Test of setAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testSetAutorCorrespondente_AutorCorrespondente() {
        System.out.println("setAutorCorrespondente");
        Autor autor = new Autor(u1,"UnknownPlace");
        AutorCorrespondente autorC = new AutorCorrespondente(autor,u1);
        Artigo instance = new Artigo();
        instance.addAutor(new Autor(u1,"UnknownPlace"));
        instance.setAutorCorrespondente(autorC);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(autorC,instance.getAutorCorrespondente());
    }

    /**
     * Test of setAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testSetAutorCorrespondente_Autor_Utilizador() {
        System.out.println("setAutorCorrespondente");
        Autor author = new Autor(u1,"UnknownPlace");
        Utilizador u = u1;
        Artigo instance = new Artigo();
        instance.addAutor(author);
        instance.setAutorCorrespondente(author, u);
        AutorCorrespondente autorC = new AutorCorrespondente(author,u1);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(autorC,instance.getAutorCorrespondente());
    }

    /**
     * Test of setFicheiro method, of class Artigo.
     */
    @Test
    public void testSetFicheiro() {
        System.out.println("setFicheiro");
        String file = "Biologia.pdf";
        Artigo instance = new Artigo();
        instance.setFicheiro(file);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(file,instance.getFicheiro());
    }

    /**
     * Test of getTitulo method, of class Artigo.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Artigo instance = new Artigo();
        instance.setTitulo("Marés Vivas");
        String expResult = "Marés Vivas";
        String result = instance.getTitulo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getResumo method, of class Artigo.
     */
    @Test
    public void testGetResumo() {
        System.out.println("getResumo");
        Artigo instance = new Artigo();
        instance.setResumo("The cosine of u root, divided by...");
        String expResult = "The cosine of u root, divided by...";
        String result = instance.getResumo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaAutores method, of class Artigo.
     */
    @Test
    public void testGetListaAutores() {
        System.out.println("getListaAutores");
        Artigo instance = new Artigo();
        instance.addAutor(new Autor(u1,"UnknownPlace"));
        ArrayList<Autor> expResult = new ArrayList();
        expResult.add(new Autor(u1,"UnknownPlace"));
        ArrayList<Autor> result = instance.getListaAutores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getFicheiro method, of class Artigo.
     */
    @Test
    public void testGetFicheiro() {
        System.out.println("getFicheiro");
        Artigo instance = new Artigo();
        instance.setFicheiro("Reinforcement Learning.pdf");
        String expResult = "Reinforcement Learning.pdf";
        String result = instance.getFicheiro();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testGetAutorCorrespondente() {
        System.out.println("getAutorCorrespondente");
        Artigo instance = new Artigo();
        Autor aut = new Autor(u1,"UnknownPlace");
        instance.addAutor(new Autor(u1,"UnknownPlace"));
        instance.setAutorCorrespondente(new AutorCorrespondente(aut,u1));
        AutorCorrespondente expResult = new AutorCorrespondente(aut,u1);
        AutorCorrespondente result = instance.getAutorCorrespondente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutor() {
        System.out.println("novoAutor");
        String strNome = "";
        String email = "";
        String strAfiliacao = "";
        Artigo instance = new Artigo();
        Autor expResult = null;
        //Autor result = instance.novoAutor(strNome, email, strAfiliacao);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addAutor method, of class Artigo.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = null;
        Artigo instance = new Artigo();
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of removeAutor method, of class Artigo.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testRemoveAutor() {
        System.out.println("removeAutor");
        Autor author = null;
        Artigo instance = new Artigo();
        instance.removeAutor(author);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getInfo method, of class Artigo.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Artigo instance = new Artigo();
        Autor author = new Autor(u1,"UnknownPlace");
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        instance.setTitulo(title);
        instance.setResumo(summary);
        instance.setFicheiro(file);
        instance.addAutor(author);
        instance.setAutorCorrespondente(author,u1);
        String expResult = "Título: "+title+";Resumo: "+summary+";Autor Correspondente: "
                +"Autor: "+author+";Utilizador: "+u1+";Ficheiro: "+file+";Lista de autores:"+author;
        String result = instance.getInfo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getListaAutorsAsString method, of class Artigo.
     */
    @Test
    public void testGetListaAutorsAsString() {
        System.out.println("getListaAutorsAsString");
        Artigo instance = new Artigo();
        String expResult = "";
        String result = instance.getListaAutorsAsString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of valida method, of class Artigo.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Artigo instance = new Artigo();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class Artigo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Artigo instance = new Artigo();
        Autor author = new Autor(u1,"UnknownPlace");
        String title = "Experiment Title";
        String summary = "Summary example.";
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        instance.setTitulo(title);
        instance.setResumo(summary);
        instance.setFicheiro(file);
        instance.addAutor(author);
        instance.setAutorCorrespondente(author,u1);
        String expResult = "Título: "+title+";Resumo: "+summary+";Autor Correspondente: "
                +"Autor: "+author+";Utilizador: "+u1+";Ficheiro: "+file+";Lista de autores:"+author;
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Artigo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        Artigo instance = new Artigo();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class Artigo.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Artigo instance = new Artigo();
        String expResult="\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setAutorCriador method, of class Artigo.
     */
    @Test
    public void testSetAutorCriador() {
        System.out.println("setAutorCriador");
        Utilizador u = null;
        Artigo instance = new Artigo();
        instance.setAutorCriador(u);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setDataCriado method, of class Artigo.
     */
    @Test
    public void testSetDataCriado() {
        System.out.println("setDataCriado");
        Data value = null;
        Artigo instance = new Artigo();
        instance.setDataCriado(value);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of addUtilizadorComoAutor method, of class Artigo.
     */
    @Test
    public void testAddUtilizadorComoAutor() {
        System.out.println("addUtilizadorComoAutor");
        Utilizador u = u1;
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.addUtilizadorComoAutor(u,"UnknownPlace");
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class Artigo.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Artigo instance = new Artigo();
        Autor aut = new Autor(u1,"UnknownPlace");
        instance.addAutor(aut);
        List<Autor> expResult = new ArrayList();
        expResult.add(aut);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes(regU);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of temAutor method, of class Artigo.
     */
    @Test
    public void testTemAutor() {
        System.out.println("temAutor");
        String id = "";
        Artigo instance = new Artigo();
        boolean expResult = false;
        boolean result = instance.temAutor(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getPalavrasChave method, of class Artigo.
     */
    @Test
    public void testGetPalavrasChave() {
        System.out.println("getPalavrasChave");
        Artigo instance = new Artigo();
        List<String> k = new ArrayList();
        k.add("Coldplay");
        k.add("is");
        k.add("Mediocre");
        instance.setPalavrasChave(k);
        List<String> expResult = k;
        List<String> result = instance.getPalavrasChave();
        assertEquals(expResult, result);

    }

    /**
     * Test of setPalavrasChave method, of class Artigo.
     */
    @Test
    public void testSetPalavrasChave() {
        System.out.println("setPalavrasChave");
        List<String> k = new ArrayList();
        k.add("Coldplay");
        k.add("is");
        k.add("Mediocre");
        Artigo instance = new Artigo();
        instance.setPalavrasChave(k);
        List<String> expResult = k;
        List<String> result = instance.getPalavrasChave();
        assertEquals(expResult, result);
    } 

    /**
     * Test of getTopicos method, of class Artigo.
     */
    @Test
    public void testGetTopicos() {
        System.out.println("getTopicos");
        Artigo instance = new Artigo();
        ArrayList<String> string= new ArrayList();
        string.add("cidkc");
        instance.setPalavrasChave(string);
        ArrayList<String> expResult = string;
        ArrayList<String> result = instance.getTopicos();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of getAutorCriador method, of class Artigo.
     */
    @Test
    public void testGetAutorCriador() {
        System.out.println("getAutorCriador");
        Artigo instance = new Artigo();
        instance.setAutorCriador(u1);
        Utilizador expResult = u1;
        Utilizador result = instance.getAutorCriador();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataCriado method, of class Artigo.
     */
    @Test
    public void testGetDataCriado() {
        System.out.println("getDataCriado");
        Artigo instance = new Artigo();
        Data k = new Data(12,12,12);
        instance.setDataCriado(k);
        Data expResult = new Data(12,12,12);
        Data result = instance.getDataCriado();
        assertEquals(expResult, result);
    }
    
}
