/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class OrganizadorTest {
    
    public OrganizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of valida method, of class Organizador.
     */
    @Test
    public void testValidaComUtilizador() {
        System.out.println("valida");
        Organizador instance = new Organizador(new Utilizador("Gonçalo", "greis", "BABAAB1", "greis@email.com", new NaoFazNada()));
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUtilizador method, of class Organizador.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador expResult = u1;
        Utilizador result = new Organizador(u1).getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Organizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Organizador instance = new Organizador(u1);
        String expResult = u1.toString();
        String result = new Organizador(u1).toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Organizador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Organizador instance = new Organizador(u1);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of getUserEmail method, of class Organizador.
     */
    @Test
    public void testGetUserEmail() {
        System.out.println("getUserEmail");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Organizador instance = new Organizador(u1);
        String expResult = u1.getEmail();
        String result = instance.getUserEmail();
        assertEquals(expResult, result);

    }

    /**
     * Test of getUserUsername method, of class Organizador.
     */
    @Test
    public void testGetUserUsername() {
        System.out.println("getUserUsername");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Organizador instance = new Organizador(u1);
        String expResult = u1.getUsername();
        String result = instance.getUserUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class Organizador.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Organizador instance = new Organizador();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of alertaOrganizador method, of class Organizador.
     */
    @Test
    public void testAlertaOrganizador() {
        System.out.println("alertaOrganizador");
        Revisor r = new Revisor();
        String message = "This revisor is not good.";
        Organizador instance = new Organizador();
        boolean expResult = true;
        boolean result = instance.alertaOrganizador(r, message);
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaAlertas method, of class Organizador.
     */
    @Test
    public void testSetListaAlertas() {
        System.out.println("setListaAlertas");
        List<Alerta> lAlertas = new ArrayList();
        Organizador instance = new Organizador();
        instance.setListaAlertas(lAlertas);
    }
    
}
