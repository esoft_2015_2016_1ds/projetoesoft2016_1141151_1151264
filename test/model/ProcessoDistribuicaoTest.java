/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDistribuicao;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import listas.ListaConflitosDetetados;
import listas.ListaRevisoes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import states.submissao.SubmissaoRegistadoState;
import utils.NaoFazNada;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class ProcessoDistribuicaoTest {

    public ProcessoDistribuicaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of saveRevisao method, of class ProcessoDistribuicao.
     */
    @Test
    public void testSaveRevisao() 
    {
                System.out.println("setSubmissao");
                        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K2323");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K2323");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.addListaRevisoes(lr);
        boolean expResult = false;
        boolean result = instance.saveRevisao(rev1);
        assertEquals(expResult, result);
;
    }

    /**
     * Test of getRevisoes method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetRevisoes() 
    {
        System.out.println("getRevisoes");
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setListRevisoes(lr);
        List<Revisao> expResult = new ArrayList();
        expResult.add(rev);
        List<Revisao> result = instance.getRevisoes("Jonathan");
        assertEquals(expResult, result);
    }

    /**
     * Test of setMecanismoDistribuicao method, of class ProcessoDistribuicao.
     */
    @Test
    public void testSetMecanismoDistribuicao() 
    {
        System.out.println("setMecanismoDistribuicao");
        MecanismoDistribuicao m = null;
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setMecanismoDistribuicao(m);
    }

    /**
     * Test of setListRevisoes method, of class ProcessoDistribuicao.
     */
    @Test
    public void testSetListRevisoes() 
    {
        System.out.println("setListRevisoes");
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setListRevisoes(lr);
        List<Revisao> result = instance.getListaTodasRevisoes();
        List<Revisao> expResult = lr;
        assertEquals(result, expResult);
         
    }

    /**
     * Test of novaRevisao method, of class ProcessoDistribuicao.
     */
    @Test
    public void testNovaRevisao() {
        System.out.println("novaRevisao");
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.novaRevisao();
        String temp[] = instance.showData().split("\n");
        String compareData=temp[0];
        assertEquals(compareData,"ProcessoDistribuicao nao nulo");
    }

    /**
     * Test of getConfianca method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetConfianca() {
        System.out.println("getConfianca");
        SessaoTematica st1 = new SessaoTematica();
        List<SessaoTematica> lst = new ArrayList();
        lst.add(st1);
        Revisao rev1 = new Revisao();
        rev1.setClassificacaoConfianca(2);
        List<Revisao> listr = new ArrayList();
        ListaRevisoes lr = new ListaRevisoes(listr);
        listr.add(rev1);
        ProcessoDistribuicao instance = new ProcessoDistribuicao(lr);
        int expResult = 2;
        int result = instance.getConfianca();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAdequacao method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetAdequacao() {
        System.out.println("getAdequacao");
        SessaoTematica st1 = new SessaoTematica();
        List<SessaoTematica> lst = new ArrayList();
        lst.add(st1);
        Revisao rev1 = new Revisao();
        rev1.setClassificacaoAdequacao(2);
        List<Revisao> listr = new ArrayList();
        ListaRevisoes lr = new ListaRevisoes(listr);
        listr.add(rev1);
        ProcessoDistribuicao instance = new ProcessoDistribuicao(lr);
        int expResult = 2;
        int result = instance.getAdequacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOriginalidade method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetOriginalidade() {
        System.out.println("getOriginalidade");
        SessaoTematica st1 = new SessaoTematica();
        List<SessaoTematica> lst = new ArrayList();
        lst.add(st1);
        Revisao rev1 = new Revisao();
        rev1.setClassificacaoOriginalidade(2);
        List<Revisao> listr = new ArrayList();
        ListaRevisoes lr = new ListaRevisoes(listr);
        listr.add(rev1);
        ProcessoDistribuicao instance = new ProcessoDistribuicao(lr);
        int expResult = 2;
        int result = instance.getOriginalidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getQualidade method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetQualidade() {
        System.out.println("getQualidade");
        SessaoTematica st1 = new SessaoTematica();
        List<SessaoTematica> lst = new ArrayList();
        lst.add(st1);
        Revisao rev1 = new Revisao();
        rev1.setClassificacaoApresentacao(2);
        List<Revisao> listr = new ArrayList();
        ListaRevisoes lr = new ListaRevisoes(listr);
        listr.add(rev1);
        ProcessoDistribuicao instance = new ProcessoDistribuicao(lr);
        int expResult = 2;
        int result = instance.getQualidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRecomendacao method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetRecomendacao() {
        SessaoTematica st1 = new SessaoTematica();
        List<SessaoTematica> lst = new ArrayList();
        lst.add(st1);
        Revisao rev1 = new Revisao();
        rev1.setClassificacaoRecomendacao(2);
        List<Revisao> listr = new ArrayList();
        ListaRevisoes lr = new ListaRevisoes(listr);
        listr.add(rev1);
        ProcessoDistribuicao instance = new ProcessoDistribuicao(lr);
        int expResult = 2;
        int result = instance.getRecomendacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaTodasRevisoes method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetListaTodasRevisoes() 
    {
        System.out.println("getListaTodasRevisoes");
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setListRevisoes(lr);
        List<Revisao> result = instance.getListaTodasRevisoes();
        List<Revisao> expResult = lr;
        assertEquals(expResult, result);
    }

    /**
     * Test of getStatsTopicos method, of class ProcessoDistribuicao.
     */
    @Test
    public void testGetStatsTopicos() {
        System.out.println("getStatsTopicos");
        ArrayList ls = new ArrayList();
        ls.add("njdn");
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        ArrayList expResult = ls;
        ArrayList result = instance.getStatsTopicos(ls);
        assertEquals(expResult, result);
    }



    /**
     * Test of temRevisoes method, of class ProcessoDistribuicao.
     */
    @Test
    public void testTemRevisoes() {
        System.out.println("temRevisoes");
               Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setListRevisoes(lr);
        boolean expResult = true;
        boolean result = instance.temRevisoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of distribui method, of class ProcessoDistribuicao.
     */
    @Test
    public void testDistribui() 
    {
        System.out.println("distribui");
        List<Submissao> ls = null;
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        List<Revisao> expResult = null;
        List<Revisao> result = instance.distribui(ls);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addListaRevisoes method, of class ProcessoDistribuicao.
     */
    @Test
    public void testAddListaRevisoes() {
        System.out.println("addListaRevisoes");
        Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
        List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.addListaRevisoes(lr);
        List<Revisao> result = instance.getListaTodasRevisoes();
        List<Revisao> expResult = lr;
        assertEquals(result, expResult);
    } 

    /**
     * Test of temTodasRevisoesFeitas method, of class ProcessoDistribuicao.
     */
    @Test
    public void testTemTodasRevisoesFeitas() {
        System.out.println("temTodasRevisoesFeitas");
                Utilizador u1 = new Utilizador("Jon","Jonathan","weed123","Jon@gmail.com",new NaoFazNada());
        Utilizador u2 =  new Utilizador("name","Username","wee123","email@gmail.com",new NaoFazNada());
        String title = "Experiment Title";
        String summary = "Summary example.";
                List<String> k = new ArrayList();
        k.add("Projecto");
        k.add("Demasidado");
        k.add("Extenso");
        String file = "C:\\Users\\jbraga\\Documents\\ISEP\\Desert.pdf";
        Autor author = new Autor("AutorTeste","autor@msn.com","WindowsLive","Messenger");
        Autor author1 = new Autor("Autor","autor@msn.com","AppleLive","MacOS");
        AutorCorrespondente ac1 = new AutorCorrespondente(author,u1);
        AutorCorrespondente ac2 = new AutorCorrespondente(author1,u2);
        ListaAutores a1 = new ListaAutores();
        a1.addAutor(author);
        ListaAutores a2 = new ListaAutores();
        a2.addAutor(author1);
        Artigo artigo1 = new Artigo(a1,title,summary,file,ac1,k);
        Artigo artigo2 = new Artigo(a2,title, summary, file, ac2,k);
        Submissao sub =  new Submissao();
        Submissao sub1  = new Submissao();
        sub =new Submissao(artigo1, artigo2, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        sub1 = new Submissao(artigo2, artigo1, new SubmissaoRegistadoState(sub), new ListaConflitosDetetados());
        List<Submissao> ls = new ArrayList();
        ls.add(sub);
        ls.add(sub1);
        System.out.println("saveRevisao");
        Revisor revisor1 = new Revisor(u1);
        Revisao rev = new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        Revisao rev1  =new Revisao();
        rev.setClassificacaoAdequacao(1);
        rev.setClassificacaoApresentacao(1);
        rev.setClassificacaoConfianca(1);
        rev.setClassificacaoOriginalidade(1);
        rev.setClassificacaoRecomendacao(1);
        rev.setTextoExplicativo("K123123");
        rev.setRevisor(revisor1);
        rev.setSubmissao(sub);
        List<Revisao> lr = new ArrayList();
        lr.add(rev);
        ProcessoDistribuicao instance = new ProcessoDistribuicao();
        instance.setListRevisoes(lr);
        
        boolean expResult = true;
        boolean result = instance.temTodasRevisoesFeitas();
        assertEquals(expResult, result);

    }

}
