/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDetecao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class TipoConflitoTest {
    
    public TipoConflitoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setName method, of class TipoConflito.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String value = "Mesma instituicao";
        TipoConflito instance = new TipoConflito();
        instance.setName(value);
        assertEquals(value, instance.getNome());
    }

    /**
     * Test of setDescricao method, of class TipoConflito.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String value = "Ze descrption";
        TipoConflito instance = new TipoConflito();
        instance.setDescricao(value);
        assertEquals(value, instance.getDescricao());
    }

    /**
     * Test of valida method, of class TipoConflito.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        TipoConflito instance = new TipoConflito();
        instance.setName("nomeito");
        instance.setDescricao("ladescriciona");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class TipoConflito.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = new TipoConflito();
        ((TipoConflito)o).setDescricao("Description");
        ((TipoConflito)o).setName("nome");
        TipoConflito instance = new TipoConflito();
        instance.setDescricao("Description");
        instance.setName("nome");
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNome method, of class TipoConflito.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        TipoConflito instance = new TipoConflito();
        instance.setName("nome");
        String expResult = "nome";
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class TipoConflito.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        TipoConflito instance = new TipoConflito();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

    /**
     * Test of detetarConflitosCom method, of class TipoConflito.
     */
    @Test
    public void testDetetarConflitosCom() {
        System.out.println("detetarConflitosCom");
        Revisor r = null;
        Submissao s = null;
        TipoConflito instance = new TipoConflito();
        boolean expResult = false;
        boolean result = instance.detetarConflitosCom(r, s);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMecanismoDetecao method, of class TipoConflito.
     */
    @Test
    public void testSetMecanismoDetecao() {
        System.out.println("setMecanismoDetecao");
        MecanismoDetecao md = null;
        TipoConflito instance = new TipoConflito();
        instance.setMecanismoDetecao(md);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
