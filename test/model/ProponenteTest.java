/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class ProponenteTest {
    
    public ProponenteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setUtilizador method, of class Proponente.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador utilizador = u1;
        Proponente instance = new Proponente(u1);
        instance.setUtilizador(utilizador);
        assertEquals(instance.getUtilizador(),utilizador);
    }

    /**
     * Test of valida method, of class Proponente.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
         Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Utilizador utilizador = u1;
        Proponente instance = new Proponente(u1);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNome method, of class Proponente.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        String expResult = u1.getName();
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUtilizador method, of class Proponente.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        Utilizador expResult = u1;
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Proponente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        String expResult = u1.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmailUser method, of class Proponente.
     */
    @Test
    public void testGetEmailUser() {
        System.out.println("getEmailUser");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        String expResult = u1.getEmail();
        String result = instance.getEmailUser();
        assertEquals(expResult, result);

    }

    /**
     * Test of getUsernameUser method, of class Proponente.
     */
    @Test
    public void testGetUsernameUser() {
        System.out.println("getUsernameUser");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        String expResult = u1.getUsername();
        String result = instance.getUsernameUser();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class Proponente.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Utilizador u1=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Proponente instance = new Proponente(u1);
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);

    }
    
}
