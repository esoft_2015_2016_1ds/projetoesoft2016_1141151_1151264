/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class AutorTest {
    
    public AutorTest() {
    }
    RegistoUtilizadores regU;
    Utilizador u1,u2,u3;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        u2 = new Utilizador("adminFunny","adminB","1e44a5sy","adminB@gmail.com",new NaoFazNada());
        u3 = new Utilizador("Hidden User","adminHidden","123easyHide","adminHidden@gmail.com",new NaoFazNada());
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(u2);
        regU.addUtilizador(new Utilizador("Teste","TestingUser","1234medium","teste@gmail.com",new NaoFazNada()));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setNome method, of class Autor.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String strNome = "Mozart";
        Autor instance = new Autor();
        instance.setNome(strNome);
        // TODO review the generated test code and remove the default call to fail.
        String[] chunk = instance.showData().split("\n");
        String compareData = chunk[0];
        assertEquals(compareData,strNome);
    }

    /**
     * Test of setEmail method, of class Autor.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String value = "myemail@gmail.com";
        Autor instance = new Autor();
        instance.setEmail(value);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getEmail(),value);
    }

    /**
     * Test of setAfiliacao method, of class Autor.
     */
    @Test
    public void testSetAfiliacao() {
        System.out.println("setAfiliacao");
        String strAfiliacao = "Summoner's Rift";
        Autor instance = new Autor();
        instance.setAfiliacao(strAfiliacao);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(strAfiliacao,instance.getInstituicaoAfiliacao());
    }

    /**
     * Test of getName method, of class Autor.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Autor instance = new Autor();
        instance.setNome("ABCD");
        String expResult = "ABCD";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getEmail method, of class Autor.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Autor instance = new Autor();
        instance.setEmail("BestPlaceOnEarth@gmail.com");
        String expResult = "BestPlaceOnEarth@gmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getInstituicaoAfiliacao method, of class Autor.
     */
    @Test
    public void testGetInstituicaoAfiliacao() {
        System.out.println("getInstituicaoAfiliacao");
        Autor instance = new Autor();
        instance.setAfiliacao("BestPlaceOnEarth");
        String expResult = "BestPlaceOnEarth";
        String result = instance.getInstituicaoAfiliacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of valida method, of class Autor with data to make the validation
     * return false.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of valida method, of class Autor with a valid author to test.
     */
    @Test
    public void testValidaValid() {
        System.out.println("valida (valid autor test)");
        Autor instance = new Autor("Dr. Zeus","mythology@gmail.com","Olympus","SparkGuy");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of enviarNotificacao method, of class Autor.
     */
    @Test
    public void testEnviarNotificacao() {
        System.out.println("enviarNotificacao");
        String message = "";
        Autor instance = new Autor();
        instance.enviarNotificacao(message);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor("Dr. Zeus","mythology@gmail.com","Olympus","SparkGuy");
        String expResult = "Dr. Zeus - mythology@gmail.com - Olympus - SparkGuy";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of showData method, of class Autor.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Autor instance = new Autor();
        String expResult = "\n\n\n\n";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setUsername method, of class Autor. 
     * In this test, the username is lower than 2 digits and should result in
     * an error message.
     */
    @Test
    public void testSetUsernameTooShort() {
        System.out.println("setUsername (username too short)");
        String strNome = "ola";
        Autor instance = new Autor();
        instance.setUsername(strNome);
        assertEquals(strNome, instance.getUsername());
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of setUsername method, of class Autor.
     */
    @Test
    public void testSetUsernameValid() {
        System.out.println("setUsername (correct length username)");
        String strNome = "Rui";
        Autor instance = new Autor();
        instance.setUsername(strNome);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getUsername method, of class Autor.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        Autor instance = new Autor();
        instance.setUsername("Cheesy");
        String expResult = "Cheesy";
        String result = instance.getUsername();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Autor instance = new Autor("adminFunny","adminB@gmail.com","1e44a5sy","adminBs");
        boolean expResult = true;
        boolean result = instance.podeSerCorrespondente(regU);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of setUsername method, of class Autor.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String strNome = "Maria";
        Autor instance = new Autor();
        instance.setUsername(strNome);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(strNome,instance.getUsername());
    }
    
}
