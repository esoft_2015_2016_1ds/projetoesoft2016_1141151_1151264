/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class RevisaoTest {
    
    public RevisaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  

    /**
     * Test of setClassificacaoConfianca method, of class Revisao.
     */
    @Test
    public void testSetClassificacaoConfianca() {
        System.out.println("setClassificacaoConfianca");
        int classificacaoConfianca = 2;
        Revisao instance = new Revisao();
        instance.setClassificacaoConfianca(classificacaoConfianca);
        assertEquals(classificacaoConfianca,instance.getClassificacaoConfianca());

        
    }

    /**
     * Test of setClassificacaoOriginalidade method, of class Revisao.
     */
    @Test
    public void testSetClassificacaoOriginalidade() {
        System.out.println("setClassificacaoOriginalidade");
        int classificacaoOriginalidade = 2;
        Revisao instance = new Revisao();
        instance.setClassificacaoOriginalidade(classificacaoOriginalidade);
        assertEquals(classificacaoOriginalidade,instance.getClassificacaoOriginalidade());
    }

    /**
     * Test of setClassificacaoAdequacao method, of class Revisao.
     */
    @Test
    public void testSetClassificacaoAdequacao() {
        System.out.println("setClassificacaoAdequacao");
        int classificacaoAdequacao = 2;
        Revisao instance = new Revisao();
        instance.setClassificacaoAdequacao(classificacaoAdequacao);
        assertEquals(classificacaoAdequacao,instance.getClassificacaoAdequacao());
        
    }

    /**
     * Test of setClassificacaoApresentacao method, of class Revisao.
     */
    @Test
    public void testSetClassificacaoApresentacao() {
        System.out.println("setClassificacaoApresentacao");
        int classificacaoApresentacao = 2;
        Revisao instance = new Revisao();
        instance.setClassificacaoApresentacao(classificacaoApresentacao);
        assertEquals(classificacaoApresentacao,instance.getClassificacaoApresentacao());
        
    }

    /**
     * Test of setClassificacaoRecomendacao method, of class Revisao.
     */
    @Test
    public void testSetClassificacaoRecomendacao() {
        System.out.println("setClassificacaoRecomendacao");
        int classificacaoRecomendacao = 2;
        Revisao instance = new Revisao();
        instance.setClassificacaoRecomendacao(classificacaoRecomendacao);
        assertEquals(classificacaoRecomendacao,instance.getClassificacaoRecomendacao());
        
    }

    /**
     * Test of setTextoExplicativo method, of class Revisao.
     */
    @Test
    public void testSetTextoExplicativo() {
        System.out.println("setTextoExplicativo");
        String textoExplicativo = "TestasOuChunbas";
        Revisao instance = new Revisao();
        instance.setTextoExplicativo(textoExplicativo);
        assertEquals(textoExplicativo,instance.getTextoExplicativo());
        
    }


    /**
     * Test of setRevisor method, of class Revisao.
     */
    @Test
    public void testSetRevisor() {
        System.out.println("setRevisor");
        Revisor r =new Revisor(new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada()));
        Revisao instance = new Revisao();
        instance.setRevisor(r);
        assertEquals(r,instance.getRevisor());
        
    }

    /**
     * Test of getClassificacaoConfianca method, of class Revisao.
     */
    @Test
    public void testGetClassificacaoConfianca() {
        System.out.println("getClassificacaoConfianca");
        Revisao instance = new Revisao();
        int expResult = 2;
        instance.setClassificacaoConfianca(expResult);
        int result = instance.getClassificacaoConfianca();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClassificacaoOriginalidade method, of class Revisao.
     */
    @Test
    public void testGetClassificacaoOriginalidade() {
        System.out.println("getClassificacaoOriginalidade");
        Revisao instance = new Revisao();
        int expResult = 2;
        instance.setClassificacaoOriginalidade(expResult);
        int result = instance.getClassificacaoOriginalidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClassificacaoAdequacao method, of class Revisao.
     */
    @Test
    public void testGetClassificacaoAdequacao() {
        System.out.println("getClassificacaoAdequacao");
        Revisao instance = new Revisao();
        int expResult = 2;
        instance.setClassificacaoAdequacao(expResult);
        int result = instance.getClassificacaoAdequacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClassificacaoApresentacao method, of class Revisao.
     */
    @Test
    public void testGetClassificacaoApresentacao() {
        System.out.println("getClassificacaoApresentacao");
        Revisao instance = new Revisao();
        int expResult = 2;
        instance.setClassificacaoApresentacao(expResult);
        int result = instance.getClassificacaoApresentacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClassificacaoRecomendacao method, of class Revisao.
     */
    @Test
    public void testGetClassificacaoRecomendacao() {
        System.out.println("getClassificacaoRecomendacao");
        Revisao instance = new Revisao();
        int expResult = 2;
        instance.setClassificacaoRecomendacao(expResult);
        int result = instance.getClassificacaoRecomendacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStatsTopicos method, of class Revisao.
     */
    @Test
    public void testGetStatsTopicos() {
        System.out.println("getStatsTopicos");
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        ArrayList ls = new ArrayList();
        ls.add("cndj");
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        a1.setPalavrasChave(ls);
        a1.setAutorCorrespondente(aut,u1);
        Submissao sub = new Submissao();
        sub.setArtigoInicial(a1);
        Revisao instance = new Revisao();
        instance.setSubmissao(sub);
        ArrayList expResult = ls;
        ArrayList result = instance.getStatsTopicos(ls);
        assertEquals(expResult, result);
    }
    /**
     * Test of getTextoExplicativo method, of class Revisao.
     */
    @Test
    public void testGetTextoExplicativo() {
        System.out.println("getTextoExplicativo");
        Revisao instance = new Revisao();
        String expResult = "TestasOuChumbas";
        instance.setTextoExplicativo(expResult);
        String result = instance.getTextoExplicativo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubmissao method, of class Revisao.
     */
    @Test
    public void testGetSubmissao() {
        System.out.println("getSubmissao");
        Revisao instance = new Revisao();
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Autor aut=new Autor(u1,"ISEP");
        List<Autor> la=new ArrayList();
        la.add(aut);
        AutorCorrespondente au = new AutorCorrespondente(aut,u1);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        List<String> strings= new ArrayList();
        strings.add("cnjd");
        a.setPalavrasChave(strings);
        Submissao sub;
        sub=new Submissao();
        a.setAutorCorrespondente(aut,u1);
        sub.setArtigoInicial(a);
        instance.setSubmissao(sub);
        
        Submissao expResult = sub;
        Submissao result = instance.getSubmissao();
        assertEquals(expResult, result);
   
    }

    /**
     * Test of setSubmissao method, of class Revisao.
     */
    @Test
    public void testSetSubmissao() {
        System.out.println("setSubmissao");
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Autor aut=new Autor(u1,"ISEP");
        List<Autor> la=new ArrayList();
        la.add(aut);
        AutorCorrespondente au = new AutorCorrespondente(aut,u1);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        Submissao sub;
        sub=new Submissao();
        List<String> strings= new ArrayList();
        strings.add("nfj");
        a.setPalavrasChave(strings);
        a.setAutorCorrespondente(au);
        sub.setArtigoInicial(a);
        Revisao instance = new Revisao();
        instance.setSubmissao(sub);
        assertEquals(sub,instance.getSubmissao());
    }

    /**
     * Test of setPreRevistoState method, of class Revisao.
     */
    @Test
    public void testSetPreRevistoState() {
        System.out.println("setPreRevistoState");
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        List<String> k = new ArrayList();
        k.add("Dark");
        k.add("Souls");
         Revisao instance = new Revisao();
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        a.setAutorCorrespondente(aut,u1);
        a.setPalavrasChave(k);
        Submissao sub;
        sub=new Submissao();
        sub.setArtigoInicial(a);
        instance.setSubmissao(sub);
        boolean expResult = true;
        boolean result = instance.setPreRevistoState();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of toString method, of class Revisao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u1=new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Revisao instance = new Revisao();
        List<Autor> la=new ArrayList();
        Autor aut=new Autor(u1,"ISEP");
        la.add(aut);
        Artigo a =new Artigo();
        a.setResumo("Ola");
        a.setTitulo("Deuses");
        a.setFicheiro("ola.pdf");
        a.setAutores(la);
        Submissao sub;
        sub=new Submissao();
        List<String> strings= new ArrayList();
        strings.add("jdn");
        a.setPalavrasChave(strings);
        a.setAutorCorrespondente(aut,u1);
        sub.setArtigoInicial(a);
        instance.setSubmissao(sub);
        String expResult = sub.getArtigoInicial().getInfo();;
        String result = instance.toString();
        assertEquals(expResult, result);
    }


    /**
     * Test of getRevisor method, of class Revisao.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        Revisao instance = new Revisao();
        Utilizador u1= new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Revisor expResult = new Revisor(u1);
        instance.setRevisor(expResult);
        Revisor result = instance.getRevisor();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Revisao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals"); 
        Utilizador u1= new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());
        Revisor r = new Revisor(u1);
        Revisao rev=new Revisao();
        Submissao sub=new Submissao();
        rev.setSubmissao(sub);
        rev.setRevisor(r);
        Object other = rev;
        Revisao instance = new Revisao(sub);
        instance.setRevisor(r);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    /**
     * Test of getArtigo method, of class Revisao.
     */
    @Test
    public void testGetArtigo() {
        System.out.println("getArtigo");
        Utilizador u1= new Utilizador("admin","adminA","123easy","admin@gmail.com",new NaoFazNada());   
        Artigo a1 = new Artigo();
        Autor aut= new Autor(u1,"UnknownPlace");
        a1.setTitulo("Pikachu");
        a1.setResumo("Pikachu, the electric-mouse pokémon.");
        a1.setFicheiro("pikachu.pdf");
        a1.addAutor(new Autor(u1,"UnknownPlace"));
        Submissao sub = new Submissao();
        List<String> strings= new ArrayList();
        strings.add("dced");
        a1.setPalavrasChave(strings);
        a1.setAutorCorrespondente(aut,u1);
        sub.setArtigoInicial(a1);
        Artigo expResult = sub.getArtigoInicial();
        Revisao instance = new Revisao();
        instance.setSubmissao(sub);
        Artigo result = instance.getArtigo();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class Revisao.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Revisao instance = new Revisao();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    
}
