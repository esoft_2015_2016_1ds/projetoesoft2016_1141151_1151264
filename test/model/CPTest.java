/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class CPTest {

    public CPTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }
    private CP instance;
    private Revisor r1, r2, r3, r4;
    private List<Revisor> lr;
    RegistoEventos reg;
    Utilizador u1, u2, u3,u4;
    Utilizador other;
    RegistoUtilizadores regU;
    Evento ev;
    CP cp;
    

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        r2 = new Revisor();
        lr = new ArrayList();

        lr.add(r2);
        u1 = new Utilizador("admin", "adminA", "123easy", "admin@gmail.com", new NaoFazNada());
        u2 = new Utilizador("Mario", "Super Mario", "toadcastle", "nintendo@gmail.com", new NaoFazNada());
        u3 = new Utilizador("Sonic The Hedgehog", "Sonic", "iamafraidofwater", "sega@gmail.com", new NaoFazNada());
        r1 = new Revisor(u1);
        lr.add(r1);
        regU = new RegistoUtilizadores();
        regU.addUtilizador(u1);
        regU.addUtilizador(new Utilizador("Teste", "TestingUser", "1234medium", "teste@gmail.com", new NaoFazNada()));
        reg = new RegistoEventos(regU);
        other = new Utilizador("outro", "OtherPancakes", "ilovecandy224", "random@gmail.com", new NaoFazNada());
        ev = new Evento();
        cp = new CP();
        r3 = new Revisor(u1);
        r4 = new Revisor(u2);
        cp.addMembroCP(r3);
        cp.addMembroCP(r4);
        ev.setCP(cp);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addMembroCP method, of class CP.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        boolean expResult = true;
        CP instance = new CP();
        boolean result = instance.addMembroCP(r1);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaMembroCP method, of class CP.
     */
    @Test
    public void testRegistaMembroCP() {
        System.out.println("registaMembroCP");
        CP instance = new CP(lr);
        boolean expResult = true;
        boolean result = instance.registaMembroCP(r1);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class CP.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CP instance = new CP(lr);
        String expResult = "Membros CP: "+lr;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of addMembroCP method, of class CP.
     */
    @Test
    public void testAddMembroCP_Utilizador() {
        System.out.println("addMembroCP");
        Utilizador u = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        CP instance = new CP();
        boolean expResult = true;
        boolean result = instance.addMembroCP(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of addMembroCP method, of class CP.
     */
    @Test
    public void testAddMembroCP_Revisor() {
        System.out.println("addMembroCP");
        Revisor r = new Revisor(new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada()));
        CP instance = new CP();
        boolean expResult = true;
        boolean result = instance.addMembroCP(r);
        assertEquals(expResult, result);
    }

    /**
     * Test of temMembro method, of class CP.
     */
    @Test
    public void testTemMembro() {
        System.out.println("temMembro");
        String id = u2.getEmail();
        CP instance = cp;
        boolean expResult = true;
        boolean result = instance.temMembro(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of temMembro method, of class CP. In this case, the user is not a
     * revisor of the CP.
     */
    @Test
    public void testTemMembroNaoExiste() {
        System.out.println("temMembro (does not exist)");
        String id = u3.getEmail();
        CP instance = cp;
        boolean expResult = false;
        boolean result = instance.temMembro(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of novoMembroCP method, of class CP.
     */
    @Test
    public void testNovoMembroCP() {
        System.out.println("novoMembroCP");
        Utilizador u = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        CP instance = new CP(lr);
        Revisor expResult = new Revisor(u);
        Revisor result = instance.novoMembroCP(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of addRevisor method, of class CP.
     */
    @Test
    public void testAddRevisor() {
        System.out.println("addRevisor");
        Revisor r = r3;
        CP instance = new CP();
        boolean expResult = true;
        boolean result = instance.addRevisor(r);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisor method, of class CP.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        u4=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor r=new Revisor(u4);
        String id = u4.getEmail();
        List<Revisor> lr1=new ArrayList();
        lr1.add(r);
        CP instance = new CP(lr1);
        Revisor expResult = r;
        Revisor result = instance.getRevisor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class CP.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        CP instance = new CP(lr);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of tamanhoListaRevisores method, of class CP.
     */
    @Test
    public void testTamanhoListaRevisores() {
        System.out.println("tamanhoListaRevisores");
        CP instance = cp;
        int expResult = 2;
        int result = instance.tamanhoListaRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisores method, of class CP.
     */
    @Test
    public void testGetRevisores() {
        System.out.println("getRevisores");
        List<Revisor> expResult = new ArrayList();
        expResult.add(r3);
        expResult.add(r4);
        List<Revisor> result = cp.getRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class CP.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        CP instance = new CP();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
    }

}
