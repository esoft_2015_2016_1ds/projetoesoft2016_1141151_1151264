/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.NaoFazNada;

/**
 *
 * @author jbraga
 */
public class RevisorTest {
    
    public RevisorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class Revisor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor instance = new Revisor(u);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNome method, of class Revisor.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Utilizador u=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor instance = new Revisor(u);
        String expResult = u.getName();
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUtilizador method, of class Revisor.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Utilizador u=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor instance = new Revisor(u);    
        Utilizador expResult = u;
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUtilizador method, of class Revisor.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador u=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor instance = new Revisor(u);
        instance.setUtilizador(u);
    }

    /**
     * Test of toString method, of class Revisor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u=new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());
        Revisor instance = new Revisor(u);
        String expResult = "Revisor André Vieira";
        String result = instance.toString();
        assertEquals(expResult, result);
    }


    /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Utilizador("André Vieira", "vieira", "abc123", "andrevieira_1996@hotmail.com", new NaoFazNada());;
        Revisor instance = new Revisor();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    
}
