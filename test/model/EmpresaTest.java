/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDecisao;
import interfaces.MecanismoDistribuicao;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.Coder;
import utils.Data;

/**
 *
 * @author jbraga
 */
public class EmpresaTest {
    
    public EmpresaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoTipoConflito method, of class Empresa.
     */
    @Test
    public void testNovoTipoConflito() {
        System.out.println("novoTipoConflito");
        Empresa instance = new Empresa();
        boolean expResult = true;
        boolean result = instance.novoTipoConflito()!=null;
        assertEquals(expResult, result);

    }

    /**
     * Test of getSessoesTematicasOrganizador method, of class Empresa.
     */
  
    /**
     * Test of getListaTipoConflitos method, of class Empresa.
     */
    @Test
    public void testGetListaTipoConflitos() {
        System.out.println("getListaTipoConflitos");
        Empresa instance = new Empresa();
        TipoConflito tc = new TipoConflito();
        List<TipoConflito> expResult = new ArrayList<TipoConflito>();
        expResult.add(tc);
        instance.addTipoConflito(tc);
        List<TipoConflito> result = instance.getListaTipoConflitos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMecanismosDecisao method, of class Empresa.
     */
    @Test
    public void testGetMecanismosDecisao() {
        System.out.println("getMecanismosDecisao");
        Empresa instance = new Empresa();
        instance.getMecanismosDecisao().add(new MecanismoDecisao(){public List<Decisao> decide(){return new ArrayList<Decisao>();}});
        boolean expResult = true;
        boolean result = instance.getMecanismosDecisao()!=null;
        assertEquals(expResult, result);

    }

    /**
     * Test of getMecanismosDistribuicao method, of class Empresa.
     */
    @Test
    public void testGetMecanismosDistribuicao() {
        System.out.println("getMecanismosDistribuicao");
        Empresa instance = new Empresa();
        boolean expResult = true;
        boolean result = instance.getMecanismosDistribuicao()!=null;
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRegistoUtilizadores method, of class Empresa.
     */
    @Test
    public void testGetRegistoUtilizadores() {
        System.out.println("getRegistoUtilizadores");
        Empresa instance = new Empresa();
        Empresa akk = new Empresa();
        RegistoUtilizadores expResult = akk.getRegistoUtilizadores();
        RegistoUtilizadores result = instance.getRegistoUtilizadores();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRegistoEventos method, of class Empresa.
     */
    @Test
    public void testGetRegistoEventos() {
        System.out.println("getRegistoEventos");
        Empresa instance = new Empresa();
        Empresa akk = new Empresa();
        RegistoEventos expResult = akk.getRegistoEventos();
        RegistoEventos result = instance.getRegistoEventos();
        assertEquals(expResult, result);

    }

    /**
     * Test of addTipoConflito method, of class Empresa.
     */
    @Test
    public void testAddTipoConflito() {
        System.out.println("addTipoConflito");
        TipoConflito tipoConflito = new TipoConflito();
        Empresa instance = new Empresa();
        boolean expResult = true;
        boolean result = instance.addTipoConflito(tipoConflito);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Empresa.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        TipoConflito tipoConflito = new TipoConflito();
        Empresa instance = new Empresa();
        boolean expResult = true;
        boolean result = instance.valida(tipoConflito);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTabelasFreq method, of class Empresa.
     */
    @Test
    public void testGetTabelasFreq() {
        System.out.println("getTabelasFreq");
        Empresa instance = new Empresa();
        List<Coder> expResult = null;
        List<Coder> result = instance.getTabelasFreq();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTabelasFreq method, of class Empresa.
     */
    @Test
    public void testSetTabelasFreq() {
        System.out.println("setTabelasFreq");
        List<Coder> tabelasFreq = null;
        Empresa instance = new Empresa();
        instance.setTabelasFreq(tabelasFreq);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRandomTabelaFreq method, of class Empresa.
     */
    @Test
    public void testGetRandomTabelaFreq() {
        System.out.println("getRandomTabelaFreq");
        Empresa instance = new Empresa();
        /*instance.setTabelasFreq(new ArrayList<TabelaFrequencia>(){{
          add(new TabelaFrequencia(new char[]{'A','B'},new double[]{0.6,0.4}));
          add(new TabelaFrequencia(new char[]{'a','b','c'}, new double[]{2.2,3.2,4.3}));
        }});
        TabelaFrequencia expResult = new TabelaFrequencia(new char[]{'a','b','c'}, new double[]{2.2,3.2,4.3});
        TabelaFrequencia result = instance.getRandomTabelaFreq();
        assertEquals(expResult, result);*/
       
    }

    /**
     * Test of showData method, of class Empresa.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        Empresa instance = new Empresa();
        String expResult = "";
        String result = instance.showData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of schedule method, of class Empresa.
     */
    @Test
    public void testSchedule() {
        System.out.println("schedule");
        TimerTask tt = new TimerTask(){
            public void run(){
                System.out.println("THE PROPHECY HAS BEEN FULFILLED!! WARN THE KEEPERS!!");
            }
        };
        Data d = new Data(27,9,2015);
        Empresa instance = new Empresa();
        instance.schedule(tt, d);
    }

    /**
     * Test of isAdmin method, of class Empresa.
     */
    @Test
    public void testIsAdmin() {
        System.out.println("isAdmin");
        Utilizador u = null;
        Empresa instance = new Empresa();
        boolean expResult = false;
        boolean result = instance.isAdmin(u);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUtilizadorLogged method, of class Empresa.
     */
    @Test
    public void testSetUtilizadorLogged() {
        System.out.println("setUtilizadorLogged");
        Utilizador u = null;
        Empresa instance = new Empresa();
        instance.setUtilizadorLogged(u);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUtilizadorAutenticado method, of class Empresa.
     */
    @Test
    public void testGetUtilizadorAutenticado() {
        System.out.println("getUtilizadorAutenticado");
        Empresa instance = new Empresa();
        Utilizador expResult = null;
        Utilizador result = instance.getUtilizadorAutenticado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRandomCoder method, of class Empresa.
     */
    @Test
    public void testGetRandomCoder() {
        System.out.println("getRandomCoder");
        Empresa instance = new Empresa();
        Coder expResult = null;
        Coder result = instance.getRandomCoder();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveData method, of class Empresa.
     */
    @Test
    public void testSaveData() {
        System.out.println("saveData");
        Empresa instance = new Empresa();
        instance.saveData();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
