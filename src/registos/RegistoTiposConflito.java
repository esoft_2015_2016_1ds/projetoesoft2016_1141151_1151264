/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registos;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.TipoConflito;

/**
 *
 * @author jbraga
 */
public class RegistoTiposConflito implements iTOCS{

    private List<TipoConflito> listaTiposConflitos;
    /**
     * Creates a new instance of object {@link registos.RegistoTiposConflito} with null parameters.
     */
    public RegistoTiposConflito()
    {
        this(new ArrayList());
    }
    /**
     * Creates a new instance of object {@link registos.RegistoTiposConflito} with the specified parameters.
     * @param ltc (List&lt;{@link model.TipoConflito}&gt;) A list of types of conflict to populate this
     * registry.
     */
    public RegistoTiposConflito(List<TipoConflito> ltc)
    {
        listaTiposConflitos = new ArrayList(ltc);
    }
    /**
     * Returns a list containing all of the types of conflicts available in the company.
     * @return (List&lt;{@link model.TipoConflito}&gt;) List containing the types of conflict.
     */
    public List<TipoConflito> getListaTiposConflito()
    {
        return new ArrayList(listaTiposConflitos);
    }
    /**
     * Adds a new type of conflict to this registry.
     * @param tipoConflito ({@link model.TipoConflito}) The new type of conflict to be added.
     * @return (boolean) True if successfully added.
     */
    public boolean addTipoConflito(TipoConflito tipoConflito) {
        return listaTiposConflitos.add(tipoConflito);
    }
    /**
     * Checks if the specified type of conflict already exists or not in this
     * registry.
     * @param tipoConflito ({@link model.TipoConflito}) The target type of conflict.
     * @return 
     */
    public boolean valida(TipoConflito tipoConflito) {
        boolean result = true;
        for (TipoConflito element : listaTiposConflitos) {
            if (element.equals(tipoConflito)) {
                result = false;
                break;
            }
        }
        return result;
    }
    /**
     * @see #showData
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
    
}
