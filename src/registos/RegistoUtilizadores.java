package registos;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Evento;
import model.Revisao;
import model.Revisor;
import model.Utilizador;

/**
 *
 * @author Gonçalo
 */
public class RegistoUtilizadores implements iTOCS {

    /**
     * List that contains the registry's users.
     */
    private List<Utilizador> lista;
    
    /**
     * Empty Constructor of Registoutilizadores
     */
    public RegistoUtilizadores() {
        this(new ArrayList());
    }

    /**
     * Constructor of RegistoUtilizadores
     *
     * @param lista The List with the users to register.
     */
    public RegistoUtilizadores(List<Utilizador> lista) {
        this.lista = new ArrayList(lista);
    }

    /**
     * Copy Construtor of RegistoUtilizador
     *
     * @param reg The RegistoUtilizadores that will be copied
     */
    public RegistoUtilizadores(RegistoUtilizadores reg) {
        this(reg.getLista());
    }

    /**
     * @return a copy of The List of users available in this registry
     */
    public List<Utilizador> getLista() {
        ArrayList<Utilizador> reg = new ArrayList<Utilizador>();
        reg.addAll(this.lista);
        return reg;
    }

    /**
     * @param registo The new List of users of this registry
     */
    public void setRegisto(ArrayList<Utilizador> registo) {
        this.lista = registo;
    }

    /**
     * Returns a list of all revisores in the system.
     *
     * @param reg ({@link registos.RegistoEventos}) The event registry of the
     * company.
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all
     * revisors.
     */
    public List<Revisor> getListaTodosRevisores(RegistoEventos reg) {
        List<Revisor> result = new ArrayList();
        for (Utilizador focus : lista) {
            Revisor r = reg.getRevisor(focus);
            if (r != null) {
                result.add(r);
            }
        }
        return result;
    }

    /**
     * Creates a new user
     *
     * @return The new user.
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     * Logs in a user with the specified credentials
     *
     * @param username (String) The user's username.
     * @param password (String) The user's password.
     * @return ({@link model.Utilizador}) The logged in user. null if the user
     * wasn't found.
     */
    public Utilizador login(String username, String password) {
        Utilizador result = null;
        for (Utilizador focus : lista) {
            if (focus.getUsername().equals(username) && focus.getPassword().equals(password)) {
                result = focus;
                break;
            }
        }
        return result;
    }

    /**
     * Adds an user if possible, using the addUtilizador(u) method
     *
     * @param u The user to be registered
     */
    public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return this.lista.add(u);
        } else {
            return false;
        }
    }

    /**
     * Searches for an {@link model.Utilizador} within the registry using it's
     * ID
     *
     * @param strId (String) A user's ID that will be used to find a match in
     * the registry's user list
     * @return Utilizador - The user with the corresponding ID
     */
    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : lista) {
            String s1 = u.getUsername();
            String s2 = u.getEmail();
            if (s1.equals(strId)) {
                return u;
            } else if (s2.equals(strId)) {
                return u;
            }
        }
        return null;
    }

    /**
     * Get the index of the position that an user occupies within the registry's
     * user list
     *
     * @param u the user whose index will be returned
     * @return user's position index
     */
    public int getPosition(Utilizador u) {
        int result = -1;
        for (int i = 0; i < lista.size(); i++) {
            Utilizador focus = lista.get(i);
            if (focus.getUsername().equals(u.getUsername()) && focus.getEmail().equals(u.getEmail())) {
                return i;
            }
        }
        return result;
    }

    /**
     * Checks if there is an user with the same email or username
     *
     * @param u The user that needs to be validated
     * @return true if there is an user with the same identity atributes.
     */
    public boolean validaUtilizador(Utilizador u) {
        //Check if there is an user with the same email or username
        for (Utilizador focus : lista) {
            if (focus.getUsername().equals(u.getUsername()) || focus.getEmail().equals(u.getEmail())) {
                return false;
            }
        }
        return true;
    }

    /**
     * adiciona utilizador ao registo
     *
     * @param u o utilizador a adicionar
     * @return true or false depending on the sucess of the operation
     */
    public boolean addUtilizador(Utilizador u) {
        return lista.add(u);
    }

    /**
     * Checks if an user's change is valid
     *
     * @param uClone the Utilizador clone containing the new user's atributes
     * @param i the position of the user within the registry's user List
     * @return true if the change is valid, false if otherwise
     */
    public boolean validaAlteracaoUtilizador(Utilizador uClone, int i) {
        boolean result = true;
        for (int j = 0; j < lista.size() && result; j++) {
            Utilizador focus = lista.get(j);

            if ((focus.getUsername().equals(uClone.getUsername()) || focus.getEmail().equals(uClone.getEmail())) && j != i) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Checks if the company has a user with the specified email.
     *
     * @param email (String) The email of the user to search for.
     * @return (boolean) True if a match has been found.
     */
    public boolean temUtilizadorEmail(String email) {
        boolean result = false;
        for (Utilizador focus : lista) {
            if (focus.getEmail().equals(email)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Checks if the company has a user with the specified username.
     *
     * @param username (String) The username of the user to search for.
     * @return (boolean) True if a match has been found.
     */
    public boolean temUtilizadorUsername(String username) {
        boolean result = false;
        for (Utilizador focus : lista) {
            if (focus.getUsername().equals(username)) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public String showData() {
        return "";
    }

    /**
     * Shows information of Registo
     *
     * @return String containing information about the users of the registry
     */
    @Override
    public String toString() {
        return lista.toString();
    }

    /**
     * Compares this RegistoUtilizadores to another object
     *
     * @param other the other object to compare to
     * @return true or false depending the object passes or not, respectively,
     * the test
     */
    @Override
    public boolean equals(Object other) {
        boolean result = (other != null) && (getClass() == other.getClass());
        if (result) {
            RegistoUtilizadores temp = (RegistoUtilizadores) other;
            if (lista != null) {
                result = true;
            }
            if (result) {
                result = lista.equals(temp.getLista());
            } else {
                result = lista == temp.getLista();
            }
        }
        return result;
    }
}
