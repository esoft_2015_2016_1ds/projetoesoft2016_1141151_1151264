/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registos;

import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import interfaces.iTOCS;
import java.awt.Event;
import model.Evento;
import model.Submissao;
import java.util.ArrayList;
import java.util.List;
import listas.ListaSubmissoes;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Utilizador;

/**
 *
 * @author Diogo
 */
public class RegistoEventos implements iTOCS {

    private List<Evento> listaEventos;
    private RegistoUtilizadores registoUtilizadores;

    /**
     * Creates an instance of object type {@link registos.RegistoEventos} with
     * the specified parameters.
     *
     * @param registoUtilizadores ({@link registos.RegistoUtilizadores}) The
     * user registry of the company.
     */
    public RegistoEventos(RegistoUtilizadores registoUtilizadores) {
        this(registoUtilizadores, new ArrayList());
    }

    /**
     * Creates a copy of the target instance of object type
     * {@link registos.RegistoEventos}.
     *
     * @param outroRegisto({@link registos.RegistoEventos}) The target registry
     * to copy.
     */
    public RegistoEventos(RegistoEventos outroRegisto) {
        listaEventos = outroRegisto.getListaEventos();
        registoUtilizadores = outroRegisto.getRegistoUtilizadores();
    }

    /**
     * Creates an instance of object type {@link registos.RegistoEventos} with
     * the specified parameters.
     *
     * @param registoUtilizadores ({@link registos.RegistoUtilizadores}) The
     * user registry of the company.
     * @param listaEventos (List&lt;{@link model.Evento}&gt;) The list of events
     * to inject on this registry.
     */
    public RegistoEventos(RegistoUtilizadores registoUtilizadores, List<Evento> listaEventos) {
        this.registoUtilizadores = registoUtilizadores;
        this.listaEventos = new ArrayList(listaEventos);
    }

    /**
     * Método que procura todos os Eventos de um Organizador que se encontram no
     * estado de EmSubmissaoState
     * @param id (String) o id do Utilizador
     * @return List&lt;{@link model.Evento}&gt; a lista de todos os eventos do
     * organizador
     */
    public List<Evento> getListaEventosOrganizadorEmSubmissaoCameraReady(String id) {
        List<Evento> le = new ArrayList();
        for (Evento elemento : listaEventos) {
            if (elemento.temOrganizador(id) && elemento.isInEmSubmissaoCameraReadyState()) {
                le.add(elemento);
            }
        }
        return le;
    }
    /**
     * Returns a list containing all the events with the specified revisor.
     * @param r ({@link model.Revisor}) The target revisor to search for.
     * @return (List&lt;{@link model.Evento}&gt;) The event list.
     */
    public List<Evento> getListaEventosComRevisor(Revisor r) {
        List<Evento> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.temRevisor(r.getUtilizador().getEmail())) {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * Returns the revisor that corresponds to the specified user.
     * If the revisor does not exist anywhere on any event or its thematic
     * sessions, null is returned.
     * @param u ({@link model.Utilizador}) The target user.
     * @return ({@link model.Revisor}) The revisor.
     */
    public Revisor getRevisor(Utilizador u)
    {
        Revisor result = null;
        for (Evento focus:listaEventos)
        {
            result = focus.getRevisor(u);
            if (result!=null)
            {
                break;
            }
        }
        return result;
    }

    /**
     * Returns a list of all revisions in the system.
     *
     * @return (List&lt;{@link model.Revisao}&gt;) A list containing all
     * revisions.
     */
    public List<Revisao> getListaTodasRevisoes() {
        List<Revisao> result = new ArrayList();
        for (Evento focus : listaEventos) {
            List<Revisao> lr = focus.getListaTodasRevisoes();
            result.addAll(lr);
        }
        return result;
    }

    /**
     * Returns a list of {@link interfaces.Licitavel} that are currently
     * accepting licitations (@link model.Licitacao} from a user with the
     * specified id.
     *
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Licitavel}&gt;) The list of
     * submissiveis.
     */
    public List<Licitavel> getListaLicitaveisEmLicitacaoDe(String id) {
        ArrayList<Licitavel> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.isInEmLicitacaoState() && focus.temRevisor(id)) {
                result.add(focus);
            }
            result.addAll(focus.getListaLicitaveisEmLicitacaoDe(id));
        }
        return result;
    }

    public List<Evento> getListaEventosNaoCameraReadyDe(String id) {
        List<Evento> result = new ArrayList();
        for (Evento element : listaEventos) {
            if (!element.isInCameraReadyState() && element.temSubmissoesAutor(id)) {
                result.add(element);
            }
        }
        return result;
    }

    /**
     * Metodo que procura por um dado id, a lista de eventos desse utilizador
     * que estejam no estado de SessaoTematicaDefinida
     *
     * @param strId(String) do utilizador
     * @return List&lt;{@link model.Evento}&gt; lista de eventos
     */
    public List<Evento> getEventosOrganizadorEmEstadoSessaoTematicaDefinida(String strId) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        for (Evento elemento : listaEventos) {
            if (elemento.temOrganizador(strId) && elemento.isInSessaoTematicaDefinidaState()) {
                leOrganizador.add(elemento);
            }
        }
        return leOrganizador;
    }

    /**
     * Returns a list of all classes that implement
     * {@link interfaces.Submissivel} who are currently accepting submissions.
     *
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of
     * submissiveis.
     */
    public List<Submissivel> getListaSubmissiveisEmEstadoAceitaSubmissoes() {
        List<Submissivel> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.isInAceitaSubmissoesState()) {
                result.add(focus);
            }
            List<Submissivel> ls = focus.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes();
            if (ls != null) {
                result.addAll(ls);
            }
        }
        return result;
    }
    /**
     * Returns a list of {@link interfaces.Submissivel} that are accepting submissions and
     * belong to the specified user id.
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of submissiveis.
     */
    public List<Submissivel> getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador(String id) {
        List<Submissivel> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.isInAceitaSubmissoesState() && focus.temSubmissoesAutor(id)) {
                result.add(focus);
            }
            List<Submissivel> ls = focus.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
            if (ls != null) {
                result.addAll(ls);
            }
        }
        return result;
    }
    /**
     * Returns a list of events, in this event registry, that are accepting
     * submissions.
     * @return (List&lt;{@link model.Evento}&gt;) The list of events that are accepting
     * submissions.
     */
    public List<Evento> getEventosAceitaSubmissoes()
    {
        List<Evento> result= new ArrayList();
        for (Evento focus:listaEventos)
        {
            if (focus.isInAceitaSubmissoesState())
           {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * Metodo que procura por um dado id, a lista de sessoes temáticas desse
     * utilizador que estejam no estado de SessaoTematicaRegistadoState
     *
     * @param id(String) do utilizador
     * @return lista de sessões temáticas
     */
    public List<SessaoTematica> getSessoesTematicasProponenteEmEstadoRegistado(String id) {
        List<SessaoTematica> lst = new ArrayList<SessaoTematica>();
        for (Evento element : listaEventos) {
            lst.addAll(element.getSessoesTematicasComProponenteRegistadoState(id));
        }
        return lst;
    }

    /**
     * This method searches for Events of an {@link model.Organizador} that are
     * in EventoRegisteredState, adds them to a List and returns it.
     *
     * @param id (String) the ID of the {@link model.Organizador}
     * @return List&gt;{@link model.Evento}&lt; The list of the the events of the
     * {@link model.Organizador}
     */
    public List<Evento> getListaEventosRegistadosOrganizador(String id) {

        List<Evento> le = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.temOrganizador(id) && focus.isInRegistadoState()) {
                le.add(focus);
            }

        }
        return le;
    }

    /**
     * This method procures {{@link model.Evento}} that have have the "Main Event" or at least one {@link model.SessaoTematica} in "EmDecisaoState" whose {@link model.Proponente} is the user
     * @param id (String) The ID of the user
     * @return List (&gt;{@link model.Evento}&lt;) A list of {{@link model.Evento}} that have have the "Main Event" or at least one {@link model.SessaoTematica} in "EmDecisaoState" whose {@link model.Proponente} is the user
     */
    public List<Evento> getListaEventosEmDecisao(String id)
    {
        List<Evento> result = new ArrayList();
        for(Evento focus : listaEventos)
        {
            if((focus.temOrganizador(id)&&focus.isInEmDecisaoState()) || focus.temSessoesEmDecisao(id))
            {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * This method procures {@link model.Evento} that have at least one
     * {@link model.Submissao} that are in
     * {@link states.submissao.SubmissaoAceiteState} made by the user
     *
     * @param id (String) the user ID
     * @return result (List gt;{@link model.Evento}lt;) Returns a list of
     * {@link model.Evento} that verify the condition
     */
    public List<Evento> getListaEventosEmSubmissaoCR(String id) {
        List<Evento> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.temSubmissoesAceitesUtilizador(id) && focus.isInEmSubmissaoCameraReadyState()) {

                result.add(focus);
            }
        }
        return result;
    }

    /**
     * This method procures {@link model.Evento} that have the user as a {@link model.Organizador} or that have {@link model.SessaoTematica} where the user is it's {@link model.Proponente}
     * @param id (String) The user's ID
     * @return List &gt;{@link model.Evento}&lt; that have at least one {@link model.SessaoTematica} in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState}
     */
    public List<Evento> getEventosDistribuiveis(String id)
    {
        List<Evento> le = new ArrayList();
        for(Evento focus : listaEventos)
        {
            if((focus.temOrganizador(id)&& focus.isInEmLicitacaoState()) || focus.temSessoesDistribuiveis(id))
            {
                le.add(focus);
            }
        }
        return le;
    }
    /**
     * This method returns a list of {@link model.Submissao} of a user.
     * @param id (String) the user's ID
     * @return List of user Submissions
     */
    public List<Submissao> getSubmissoesUtilizador(String id) {
        ArrayList<Submissao> result = new ArrayList<Submissao>();
        for (Evento focus : listaEventos) {
            result.addAll(focus.getSubmissoesUtilizador(id));
        }
        return result;
    }

    /**
     * This method returns a list of the user's registered Submissions
     * throughout the existing events.
     *
     * @param id (String) the user's ID
     * @return List&lt;{@link model.Submissao}&gt; A list of the user's
     * registered Submissions
     */
    public List<Submissao> getSubmissoesRegistadasUtilizador(String id) {
        ArrayList<Submissao> result = new ArrayList();
        for (Evento focus : listaEventos) {
            if (focus.isInAceitaSubmissoesState()) {
                List<Submissao> lst = focus.getSubmissoesRegistadasUtilizador(id);
                if (!lst.isEmpty()) {
                    result.addAll(lst);
                }
            }
        }
        return result;
    }
    /*public RegistoSubmissoes getRegistoSubmissoes(Submissao sub)
     {
     RegistoSubmissoes result=null;
     for (Evento focus:getListaEventos())
     {
     if (result==null)
     {
     result = focus.getRegistoSubmissoes(sub);
     if (result==null)
     {
     result=focus.getRegistoSessoesTematicas().getRegistoSubmissoes(sub);
     }
     }
     }
     return result;
     }*/

    /**
     * Método que busca a Lista de Revisiveis de um dado utilizador que se
     * encontre no estado em Revisao
     *
     * @param id (String) id do utilizador
     * @return a lista revisivel
     */
    public List<Revisivel> getListaRevisivelUtilizadorEmEstadoRevisao(String id) {
        List<Revisivel> lr = new ArrayList();
        for (Evento elemento : listaEventos) {
            if (elemento.isInEmRevisao() && elemento.temOrganizador(id)) {
                lr.add(elemento);
            }
            lr.addAll(elemento.getListaRevisivelUtilizadorEmEstadoRevisao(id));
        }

        return lr;
    }

    public List<Submissao> getListaSubmissoesUtilizadorEmCameraReady(String id) {
        List<Submissao> lsFinal = new ArrayList();
        return lsFinal;
    }

    public ListaSubmissoes getListaSubmissao(Submissao sub) {
        ListaSubmissoes ls = new ListaSubmissoes();
        return ls;
    }

    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    private boolean addEvento(Evento e) {
        return listaEventos.add(e);
    }

    private boolean validaEvento(Evento e) {
        return true;
    }

    public Evento novoEvento() {
        return new Evento();
    }

    /**
     * This method searches the registry for events with a certain user as an
     * organizer needs testing
     *
     * @param id the identification (Email/username) of the user to search for
     * @return a list containing the events with the user as an organizer
     */
    public List<Evento> getEventosDeOrganizador(String id) {
        ArrayList<Evento> le = new ArrayList();
        for (Evento e : listaEventos) {
            if (e.temOrganizador(id)) {
                le.add(e);
            }
        }
        return le;
    }

    /**
     * This method searches the registry for events with thematic sessions that
     * have a certain user as an organizer needs testing
     *
     * @param id the identification (Email/username) of the user to search for
     * @return a list containing the events with thematic sessions that have the
     * user as an organizerr
     */
    public List<Evento> getEventosComSTDeProponente(String id) {
        ArrayList<Evento> le = new ArrayList<Evento>();
        for (Evento e : listaEventos) {
            if (e.temSessaoComProponente(id)) {
                le.add(e);
            }
        }
        return le;
    }

    /**
     * @return a copy of listaEventos
     */
    public ArrayList<Evento> getListaEventos() {
        ArrayList<Evento> le = new ArrayList();
        le.addAll(listaEventos);
        return le;
    }

    /**
     * This method returns all events that have gone through with the decision
     * process in this registry
     *
     * @return all events that have gone through with the decision process in
     * this registry
     */
    public ArrayList<Evento> getListaEventosDecididos() {
        ArrayList<Evento> le = new ArrayList();
        for (Evento e : this.listaEventos) {
            if (e.isInCameraReadyState() || e.isInEmSubmissaoCameraReadyState()) {
                le.add(e);
            }
        }
        return le;
    }

    /**
     * @return copy of the registoUtilizadores
     */
    public RegistoUtilizadores getRegistoUtilizadores() {
        return new RegistoUtilizadores(registoUtilizadores);
    }

    /**
     * @see #showData() 
     * @return
     */
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * To Stirng method
     * @return  a strinhg representative of the event registry
     */
    @Override
    public String toString(){
        String res = "";
        for(Evento e : this.listaEventos){
            res += e.toString();
            res +="\n";
        }
        return res;
    }
    
    /**
     * equals method
     * @param o object to comapre to
     * @return true or false depending on the equality of the objects
     */
    @Override
    public boolean equals(Object o){
        if(this.getClass() != o.getClass()){
            return false;
        }
        RegistoEventos reg = (RegistoEventos) o;
        for(int i = 0; i < this.listaEventos.size() ; i++){
            if(!this.listaEventos.get(i).equals(reg.getListaEventos().get(i))){
                return false;
            }
        }
        return true;
    }
}
