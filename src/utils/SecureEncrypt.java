/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class uses the Advanced Encryption Standard algorithm (AES), also referenced as Rijndael algorithm, 
 * to encrypt data in order to keep it secure. 
 * 
 * !!!! This class is currently in beta and does not work correctly. !!!!!
 * 
 * 
 * @author Diogo
 */
public class SecureEncrypt {
    /**
     * A special key to use in the process of encrypting or decrypting.
     */
    private byte[] key;
    private byte[] IV=new byte[16];
    private static byte[] defaultKey={'E','s','o','f','t','+','P','p','r','o','g','1','2','3','4','5'};
    /**
     * Creates an instance of object SecureEncrypt with null parameters.
     */
    public SecureEncrypt()
    {
        this(defaultKey);
    }
    /**
     * Creates an instance of object SecureEncrypt with the specified parameters.
     * @param key (byte[]) A special key used in the encryption/decryption process. An example of a key would be the byte array:
     * {'H','E','L','L','O'}
     */
    public SecureEncrypt(byte[] key)
    {
        setKey(key);
        (new SecureRandom()).nextBytes(IV);
    }
    /**
     * Sets the new value of the key used in the encryption/decryption process.
     * @param newKey (byte[]) New key.
     */
    public void setKey(byte[] newKey)
    {
        key=newKey;
    }
    /**
     * Returns the value of the current key of this instance.
     * @return (byte[]) Key.
     */
    public byte[] getKey()
    {
        return key;
    }
    /**
     * Encrypts an array of bytes with the AES encryption. 
     * The key inserted must be kept for decrypting the data afterwards.
     * @param dataArray (byte[]) Data to encrypt.
     * @return (byte[]) Encrypted data.
     */
    public byte[] encryptByteArray(byte[] dataArray)
    {
        byte[] result=null;
        SecretKeySpec keySpec = new SecretKeySpec(key,"AES");
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec,new IvParameterSpec(IV));
            result = cipher.doFinal(dataArray);
        }
        catch (NoSuchAlgorithmException |NoSuchPaddingException |InvalidKeyException | IllegalBlockSizeException |BadPaddingException |InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
            System.out.println("Encryption failed. Please see message above.");
        }
        if (IV==null)
        {
            System.out.println("NULL IV! Error incoming!");
        }
        return result;
    }
    /**
     * Encrypts a string with the AES encryption and UTF-8 encoding.
     * The key inserted must be kept for decrypting the data afterwards.
     * @param plainText (String) The text to encrypt.
     * @return (String) Encrypted data in String form.
     */
    public String encryptString(String plainText)
    {
        String result="";
        try
        {
            result = new String(encryptByteArray(plainText.getBytes("UTF-8")),"UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            System.out.println("Encryption failed. Please see message above.");
        }
        return (result);
    }
    /**
     * Decrypts an array of bytes with the AES encryption.
     * The key inserted must be the one used for encrypting the data.
     * @param dataArray (byte[]) Data to decrypt.
     * @return (byte[]) Decrypted data.
     */
    public byte[] decryptByteArray(byte[] dataArray)
    {
        byte[] result=null;
        SecretKeySpec keySpec = new SecretKeySpec(key,"AES");
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec,new IvParameterSpec(IV));
            result = cipher.doFinal(dataArray);
        }
        catch (NoSuchAlgorithmException |NoSuchPaddingException |InvalidKeyException | IllegalBlockSizeException |BadPaddingException |InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
            System.out.println("Decryption failed. Please see message above.");
        }
        if (result==null)
        {
            System.out.println("Null decryption");
        }
        return result;
    }
    /**
     * Decrypts a string with the AES encryption and UTF-8 encoding.
     * The key inserted must be the one used for encrypting the data.
     * @param cypherText (String) The cypher text to decrypt.
     * @return (String) Decrypted data in string form.
     */
    public String decryptString(String cypherText)
    {
        String result="";
        try
        {
            result = new String(decryptByteArray(cypherText.getBytes("UTF-8")),"UTF-8");
        }
        catch(UnsupportedEncodingException e)
        {
            e.printStackTrace();
            System.out.println("Decryptiong failed. Please see message above.");
        }
        if (result==null)
        {
            System.out.println("Null decryption");
        }
        return result;
    }
}
