/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.wrappers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import model.Autor;
import utils.xmlFigures.figures.AutorFigure;
import utils.xmlFigures.figures.XMLSetUp;

/**
 * The xml representation of a group of {@link model.Autor} classes.
 * This class allows for easy saving and loading of all contents of authors to
 * an xml file.
 * @author jbraga
 */
@XmlRootElement(name="ListaAutores")
@XmlType(propOrder={"numeroAutores","listaAutores"})
public class AutorWrapper implements XMLSetUp{
    private int numeroAutores;
    private List<AutorFigure> listaAutores;
    @Override
    public void setUpSave() {
        for (AutorFigure focus:listaAutores)
        {
            focus.setUpSave();
        }
    }

    @Override
    public void setUpLoad() {
        for (AutorFigure focus:listaAutores)
        {
            focus.setUpLoad();
        }
    }

    /**
     * @return the numeroAutores
     */
    public int getNumeroAutores() {
        return numeroAutores;
    }

    /**
     * @param numeroAutores the numeroAutores to set
     */
    @XmlElement (name="NumeroAutores")
    public void setNumeroAutores(int numeroAutores) {
        this.numeroAutores = numeroAutores;
    }
    
    /**
     * @return the listaAutores
     */
    public List<AutorFigure> getListaAutores() {
        return listaAutores;
    }

    /**
     * @param listaAutores the listaAutores to set
     */
    @XmlElementWrapper(name = "Autores")
    @XmlElement(name="Autor")
    public void setListaAutores(List<AutorFigure> listaAutores) {
        this.listaAutores = listaAutores;
    }

    @Override
    public List<Autor> convertFromXMLFigure() {
        List<Autor> result=new ArrayList();
        for (AutorFigure focus:listaAutores)
        {
            if (focus.convertFromXMLFigure()!=null)
            {
                result.add(focus.convertFromXMLFigure());
            }
        }
        return result;
    }

    @Override
    public void convertToXMLFigure(Object e) {
        List<AutorFigure> result=new ArrayList();
        if (e instanceof List)
        {
            List<Autor> u=(List<Autor>) e;
            if (u.get(0) instanceof Autor)
            {
                for (Autor focus:u)
                {
                    result.add(new AutorFigure(focus));
                }
                setListaAutores(result);
            }
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }
    
}
