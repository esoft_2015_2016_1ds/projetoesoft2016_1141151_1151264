/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.wrappers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import model.Empresa;
import model.Utilizador;
import utils.xmlFigures.figures.UtilizadorFigure;
import utils.xmlFigures.figures.XMLSetUp;

/**
 * The xml representation of a group of {@link model.Utilizador} classes.
 * This class allows for easy saving and loading of all contents of users to
 * an xml file.
 * @author jbraga
 */
@XmlRootElement(name="ListaUtilizadores")
@XmlType(propOrder={"numeroUtilizadores","listaUtilizadores"})
public class UtilizadorWrapper implements XMLSetUp{
    private int numeroUtilizadores;
    private List<UtilizadorFigure> listaUtilizadores;
    private Empresa e;
    /**
     * Creates an instance of {@link utils.xmlFigures.wrappers.UtilizadorWrapper}
     * with null parameters.
     */
    public UtilizadorWrapper()
    {
        
    }
    /**
     * Creates an instance of {@link utils.xmlFigures.wrappers.UtilizadorWrapper}
     * with the specified parameters.
     * @param e  (@link model.Empresa) The company of the users. This is used
     * to get a random coding mecanism.
     */
    public UtilizadorWrapper(Empresa e)
    {
        this.e=e;
    }
    /**
     * @see #setUpSave() 
     */
    @Override
    public void setUpSave() {
        for (UtilizadorFigure focus:listaUtilizadores)
        {
            focus.setUpSave();
        }
    }
    /**
     * @see #setUpLoad()
     */
    @Override
    public void setUpLoad() {
        for (UtilizadorFigure focus:listaUtilizadores)
        {
            focus.setUpLoad();
        }
    }

    /**
     * @return the numeroUtilizadores
     */
    public int getNumeroUtilizadores() {
        return numeroUtilizadores;
    }

    /**
     * @param numeroUtilizadores the numeroUtilizadores to set
     */
    @XmlElement (name="NumeroElementos")
    public void setNumeroUtilizadores(int numeroUtilizadores) {
        this.numeroUtilizadores = numeroUtilizadores;
    }
    
    /**
     * @return the listaUtilizadores
     */
    public List<UtilizadorFigure> getListaUtilizadores() {
        return listaUtilizadores;
    }

    /**
     * @param listaUtilizadores the listaUtilizadores to set
     */
    @XmlElementWrapper(name = "Utilizadores")
    @XmlElement(name="Utilizador")
    public void setListaUtilizadores(List<UtilizadorFigure> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
    /**
     * @see #convertFromXMLFigure() 
     * @return 
     */
    @Override
    public List<Utilizador> convertFromXMLFigure() {
        List<Utilizador> result=new ArrayList();
        setUpLoad();
        for (UtilizadorFigure focus:listaUtilizadores)
        {
            if (focus.convertFromXMLFigure()!=null)
            {
                result.add(focus.convertFromXMLFigure());
            }
        }
        return result;
    }
    /**
     * @see #convertToXMLFigure(java.lang.Object) 
     * @param e 
     */
    @Override
    public void convertToXMLFigure(Object e) {
        List<UtilizadorFigure> result=new ArrayList();
        setUpSave();
        if (e instanceof List)
        {
            List<Utilizador> u=(List<Utilizador>) e;
            if (u.get(0) instanceof Utilizador)
            {
                for (Utilizador focus:u)
                {
                    result.add(new UtilizadorFigure(focus, this.e));
                }
                setListaUtilizadores(result);
            }
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }

    /**
     * @return the e
     */
    public Empresa getEmpresa() {
        return e;
    }

    /**
     * @param e the e to set
     */
    @XmlTransient
    public void setEmpresa(Empresa e) {
        this.e = e;
        for (UtilizadorFigure focus:listaUtilizadores)
        {
            focus.setE(e);
        }
    }
    
}
