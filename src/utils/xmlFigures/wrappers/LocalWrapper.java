/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.wrappers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import model.Local;
import utils.xmlFigures.figures.LocalFigure;
import utils.xmlFigures.figures.XMLSetUp;

/**
 * The xml representation of a group of {@link model.Local} classes.
 * This class allows for easy saving and loading of all contents of locals to
 * an xml file.
 * @author Diogo
 */
@XmlRootElement(name="ListaLocais")
@XmlType(propOrder={"numeroLocais","listaLocais"})
public class LocalWrapper implements XMLSetUp{
    private int numeroLocais;
    private List<LocalFigure> listaLocais;
    @Override
    public void setUpSave() {
        for (LocalFigure focus:listaLocais)
        {
            focus.setUpSave();
        }
    }

    @Override
    public void setUpLoad() {
        for (LocalFigure focus:listaLocais)
        {
            focus.setUpLoad();
        }
    }

    /**
     * @return the numeroLocais
     */
    public int getNumeroLocais() {
        return numeroLocais;
    }

    /**
     * @param numeroLocais the numeroLocais to set
     */
    @XmlElement (name="NumElementos")
    public void setNumeroLocais(int numeroLocais) {
        this.numeroLocais = numeroLocais;
    }
    
    /**
     * @return the listaLocais
     */
    public List<LocalFigure> getListaLocais() {
        return listaLocais;
    }

    /**
     * @param listaLocais the listaLocais to set
     */
    @XmlElementWrapper(name = "Locais")
    @XmlElement(name="Local")
    public void setListaLocais(List<LocalFigure> listaLocais) {
        this.listaLocais = listaLocais;
    }

    @Override
    public List<Local> convertFromXMLFigure() {
        List<Local> result=new ArrayList();
        for (LocalFigure focus:listaLocais)
        {
            if (focus.convertFromXMLFigure()!=null)
            {
                result.add(focus.convertFromXMLFigure());
            }
        }
        return result;
    }

    @Override
    public void convertToXMLFigure(Object e) {
        List<LocalFigure> result=new ArrayList();
        if (e instanceof List)
        {
            List<Local> u=(List<Local>) e;
            if (u.get(0) instanceof Local)
            {
                for (Local focus:u)
                {
                    result.add(new LocalFigure(focus));
                }
                setListaLocais(result);
            }
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }
}
