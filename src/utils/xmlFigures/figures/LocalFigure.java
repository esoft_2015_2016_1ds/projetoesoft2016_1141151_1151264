/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.Local;

/**
 * The xml representation of a {@link model.Local} class.
 * This class allows for easy saving and loading of all contents of a local to
 * an xml file.
 * @author Diogo
 */
@XmlRootElement
public class LocalFigure implements XMLSetUp{

    private String localID;
    private String designacao;
    
    public LocalFigure()
    {
        
    }
    public LocalFigure(Local l)
    {
        convertToXMLFigure(l);
    }
    @Override
    public void convertToXMLFigure(Object us)
    {
        if (us instanceof Local)
        {
            Local u=(Local) us;
            setLocalID(u.getId());
            setDesignacao(u.getLocal());
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }
    @Override
    public Local convertFromXMLFigure()
    {
        return new Local(localID,designacao);
    }
    
    @Override
    public void setUpSave() {
        
    }
    
    @Override
    public void setUpLoad() {
        
    }

    /**
     * @return the localID
     */
    public String getLocalID() {
        return localID;
    }

    /**
     * @param localID the localID to set
     */
    @XmlElement (name="LocalID")
    public void setLocalID(String localID) {
        this.localID = localID;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao the designacao to set
     */
    @XmlElement(name="Designacao")
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }
    
}
