/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import utils.Data;

/**
 * The xml representation of a {@link utils.Data} class.
 * This class allows for easy saving and loading of all contents of a date to
 * an xml file.
 * @author Diogo
 */
public class DataFigure implements XMLSetUp{
    private int ano;
    private int mes;
    private int dia;
    private String outputData;
    @Override
    public void setUpSave() {
        outputData = String.valueOf(ano)+"/"+String.valueOf(mes)+"/"+String.valueOf(dia);
    }

    @Override
    public void setUpLoad() {
        String[] chunk = outputData.split("/");
        if (chunk.length!=3)
        {
            throw new RuntimeException("Erro ao ler o ficheiro.");
        }
        ano=Integer.parseInt(chunk[0]);
        mes=Integer.parseInt(chunk[1]);
        dia=Integer.parseInt(chunk[2]);
    }

    @Override
    public Data convertFromXMLFigure() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void convertToXMLFigure(Object e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
