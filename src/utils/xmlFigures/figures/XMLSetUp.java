/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

/**
 * Interface to allow figures to prepare to be saved and loaded.
 * @author jbraga
 */
public interface XMLSetUp {
    /**
     * Method that a figure executes to set up any operations before being saved.
     */
    public void setUpSave();
    /**
     * Method that a figure executes to set up any operations before being loaded.
     */
    public void setUpLoad();
    /**
     * Converts an xml figure into the corresponding object.
     * @return (Object) The designated object.
     */
    public Object convertFromXMLFigure();
    /**
     * Converts an object into the corresponding xml figure.
     * @param e (Object) The target object.
     */
    public void convertToXMLFigure(Object e);
}
