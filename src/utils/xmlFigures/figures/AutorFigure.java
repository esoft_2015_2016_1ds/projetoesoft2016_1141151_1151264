/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.Autor;

/**
 * The xml representation of a {@link model.Autor} class.
 * This class allows for easy saving and loading of all contents of an author to
 * an xml file.
 * @author jbraga
 */
@XmlRootElement
public class AutorFigure implements XMLSetUp{
    private String nomeAutor;
    private String emailAutor;
    private String filiacao;
    private String usernameAutor;
    public AutorFigure()
    {
        
    }
    public AutorFigure(Autor a)
    {
        convertToXMLFigure(a);
    }
    @Override
    public void setUpSave() {
        
    }

    @Override
    public void setUpLoad() {
        
    }

    @Override
    public Autor convertFromXMLFigure() {
        return new Autor(nomeAutor,emailAutor,filiacao,usernameAutor);
    }

    @Override
    public void convertToXMLFigure(Object e) {
        if (e instanceof Autor)
        {
            Autor a=(Autor) e;
            setNomeAutor(a.getName());
            setFiliacao(a.getInstituicaoAfiliacao());
            setUsernameAutor(a.getUsername());
            setEmailAutor(a.getEmail());
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }

    /**
     * @return the nomeAutor
     */
    public String getNomeAutor() {
        return nomeAutor;
    }

    /**
     * @param nomeAutor the nomeAutor to set
     */
    @XmlElement
    public void setNomeAutor(String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }

    /**
     * @return the emailAutor
     */
    public String getEmailAutor() {
        return emailAutor;
    }

    /**
     * @param emailAutor the emailAutor to set
     */
    @XmlElement
    public void setEmailAutor(String emailAutor) {
        this.emailAutor = emailAutor;
    }

    /**
     * @return the filiacao
     */
    public String getFiliacao() {
        return filiacao;
    }

    /**
     * @param filiacao the filiacao to set
     */
    @XmlElement
    public void setFiliacao(String filiacao) {
        this.filiacao = filiacao;
    }

    /**
     * @return the usernameAutor
     */
    public String getUsernameAutor() {
        return usernameAutor;
    }

    /**
     * @param usernameAutor the usernameAutor to set
     */
    @XmlElement
    public void setUsernameAutor(String usernameAutor) {
        this.usernameAutor = usernameAutor;
    }
    
}
