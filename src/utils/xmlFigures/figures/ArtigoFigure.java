/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import listas.ListaAutores;
import model.Artigo;
import model.AutorCorrespondente;
import utils.Data;

/**
 * The xml representation of a {@link model.Artigo} class.
 * This class allows for easy saving and loading of all contents of an article to
 * an xml file.
 * @author jbraga
 */
@XmlRootElement
public class ArtigoFigure implements XMLSetUp{
    private String tituloArtigo;
    private String resumo;
    private ListaAutores listaAutores;
    private AutorCorrespondente autorCorrespondente;
    private String ficheiro;
    private Data dataSubmissao;
    private List<String> palavrasChave;
    public ArtigoFigure()
    {
        
    }
    public ArtigoFigure(Artigo u)
    {
        convertToXMLFigure(u);
    }
    @Override
    public void setUpSave() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUpLoad() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Artigo convertFromXMLFigure() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void convertToXMLFigure(Object e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the tituloArtigo
     */
    public String getTituloArtigo() {
        return tituloArtigo;
    }

    /**
     * @param tituloArtigo the tituloArtigo to set
     */
    @XmlElement
    public void setTituloArtigo(String tituloArtigo) {
        this.tituloArtigo = tituloArtigo;
    }

    /**
     * @return the resumo
     */
    public String getResumo() {
        return resumo;
    }

    /**
     * @param resumo the resumo to set
     */
    @XmlElement
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * @return the listaAutores
     */
    public ListaAutores getListaAutores() {
        return listaAutores;
    }

    /**
     * @param listaAutores the listaAutores to set
     */
    @XmlElement
    public void setListaAutores(ListaAutores listaAutores) {
        this.listaAutores = listaAutores;
    }

    /**
     * @return the autorCorrespondente
     */
    public AutorCorrespondente getAutorCorrespondente() {
        return autorCorrespondente;
    }

    /**
     * @param autorCorrespondente the autorCorrespondente to set
     */
    @XmlElement
    public void setAutorCorrespondente(AutorCorrespondente autorCorrespondente) {
        this.autorCorrespondente = autorCorrespondente;
    }

    /**
     * @return the ficheiro
     */
    public String getFicheiro() {
        return ficheiro;
    }

    /**
     * @param ficheiro the ficheiro to set
     */
    @XmlElement
    public void setFicheiro(String ficheiro) {
        this.ficheiro = ficheiro;
    }

    /**
     * @return the dataSubmissao
     */
    public Data getDataSubmissao() {
        return dataSubmissao;
    }

    /**
     * @param dataSubmissao the dataSubmissao to set
     */
    @XmlElement
    public void setDataSubmissao(Data dataSubmissao) {
        this.dataSubmissao = dataSubmissao;
    }

    /**
     * @return the palavrasChave
     */
    public List<String> getPalavrasChave() {
        return palavrasChave;
    }

    /**
     * @param palavrasChave the palavrasChave to set
     */
    @XmlElement
    public void setPalavrasChave(List<String> palavrasChave) {
        this.palavrasChave = palavrasChave;
    }
    
}
