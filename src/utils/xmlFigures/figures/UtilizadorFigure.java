/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import model.Empresa;
import model.Utilizador;
import utils.Coder;
import utils.NaoFazNada;

/**
 * The xml representation of a {@link model.Utilizador} class.
 * This class allows for easy saving and loading of all contents of a user to
 * an xml file.
 * @author jbraga
 */
@XmlRootElement
public class UtilizadorFigure implements XMLSetUp{
    private String nome;
    private String username;
    private String password;
    private String email;
    private Coder tabela;
    private Empresa e;
    /**
     * Creates an instance of object {@link utils.xmlFigures.figures.UtilizadorFigure}
     * with null parameters.
     */
    public UtilizadorFigure()
    {
        
    }
    /**
     * Creates an instance of object {@link utils.xmlFigures.figures.UtilizadorFigure}
     * with the specified parameters.
     * @param u ({@link model.Utilizador}) The user to conver to xml.
     * @param e ({@link model.Empresa}) The company where this user belongs to.
     */
    public UtilizadorFigure(Utilizador u,Empresa e)
    {
        convertToXMLFigure(u);
        this.e=e;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    @XmlElement(name="Nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    @XmlElement(name="Username")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    @XmlElement(name="Password")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    @XmlElement(name="Email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tabela
     */
    public Coder getTabela() {
        return tabela;
    }

    /**
     * @param tabela the tabela to set
     */
    @XmlTransient
    public void setTabela(Coder tabela) {
        this.tabela = tabela;
    }
    @Override
    public void convertToXMLFigure(Object us)
    {
        if (us instanceof Utilizador)
        {
            Utilizador u=(Utilizador) us;
            setNome(u.getName());
            setPassword(u.getPassword());
            setEmail(u.getEmail());
            setUsername(u.getUsername());
            setTabela(u.getTabela());
        }
        else
        {
            throw new IllegalArgumentException("Type missmatch.");
        }
    }
    @Override
    public Utilizador convertFromXMLFigure()
    {
        return new Utilizador(nome,username,password,email,tabela);
    }
    
    
    @Override
    public void setUpSave() {
        setPassword(tabela.toString()+";"+tabela.decode(password));
    }
    
    @Override
    public void setUpLoad() {
        String[] chunk=password.split(";");
        String typeCoder = chunk[0];
        if (typeCoder.equalsIgnoreCase("NFN"))
        {
            tabela=new NaoFazNada();
        }
        else if (typeCoder.equalsIgnoreCase("CA"))
        {
            tabela=e.getRandomCoder();
        }
        setPassword(tabela.encode(chunk[1]));
    }

    /**
     * @return the e
     */
    public Empresa getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    @XmlTransient
    public void setE(Empresa e) {
        this.e = e;
    }
}
