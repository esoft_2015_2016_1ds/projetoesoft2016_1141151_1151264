/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.figures;

import java.io.File;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author jbraga
 */
public interface FigureLoader {
    /**
     * Gets the class whom this loader is responsible for.
     * @return 
     */
    public String loaderReponsibleClass();
    /**
     * Unmarshalls an object of this loader's type.
     * @param jaxbUnmarshaller (Unmarsheller) Unmarsheller used.
     * @param file (File) XML file to read from.
     * @return 
     */
    public Object loadObject(Unmarshaller jaxbUnmarshaller,File file);
    /**
     * Returns the wrapper associated with this loader.
     * @return (XMLSetUp) An XMLSetUp object that is a wrapper.
     */
    public Object getWrapper();
}
