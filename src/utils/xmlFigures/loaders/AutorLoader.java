/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.loaders;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import utils.xmlFigures.figures.FigureLoader;
import utils.xmlFigures.wrappers.AutorWrapper;

/**
 * A class responsible to load all authors in an xml file onto a 
 * {@link utils.xmlFigures.wrappers.AutorWrapper} object.
 * @author jbraga
 */
public class AutorLoader implements FigureLoader{
    private AutorWrapper autorWrapper;
    @Override
    public String loaderReponsibleClass() {
        return "AutorWrapper";
    }

    @Override
    public AutorWrapper loadObject(Unmarshaller jaxbUnmarshaller, File file) {
        autorWrapper = null;
        try 
        {
            autorWrapper = (AutorWrapper)jaxbUnmarshaller.unmarshal(file);
        } 
        catch (JAXBException ex) 
        {
            File errorFile = new File(file.getPath()+"loadingError.txt");
            try 
            {
                BufferedWriter buffer = new BufferedWriter(new FileWriter(errorFile));
                buffer.write(ex.getMessage());
                buffer.close();
            } 
            catch (IOException ex1) 
            {
                Logger.getLogger(UtilizadorLoader.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
        return autorWrapper;
    }
    @Override
    public AutorWrapper getWrapper()
    {
        return autorWrapper;
    }
}

