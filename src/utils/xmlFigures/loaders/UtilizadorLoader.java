/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.loaders;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import utils.xmlFigures.figures.FigureLoader;
import utils.xmlFigures.wrappers.UtilizadorWrapper;

/**
 * A class responsible to load all users in an xml file onto a 
 * {@link utils.xmlFigures.wrappers.UtilizadorWrapper} object.
 * @author jbraga
 */
public class UtilizadorLoader implements FigureLoader{
    private UtilizadorWrapper utilizadorWrapper;
    @Override
    public String loaderReponsibleClass() {
        return "UtilizadorWrapper";
    }

    @Override
    public UtilizadorWrapper loadObject(Unmarshaller jaxbUnmarshaller, File file) {
        utilizadorWrapper = null;
        try 
        {
            utilizadorWrapper = (UtilizadorWrapper)jaxbUnmarshaller.unmarshal(file);
        } 
        catch (JAXBException ex) 
        {
            File errorFile = new File(file.getPath()+"loadingError.txt");
            try 
            {
                BufferedWriter buffer = new BufferedWriter(new FileWriter(errorFile));
                buffer.write(ex.getMessage());
                buffer.close();
            } 
            catch (IOException ex1) 
            {
                Logger.getLogger(UtilizadorLoader.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
        return utilizadorWrapper;
    }
    @Override
    public UtilizadorWrapper getWrapper()
    {
        return utilizadorWrapper;
    }
}
