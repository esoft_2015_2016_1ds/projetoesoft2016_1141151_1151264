/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures.loaders;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import utils.xmlFigures.figures.FigureLoader;
import utils.xmlFigures.wrappers.LocalWrapper;

/**
 *
 * @author Diogo
 */
/**
 * A class responsible to load all locals in an xml file onto a 
 * {@link utils.xmlFigures.wrappers.LocalWrapper} object.
 * @author jbraga
 */
public class LocalLoader implements FigureLoader{
    private LocalWrapper localWrapper;
    @Override
    public String loaderReponsibleClass() {
        return "LocalWrapper";
    }
    @Override
    public LocalWrapper loadObject(Unmarshaller jaxbUnmarshaller, File file) {
        localWrapper = null;
        try 
        {
            localWrapper = (LocalWrapper)jaxbUnmarshaller.unmarshal(file);
        } 
        catch (JAXBException ex) 
        {
            File errorFile = new File(file.getPath()+"loadingError.txt");
            try 
            {
                BufferedWriter buffer = new BufferedWriter(new FileWriter(errorFile));
                buffer.write(ex.getMessage());
                buffer.close();
            } 
            catch (IOException ex1) 
            {
                Logger.getLogger(LocalLoader.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
        return localWrapper;
    }
   
    @Override
    public LocalWrapper getWrapper()
    {
        return localWrapper;
    }
}

