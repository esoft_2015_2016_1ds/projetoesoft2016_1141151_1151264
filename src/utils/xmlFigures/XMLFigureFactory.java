/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.xmlFigures;

import utils.xmlFigures.figures.FigureLoader;
import utils.xmlFigures.figures.XMLSetUp;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * A factory responsible to save and load classes of this application.
 * @author jbraga
 */
public abstract class XMLFigureFactory {
    private static final List<FigureLoader> loaderList=new ArrayList();
    public static final void convertFigureToXML(XMLSetUp object,File file) throws JAXBException
    {
        object.setUpSave();
        JAXBContext context = JAXBContext.newInstance(object.getClass());
        Marshaller  marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        marshaller.marshal(object, file);
        //Wmarshaller.marshal(object, System.out);
    }
    public static final void convertXMLtoFigure(Object object,File file) throws JAXBException
    {
	JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
 
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        for (int i=0;i<loaderList.size();i++)
        {
            FigureLoader loader = loaderList.get(i);
            String objectClass = object.getClass().toString().
                    substring(1+object.getClass().toString().lastIndexOf("."));
            if (loader.loaderReponsibleClass().equals(objectClass))
            {
                loader.loadObject(jaxbUnmarshaller,file);
            }
        }
    }
    public static final boolean addClassLoader(FigureLoader loader)
    {
        return loaderList.add(loader);
    }
}
