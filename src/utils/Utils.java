/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import model.Empresa;
import model.Evento;
import model.Utilizador;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author
 */
public class Utils
{
    /**
     * The modes used in countElements();
     */
    public static final int COUNT_NUMBERS=0,COUNT_LETTERS=1;
    /**
     * The regex pattern used to check if an email is valid.
     */
    private static final String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String pdf = "pdf";
    public static final String txt = "txt";
    public static final String xml = "xml";
    public static final String bin = "bin";
    public static final String xmlListaEventos="ListaEventos";
    public static final String xmlUtilizador="Utilizador";
    public static final String xmlLocal="Local";
    private static List<Evento> listEvent;
    private static List<Utilizador> listUser;
    /**
     * Returns the content of a file in a string format.
     * @param file (File) The target file.
     * @return (String) The content stored in the file.
     * @throws IOException In case there was a reading error.
     */
    public static String getDataFromFile(File file) throws IOException
    {
        String result="";
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String temp;
        while ((temp=buffer.readLine())!=null)
        {
            result+=temp+"\n";
        }
        result=result.substring(0,result.length()-1);
        buffer.close();
        return result;
    }
    /**
     * Gets the extension of a file.
     * @param file (File) The file to obtain the extension.
     * @return (String) The file's extension.
     */
    public static String getExtension(File file)
    {
        return getExtension(file.getName());
    }
    /**
     * Gets the extension of a file.
     * @param file (String) The file to obtain the extension.
     * @return (String) The file's extension.
     */
    public static String getExtension(String file)
    {
        String result="";
        for (int i=file.lastIndexOf('.')+1;i<file.length();i++)
        {
            result+=file.charAt(i);
        }
        return result.toLowerCase();
    }
    /**
     * Counts the amount of elements in a string according to the select mode.
     * 
     * The currently available modes for this method are as follows:
     * 
     * COUNT_NUMBERS - Counts the amount of numbers in a string.
     * COUNT_LETTER - Counts the amount of letters in a string.
     * @param data (String) Data to proccess
     * @param mode (int) The mode to choose from (constants avialable in Utilites.)
     * @return (int) The amount found.
     */
    public static int countElements(String data,int mode)
    {
        int result=0;
        switch (mode)
        {
            case COUNT_NUMBERS:
            for (int i=0;i<data.length();i++)
            {
                int focus=(int)data.charAt(i);
                result+=(focus<58 && focus>=48) ? 1 : 0;
            }
            break;
            case COUNT_LETTERS:
                for (int i=0;i<data.length();i++)
                {
                    int focus=(int)data.charAt(i);
                    result+=((focus<91 && focus>=65) || (focus<=122 && focus>=97)) ? 1 : 0;
                }
                break;
        }
        return result;
    }
    /**
     * Checks if the specified string has invalid characters.
     * @param in (String) The string to check.
     * @return (boolean) True if no invalid characters were found.
     */
    public static boolean checkForInvalidChars(String in)
    {
        boolean chars = true;
        for(char letter : in.toCharArray())
        {
            int focus = (int)letter;
            if((focus<65) || (focus<97 && focus>91) || (focus>122))
            {
                chars = false;
            }
        }
        return chars;
    }
    /**
     * Checks if the specified email is valid.
     * @param strEmail (String) The email to check.
     * @return (boolean) True if valid.
     */
    public static boolean checkEmail(String strEmail) // true if good
    {
        return strEmail.matches(emailPattern);
    }
    /**
     * Checks if the specified password is valid.
     * @param strPass (String) The password to check.
     * @return (boolean) True if valid.
     */
    public static boolean checkPassword(String strPass) 
    {
        return (strPass.length()>3) && (Utils.countElements(strPass,Utils.COUNT_LETTERS)>=3);
    }
    /**
     * Checks if the specified name is valid.
     * @param strName (String) The name to check.
     * @return (boolean) True if valid.
     */
    public static boolean checkName(String strName)
    {
        return (strName.length()>=2 && (countElements(strName,COUNT_LETTERS)!=0));
    }
    /**
     * Checks if the specified username is valid.
     * @param usrName (String) The username to check.
     * @return (boolean) True if valid.
     */
    public static boolean checkUsername(String usrName)
    {
        return  (usrName.length()>=2 && (countElements(usrName,COUNT_LETTERS)!=0));
    }
    /**
     * Parses a binary document with data related to the objects of this project and generates them,
     * retrieving a company object with all the saved data.
     * @param file (File) Binary file with the data.
     * @throws Exception
     * @return ({@code eventoscientificos.model.Empresa}) A debug string containing all the parsed data and messages
     * related to the parsing process.
     */
    public static Empresa generateDataFromBinary(File file) throws Exception
    {
        Empresa result=null;
            FileInputStream fstream = new FileInputStream(file);
            ObjectInputStream ostream = new ObjectInputStream(fstream);
            while (true)
            {
                Object obj=null;
                try 
                {
                    obj=ostream.readObject();
                }
                catch (EOFException | ClassNotFoundException e)
                {
                    if (e instanceof EOFException)
                    {
                        break;
                    }
                }
                if (obj!=null)
                result=(Empresa)obj;
            }
        return result;
    }
    /**
     * Generates a binary file with the data from a {@link model.Empresa} object.
     * @param file (File) Binary file with the data.
     * @param company ({@link model.Empresa}) The company object to save.
     * @throws Exception An exceptiong whilst writing.
     */
    public static void writeEmpresaAsBinary(File file,Empresa company) throws Exception
    {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(company);
        out.close();
    }
    /**
     * Returns the event list that contains the parsed data of generateDataFromXML().
     * @return (List&lt;EventO&gt;) The event list.
     */
    public static List<Evento> getParsedEventList()
    {
        return new ArrayList(listEvent);
    }
    /**
     * Returns the user list that contains the parsed data of generateDataFromXML().
     * @return (List&lt;User&gt;) The user list.
     */
    public static List<Utilizador> getParsedUserList()
    {
        return new ArrayList(listUser);
    }
    /**
     * Parses a date to an array containing the day, month and year.
     * @param date (String) Date to parse.
     * @param delimiter (String) The delimiter that seperates each component of the date.
     * @return (int[]) Array containing the components.
     */
    public static int[] convertStringDateToInteger(String date,String delimiter)
    {
        String[] arr = date.split(delimiter);
        if (arr.length!=3)
        {
            throw new IllegalArgumentException("A data encontra-se no formato errado!");
        }
        int[] result=new int[arr.length];
        for (int i=0;i<result.length;i++)
        {
            result[i]=Integer.parseInt(arr[i]);
        }
        return result;
    }
}
