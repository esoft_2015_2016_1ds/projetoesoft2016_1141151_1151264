/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import interfaces.iTOCS;
import java.util.Calendar;
import java.util.Date;

/** 
 * Representa uma data através do dia, mês e ano.
 * 
 * Nota: Esta classe é da autoria dos docentes de PPROG.
 * 
 * 
 * @author ISEP-DEI-PPROG
 * 
 */
public class Data implements iTOCS{
        
    /**
     * O ano da data.
     */
    private int ano;
    
    /**
    * O mês da data.
    */
    private int mes;
     
    /**
    * O dia da data.
    */
    private int dia;
        
    /**
    * Nomes dos dias da semana.
    */
    private static String[] nomeDiaDaSemana = {"Domingo", "Segunda-feira",
                                               "Terça-feira", "Quarta-feira",
                                               "Quinta-feira", "Sexta-feira",
                                               "Sábado"};
    
    /**
    * Número de dias de cada mês do ano.
    */
    private static int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30,
                                       31, 30, 31};
    
    /**
    * Nomes dos meses do ano.
    */
    private static String[] nomeMes = {"Inválido", "Janeiro", "Fevereiro",
                                       "Março", "Abril", "Maio", "Junho",
                                       "Julho", "Agosto", "Setembro",
                                       "Outubro", "Novembro", "Dezembro"};
   
    /**
     * O dia por omissão.
     */
    private static final int DIA_POR_OMISSAO = 1;
    
    /**
     * O mês por omissão.
     */
    private static final int MES_POR_OMISSAO = 1;
    
    /**
     * O ano por omissão.
     */
    private static final int ANO_POR_OMISSAO = 1;    
    
    /**
     * Constrói uma instância de Data recebendo o ano, o mês e o dia. 
     * 
     * @param ano o ano da data
     * @param mes o mês da data
     * @param dia o dia da data
     */
    public Data(int dia, int mes, int ano) {        
        this.ano = ano;
        this.mes = mes;
        this.dia = dia;              
    }

    /**
     * Constrói uma instância de Data com a data por omissão.  
     */
    public Data() {
        this(Data.DIA_POR_OMISSAO, Data.MES_POR_OMISSAO, Data.ANO_POR_OMISSAO);      
    }
    /**
     * Constrói uma instância de Data com as mesmas caraterísticas da data
     * recebida por parâmetro.   
     * 
     * @param outraData a data com as características a copiar
     */    
     public Data(Data outraData) {
        this(outraData.getDia(), outraData.getMes(), outraData.getAno());
    }
        
    /**
     * Devolve o ano da data.
     * 
     * @return ano da data
     */
    public int getAno() {
        return this.ano;
    }
    
    /**
     * Devolve o mês da data.
     * 
     * @return mês da data
     */
    public int getMes() {
        return this.mes;
    }

    /**
     * Devolve o dia da data.
     * 
     * @return dia da data
     */    
    public int getDia() {
        return this.dia;
    }
    /**
     * Converts this object into a {@link java.util.Date}.
     * @return ({@link java.util.Date}) The corresponding date object.
     */
    public Date toDate()
    {
        long mili = 86400000 *this.diferenca(new Data(1,1,1970));
        return new Date(mili);
        
    }
    /**
     * Modifica o ano, o mês e o dia da data.
     * 
     * @param ano o novo ano da data
     * @param mes o novo mês da data
     * @param dia o novo dia da data
     */    
    public void setData(int dia, int mes, int ano) {
        this.ano = ano;         
        this.mes = mes;       
        this.dia = dia;          
    }
    
    /**
     * Devolve a descrição textual da data no formato: diaDaSemana, dia de mês
     * de ano.
     * 
     * @return caraterísticas da data
     */
    @Override
    public String toString() {
        return this.diaDaSemana() + ", " + this.dia + " de " + 
                Data.nomeMes[this.mes] + " de " + this.ano;
    }
   
    /**
     * Devolve o dia da semana da data. 
     * 
     * @return dia da semana da data
     */
    public String diaDaSemana() {
        int totalDias = this.contaDias();
        totalDias = totalDias % 7;

        return Data.nomeDiaDaSemana[totalDias];
    }
    
    /**
     * Devolve o número de dias desde o dia 1/1/1 até à data.
     * 
     * @return número de dias desde o dia 1/1/1 até à data
     */
    private int contaDias() {
        int totalDias = 0;

        for (int i = 1; i < this.ano; i++) {
            totalDias += Data.isAnoBissexto(i) ? 366 : 365;
        }
        for (int i = 1; i < this.mes; i++) {
            totalDias += Data.diasPorMes[i];
        }
        totalDias += (Data.isAnoBissexto(this.ano) && this.mes > 2) ? 1 : 0;
        totalDias += this.dia;

        return totalDias;
    }

    /**
     * Devolve a data no formato:%04d/%02d/%02d.
     * 
     * @return caraterísticas da data
     */
    public String toAnoMesDiaString() {
        return String.format("%04d/%02d/%02d", this.ano, this.mes, this.dia);
    }
   
    /**
     * Devolve true se a data for maior do que a data recebida por parâmetro.  
     * Se a data for menor ou igual à data recebida por parâmetro, devolve
     * false.
     * 
     * @param outraData a outra data com a qual se compara a data
     * @return true se a data for maior do que a data recebida por parâmetro,
     *         caso contrário devolve false
     */
    public boolean isMaior(Data outraData) {
        int totalDias = this.contaDias();
        int totalDias1 = outraData.contaDias();
        return totalDias > totalDias1;
    }

    /**
     * Devolve a diferença em número de dias entre a data e a data recebida por
     * parâmetro.     
     * 
     * @param outraData a outra data com a qual se compara a data para calcular
     *        a diferença do número de dias
     * @return diferença em número de dias entre a data e a data recebida por
     *         parâmetro
     */
    public int diferenca(Data outraData) {
        int totalDias = this.contaDias();
        int totalDias1 = outraData.contaDias();

        return Math.abs(totalDias - totalDias1);
    }
    
    /**    
     * Devolve a diferença em número de dias entre a data e a data recebida por
     * parâmetro com ano, mês e dia    
     * 
     * @param ano o ano da data com a qual se compara a data para calcular a
     *        diferença do número de dias
     * @param mes o mês da data com a qual se compara a data para calcular a
     *        diferença do número de dias
     * @param dia o dia da data com a qual se compara a data para calcular a
     *        diferença do número de dias
     * @return diferença em número de dias entre a data e a data recebida por
     *         parâmetro com ano, mês e dia  
     */
    public int diferenca(int dia, int mes, int ano) {
        int totalDias = this.contaDias();
        Data outraData = new Data(dia, mes, ano);
        int totalDias1 = outraData.contaDias();

        return Math.abs(totalDias - totalDias1);
    }
    
    /**
     * Devolve true se o ano passado por parâmetro for bissexto.
     * Se o ano passado por parâmetro não for bissexto, devolve false.
     * 
     * @param ano o ano a validar
     * @return true se o ano passado por parâmetro for bissexto, caso contrário
     *         devolve false
     */        
    public static boolean isAnoBissexto(int ano) {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }
    /**
     * Gets the current date of the system.
     * @return (Data) The current Date.
     */
    public static Data getDataAtual()
    {
        Calendar cal = Calendar.getInstance();
        return new Data(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)); 
    }
    /**
     * Checks if this date is within the specified dates.
     * @param di (Data) The initial date.
     * @param df (Data) The ending date.
     * @return (boolean) 
     */
    public boolean checkDataWithin(Data di, Data df)
    {
       return isMaior(di) && df.isMaior(this);
    }

    @Override
    public String showData() {
        return ano+"/"+mes+"/"+dia;
    }
    
    /**
     * Euqals
     * @param o object to compare to
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean equals(Object o){
        if(this.getClass() != o.getClass())return false;
        Data d = (Data) o;
        return (d.contaDias()==this.contaDias());
    }
}

