
package utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The development of this class was based on the code available at:
 * https://code.google.com/p/jawelet/source/browse/trunk/src/ru/ifmo/diplom/kirilchuk/coding/ArithmeticCoding.java?r=25
 * with all rights reserved to the person who developed it.
 * @author Diogo
 */
public class TabelaFrequencia implements Coder{
    
    private List<Integer> alphabet;
    private double[] probabilities;
    private String id;
    
    public TabelaFrequencia(){
        this.alphabet = new ArrayList<Integer>(){};
        this.alphabet.add(1);
        this.alphabet.add(2);
        this.alphabet.add(3);
        
        this.probabilities = new double[]{0.4,0.35,0.25};
    }
    
    public TabelaFrequencia(List<Integer> alphabet, double[] probabilities){
        this.alphabet = alphabet;
        this.probabilities = probabilities;
    }
    
    public double shortEncode(int[] sequence) {
        int n = sequence.length;      

        int alphabetSize = alphabet.size();
        double[] qumulative = new double[alphabetSize];

        for (int i = 1; i < alphabetSize; ++i) {
            qumulative[i] = qumulative[i - 1] + probabilities[i - 1];
        }

        BigDecimal f = new BigDecimal(0);
        BigDecimal g = new BigDecimal(1);
        for (int i = 0; i < n; ++i) {
            int index = alphabet.indexOf(sequence[i]);
            double p = probabilities[index];
            double q = qumulative[index];

            BigDecimal pG = g.multiply(new BigDecimal(p));                                  
            BigDecimal qG = g.multiply(new BigDecimal(q));

            f = f.add(qG);
            //System.out.println("f: " + f.doubleValue());

            g = pG;
            //System.out.println("g: " + g.doubleValue());
        }
        double result = f.doubleValue() + g.doubleValue()/2;
        long codeLength = (long)(-(Math.log(g.doubleValue()) / Math.log(2.0)) + 1) + 1;
        return result;
        //System.out.println("length: " + codeLength);
}

    public List<Integer> shortDecode(int decodeLength, double code) {
        List<Integer> result = new ArrayList<Integer>();

        int alphabetSize = alphabet.size();
        double[] qumulative = new double[alphabetSize + 1];
        for (int i = 1; i < alphabetSize; ++i) {
                qumulative[i] = qumulative[i - 1] + probabilities[i - 1];
        }

        qumulative[alphabetSize] = 1;

        BigDecimal s = new BigDecimal(0);
        BigDecimal g = new BigDecimal(1);

        for (int i = 0; i < decodeLength; ++i) {
            int j = 0;                                                              
            while(s.add(g.multiply(new BigDecimal(qumulative[j+1]))).doubleValue() < code) {
                    j++;
            }

            s = s.add(g.multiply(new BigDecimal(qumulative[j])));   
            g = g.multiply(new BigDecimal(probabilities[j]));                       

            result.add(alphabet.get(j));
        }
        return result;
    }
    
    public List<String> pack(String message){
        ArrayList<String> packed = new ArrayList();
        int length = message.length();
        int sub = 0;
        String temp;
        if(message.length() < 5){
           packed.add(message);
           return packed;
        }
        
        while(length >= 5){
            length -=5;
            temp = message.substring(sub*5,(sub+1)*5);
            packed.add(temp);
            sub++;
        }
        return packed;
    }
    
    public String unpack(List<String> message){
        String result ="";
        for(String i : message){
            result += i;
        }
        return result;
    }
    
    public String encode(String message){
        int length = message.length();
        List<String> packed = pack(message);
        String temp = "";
        for(String i : packed){
            char[] tempChar = i.toCharArray();
            int[] seq = new int[tempChar.length];
            for(int j =0; j < tempChar.length; j++){
                seq[j] = tempChar[j];
            }
            temp += shortEncode(seq)+"#";
        }
        temp += length;
        return temp;
    }
    
    public String decode(String message){
        String temp[];
        temp = message.split("#");
        int length = Integer.parseInt(temp[temp.length -1]);
        List<String> result= new ArrayList();
        List<Integer> tempInt;
        char[] chars;
        for(int i = 0; i < temp.length-1; i++){
            double code = Double.parseDouble(temp[i]);
            if(length >= 5){
                tempInt = shortDecode(5,code);
                chars = new char[tempInt.size()];
                for(int j = 0 ; j< chars.length; j++){
                    chars[j] =(char) tempInt.get(j).intValue();
                }
                result.add(new String(chars));
            } else {
                tempInt = shortDecode(length,code);
                chars = new char[tempInt.size()];
                for(int j = 0 ; j< chars.length; j++){
                    chars[j] =(char) tempInt.get(j).intValue();
                }
                result.add(new String(chars));
            }
            length -= 5;
        }
        return unpack(result);
    }
    
    /**
     * 
     * @return a String representative of this class's instance 
     */
    @Override
    public String toString(){
        return "CA;"+getId();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
