/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Gonçalo
 */
public class NaoFazNada implements Coder{
    
    /**
     * NaoFazNada constructor
     */
    public NaoFazNada(){}
    
    /**
     * @param message the message to copy
     * @return a copy of the message
     */
    @Override
    public String encode(String message){
        return message;
    }
    
    /**
     * @param message the message to copy
     * @return a copy of the message
     */
    @Override
    public String decode(String message){
        return message;
    }
    
    /**
     * Método equals de não faz nada, como todos os NaoFaz nada sao iguais retorna
     * sempre true desde que sejam da mesma classe
     * @param o the object to compare to.
     */
    @Override
    public boolean equals(Object o){
        return this.getClass() == o.getClass();
    }
    /**
     * The textual format of this class.
     * It retrieves the type of coding of this class.
     * @return (String) Text form.
     */
    @Override
    public String toString()
    {
        return "NFN";
    }
}
