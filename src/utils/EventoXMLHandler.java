/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import interfaces.EventoState;
import interfaces.SessaoTematicaState;
import interfaces.SubmissaoState;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import listas.ListaAutores;
import model.Artigo;
import model.Autor;
import model.CP;
import model.Decisao;
import model.Evento;
import model.Local;
import model.Organizador;
import model.ProcessoDecisao;
import model.ProcessoDistribuicao;
import model.Proponente;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import registos.RegistoUtilizadores;
import ui.MainMenu;
import static utils.Utils.convertStringDateToInteger;

/**
 * This class is reponsible for loading the information of several events in an XML
 * file.
 * @author Diogo
 */
public class EventoXMLHandler {
    /**
     * Parses an XML document with data related to the events of this project and generates them,
     * @param file (File) XML file with the data.
     * @param reg ({@link registos.RegistoUtilizadores}) The registry of users of the company.
     * @param local (List&lt;{@link model.Local}&gt;) The list of places of the company.
     * @throws Exception
     * @return (String) A debug string containing all the parsed data and messages
     * related to the parsing process.
     */
    public static List<Evento> generateEventDataFromXML(File file,RegistoUtilizadores reg,List<Local> local) throws Exception
    {
        List<Evento> result=new ArrayList();
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(file);
        doc.getDocumentElement().normalize();
        doc.normalize();
        result.addAll(getEventDataFromDocument(doc,reg,local));
        return result;
    }
    /**
     * Generates a list of events from the specified document.
     * @param doc (Document) The document that contains data about several events
     * in an XML format.
     * @param reg ({@link registos.RegistoUtilizadores}) The company's event registry.
     * @param local (List&lt;{@link model.Local}&gt;) The company's list of places.
     * @return (List&lt;{@link model.Evento}&gt;) The generated list of events.
     */
    public static List<Evento> getEventDataFromDocument(Document doc,RegistoUtilizadores reg,List<Local> local)
    {
        List<Evento> listEvent= new ArrayList();
        NodeList innerObject = doc.getElementsByTagName("Eventos");
        NodeList listOfObjects = (NodeList)((Element)innerObject.item(0)).getElementsByTagName("Evento");
        Evento lastEventParsed = null;
        for (int i=0;i<listOfObjects.getLength();i++)
        {
            Node nextElement = listOfObjects.item(i);
            if (nextElement.getNodeType() == Node.ELEMENT_NODE)
            {
                Element object = (Element) nextElement;
                    String title = object.getElementsByTagName("Titulo").item(0).getTextContent(),
                            description = object.getElementsByTagName("Descricao").item(0).getTextContent(),
                            inicialDate=object.getElementsByTagName("DataInicio").item(0).getTextContent(),
                            finalDate=object.getElementsByTagName("DataFim").item(0).getTextContent(),
                            dataInicioSubmissao=object.getElementsByTagName("DataInicioSubmissao").item(0).getTextContent(),
                            dataFimSubmissao=object.getElementsByTagName("DataFimSubmissao").item(0).getTextContent(),
                            dataInicioDistribuicao=object.getElementsByTagName("DataInicioDistribuicao").item(0).getTextContent(),
                            dataLimiteRevisao=object.getElementsByTagName("DataLimiteRevisao").item(0).getTextContent(),
                            dataLimiteSubmissaoFinal=object.getElementsByTagName("DataLmiteSubmissaoFinal").item(0).getTextContent(),
                            estadoEvento=object.getElementsByTagName("EstadoEvento").item(0).getTextContent(),
                            localText=object.getElementsByTagName("Local").item(0).getTextContent();
                            
                    int[] initDate=convertStringDateToInteger(inicialDate,"/"),
                            finDate=convertStringDateToInteger(finalDate,"/"),
                            initSubDate=convertStringDateToInteger(dataInicioSubmissao,"/"),
                            finSubDate=convertStringDateToInteger(dataFimSubmissao,"/"),
                            initDistDate=convertStringDateToInteger(dataInicioDistribuicao,"/"),
                            revDate=convertStringDateToInteger(dataLimiteRevisao, "/"),
                            subFinalDate=convertStringDateToInteger(dataLimiteSubmissaoFinal, "/");
                    Local eventLocal = null;
                    for (Local focus:local)
                    {
                        if (focus.getId().equals(localText))
                        {
                            eventLocal=focus;
                            break;
                        }
                    }
                    lastEventParsed = new Evento();
                    if (eventLocal==null)
                    {
                        throw new IllegalArgumentException("Local não existe na empresa!");
                    }
                    NodeList orgTree = ((Element) object.getElementsByTagName("ListaOrganizadores").item(0)).getElementsByTagName("Organizador");
                    for (int j=0;j<orgTree.getLength();j++)
                    {
                        String prop = orgTree.item(j).getTextContent();
                        if (reg.temUtilizadorUsername(prop))
                        {
                            lastEventParsed.addOrganizador(reg.getUtilizador(prop));
                        }
                        else
                        {
                            throw new IllegalArgumentException("Organizador não pertence ao registo de utilizadores!");
                        }
                    }
                    CP cp = new CP();
                    NodeList cpTree = ((Element) object.getElementsByTagName("CP").item(0)).getElementsByTagName("MembroCP");
                    for (int j=0;j<cpTree.getLength();j++)
                    {
                        String cpMembro = cpTree.item(j).getTextContent();
                        if (reg.temUtilizadorUsername(cpMembro))
                        {
                            cp.addMembroCP(new Revisor(reg.getUtilizador(cpMembro)));
                        }
                        else
                        {
                            throw new IllegalArgumentException("O revisor da CP do evento não é um utilizador registado no sistema.");
                        }
                    }
                    NodeList stTree = ((Element) object.getElementsByTagName("ListaSessoesTematicas").item(0)).getElementsByTagName("SessaoTematica");
                    List<SessaoTematica> stList = parseThematicSessionsFromNode(stTree,reg);
                    ProcessoDistribuicao pd = new ProcessoDistribuicao();
                    ProcessoDecisao pdc = new ProcessoDecisao();
                    List<Submissao> subList=null;
                    if (((Element) object.getElementsByTagName("ListaSubmissoesEvento").item(0)).getElementsByTagName("Submissao").getLength()!=0)
                    {
                        subList = parseSubmissionsFromNode(((Element) object.getElementsByTagName("ListaSubmissoesEvento").item(0)).getElementsByTagName("Submissao"),reg,pd,pdc);
                    }
                    lastEventParsed.setTitulo(title);
                    if (cp.tamanhoListaRevisores()!=0)
                    {
                        lastEventParsed.setCP(cp);
                    }
                    lastEventParsed.setDescricao(description);
                    lastEventParsed.setLocal(eventLocal);
                    lastEventParsed.setdataInicio(new Data(initDate[2],initDate[1],initDate[0]));
                    lastEventParsed.setdataFim(new Data(finDate[2],finDate[1],finDate[0]));
                    lastEventParsed.setdataInicioSubmissao(new Data(initSubDate[2],initSubDate[1],initSubDate[0]));
                    lastEventParsed.setdataFimSubmissao(new Data(finSubDate[2],finSubDate[1],finSubDate[0]));
                    lastEventParsed.setdataInicioDistribuicao(new Data(initDistDate[2],initDistDate[1],initDistDate[0]));
                    lastEventParsed.setdataLimiteRevisao(new Data(revDate[2],revDate[1],revDate[0]));
                    lastEventParsed.setDataLimiteSubmissaoFinal(new Data(subFinalDate[2],subFinalDate[1],subFinalDate[0]));
                    String finalClass = "Evento"+estadoEvento.replace("EventoState", "")+"State";
                    ClassLoader classLoader = MainMenu.class.getClassLoader();
                    try 
                    {
                        Class aClass = classLoader.loadClass("states.evento."+finalClass);
                        Constructor c = aClass.getConstructor(Evento.class);
                        lastEventParsed.setState((EventoState)c.newInstance(lastEventParsed));
                    } 
                    catch (InstantiationException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException| InvocationTargetException|IllegalArgumentException e) 
                    {
                        throw new RuntimeException(e.getMessage());
                    }
                    if (subList!=null)
                    {
                        lastEventParsed.addAllSubmissoes(subList);
                    }
                    lastEventParsed.setListaSessoesTematicas(stList);
                    lastEventParsed.setProcessoDistribuicao(pd);
                    lastEventParsed.setProcessoDecisao(pdc);
                    
                    listEvent.add(lastEventParsed);
            }
        }
        return listEvent;
    }
    /**
     * Parses a node list for a list of thematic sessions.
     * @param node (NodeList) The list where all the thematic sessions kept.
     * @param reg ({@link registos.RegistoUtilizadores}) The company's user registry.
     * @return (List&lt;{@link model.SessaoTematica}&gt;) The parsed list of thematic sessions.
     */
    private static List<SessaoTematica> parseThematicSessionsFromNode(NodeList node,RegistoUtilizadores reg)
    {
        List<SessaoTematica> result = new ArrayList();
        SessaoTematica lastST=new SessaoTematica();
        for (int i=0;i<node.getLength();i++)
        {
            Element object = (Element) node.item(i);
            String codigo = object.getElementsByTagName("CodigoST").item(0).getTextContent(),
                            description = object.getElementsByTagName("DescricaoST").item(0).getTextContent(),
                            inicialDate=object.getElementsByTagName("DataInicio").item(0).getTextContent(),
                            finalDate=object.getElementsByTagName("DataFim").item(0).getTextContent(),
                            dataInicioSubmissao=object.getElementsByTagName("DataInicioSubmissao").item(0).getTextContent(),
                            dataFimSubmissao=object.getElementsByTagName("DataFimSubmissao").item(0).getTextContent(),
                            dataInicioDistribuicao=object.getElementsByTagName("DataInicioDistribuicao").item(0).getTextContent(),
                            dataLimiteRevisao=object.getElementsByTagName("DataLimiteRevisao").item(0).getTextContent(),
                            dataLimiteSubmissaoFinal=object.getElementsByTagName("DataLmiteSubmissaoFinal").item(0).getTextContent(),
                            estadoST=object.getElementsByTagName("EstadoST").item(0).getTextContent();
                            
                    List<Proponente> propList = new ArrayList();
                    NodeList propTree = ((Element) object.getElementsByTagName("ListaProponentes").item(0)).getElementsByTagName("Proponente");
                    for (int j=0;j<propTree.getLength();j++)
                    {
                        String prop = propTree.item(j).getTextContent();
                        if (reg.temUtilizadorUsername(prop))
                        {
                            propList.add(new Proponente(reg.getUtilizador(prop)));
                        }
                        else
                        {
                            throw new IllegalArgumentException("Proponente não pertence ao registo de utilizadores!");
                        }
                    }
                    CP cp = new CP();
                    NodeList cpTree = ((Element) object.getElementsByTagName("CPSessao").item(0)).getElementsByTagName("MembroCPSessao");
                    for (int j=0;j<cpTree.getLength();j++)
                    {
                        String cpMembro = cpTree.item(i).getTextContent();
                        if (reg.temUtilizadorUsername(cpMembro))
                        {
                            cp.addMembroCP(new Revisor(reg.getUtilizador(cpMembro)));
                        }
                        else
                        {
                            throw new IllegalArgumentException("O revisor da CP da sessão temática não é um utilizador registado no sistema.");
                        }
                    }
                    int[] initDate=convertStringDateToInteger(inicialDate,"/"),
                            finDate=convertStringDateToInteger(finalDate,"/"),
                            initSubDate=convertStringDateToInteger(dataInicioSubmissao,"/"),
                            finSubDate=convertStringDateToInteger(dataFimSubmissao,"/"),
                            initDistDate=convertStringDateToInteger(dataInicioDistribuicao,"/"),
                            revDate=convertStringDateToInteger(dataLimiteRevisao, "/"),
                            subFinalDate=convertStringDateToInteger(dataLimiteSubmissaoFinal, "/");
                    lastST.setCodigo(codigo);
                    lastST.setDescricao(description);
                    for (Proponente focus:propList)
                    {
                        lastST.addProponente(focus);
                    }
                    if (cp.tamanhoListaRevisores()!=0)
                    {
                        lastST.setCP(cp);
                    }
                    lastST.setDataInicio(new Data(initDate[2],initDate[1],initDate[0]));
                    lastST.setDataInicioSubmissao(new Data(initSubDate[2],initSubDate[1],initSubDate[0]));
                    lastST.setDataFimSubmissao(new Data(finSubDate[2],finSubDate[1],finSubDate[0]));
                    lastST.setDataInicioDistribuicao(new Data(initDistDate[2],initDistDate[1],initDistDate[0]));
                    lastST.setDataLimiteRevisao(new Data(revDate[2],revDate[1],revDate[0]));
                    lastST.setDataFim(new Data(finDate[2],finDate[1],finDate[0]));
                    lastST.setDataLimiteSubmissaoFinal(new Data(subFinalDate[2],subFinalDate[1],subFinalDate[0]));
                    ProcessoDistribuicao pd = new ProcessoDistribuicao();
                    ProcessoDecisao pdc = new ProcessoDecisao();
                    List<Submissao> submissoes = parseSubmissionsFromNode(((Element) object.getElementsByTagName("ListaSubmissoes").item(0)).getElementsByTagName("Submissao"),reg,pd,pdc);
                    String finalClass = "SessaoTematica"+estadoST.replace("STState", "")+"State";
            ClassLoader classLoader = MainMenu.class.getClassLoader();
            try 
            {
                Class aClass = classLoader.loadClass("states.sessaotematica."+finalClass);
                Constructor c = aClass.getConstructor(SessaoTematica.class);
                lastST.setState((SessaoTematicaState)c.newInstance(lastST));
            } 
            catch (InstantiationException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException| InvocationTargetException|IllegalArgumentException e) 
            {
                throw new RuntimeException(e.getMessage());
            }
            lastST.addAllSubmissoes(submissoes);
            lastST.setProcessoDecisao(pdc);
            lastST.setProcessoDistribuicao(pd);
            result.add(lastST);
        }
        return result;
    }
    /**
     * Parses a node list for a list of submissions.
     * @param node (NodeList) The node list where the submissions are kept.
     * @param reg ({@link registos.RegistoUtilizadores}) The company's user registry.
     * @param pd ({@link model.ProcessoDistribuicao}) The distribution proccess where the
     * revisions will be stored.
     * @param pdc ({@link model.ProcessoDecisao}) The decision process where the decisions will be
     * stored.
     * @return 
     */
    private static List<Submissao> parseSubmissionsFromNode(NodeList node,RegistoUtilizadores reg,ProcessoDistribuicao pd,ProcessoDecisao pdc)
    {
        List<Submissao> result = new ArrayList();
        for (int i=0;i<node.getLength();i++)
        {
            Submissao s = new Submissao();
            Element object = (Element) node.item(i);
            String state = object.getElementsByTagName("EstadoSubmissao").item(0).getTextContent();
            Element art = (Element)object.getElementsByTagName("Artigo").item(0);
            Artigo artigoInicial=null,artigoFinal=null;
            if (art.getAttributes().getNamedItem("tipo").getNodeValue().equals("INICIAL"))
            {
                artigoInicial = parseArtigoFromNode(art,reg);
            }
            else if (art.getAttributes().getNamedItem("tipo").getNodeValue().equals("FINAL"))
            {
                artigoFinal = parseArtigoFromNode(art,reg);
            }
            art = (Element)object.getElementsByTagName("Artigo").item(1);
            if (art!=null)
            {
                if (art.getAttributes().getNamedItem("tipo").getNodeValue().equals("INICIAL"))
                {
                    artigoInicial = parseArtigoFromNode(art,reg);
                }
                else if (art.getAttributes().getNamedItem("tipo").getNodeValue().equals("FINAL"))
                {
                    artigoFinal = parseArtigoFromNode(art,reg);
                }
            }
            if (artigoInicial==null)
            {
                throw new IllegalArgumentException("Não existe artigo inicial.");
            }
            if (artigoFinal!=null)
            {
                s.setArtigoFinal(artigoFinal);
            }
            s.setArtigoInicial(artigoInicial);
            if (object.getElementsByTagName("Decisao").item(0)!=null)
            {
                String decisao = object.getElementsByTagName("Decisao").item(0).getTextContent();
                if (decisao!=null)
                {
                    pdc.addDecisaoUnchecked(new Decisao(4,decisao.equalsIgnoreCase("aceite"),s));
                }
            }
            List<Revisao> listaRevisoes = 
                    parseRevisionsFromNode(((Element)object.getElementsByTagName("ListaRevisoes").item(0)).getElementsByTagName("Revisao"),reg,s,pd);
            pd.addListaRevisoes(listaRevisoes);
            String finalClass = "Submissao"+state.replace("SubmissaoState", "")+"State";
            ClassLoader classLoader = MainMenu.class.getClassLoader();
            try 
            {
                Class aClass = classLoader.loadClass("states.submissao."+finalClass);
                Constructor c = aClass.getConstructor(Submissao.class);
                s.setState((SubmissaoState)c.newInstance(s));
            } 
            catch (InstantiationException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException| InvocationTargetException|IllegalArgumentException e) 
            {
                throw new RuntimeException(e.getMessage());
            }
            result.add(s);
        }
        return result;
    }
    /**
     * Parses an xml element for an article.
     * @param element (Element) The target xml element that contains data of an article.
     * @param reg ({@link registos.RegistoUtilizadores}) The company's user registry.
     * @return ({@link model.Artigo}) The parsed article.
     */
    private static Artigo parseArtigoFromNode(Element element,RegistoUtilizadores reg)
    {
        Artigo result=new Artigo();
        String autorCorrespondente=element.getElementsByTagName("AutorCorrespondente").item(0).getTextContent(),
                autorSubmissao = element.getElementsByTagName("AutorSubmissao").item(0).getTextContent(),
                dataSubmissao = element.getElementsByTagName("DataSubmissao").item(0).getTextContent(),
                tituloArtigo = element.getElementsByTagName("TituloArtigo").item(0).getTextContent(),
                resumo =element.getElementsByTagName("Resumo").item(0).getTextContent(),
                file =element.getElementsByTagName("Ficheiro").item(0).getTextContent();
                ListaAutores listaAutores = 
                        new ListaAutores(parseAutoresFromNode(((Element)(element.getElementsByTagName("ListaAutores").item(0))).getElementsByTagName("Autor"),reg));
                String[] palavrasChave = element.getElementsByTagName("PalavrasChave").item(0).getTextContent().split(";");
                List<String> pc = new ArrayList();
                result.setAutores(listaAutores.getListaAutores());
                for (String focus:palavrasChave)
                {
                    pc.add(focus);
                }
                if (reg.temUtilizadorUsername(autorSubmissao))
                {
                    result.setAutorCriador(reg.getUtilizador(autorSubmissao));
                }
                else
                {
                    throw new IllegalArgumentException("O autor que criou a submissão não consta do registo de utilizadores do sistema ou da lista de autores.");
                }
                result.setResumo(resumo);
                result.setTitulo(tituloArtigo);
                result.setFicheiro(file);
                result.setPalavrasChave(pc);
                int[] data = convertStringDateToInteger(dataSubmissao, "/");
                result.setDataCriado(new Data(data[2],data[1],data[0]));
                if (reg.temUtilizadorUsername(autorCorrespondente) && result.temAutor(autorCorrespondente))
                {
                    result.setAutorCorrespondente(listaAutores.getAutor(autorCorrespondente), reg.getUtilizador(autorCorrespondente));
                }
                
        return result;
    }
    /**
     * Parses a nodelist for a list of authors.
     * @param node (nodeList) The node list where the authors are kept.
     * @return (List&lt;{@link model.Autor}&gt;) The parsed list of authors.
     */
    private static List<Autor> parseAutoresFromNode(NodeList node,RegistoUtilizadores reg)
    {
        List<Autor> result;
        ListaAutores lista = new ListaAutores();
        for (int i=0;i<node.getLength();i++)
        {
            Element element = (Element) node.item(i);
            String nome=element.getElementsByTagName("NomeAutor").item(0).getTextContent(),
                    email=element.getElementsByTagName("EmailAutor").item(0).getTextContent(),
                    filiacao=element.getElementsByTagName("Filiacao").item(0).getTextContent(),
                    username=element.getElementsByTagName("UsernameAutor").item(0).getTextContent();
            Autor autor = new Autor();
            autor.setUsername(username);
            autor.setNome(nome);
            autor.setAfiliacao(filiacao);
            autor.setEmail(email);
            if (!lista.addAutor(autor))
            {
                throw new IllegalArgumentException("O autor já existe na lista de autores.");
            }
        }
        result=lista.getListaAutores();
        return result;
    }
    /**
     * Parses a nodelist for a list of revisions.
     * @param node (NodeList) The node list where the revisions are kept.
     * @param reg ({@link registos.RegistoUtilizadores}) The registry of users.
     * @param s ({@link model.Submissao}) The submission where this list belongs to.
     * @param pd ({@link model.ProcessoDistribuicao}) The distribution process to store the revisions.
     * @return (List&lt;{@link model.Revisao}&gt;) The parsed list of revisions.
     */
    private static List<Revisao> parseRevisionsFromNode(NodeList node,RegistoUtilizadores reg,Submissao s,ProcessoDistribuicao pd)
    {
        List<Revisao> result=new ArrayList();
        for (int i=0;i<node.getLength();i++)
        {   
            Element element = (Element) node.item(i);
            String revisorUsername = element.getElementsByTagName("Revisor").item(0).getTextContent(),
                    confianca = element.getElementsByTagName("Confianca").item(0).getTextContent(),
                    adequacao = element.getElementsByTagName("Adequacao").item(0).getTextContent(),
                    originalidade = element.getElementsByTagName("Originalidade").item(0).getTextContent(),
                    apresentacao = element.getElementsByTagName("Apresentacao").item(0).getTextContent(),
                    recomendacao = element.getElementsByTagName("Recomendacao").item(0).getTextContent(),
                    justificacao = element.getElementsByTagName("Justificacao").item(0).getTextContent();
            if (reg.temUtilizadorUsername(revisorUsername))
            {
                Revisor r = new Revisor();
                r.setUtilizador(reg.getUtilizador(revisorUsername));
                Revisao rev = new Revisao();
                rev.setTextoExplicativo(justificacao);
                rev.setClassificacaoAdequacao(Integer.parseInt(adequacao));
                rev.setClassificacaoConfianca(Integer.parseInt(confianca));
                rev.setClassificacaoOriginalidade(Integer.parseInt(originalidade));
                rev.setClassificacaoApresentacao(Integer.parseInt(apresentacao));
                rev.setClassificacaoRecomendacao(Integer.parseInt(recomendacao));
                rev.setRevisor(r);
                rev.setSubmissao(s);
                result.add(rev);
            }
            else
            {
                throw new IllegalArgumentException("O username de um revisor não consta do registo de utilizadores.");
            }
        }
        return result;
    }
    /**
     * Writes the data present in a list of events to the target output file in an
     * xml format.
     * @param output (File) The xml file to write to.
     * @param le (List&lt;{@link model.Evento}&gt;) The list of events to write.
     * @throws javax.xml.parsers.ParserConfigurationException Parser exception.
     * @throws javax.xml.transform.TransformerException Transformer exception.
     */
    public static void writeEventDataToXML(File output,List<Evento> le) throws ParserConfigurationException, TransformerException 
    {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("ListaEventos");
        doc.appendChild(rootElement);
        Element numEvents = doc.createElement("NumeroEventos");
        numEvents.appendChild(doc.createTextNode(String.valueOf(le.size())));
        rootElement.appendChild(numEvents);
        rootElement.appendChild(writeListOfEvents(doc,le));
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);
        transformer.transform(source, result);
    }
    /**
     * Writes a list of event into document.
     * @param doc (Document) The document to use.
     * @param le (List&lt;{@link model.Evento}&gt;) The list of events to write.
     * @return (Element) An element containing the list of events.
     */
    public static Element writeListOfEvents(Document doc,List<Evento> le)
    {
        Element result=doc.createElement("Eventos");
        for (int i=0;i<le.size();i++)
        {
            result.appendChild(writeEvent(doc,le.get(i)));
        }
        return result;
    }
    /**
     * Writes an event onto the specified document.
     * @param doc (Document) The target document.
     * @param ev ({@link model.Evento}) The event to write.
     * @return (Element) An element containing the event.
     */
    public static Element writeEvent(Document doc,Evento ev)
    {
        Element rootEvent = doc.createElement("Evento");
        //Title
        Element title = doc.createElement("Titulo");
        title.setTextContent(ev.getTitle());
        rootEvent.appendChild(title);
        //Description, etc....
        Element descr = doc.createElement("Descricao");
        descr.setTextContent(ev.getDescription());
        rootEvent.appendChild(descr);
        Element di = doc.createElement("DataInicio");
        di.setTextContent(ev.getDataInicio().showData());
        rootEvent.appendChild(di);
        Element df = doc.createElement("DataFim");
        df.setTextContent(ev.getDataFim().showData());
        rootEvent.appendChild(df);
        Element dis = doc.createElement("DataInicioSubmissao");
        dis.setTextContent(ev.getDataInicioSubmissao().showData());
        rootEvent.appendChild(dis);
        Element dfs = doc.createElement("DataFimSubmissao");
        dfs.setTextContent(ev.getDataFimSubmissao().showData());
        rootEvent.appendChild(dfs);
        Element did = doc.createElement("DataInicioDistribuicao");
        did.setTextContent(ev.getDataInicioDistribuicao().showData());
        rootEvent.appendChild(did);
        Element dlr = doc.createElement("DataLimiteRevisao");
        dlr.setTextContent(ev.getDataLimiteRevisao().showData());
        rootEvent.appendChild(dlr);
        Element dlsf = doc.createElement("DataLmiteSubmissaoFinal");
        dlsf.setTextContent(ev.getDataLimiteSubmissaoFinal().showData());
        rootEvent.appendChild(dlsf);
        Element state = doc.createElement("EstadoEvento");
        state.setTextContent(ev.getState());
        rootEvent.appendChild(state);
        Element local = doc.createElement("Local");
        local.setTextContent(ev.getLocal().getId());
        rootEvent.appendChild(local);
        rootEvent.appendChild(writeListaOrganizadores(doc,ev));
        rootEvent.appendChild(writeCP(doc,ev));
        rootEvent.appendChild(writeListaSessoesTematicas(doc,ev));
        rootEvent.appendChild(writeListaSubmissoes(doc, ev));
        return rootEvent;
    }
    /**
     * Writes a list of thematic sessions into document.
     * @param doc (Document) The document to use.
     * @param ev ({@link model.Empresa}) The event that has the thematic sessions.
     * @return (Element) An element containing the list of thematic sessions 
     * of the event.
     */
    public static Element writeListaSessoesTematicas(Document doc,Evento ev)
    {
        Element rootList = doc.createElement("ListaSessoesTematicas");
        Element numElements = doc.createElement("NumeroSessoesTematicas");
        numElements.setTextContent(String.valueOf(ev.getListaSessoesTematicas().size()));
        rootList.appendChild(numElements);
        List<SessaoTematica> stList = ev.getListaSessoesTematicas();
        Element sessoesTematicas = doc.createElement("SessoesTematicas");
        for (int i=0;i<stList.size();i++)
        {
            Element nextST = writeSessaoTematica(doc,stList.get(i));
            sessoesTematicas.appendChild(nextST);
        }
        rootList.appendChild(sessoesTematicas);
        return rootList;
    }
    /**
     * Writes a thematic session onto the specified document.
     * @param doc (Document) The target document.
     * @param st ({@link model.SessaoTematica}) The thematic session to write.
     * @return (Element) An element containing the thematic session.
     */
    public static Element writeSessaoTematica(Document doc,SessaoTematica st)
    {
        Element rootST = doc.createElement("SessaoTematica");
        Element cod = doc.createElement("CodigoST");
        cod.setTextContent(st.getDescricao());
        rootST.appendChild(cod);
        Element descr = doc.createElement("DescricaoST");
        descr.setTextContent(st.getDescricao());
        rootST.appendChild(descr);
        Element di = doc.createElement("DataInicio");
        di.setTextContent(st.getDataInicio().showData());
        rootST.appendChild(di);
        Element df = doc.createElement("DataFim");
        df.setTextContent(st.getDataFim().showData());
        rootST.appendChild(df);
        Element dis = doc.createElement("DataInicioSubmissao");
        dis.setTextContent(st.getDataInicioSubmissao().showData());
        rootST.appendChild(dis);
        Element dfs = doc.createElement("DataFimSubmissao");
        dfs.setTextContent(st.getDataFimSubmissao().showData());
        rootST.appendChild(dfs);
        Element did = doc.createElement("DataInicioDistribuicao");
        did.setTextContent(st.getDataInicioDistribuicao().showData());
        rootST.appendChild(did);
        Element dlr = doc.createElement("DataLimiteRevisao");
        dlr.setTextContent(st.getDataLimiteRevisao().showData());
        rootST.appendChild(dlr);
        Element dlsf = doc.createElement("DataLmiteSubmissaoFinal");
        dlsf.setTextContent(st.getDataLimiteSubmissaoFinal().showData());
        rootST.appendChild(dlsf);
        Element state = doc.createElement("EstadoST");
        state.setTextContent(st.getState());
        rootST.appendChild(state);
        rootST.appendChild(writeListaProponentes(doc,st));
        rootST.appendChild(writeCP(doc,st));
        rootST.appendChild(writeListaSubmissoes(doc,st));
        return rootST;
    }
    /**
     * Writes a list of submissions to the specified document.
     * @param doc (Document) The target document.
     * @param e ({@link model.Evento}) The target event with the submission list.
     * @return (Element) An element containing the list of submissions.
     */
    public static Element writeListaSubmissoes(Document doc,Evento e)
    {
        Element rootList = doc.createElement("ListaSubmissoesEvento");
        Element numElements = doc.createElement("NumeroSubmissoes");
        numElements.setTextContent(String.valueOf(e.getListaSubmissoes().size()));
        List<Submissao> ls = e.getListaSubmissoes();
        Element submissoes = doc.createElement("Submissoes");
        for (int i=0;i<ls.size();i++)
        {
            Submissao s = ls.get(i);
            Element sub=doc.createElement("Submissao");
            Element subState = doc.createElement("EstadoSubmissao");
            subState.setTextContent(s.getState());
            sub.appendChild(subState);
            if (s.getArtigoFinal()!=null)
            {
                Element artigoInicial = writeArtigo(doc,s.getArtigoInicial(),"INICIAL");
                sub.appendChild(artigoInicial);
            }
            if (s.getArtigoFinal()!=null)
            {
                Element artigoFinal=writeArtigo(doc,s.getArtigoFinal(),"FINAL");
                sub.appendChild(artigoFinal);
            }
            List<Revisao> lRev = e.getRevisoes();
            List<Revisao> submissaolRev = new ArrayList();
            for (Revisao focus:lRev)
            {
                if (focus.getSubmissao().equals(s))
                {
                    submissaolRev.add(focus);
                }
            }
            sub.appendChild(writeProcessoDistribuicao(doc,submissaolRev));
            submissoes.appendChild(sub);
        }
        rootList.appendChild(numElements);
        rootList.appendChild(submissoes);
        return rootList;
    }
    /**
     * Writes a list of submissions to the specified document.
     * @param doc (Document) The target document.
     * @param st ({@link model.SessaoTematica}) The target thematic session with the submission list.
     * @return (Element) An element containing the list of submissions.
     */
    public static Element writeListaSubmissoes(Document doc,SessaoTematica st)
    {
        Element rootList = doc.createElement("ListaSubmissoes");
        Element numElements = doc.createElement("NumeroSubmissoes");
        numElements.setTextContent(String.valueOf(st.getListaSubmissoesSessao().size()));
        List<Submissao> ls = st.getListaSubmissoes();
        Element submissoes = doc.createElement("Submissoes");
        for (int i=0;i<ls.size();i++)
        {
            Submissao s = ls.get(i);
            Element sub=doc.createElement("Submissao");
            Element subState = doc.createElement("EstadoSubmissao");
            subState.setTextContent(s.getState());
            sub.appendChild(subState);
            if (s.getArtigoInicial().getAutorCorrespondente()!=null)
            {
                Element artigoInicial = writeArtigo(doc,s.getArtigoInicial(),"INICIAL");
                sub.appendChild(artigoInicial);
            }
            if (s.getArtigoFinal().valida())
            {
                Element artigoFinal=writeArtigo(doc,s.getArtigoFinal(),"FINAL");
                sub.appendChild(artigoFinal);
            }
            List<Revisao> lRev = st.getRevisoes();
            List<Revisao> submissaolRev = new ArrayList();
            for (Revisao focus:lRev)
            {
                if (focus.getSubmissao().equals(s))
                {
                    submissaolRev.add(focus);
                }
            }
            sub.appendChild(writeProcessoDistribuicao(doc,submissaolRev));
            submissoes.appendChild(sub);
        }
        rootList.appendChild(numElements);
        rootList.appendChild(submissoes);
        return rootList;
    }
    /**
     * Writes the distribution process to the specified document.
     * @param doc (Document) The target document.
     * @param lr (list&lt;{@link model.Revisao};&gt;) The list of revisions to write.
     * @return (Element) An element containing the list of revisions.
     */
    public static Element writeProcessoDistribuicao(Document doc,List<Revisao> lr)
    {
        Element rootList=doc.createElement("ListaRevisoes");
        Element numElemns = doc.createElement("NumeroRevisoes");
        numElemns.setTextContent(String.valueOf(lr.size()));
        Element revisoes = doc.createElement("Revisoes");
        for (int i=0;i<lr.size();i++)
        {
            Element revisao = doc.createElement("Revisao");
            Revisao nextRev = lr.get(i);
            Element revisor = doc.createElement("Revisor");
            revisor.setTextContent(nextRev.getRevisor().getUtilizador().getUsername());
            revisao.appendChild(revisor);
            Element estadoRevisao = doc.createElement("EstadoRevisao");
            estadoRevisao.setTextContent("");
            revisao.appendChild(estadoRevisao);
            Element confianca = doc.createElement("Confianca");
            confianca.setTextContent(String.valueOf(nextRev.getClassificacaoConfianca()));
            revisao.appendChild(confianca);
            Element adequacao = doc.createElement("Adequacao");
            adequacao.setTextContent(String.valueOf(nextRev.getClassificacaoAdequacao()));
            revisao.appendChild(adequacao);
            Element originalidade = doc.createElement("Originalidade");
            originalidade.setTextContent(String.valueOf(nextRev.getClassificacaoOriginalidade()));
            revisao.appendChild(originalidade);
            Element apresentacao = doc.createElement("Apresentacao");
            apresentacao.setTextContent(String.valueOf(nextRev.getClassificacaoApresentacao()));
            revisao.appendChild(apresentacao);
            Element recomendacao = doc.createElement("Recomendacao");
            recomendacao.setTextContent(String.valueOf(nextRev.getClassificacaoRecomendacao()));
            revisao.appendChild(recomendacao);
            Element justificacao = doc.createElement("Justificacao");
            justificacao.setTextContent(nextRev.getTextoExplicativo());
            revisao.appendChild(justificacao);
            revisoes.appendChild(revisao);
        }
        rootList.appendChild(numElemns);
        rootList.appendChild(revisoes);
        return rootList;
    }
    /**
     * Writes the article to the specified document.
     * @param doc (Document) The target document.
     * @param a ({@link model.Artigo}) The article to write.
     * @param type (String) Whether it's the initial article (INICIAL) or the final
     * article (FINAL).
     * @return (Element) An element containing the article.
     */
    public static Element writeArtigo(Document doc,Artigo a,String type)
    {
        Element rootElement=doc.createElement("Artigo");
        rootElement.setAttribute("tipo", type);
        Element autorC = doc.createElement("AutorCorrespondente");
        autorC.setTextContent(a.getAutorCorrespondente().getUtilizador().getUsername());
        rootElement.appendChild(autorC);
        Element autorS = doc.createElement("AutorSubmissao");
        autorS.setTextContent(a.getAutorCriador().getUsername());
        rootElement.appendChild(autorS);
        Element dataS = doc.createElement("DataSubmissao");
        dataS.setTextContent(a.getDataCriado().showData());
        rootElement.appendChild(dataS);
        Element title = doc.createElement("TituloArtigo");
        title.setTextContent(a.getTitulo());
        rootElement.appendChild(title);
        Element resumo = doc.createElement("Resumo");
        resumo.setTextContent(a.getResumo());
        rootElement.appendChild(resumo);
        rootElement.appendChild(writeListaAutores(doc,a));
        Element palavrasChave = doc.createElement("PalavrasChave");
        List<String> pcTemp = a.getPalavrasChave();
        String pc="";
        for (String focus:pcTemp)
        {
            pc+=focus;
        }
        palavrasChave.setTextContent(pc);
        rootElement.appendChild(palavrasChave);
        Element ficheiro = doc.createElement("Ficheiro");
        ficheiro.setTextContent(a.getFicheiro());
        rootElement.appendChild(ficheiro);
        return rootElement;
    }
    /**
     * Writes a list of authors of an article into the specified document.
     * @param doc (Document) The target document.
     * @param a ({@link model.Artigo}) The article containing the list of authors
     * @return (Element) An element containing the list of authors.
     */
    public static Element writeListaAutores(Document doc,Artigo a)
    {
        Element rootList = doc.createElement("ListaAutores");
        Element numElements = doc.createElement("NumeroAutores");
        numElements.setTextContent(String.valueOf(a.getListaAutores().size()));
        rootList.appendChild(numElements);
        List<Autor> propList = a.getListaAutores();
        Element organizadores = doc.createElement("Autores");
        for (int i=0;i<propList.size();i++)
        {
            Element nextOrg = doc.createElement("Autor");
            Autor aut = propList.get(i);
            Element autNome = doc.createElement("NomeAutor");
            autNome.setTextContent(aut.getName());
            Element autEmail = doc.createElement("EmailAutor");
            autEmail.setTextContent(aut.getEmail());
            Element autFiliacao = doc.createElement("Filiacao");
            autFiliacao.setTextContent(aut.getInstituicaoAfiliacao());
            Element userAutor = doc.createElement("UsernameAutor");
            userAutor.setTextContent(aut.getUsername());
            nextOrg.appendChild(autNome);
            nextOrg.appendChild(autEmail);
            nextOrg.appendChild(autFiliacao);
            nextOrg.appendChild(userAutor);            
            organizadores.appendChild(nextOrg);
        }
        rootList.appendChild(organizadores);
        return rootList;
    }
    /**
     * Writes a list of proponents of a thematic to the specified document.
     * @param doc (Document) The target document.
     * @param st ({@link model.SessaoTematica}) The thematic session with the list of
     * proponents.
     * @return (Element) An element containing the list of proponents.
     */
    public static Element writeListaProponentes(Document doc,SessaoTematica st)
    {
        Element rootList = doc.createElement("ListaProponentes");
        Element numElements = doc.createElement("NumeroProponentes");
        numElements.setTextContent(String.valueOf(st.getListaProponentes().size()));
        rootList.appendChild(numElements);
        List<Proponente> propList = st.getListaProponentes();
        Element organizadores = doc.createElement("Proponentes");
        for (int i=0;i<propList.size();i++)
        {
            Element nextOrg = doc.createElement("Proponente");
            nextOrg.setTextContent(propList.get(i).getUsernameUser());
            organizadores.appendChild(nextOrg);
        }
        rootList.appendChild(organizadores);
        return rootList;
    }
    /**
     * Writes the {@link model.CP} of a thematic session into the specified document.
     * @param doc (Document) The target document.
     * @param st ({@link model.SessaoTematica}) The thematic session to write.
     * @return (Element) An element containing the cp.
     */
    public static Element writeCP(Document doc,SessaoTematica st)
    {
        Element rootList = doc.createElement("CPSessao");
        Element numElements = doc.createElement("NumeroMembrosCPST");
        numElements.setTextContent(String.valueOf(st.getListaTodosRevisores().size()));
        rootList.appendChild(numElements);
        List<Revisor> revList = st.getListaTodosRevisores();
        Element revisores = doc.createElement("MembrosCPSessao");
        for (int i=0;i<revList.size();i++)
        {
            Element nextRev = doc.createElement("MembroCPSessao");
            nextRev.setTextContent(revList.get(i).getUtilizador().getUsername());
            revisores.appendChild(nextRev);
        }
        rootList.appendChild(revisores);
        return rootList;
    }
    /**
     * Writes a list of {@link model.Organizador} to the specified document.
     * @param doc (Document) The target document.
     * @param e ({@link model.Evento}) The event with the list of organizadores
     * to write.
     * @return (Element) An element containing the list of organizadores.
     */
    public static Element writeListaOrganizadores(Document doc,Evento e)
    {
        Element rootList = doc.createElement("ListaOrganizadores");
        Element numElements = doc.createElement("NumeroOrganizadores");
        numElements.setTextContent(String.valueOf(e.getListaOrganizadores().size()));
        rootList.appendChild(numElements);
        List<Organizador> orgList = e.getListaOrganizadores();
        Element organizadores = doc.createElement("Organizadores");
        for (int i=0;i<orgList.size();i++)
        {
            Element nextOrg = doc.createElement("Organizador");
            nextOrg.setTextContent(orgList.get(i).getUserUsername());
            organizadores.appendChild(nextOrg);
        }
        rootList.appendChild(organizadores);
        return rootList;
    }
    /**
     * Writes the {@link model.CP} of an event into the specified document.
     * @param doc (Document) The target document.
     * @param e ({@link model.Evento}) The event to write.
     * @return (Element) An element containing the cp of the event.
     */
    public static Element writeCP(Document doc,Evento e)
    {
        Element rootList = doc.createElement("CP");
        Element numElements = doc.createElement("NumeroMembrosCP");
        numElements.setTextContent(String.valueOf(e.getListaTodosRevisores().size()));
        rootList.appendChild(numElements);
        List<Revisor> revList = e.getListaTodosRevisores();
        Element revisores = doc.createElement("MembrosCP");
        for (int i=0;i<revList.size();i++)
        {
            Element nextRev = doc.createElement("MembroCP");
            nextRev.setTextContent(revList.get(i).getUtilizador().getUsername());
            revisores.appendChild(nextRev);
        }
        rootList.appendChild(revisores);
        return rootList;
    }
}
