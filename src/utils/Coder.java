package utils;




/**
 *
 * @author Gonçalo
 */
public interface Coder {
   /**
    * Encodes a message
    * @param message the message to encode
    * @return the encoded message
    */
    public String encode(String message);
    
    /**
     * Decodes a message
     * @param code the message to decode
     * @return the decoded message
     */
    public String decode(String code);
}
