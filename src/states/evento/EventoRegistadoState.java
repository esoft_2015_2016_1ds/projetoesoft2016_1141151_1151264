/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import interfaces.EventoState;
import interfaces.iTOCS;
import model.Evento;

/**
 *
 * @author Diogo
 */
public class EventoRegistadoState implements EventoState,iTOCS {
    private Evento sub;
    
    /**
     * Constructor of the state
     * @param sub the event to associate state to
     */
    public EventoRegistadoState(Evento sub) {
        this.sub = sub;
    }
    
    /**
     * Validation method for this state
     * @return true or false depending on the result of the test(true for positive) 
     */
    @Override
    public boolean valida() {
        return sub.temSessaoTematica();
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCriado() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setRegistado() {
        return true;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCPDefinida() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setAceitaSubmissoes() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDetecao() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDistribuicao() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDecisao() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmSubmissaoCameraReady() {
        return false;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setSessaoTematicaDefinida() {
        boolean result = false;
        if (valida()) {
            sub.setState(new EventoSessaoTematicaDefinidaState((Evento) sub));
            result = true;
        }
        return result;
    }

    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCameraReady() {
        return false;
    }
    
    
    @Override
    public boolean isInCriadoState()
    {
        return false;
    }
    
    @Override
    public boolean isInRegistadoState()
    {
        return true;
    }
    
    @Override
    public boolean isInSessaoTematicaDefinidaState()
    {
        return false;
    }
    
    @Override
    public boolean isInCPDefinidaState()
    {
        return false;
    }
    
    @Override
    public boolean isInAceitaSubmissoesState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmDetecaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmLicitacaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmDistribuicaoState()
    {
        return false;
    }
    /**
     * @see #isInEmRevisaoState() 
     */
    @Override
    public boolean isInEmRevisaoState()
    {
        return false;
    }
    /**
     * @see #isInEmDecisaoState() 
     */
    @Override
    public boolean isInEmDecisaoState()
    {
        return false;
    }

    @Override
    public boolean isInEmSubmissaoCameraReadyState()
    {
        return false;
    }

    @Override
    public boolean isInCameraReadyState()
    {
        return false;
    }

    @Override
    public String showData() {
        return "";
    }
    @Override
    public String toString()
    {
        return "EventoStateRegistado";
    }
}
