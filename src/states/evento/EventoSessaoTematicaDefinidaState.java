/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import interfaces.EventoState;
import interfaces.iTOCS;
import model.Evento;

/**
 *
 * @author Diogo
 */
public class EventoSessaoTematicaDefinidaState implements EventoState, iTOCS {

    private Evento evento;

    public EventoSessaoTematicaDefinidaState(Evento evento) {
        this.evento = evento;
    }

    
    @Override
    public boolean valida() {
        return evento.temCP();
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return false;
    }

    
    @Override
    public boolean setCPDefinida() {
        boolean result = false;
        if (valida()) {
            evento.setState(new EventoCPDefinidaState(evento));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setAceitaSubmissoes() {
        return false;
    }

    
    @Override
    public boolean setEmDetecao() {
        return false;
    }

    
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    
    @Override
    public boolean setEmDistribuicao() {
        return false;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setEmDecisao() {
        return false;
    }

    
    @Override
    public boolean setEmSubmissaoCameraReady() {
        return false;
    }

    
    @Override
    public boolean setSessaoTematicaDefinida() {
        return true;
    }

    
    @Override
    public boolean setCameraReady() {
        return false;
    }

    
    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    
    @Override
    public boolean isInRegistadoState() {
        return false;
    }

    
    
    @Override
    public boolean isInSessaoTematicaDefinidaState() {
        return true;
    }

    
    
    @Override
    public boolean isInCPDefinidaState() {
        return false;
    }

    
    
    @Override
    public boolean isInAceitaSubmissoesState() {
        return false;
    }

    
    
    @Override
    public boolean isInEmDetecaoState() {
        return false;
    }

    
    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    
    @Override
    public boolean isInEmDistribuicaoState() {
        return false;
    }

    
    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    
    @Override
    public boolean isInEmDecisaoState() {
        return false;
    }

    /**
     * @see #isInEmSubmissaoCameraReadyState() 
     */
    @Override
    public boolean isInEmSubmissaoCameraReadyState() {
        return false;
    }

    /**
     * @see #isInCameraReadyState() 
     */
    @Override
    public boolean isInCameraReadyState() {
        return false;
    }

    /**
     * @see #showData() 
     */
    @Override
    public String showData() {
        return "";
    }
    @Override
    public String toString()
    {
        return "EventoStateSessaoTematicaDefinida";
    }

}
