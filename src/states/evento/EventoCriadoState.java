/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import interfaces.EventoState;
import interfaces.iTOCS;
import model.Evento;

/**
 *
 * @author Diogo
 */
public class EventoCriadoState implements EventoState,iTOCS{
    private Evento sub;
    
    /**
     * Construtor de EventoCriadoState
     * @param sub o Evento a associar ao evento
     */
    public EventoCriadoState(Evento sub)
    {
        this.sub=sub;
    }
    
    
    /**
     * Validates this state
     * @return true or false depending on the result of the test
     */
    @Override
    public boolean valida() {
        return true;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCriado() {
        return true;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setRegistado() {
        boolean result=false;
        if (valida())
        {
            sub.setState(new EventoRegistadoState(sub));
            result=true;
        }
        return result;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCPDefinida() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setAceitaSubmissoes() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDetecao() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmLicitacao() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDistribuicao() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmRevisao() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmDecisao() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setEmSubmissaoCameraReady() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setSessaoTematicaDefinida() {
        return false;
    }
    
    /**
     * Sets another state in the event
     * @return true or false depending on the sucess of the operation
     */
    @Override
    public boolean setCameraReady() {
        return false;
    }
    
    @Override
    public boolean isInCriadoState()
    {
        return true;
    }
    
    @Override
    public boolean isInRegistadoState()
    {
        return false;
    }
    
    @Override
    public boolean isInSessaoTematicaDefinidaState()
    {
        return false;
    }
    
    @Override
    public boolean isInCPDefinidaState()
    {
        return false;
    }
    
    @Override
    public boolean isInAceitaSubmissoesState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmDetecaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmLicitacaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmDistribuicaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmRevisaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmDecisaoState()
    {
        return false;
    }
    
    @Override
    public boolean isInEmSubmissaoCameraReadyState()
    {
        return false;
    }
    
    @Override
    public boolean isInCameraReadyState()
    {
        return false;
    }
    @Override
    public String showData() {
        return "";
    }
    @Override
    public String toString()
    {
        return "EventoStateCriado";
    }
}
