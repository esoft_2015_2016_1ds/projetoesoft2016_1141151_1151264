/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.evento;

import interfaces.EventoState;
import interfaces.iTOCS;
import model.Evento;

/**
 *
 * @author jbraga
 */
public class EventoCameraReadyState implements EventoState, iTOCS {

    private Evento sub;

    public EventoCameraReadyState(Evento sub) {
        this.sub = sub;
    }

    
    @Override
    public boolean valida() {
        return true;
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return false;
    }

    
    @Override
    public boolean setCPDefinida() {
        return false;
    }

    
    @Override
    public boolean setAceitaSubmissoes() {
        return false;
    }

    
    @Override
    public boolean setEmDetecao() {
        return false;
    }

    
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    
    @Override
    public boolean setEmDistribuicao() {
        return false;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setEmDecisao() {
        return false;
    }

    
    @Override
    public boolean setEmSubmissaoCameraReady() {
        return false;
    }

    
    @Override
    public boolean setSessaoTematicaDefinida() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return true;
    }

    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    @Override
    public boolean isInRegistadoState() {
        return false;
    }

    
    @Override
    public boolean isInSessaoTematicaDefinidaState() {
        return false;
    }

    
    @Override
    public boolean isInCPDefinidaState() {
        return false;
    }

    
    @Override
    public boolean isInAceitaSubmissoesState() {
        return false;
    }

    
    @Override
    public boolean isInEmDetecaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmDistribuicaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmDecisaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmSubmissaoCameraReadyState() {
        return false;
    }

    
    @Override
    public boolean isInCameraReadyState() {
        return true;
    }

    
    @Override
    public String showData() {
        return "";
    }
    @Override
    public String toString()
    {
        return "EventoStateCameraReady";
    }
}
