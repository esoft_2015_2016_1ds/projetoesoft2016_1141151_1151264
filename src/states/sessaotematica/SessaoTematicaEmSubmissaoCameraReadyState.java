/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.sessaotematica;

import interfaces.SessaoTematicaState;
import interfaces.iTOCS;
import model.SessaoTematica;
import utils.Data;

/**
 *
 * @author Diogo
 */
public class SessaoTematicaEmSubmissaoCameraReadyState implements SessaoTematicaState, iTOCS {

    private SessaoTematica sub;

    public SessaoTematicaEmSubmissaoCameraReadyState(SessaoTematica sub) {
        this.sub = sub;
    }

      
    @Override
    public boolean valida() {
        return (sub.temTodasSubmissoesCameraReady() || Data.getDataAtual().isMaior(sub.getDataLimiteSubmissaoFinal()));
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return false;
    }

    
    @Override
    public boolean setCPDefinida() {
        return false;
    }

    
    @Override
    public boolean setAceitaSubmissoes() {
        return false;
    }

    
    @Override
    public boolean setEmDetecao() {
        return false;
    }

    
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    
    @Override
    public boolean setEmDistribuicao() {
        return false;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setEmDecisao() {
        return false;
    }

    
    @Override
    public boolean setEmSubmissaoCameraReady() {
        return true;
    }

    
    @Override
    public boolean setCameraReady() {
        boolean result = false;
        if (valida()) {
            sub.setState(new SessaoTematicaCameraReadyState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    @Override
    public boolean isInRegistadoState() {
        return false;
    }

    
    @Override
    public boolean isInCPDefinidaState() {
        return false;
    }

    
    @Override
    public boolean isInAceitaSubmissoesState() {
        return false;
    }

    
    @Override
    public boolean isInEmDetecaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmDistribuicaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmDecisaoState() {
        return false;
    }

    
    @Override
    public boolean isInEmSubmissaoCameraReadyState() {
        return true;
    }

    
    @Override
    public boolean isInCameraReadyState() {
        return false;
    }

    
    @Override
    public String showData() {
        return "";
    }
    @Override
    public String toString()
    {
        return "STStateEmSubmissaoCameraReady";
    }
}
