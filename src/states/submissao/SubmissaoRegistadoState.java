/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import interfaces.SubmissaoState;
import interfaces.iTOCS;
import model.Submissao;

/**
 *
 * @author Diogo
 */
public class SubmissaoRegistadoState implements SubmissaoState, iTOCS {

    private Submissao sub;

    public SubmissaoRegistadoState(Submissao sub) {
        this.sub = sub;
    }

    
    @Override
    public boolean valida() {
        return sub.validaInicial();
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return true;
    }

    
    @Override
    public boolean setEmLicitacao() {
        boolean result = false;
        if (valida()) {
            sub.setState(new SubmissaoEmLicitacaoState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setPreRevista() {
        return false;
    }

    
    @Override
    public boolean setNaoRevisto() {
        return false;
    }

    
    @Override
    public boolean setRemovida() {
        boolean result = false;
        if (validaRemover()) {
            sub.setState(new SubmissaoRemovidaState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setRejeitada() {
        return false;
    }

    
    @Override
    public boolean setRevisto() {
        return false;
    }

    
    @Override
    public boolean setAceite() {
        return false;
    }

    
    @Override
    public boolean setAceiteNaoFinal() {
        return false;
    }

    
    @Override
    public boolean setEmCameraReady() {
        return false;
    }

    @Override
    public String showData() {
        return "";
    }

    @Override
    public boolean validaRemover() {
        return true;
    }

    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    @Override
    public boolean isInRegistadoState() {
        return true;
    }

    
    @Override
    public boolean isInRemovidaState() {
        return false;
    }

    
    @Override
    public boolean isInAceiteState() {
        return false;
    }

    
    @Override
    public boolean isInEmCameraReadyState() {
        return false;
    }

    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    @Override
    public boolean isInNaoRevistoState() {
        return false;
    }

    
    @Override
    public boolean isInPreRevistaState() {
        return false;
    }

    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    @Override
    public boolean isInRejeitadaState() {
        return false;
    }

    
    @Override
    public boolean isInRevistoState() {
        return false;
    }

    
    @Override
    public boolean isInAceiteNaoFinalState() {
        return false;
    }
    @Override
    public String toString()
    {
        return "SubmissaoStateRegistado";
    }
}
