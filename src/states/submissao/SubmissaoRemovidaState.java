/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import interfaces.SubmissaoState;
import interfaces.iTOCS;
import model.Submissao;

/**
 *
 * @author jbraga
 */
public class SubmissaoRemovidaState implements SubmissaoState, iTOCS {

    private Submissao sub;

    public SubmissaoRemovidaState(Submissao sub) {
        this.sub = sub;
    }

    
    @Override
    public boolean valida() {
        return true;
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return false;
    }

    
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setPreRevista() {
        return false;
    }

    
    @Override
    public boolean setNaoRevisto() {
        return false;
    }

    
    @Override
    public boolean setRemovida() {
        return true;
    }

    
    @Override
    public boolean setRejeitada() {
        return false;
    }

    
    @Override
    public boolean setRevisto() {
        return false;
    }

    
    @Override
    public boolean setAceite() {
        return false;
    }

    
    @Override
    public boolean setAceiteNaoFinal() {
        return false;
    }

    
    @Override
    public boolean setEmCameraReady() {
        return false;
    }

    
    @Override
    public String showData() {
        return "";
    }

    
    @Override
    public boolean validaRemover() {
        return true;
    }

    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    @Override
    public boolean isInRegistadoState() {
        return false;
    }

    
    @Override
    public boolean isInRemovidaState() {
        return true;
    }

    
    @Override
    public boolean isInAceiteState() {
        return false;
    }

    
    @Override
    public boolean isInEmCameraReadyState() {
        return false;
    }

    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    @Override
    public boolean isInNaoRevistoState() {
        return false;
    }

    
    @Override
    public boolean isInPreRevistaState() {
        return false;
    }

    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    @Override
    public boolean isInRejeitadaState() {
        return false;
    }

    
    @Override
    public boolean isInRevistoState() {
        return false;
    }

    
    @Override
    public boolean isInAceiteNaoFinalState() {
        return false;
    }
    @Override
    public String toString()
    {
        return "SubmissaoStateRemovida";
    }
}
