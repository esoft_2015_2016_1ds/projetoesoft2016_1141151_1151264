/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package states.submissao;

import interfaces.SubmissaoState;
import interfaces.iTOCS;
import model.Submissao;

/**
 *
 * @author jbraga
 */
public class SubmissaoRevistoState implements SubmissaoState, iTOCS {

    private Submissao sub;

    public SubmissaoRevistoState(Submissao sub) {
        this.sub = sub;
    }

    public boolean validaRejeitada() {
        return true;
    }

    
    @Override
    public boolean valida() {
        return true;
    }

    
    @Override
    public boolean setCriado() {
        return false;
    }

    
    @Override
    public boolean setRegistado() {
        return false;
    }

    
    @Override
    public boolean setEmLicitacao() {
        return false;
    }

    
    @Override
    public boolean setEmRevisao() {
        return false;
    }

    
    @Override
    public boolean setPreRevista() {
        return false;
    }

    
    @Override
    public boolean setNaoRevisto() {
        return false;
    }

    
    @Override
    public boolean setRemovida() {
        boolean result = false;
        if (valida()) {
            sub.setState(new SubmissaoRemovidaState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setRejeitada() {
        boolean result = false;
        if (validaRejeitada()) {
            sub.setState(new SubmissaoRejeitadaState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setRevisto() {
        return true;
    }

    
    @Override
    public boolean setAceite() {
        boolean result = false;
        if (valida()) {
            sub.setState(new SubmissaoAceiteState(sub));
            result = true;
        }
        return result;
    }

    
    @Override
    public boolean setAceiteNaoFinal() {
        return false;
    }

    
    @Override
    public boolean setEmCameraReady() {
        return false;
    }

    
    @Override
    public String showData() {
        return "";
    }

    
    @Override
    public boolean validaRemover() {
        return true;
    }

    
    @Override
    public boolean isInCriadoState() {
        return false;
    }

    
    @Override
    public boolean isInRegistadoState() {
        return false;
    }

    
    @Override
    public boolean isInRemovidaState() {
        return false;
    }

    
    @Override
    public boolean isInAceiteState() {
        return false;
    }

    
    @Override
    public boolean isInEmCameraReadyState() {
        return false;
    }

    
    @Override
    public boolean isInEmLicitacaoState() {
        return false;
    }

    
    @Override
    public boolean isInNaoRevistoState() {
        return false;
    }

    
    @Override
    public boolean isInPreRevistaState() {
        return false;
    }

    
    @Override
    public boolean isInEmRevisaoState() {
        return false;
    }

    
    @Override
    public boolean isInRejeitadaState() {
        return false;
    }

    
    @Override
    public boolean isInRevistoState() {
        return true;
    }

    
    @Override
    public boolean isInAceiteNaoFinalState() {
        return false;
    }
    @Override
    public String toString()
    {
        return "SubmissaoStateRevisto";
    }
}
