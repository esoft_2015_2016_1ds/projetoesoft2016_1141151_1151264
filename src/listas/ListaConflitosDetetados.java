/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.ConflitoDetetado;
import model.TipoConflito;

/**
 * A class that represents a list of authors.
 * This class has all the necessary methods to deal with the manipulation of the list.
 * @author jbraga
 */
public class ListaConflitosDetetados implements iTOCS{
    private List<ConflitoDetetado> listaConflitosDetetados;
    /**
     * Creates an instance of object type {@link listas.ListaConflitosDetetados} with null parameters.
     */
    public ListaConflitosDetetados()
    {
        this(new ArrayList());
    }
    /**
     * Creates an instance of object {@link listas.ListaConflitosDetetados} with the 
     * specified parameters.
     * @param lcd (List&lt;{@link model.ConflitoDetetado}&gt;) A detected conflict list to populate this
     * list.
     */
    public ListaConflitosDetetados(List<ConflitoDetetado> lcd)
    {
        listaConflitosDetetados=lcd;
    }
    /**
     * Adds a new conflict to the conflict list.
     * @param c ({@link model.ConflitoDetetado}) The conflict to add.
     * @return (boolean) True if successful.
     */
    public boolean addConflitoDetetado(ConflitoDetetado c)
    {
        boolean result = false;
        if (c!=null)
        {
            result = listaConflitosDetetados.add(c);
        }
        return result;
    }
    public List<TipoConflito> getTiposConflito(String id)
    {
        List<TipoConflito> result = null;
        for (ConflitoDetetado focus:listaConflitosDetetados)
        {
            if (focus.temRevisor(id))
            {
                result = focus.getTiposConflito();
                break;
            }
        }
        return result;
    }
    
    @Override
    public String showData() {
        return "";
    }
}
