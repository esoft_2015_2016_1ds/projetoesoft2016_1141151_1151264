/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Autor;
import registos.RegistoUtilizadores;

/**
 * A class that represents a list of authors.
 * This class has all the necessary methods to deal with the manipulation of the list.
 * @author jbraga
 */
public class ListaAutores implements iTOCS{
    private List<Autor> listaAutores;
    /**
     * Creates an instance of object type {@link listas.ListaAutores} with null parameters.
     */
    public ListaAutores()
    {
        this(new ArrayList());
    }
    /**
     * Creates an instance of object {@link listas.ListaAutores} with the 
     * specified parameters.
     * @param la (List&lt;{@link model.Autor}&gt;) An author list to populate this
     * list.
     */
    public ListaAutores(List<Autor> la)
    {
        listaAutores=la;
    }
    /**
     * Returns a copy of this article's author list.
     * @return (List&lt;Autor&gt;) The article's author list.
     */
    public ArrayList<Autor> getListaAutores()
    {
        return new ArrayList<Autor>(listaAutores);
    }
    /**
     * Returns a list of the authors of this list can be the corresponding
     * author.
     * @param regU ({@link registos.RegistoUtilizadores}) {@link model.Empresa}'s registry of users.
     * @return (List&lt;Autor&gt;) The list of authors that can be the corresponding author.
     */
    public List<Autor> getPossiveisAutoresCorrespondentes(RegistoUtilizadores regU)
    {
        List<Autor> la = new ArrayList();
        
        for(Autor autor:listaAutores)
        {
           if (autor.podeSerCorrespondente(regU))
           {
               la.add(autor);
           }    
        }
        return la;
    }        
    /**
     * Adds the specified author to this list.
     * @param autor (Autor) The author to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addAutor(Autor autor)
    {
        boolean result=false;
        if (autor!=null)
        {
            if (!validaAutor(autor) && autor.valida())
            {
                result= listaAutores.add(autor);
            }
        }
        return result;
    }
    /**
     * Removes an author from this list.
     * @param author ({@link model.Autor}) The target author to remove.
     */
    public void removeAutor(Autor author)
    {
        if (listaAutores.contains(author))
        {
            listaAutores.remove(author);
        }
        else
        {
            throw new IllegalArgumentException("O autor que pretende remover não consta da lista.");
        }
    }
    /**
     * Checks to see if the author exists in this author list.
     * @param author (Autor) The author to check.
     * @return (boolean) True if exists, false if it doesn't.
     */
    public boolean validaAutor(Autor author) 
    {
        boolean authorExists=false;
        for (Autor focus:listaAutores)
        {
            if (focus.equals(author))
            {
                authorExists=true;
                break;
            }
        }
        return authorExists;
    }
    /**
     * Checks if this author list has the author with the specified
     * id.
     * @param id (String) The id of the author.
     * @return (boolean) True if list has the author.
     */
    public boolean temAutor(String id)
    {
        boolean result=false;
        for (Autor focus:listaAutores)
        {
            if (focus.getEmail().equals(id) || focus.getUsername().equals(id))
            {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * Gets the author of this list that has the specified id.
     * @param id (String) The author's username or email.
     * @return ({@link model.Autor}) The author with the specified id. Result will
     * be null if the author was not found.
     */
    public Autor getAutor(String id)
    {
        Autor result=null;
        for (Autor focus:listaAutores)
        {
            if (focus.getUsername().equals(id) || focus.getEmail().equals(id))
            {
                result = focus;
                break;
            }
        }
        return result;
    }
    /**
     * Checks if this list has any authors.
     * @return (boolean) True if at least one author is present.
     */
    public boolean isEmpty()
    {
        boolean result=true;
        for (Autor autor : listaAutores)
        {
            if (autor!=null)
            {
                result=false;
                break;
            }
        }
        return result;
    }
    /**
     * Returns a text format of this author list.
     * @return (String) The textual representation of this class.
     */
    @Override 
    public String toString()
    {
        String result="";
        for (Autor focus:listaAutores)
        {
            result+=focus.toString();
        }
        return result;
    }
    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
}
