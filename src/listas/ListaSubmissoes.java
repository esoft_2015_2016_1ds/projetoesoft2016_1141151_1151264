/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Autor;
import model.Licitacao;
import model.Submissao;

/**
 * A class that represents a list of submission. This class has all the
 * necessary methods to deal with the manipulation of the list.
 *
 * @author jbraga
 */
public class ListaSubmissoes implements iTOCS {

    private List<Submissao> listaSubmissoes;

    /**
     * Creates an instance of object {@link listas.ListaSubmissoes} with null
     * parameters.
     */
    public ListaSubmissoes() {
        this(new ArrayList());
    }

    /**
     * Creates an instance of object {@link listas.ListaSubmissoes} with the
     * specified parameters.
     *
     * @param ls (List&lt;{@link model.Submissao}&gt;) A submission list to
     * populate this list.
     */
    public ListaSubmissoes(List<Submissao> ls) {
        listaSubmissoes = ls;
    }
    /**
     * Returns a list containing all of the submissions of this list.
     * @return (List&lt;{@link model.Submissao}&gt;) A list containing all the submmissions.
     */
    public List<Submissao> getSubmissoes()
    {
        return listaSubmissoes;
    }
    /**
     * Returns a list of submissions from a specified user.
     *
     * @param id (String) The id of the user (either username or email).
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getSubmissoesUtilizador(String id) {
        ArrayList<Submissao> result = new ArrayList();
        for (Submissao focus : listaSubmissoes) {
            if (focus.temAutorArtigoInicial(id)) {
                result.add(focus);
            }
        }
        return result;
    }

      /**
     * This method procures all {@link model.Autor}
     * @return List(&gt;{@link model.Autor}&lt;) 
     */
    public List<Autor> getListaAutores()
    {
        List<Autor> result = new ArrayList();
        for(Submissao sub : listaSubmissoes)
        {
            result.addAll(sub.getArtigoInicial().getListaAutores());
        }
        return result;
    }
    
           /**
     * This method procures {@link model.Submissao} made by the ser in the {@link listas.ListaSubmissoes} that are in {@link states.submissao.SubmissaoAceiteState}
     * @param id (String) he user 
     * @return List (gt;{@link model.Submissao}lt;) A list of {@link model.Submissao} made by the ser in the {@link listas.ListaSubmissoes} that are in {@link states.submissao.SubmissaoAceiteState}
     */
    public List<Submissao> getListaSubmissoesAceites(String id)
    {
        List<Submissao> result = new ArrayList();
        for(Submissao focus : listaSubmissoes)
        {
            if(focus.isInAceiteState() && focus.temAutorArtigoInicial(id))
            {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * This method checks if all the {@link model.Submissao} are in {@link states.submissao.SubmissaoAceiteState}
     * @return (boolean) Returns false if there is at least one {@link model.Submissao} not in {@link states.submissao.SubmissaoAceiteState}
     */
    public boolean temTodasSubmissoesCameraReady()
    {
        boolean result = true;
        for(Submissao focus : listaSubmissoes)
        {
            if(!focus.isInEmCameraReadyState())
            {
                result = false;
            }
        }
        return result;
    }
    /**
     * Returns a list of submissions that are not in the "Camera Ready" state
     * from a user with the specified id.
     *
     * @param id (String) The id of the user (either username or email).
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getSubmissoesNaoCameraReadyDe(String id) 
    {
        List<Submissao> result = new ArrayList();
        for (Submissao focus : listaSubmissoes) {
            if (!focus.isInEmCameraReadyState() && focus.temAutorArtigoInicial(id)) {
                result.add(focus);
            }
        }
        return result;
    }

    /**
     * This searches this list for withdrawn submissions. needs tests.
     *
     * @return (List&lt;{@link model.Submissao}&gt;) A list containing all the
     * withdrawn submissions found.
     */
    public List<Submissao> getSubmissoesRemovidas() {
        ArrayList<Submissao> lsub = new ArrayList();
        for (Submissao sub : listaSubmissoes) {
            System.out.println("ENTREI");
            if (sub.isInRemovidaState()) {
                lsub.add(sub);
            }
        }
        return lsub;
    }
    public int getPosition(Submissao a)
    {
        int result = -1;
        for(Submissao sub : listaSubmissoes)
        {
            if(sub.equals(a))
            {
                result=listaSubmissoes.indexOf(a);
            }
        }
        return result;
    }
    /**
     * Returns a list of submissions that are in
     * {@link states.submissao.SubmissaoEmLicitacaoState} state.
     *
     * @return (List&lt;{@link model.Submissao}&gt;)List containing the
     * submissions.
     */
    public List<Submissao> getSubmissoesEmLicitacao() {
        ArrayList<Submissao> lsub = new ArrayList();
        for (Submissao sub : listaSubmissoes) {
            if (sub.isInEmLicitacaoState()) {
                lsub.add(sub);
            }
        }
        return lsub;
    }
    
        /**
     * This method check if there is a {@link model.Submissao} in this list that is in {@link states.submissao.SubmissaoAceiteState} made by the user
     * @param id (String ) the user ID
     * @return (boolean) Returns true there is at least one valid {@link model.Submissao}
     */
    public boolean temSubmissoesAceitesUtilizador(String id)
    {
        boolean result = false;
        for(Submissao focus : listaSubmissoes)
        {
            if(focus.temAutorArtigoInicial(id) && focus.isInAceiteState())
            {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * This method returns a list of registered submissions
     * @param id (String)the user's ID
     * @return (List&lt;{@link model.Submissao}&gt;)  The List of the user's registered submissions
     */
    public List<Submissao> getListaSubmissoesRegistadasUtilizador(String id)
    {
        List<Submissao> result = new ArrayList();
        for(Submissao sub:listaSubmissoes)
        {
            if(sub.temAutorArtigoInicial(id) && sub.isInRegistadoState())
            {
                result.add(sub);
            }
        }
        
        return result;
    }
    /**
     * Adds all the submissions present in the target list to this submission list.
     * @param ls (List&lt;{@link model.Submissao}&gt;) The target list of submissions to add.
     */
    public void addAllSubmissoes(List<Submissao> ls)
    {
        for (Submissao focus:ls)
        {
            addSubmissao(focus);
        }
    }
    /**
     * Adds a submission to this submission list.
     *
     * @param s ({@link model.Submissao}) The specified submission to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addSubmissao(Submissao s) {
        if (s != null) {
            if (validaSubmissao(s) && s.validaInicial()) {
                return listaSubmissoes.add(s);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

        /**
     * Returns a list of bids
     * @return List &gt;{@link model.Licitacao}&lt; 
     */
    public List<Licitacao> getLicitacoes()
    {
        List<Licitacao> result = new ArrayList();
        for(Submissao focus: listaSubmissoes)
        {
            result.addAll(focus.getLicitacoes());
            
        }
        return result;
    }
    /**
     * Creates a new submission.
     *
     * @return (Submission) The newly created submission.
     */
    public Submissao novaSubmissao() {
        return new Submissao();
    }

    /**
     * Checks if the target submission already exists in this submission list.
     * This method prevents duplication of submissions in the list.
     * <p>
     * This method should be used to check if a submission already exists when a
     * user is changing the submission's data. The original submission's
     * position must be passed so that the original submission is excluded from
     * the verification. In conclusion, the method checks if there is a
     * submission with the same identification attributes that is not the
     * original submission (meaning, that is not in the original position).
     *
     * @param sClone ({@link model.Submissao}) The target clone submission to
     * verify.
     * @param i (int) The position of the original submission.
     * @return (boolean) True if no duplication detected.
     */
    public boolean valida(Submissao sClone, int i) {
        return true;
    }

    /**
     * Checks if the target submission already exists in this submission list.
     * This method prevents duplication of submissions in the list.
     *
     * @param s ({@link model.Submissao}) The target submission to check.
     * @return (boolean) True duplication not detected.
     */
    public boolean validaSubmissao(Submissao s) {
        boolean result = true;
        for (Submissao element : listaSubmissoes) {
            if (element.equals(s)) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * Checks if this list has any articles submitted by an author with the
     * specified id.
     *
     * @param id (String) The author's id (either email or username).
     * @return (boolean) True if it has.
     */
    public boolean temSubmissoesAutor(String id) {
        boolean result = false;
        for (Submissao focus : listaSubmissoes) {
            if (focus.temAutorArtigoInicial(id)) {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * Checks if this list has has any submissions.
     * @return (boolean) True if at least one submission was found.
     */
    public boolean temSubmissoes()
    {
        boolean result = false;
        for (Submissao s:listaSubmissoes)
        {
            if (s!=null)
            {
                result=true;
                break;
            }
        }
        return result;
    }

    /**
     * Método que faz a soma de todas as submissoes aceites
     *
     * @return(int) numero de submissoes aceites
     */
    public int getNumeroSubmissoesAceites() {
        int nAceites = 0;
        for (Submissao elemento : listaSubmissoes) {
            System.out.println(elemento.isInAceiteState());
            if (elemento.isInAceiteState()) {
                nAceites++;
            }
        }
        return nAceites;
    }

    /**
     * Método que faz a soma de todas as submissões
     *
     * @return(int) numero total de submissoes
     */
    public int getNumeroTotalSubmissoes() {
        int nTotal = 0;
        for (Submissao elemento : listaSubmissoes) {
            nTotal++;
        }
        return nTotal;
    }
    
    
    /**
     * This method updates the accepted topics list with the topics of each accepted submission
     * @param ls the topics list
     * @return  the topics list
     */
    public ArrayList<String> getAcceptedTopics(ArrayList<String> ls){
        for(Submissao s : this.listaSubmissoes){
            if(s.isInAceiteState()||s.isInEmCameraReadyState()){
                ls = s.getAceptedTopics(ls);
            }
        }
        return ls;
    }
    
    /**
     * This method updates the rejected topics list with the topics of each rejected submission
     * @param ls the topics list
     * @return  the topics list
     */
    public ArrayList<String> getRejectedTopics(ArrayList<String> ls){
        for(Submissao s : this.listaSubmissoes){
            if(s.isInRejeitadaState()){
                ls = s.getRejectedTopics(ls);
            }
        }
        return ls;
    }

    @Override
    public String showData() {
        return "";
    }
}
