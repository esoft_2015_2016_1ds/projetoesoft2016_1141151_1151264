/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Revisao;

/**
 * A class that represents a list of revisions. This class has all the necessary
 * methods to deal with the manipulation of the list.
 *
 * @author jbraga
 */
public class ListaRevisoes implements iTOCS {

    private List<Revisao> listaRevisoes;

    /**
     * Creates an instance of object type {@link listas.ListaRevisoes} with null
     * parameters.
     */
    public ListaRevisoes() {
        this(new ArrayList());
    }

    /**
     * Creates an instance of object {@link listas.ListaRevisoes} with the
     * specified parameters.
     *
     * @param lr (List&lt;{@link model.Revisao}&gt;) A revision list to populate
     * this list.
     */
    public ListaRevisoes(List<Revisao> lr) {
        setListRevisoes(lr);
    }

    /**
     * Returns a list of revisions of a specified user.
     *
     * @param id (String) The revisor's username or email.
     * @return (List&lt;{@link model.Revisao}&gt;) The revisor's revisions.
     */
    public List<Revisao> getRevisoes(String id) {
        List<Revisao> result = new ArrayList();

        for (Revisao element : listaRevisoes) {
            result.add(element);

        }

        return result;
    }

    /**
     * Returns a list of all revisions in this list.
     *
     * @return (List&lt;{@link model.Revisao}&gt;) A list containing all
     * revisions.
     */
    public List<Revisao> getListaTodasRevisoes() {
        return listaRevisoes;
    }

    /**
     * Gets the mean value of the evaluation of all revisions in this list.
     *
     * @return (double) The mean value.
     */
    public double getMediaDeRevisoes() {
        double result = 0;
        for (Revisao focus : listaRevisoes) {
            result += focus.getClassificacaoRecomendacao();
        }
        result /= (double) listaRevisoes.size();
        return result;
    }
    /**
     * Adds a list of revisions to this list of revisions.
     * @param lr (List&lt;{@link model.Revisao}&gt;) The list of revisions to add
     * to this list.
     */
    public void addListaRevisoes(List<Revisao> lr)
    {
        listaRevisoes.addAll(lr);
    }
    /**
     * Sets the revision list of this list.
     *
     * @param lr (List&lt;{@link model.Revisao}&gt;) This list's new revision
     * list.
     */
    public void setListRevisoes(List<Revisao> lr) {
        listaRevisoes = lr;
    }

    /**
     * Saves the modifications done to a revision.
     *
     * @param rev (Revisao) The revision changed.
     * @return (boolean) True if successfully saved.
     */
    public boolean saveRevisao(Revisao rev) {
        boolean result = false;
        for (int i = 0; i < listaRevisoes.size() && !result; i++) {
            if (listaRevisoes.get(i).getSubmissao().equals(rev.getSubmissao())
                    && listaRevisoes.get(i).getRevisor().equals(rev.getRevisor())) {
                listaRevisoes.set(i, rev);
                result = true;
            }
        }
        return result;
    }

    /**
     * Método que devolve o valor da confiança do revisor nos tópicos do artigo
     *
     * @return(int) valor da confiança do revisor nos tópicos do artigo
     */
    public int getConfianca() {
        int nConfianca = 0;
        for (Revisao elemento : listaRevisoes) {
            nConfianca += elemento.getClassificacaoConfianca();
        }
        return nConfianca;
    }

    /**
     * Método que devolve o valor da Adequação ao evento
     *
     * @return (int) classificacao
     */
    public int getAdequacao() {
        int nAdequacao = 0;
        for (Revisao elemento : listaRevisoes) {
            nAdequacao += elemento.getClassificacaoAdequacao();
        }
        return nAdequacao;
    }

    /**
     * Método que devolve o valor da Originalidade de um evento
     *
     * @return (int) classificacao
     */
    public int getOriginalidade() {
        int nOriginalidade = 0;
        for (Revisao elemento : listaRevisoes) {
            nOriginalidade += elemento.getClassificacaoOriginalidade();
        }
        return nOriginalidade;
    }

    /**
     * Método que devolve o valor da Qualidade de um evento
     *
     * @return (int) classificação
     */
    public int getQualidade() {
        int nQualidade = 0;
        for (Revisao elemento : listaRevisoes) {
            nQualidade += elemento.getClassificacaoApresentacao();
        }
        return nQualidade;
    }

    /**
     * Método que devolve o valor da Recomendação Global de um evento
     *
     * @return (int) classificação
     */
    public int getRecomendacao() {
        int nRecomendacao = 0;
        for (Revisao elemento : listaRevisoes) {
            nRecomendacao += elemento.getClassificacaoRecomendacao();
        }
        return nRecomendacao;
    }

    /**
     * This method checks each submission of this list to see if there is an
     * ocurrance of a keyword existing in the arraylist passed as parameter in
     * it. When it finds a match it adds it's global recomendation to it and
     * increments one to the frequency of the keyword
     *
     * @param ls the array list with each keyword with it's global recomendation
     * sum and frequency
     * @return the array list with each keyword with it's global recomendation
     * sum and frequency updated with each submission
     */
    public ArrayList<String[]> getStatsTopicos(ArrayList<String[]> ls) {
        for (Revisao r : this.listaRevisoes) {
            ls = r.getStatsTopicos(ls);
        }
        return ls;
    }
    /**
     * Checks if this distribution process has any revisions.
     * @return (boolean) True if at least one revision has been found.
     */
    public boolean temRevisoes()
    {
        boolean result = false;
        for (Revisao focus:listaRevisoes)
        {
            if (focus!=null)
            {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * Checks if all of the revisions of this revision list has been done.
     * @return (boolean) True if all revisions were completed.
     */
    public boolean temTodasRevisoesFeitas()
    {
        boolean result=true;
        for (Revisao focus:listaRevisoes)
        {
            if (focus.getTextoExplicativo().length()<2)
            {
                result=false;
                break;
            }
        }
        return result;
    }

    /**
     * @see #showData() 
     * @return
     */
    @Override
    public String showData() {
        return "";
    }
}
