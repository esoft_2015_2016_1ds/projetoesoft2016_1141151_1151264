/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Revisao;
import model.Revisor;
import model.SessaoTematica;
import model.Submissao;
import model.Utilizador;

/**
 * A class that represents a list of thematic sessions. This class has all the
 * necessary methods to deal with the manipulation of the list.
 *
 * @author jbraga
 */
public class ListaSessoesTematicas implements iTOCS {

    private List<SessaoTematica> listaSessoesTematicas;
    

    /**
     * Creaets an instance of object {@link listas.ListaSessoesTematicas} with
     * null parameters.
     */
    public ListaSessoesTematicas() {
        this(new ArrayList());
    }

    /**
     * Creates an instance of object {@link listas.ListaSessoesTematicas} with
     * the specified parameters.
     *
     * @param lst (List&lt;{@link model.SessaoTematica}&gt;) A thematic session
     * list to populate this list.
     */
    public ListaSessoesTematicas(List<SessaoTematica> lst) {
        listaSessoesTematicas = new ArrayList(lst);
    }

    /**
     * This method checks for {@link model.Submissao} that are in {@link states.submissao.SubmissaoAceiteState} made by the user 
     * @param id (String) 
     * @return (boolean) 
     */
        public boolean temSubmissoesAceitesUtilizador(String id)
    {
        boolean result = false;
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temSubmissoesAceitesUtilizador(id))
            {
                result = true;
            }
        }
        return result;
    }

        /**
         * This method checks for {@link model.SessaoTematica} that have at least one {@link model.Submissao} in {@link states.submissao.SubmissaoAceiteState} made by the user
         * @param id (String) the user's ID
         * @return List (&gt;{@link model.SessaoTematica}&lt;) A list of {@link model.SessaoTematica} that have at least one {@link model.Submissao} in {@link states.submissao.SubmissaoAceiteState}
         */
    public List<SessaoTematica> getListaSessoesEmSubmissaoCR(String id)
    {
        List<SessaoTematica> result = new ArrayList();
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temSubmissoesAceitesUtilizador(id))
            {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * This method checks if there are {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
     * @param id (String) The user's ID
     * @return (boolean) Returns true if there is at least one {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
     */
    public boolean temSessoesEmDecisao(String id)
    {
        boolean result = false;
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temProponente(id) && focus.isInEmDecisaoState())
            {
                result = true;
                break;
            }
        }
        return result;
    }
          /**
         * This method checks for {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
         * @param id (String) the user's ID
         * @return List (&gt;{@link model.SessaoTematica}&lt;) A list of {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
         */
    public List<SessaoTematica> getListaSessoesEmDecisao(String id)
    {
        List<SessaoTematica> result = new ArrayList();
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temProponente(id) && focus.isInEmDecisaoState())
            {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * Returns a list containing all the submissiveis thematic sessions
     * ({@link model.SessaoTematica}) of this list that are accepting
     * submissions.
     *
     * @return (List&lt;{@link interfaces.Submissivel}&gt;)
     */
    public List<Submissivel> getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes() {
        ArrayList<Submissivel> result = new ArrayList();
        for (SessaoTematica focus : listaSessoesTematicas) {
            if (focus.isInAceitaSubmissoesState()) {
                result.add(focus);
            }
        }
        if (result.isEmpty()) {
            result = null;
        }
        return result;
    }
    /**
     * Returns a list of all the {@link interfaces.Submissivel}, that are currently
     * accepting submissions, from the specified user.
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of submissiveis.
     */
    public List<Submissivel> getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(String id)
    {
        ArrayList<Submissivel> result = new ArrayList();
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.isInAceitaSubmissoesState() && focus.temSubmissoesAutor(id))
            {
                result.add(focus);
            }
        }
        if(result.isEmpty())
        {
            result=null;
        }
        return result;
    }
    /**
     * Returns a list of all the submissiveis of this thematic session list that
     * are being reviewed and belong to a user with the specified id. The id
     * represents the unique identifier of a registered user.
     *
     * @param id (String) The user's username or email.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;)
     */
    public List<Revisivel> getRevisiveisEmEstadoDeRevisaoDe(String id) {
        List<Revisivel> result = new ArrayList();
        for (SessaoTematica element : listaSessoesTematicas) {
            if (element.isInRegistadoState()) {
                result.add(element);
            }
        }
        return result;
    }

    /**
     * Returns a list of thematic sessions of a user that are accepting bids
     * (licitações). The id represents the unique identifier of a registered
     * user.
     *
     * @param id (String) The user's username or email.
     * @return (List&lt;{@link interfaces.Licitavel}&gt;)
     */
    public List<Licitavel> getListaLicitaveisEmLicitacaoDe(String id) {
        ArrayList<Licitavel> result = new ArrayList();
        for (SessaoTematica focus : listaSessoesTematicas) {
            if (focus.isInEmLicitacaoState() && focus.temRevisor(id)) //Replace condition to check if valid
            {
                result.add(focus);
            }
        }
        return result;
    }

    /**
     * This method returns the lists of thematic sessions as a List&lt;{@link model.SessaoTematica}&gt;
     * @return List&lt;{@link model.SessaoTematica}&gt;  the list of thematic sessions
     */
    public List<SessaoTematica> getListaSessoesTematicas()
    {
        return listaSessoesTematicas;
    }
    /**
     * Método que pesquisa todas as sessoes tematicas de id
     *
     * @param id (String) ID do Utilizador
     * @return lista das sessões temáticas
     */
    public List<SessaoTematica> getSessoesTematicasProponenteEmEstadoRegistado(String id) {
        List<SessaoTematica> result = new ArrayList();
        for (SessaoTematica element : listaSessoesTematicas) {
            if (element.temProponente(id)) {
                result.add(element);
            }
        }

        return result;
    }
    /**
     * Returns a list of all revisions in this list.
     * @return (List&lt;{@link model.Revisao}&gt;) A list containing all revisions.
     */
    public List<Revisao> getListaTodasRevisoes()
    {
        List<Revisao> result = new ArrayList();
        for (SessaoTematica focus:listaSessoesTematicas)
        {
            result.addAll(focus.getListaTodasRevisoes());
        }
        return result;
    }
    /**
     * Returns a list of submissions, that belong to the specified user, from
     * all of the list's thematic sessions.
     *
     * @param id (String) The user's username or email.
     * @return (List&lt;{@link model.Submissao}&gt;)
     */
    public List<Submissao> getSubmissoesSessoesUtilizador(String id) {
        ArrayList<Submissao> result = new ArrayList<Submissao>();
        for (SessaoTematica focus : listaSessoesTematicas) {
            result.addAll(focus.getSubmissoesUtilizador(id));
        }
        return result;
    }
    /**
     *  This method returns a list of registered submissions
     * @param id (String)the user's ID
     * @return (List&lt;{@link model.Submissao}&gt;)  The List of the user's registered submissions
     */
    public List<Submissao> getListaSubmissoesRegistadasUtilizador(String id)
    {
        List<Submissao> result = new ArrayList();
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.isInAceitaSubmissoesState())
            {
                result.addAll(focus.getListaSubmissoesRegistadasUtilizador(id));
            }
        }
        return result;
    }
    /**
     * Returns a list of {@link interfaces.Submissivel} that are not in "Camera
     * Ready" state.
     *
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of
     * submissiveis.
     */
    public List<Submissivel> getListaSubmissiveisNaoCameraReadyDe(String id) {
        List<Submissivel> result = new ArrayList();
        for (SessaoTematica focus : listaSessoesTematicas) {
            if (!focus.isInCameraReadyState() && focus.temSubmissoesAutor(id)) {
                result.add(focus);
            }
        }
        return result;
    }
    /**
     * Returns the thematic session of this list with the specified code.
     * @param code (String) The unique code in the event for each thematic session.
     * @return ({@link model.SessaoTematica}) The thematic session. Null if none was found.
     */
    public SessaoTematica getSessaoTematicaCodigo(String code)
    {
        SessaoTematica result = null;
        for (SessaoTematica focus:listaSessoesTematicas)
        {
            if (focus.getCodigo().equals(code))
            {
                result = focus;
                break;
            }
        }
        return result;
    }
    /**
     * This method checks this list for {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState}
     * @param id (String) The user's ID
     * @return (boolean) Returns true if the list contaisn at least one {@link model.SessaoTematica}
     */
    public boolean temSessoesDistribuiveis(String id)
    {
        boolean result = false;
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temProponente(id) && focus.isInEmLicitacaoState())
            {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * This method procures {@link model.SessaoTematica} within the {@link listas.ListaSessoesTematicas} that have the user as {@link model.Proponente} and that are in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState}
     * @param id (String) The user's ID
     * @return List &gt;{@link model.SessaoTematica}&lt; 
     */
    public List<SessaoTematica> getSessoesTematicasEmLicitacao(String id)
    {
        List<SessaoTematica> result = new ArrayList();
        for(SessaoTematica focus : listaSessoesTematicas)
        {
            if(focus.temProponente(id) && focus.isInEmLicitacaoState())
            {
                result.add(focus);
            }
        }
        return result;
     }
        /**
     * Método que busca a Lista de Revisiveis de um dado utilizador que se
     * encontre no estado em Revisao
     *
     * @param id (String) id do utilizador
     * @return a lista revisivel
     */
    public List<Revisivel> getListaRevisivelUtilizadorEmEstadoRevisao(String id) {
        List<Revisivel> lrr =new ArrayList();
        for(SessaoTematica elemento : listaSessoesTematicas){
            if(elemento.isInEmRevisao() && elemento.temProponente(id)){
                lrr.add(elemento);
            }
        }
        return lrr;
    }
    /**
     * This searches the list for thematic sessions that have a proponent passed
     * as parameter. Needs testing
     *
     * @param id (String) the id(username or email) of the user to search for
     * @return a list of all the thematic sessions that have the user as a
     * proponent
     */
    public List<SessaoTematica> getSessoesTematicasComProponenteRegistadoState(String id) {
        ArrayList<SessaoTematica> lst = new ArrayList();
        for (SessaoTematica st : listaSessoesTematicas) {
            if (st.temProponente(id) && st.isInRegistadoState()) {
                lst.add(st);
            }
        }

        return lst;
    }
    /**
     * Returns a list of all revisores in this thematic session list.
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all revisors.
     */
    public List<Revisor> getListaTodosRevisores()
    {
        List<Revisor> result = new ArrayList();
        for (SessaoTematica focus:listaSessoesTematicas)
        {
            result.addAll(focus.getListaTodosRevisores());
        }
        return result;
    }
    /**
     * Adds all the thematic sessions the provided list to this thematic
     * session list.
     * @param st (List&lt;{@link model.SessaoTematica}&gt;) The list of thematic
     * sessions to add to this list.
     */
    public void addAllSessoesTematicas(List<SessaoTematica> st)
    {
        for (SessaoTematica focus:listaSessoesTematicas)
        {
            registaSessaoTematica(focus);
        }
    }
    /**
     * This method validates a {@link model.SessaoTematica} and adds it in the list of {@link model.SessaoTematica} that belongs to the {@link model.Evento}
     * @param st ({@link model.SessaoTematica}) The event where the {@link model.SessaoTematica} shall be registered
     * @return (boolean) Returns true if the operation is sucessfull
     */
    public boolean registaSessaoTematica(SessaoTematica st) 
    {
        boolean result = false;
        if(st.valida() && this.validaST(st))
        {
            result = listaSessoesTematicas.add(st);
        }
        return result;
    }
    /**
     * Método que devolve o valor da confiança de um revisor nos tópicos do
     * evento
     *
     * @return (int) classificacao
     */
    public int getConfianca() {
        int nConfianca = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nConfianca += elemento.getConfianca();
        }
        return nConfianca;
    }

    /**
     * Método que devolve o valor da adequacao do evento
     *
     * @return (int) classificacao
     */
    public int getAdequacao() {
        int nAdequacao = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nAdequacao += elemento.getAdequacao();
        }
        return nAdequacao;
    }

    /**
     * Método que devolve o valor da originalidade do evento
     *
     * @return (int) classificacao
     */
    public int getOriginalidade() {
        int nOriginalidade = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nOriginalidade += elemento.getOriginalidade();
        }
        return nOriginalidade;
    }

    /**
     * Método que devolve o valor da Qualidade de um evento
     *
     * @return (int) classificação
     */
    public int getQualidade() {
        int nQualidade = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nQualidade += elemento.getQualidade();
        }
        return nQualidade;
    }

    /**
     * Método que devolve o valor da Recomendação Global de um evento
     *
     * @return (int) classificação
     */
    public int getRecomendacao() {
        int nRecomendacao = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nRecomendacao += elemento.getRecomendacao();
        }
        return nRecomendacao;
    }

    /**
     * Método que procura todas as submissões aceites.
     * @return(int) o número de submissões aceites
     */
    public int getNumeroSubmissoesAceites() {
        int nAceites = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nAceites += elemento.getNumeroSubmissoesAceites();
        }
        return nAceites;
    }
    /**
     * Returns the revisor that corresponds to the specified user.
     * If the revisor does not exist anywhere on the thematic
     * sessions, null is returned.
     * @param u ({@link model.Utilizador}) The target user.
     * @return ({@link model.Revisor}) The revisor.
     */
    public Revisor getRevisor(Utilizador u)
    {
        Revisor result=null;
        for (SessaoTematica focus:listaSessoesTematicas)
        {
            result = focus.getRevisor(u.getEmail());
            if (result!=null)
            {
                break;
            }
        }
        return result;
    }

    /**
     * Método que faz a soma de todas as submissões
     * @return(int) numero total de submissoes
     */
    public int getNumeroTotalSubmissoes() {
        int nTotal = 0;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            nTotal += elemento.getNumeroTotalSubmissoes();
        }
        return nTotal;
    }
    /**
     * This method checks if a ThematicSession already exists within the Event's list of Thematic Sessions
     * @param st ({@link model.SessaoTematica})
     * @return (boolean) Returns true if the 
     */
    public boolean validaST(SessaoTematica st)
    {
        boolean result = true;
        for(SessaoTematica session : listaSessoesTematicas)
        {
            
            if(st.equals(session))
            {
                
                result=false;
                break;
            }
        }
        return result;

    }
    /**
     * Adds a thematic session to this thematic session list.
     * @param st ({@link model.SessaoTematica}) The thematic session to add to
     * the list.
     * @return (boolean) True if successfully added.
     */
    public boolean add(SessaoTematica st) {
        return listaSessoesTematicas.add(st);
    }

    /**
     * Checks if there is at least one thematic session in this list.
     *
     * @return (boolean) True if at least one thematic session exists.
     */
    public boolean temSessaoTematica() {
        boolean result = false;
        for (SessaoTematica elemento : listaSessoesTematicas) {
            if (elemento != null) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * This method checks in the list if there are any thematic sessions that
     * have a proponent passed as parameter. Needs testing
     *
     * @param id (String) The identification(Username or email) of the user to
     * search for
     * @return (boolean) True or false depending on whether there is a thematic
     * session with the user as a proponent
     */
    public boolean temSessaoComProponente(String id) {
        for (SessaoTematica st : listaSessoesTematicas) {
            if (st.temProponente(id)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the thematic sessions of this list have any articles submitted
     * by an author with the specified id.
     *
     * @param id (String) The author's id (either email or username).
     * @return (boolean) True if it has.
     */
    public boolean temSubmissoesAutor(String id) {
        boolean result = false;
        for (SessaoTematica element : listaSessoesTematicas) {
            if (element.temSubmissoesAutor(id)) {
                result = true;
                break;
            }
        }
        return result;
    }
    
    /**
     * This method returns all Sessions that have gone through with the decision process in this registry
     * @return all sessions that have gone through with the decision process in this registry
     */
    public ArrayList<SessaoTematica> getListaSessoesDecididas(){
        ArrayList<SessaoTematica> ls = new ArrayList<SessaoTematica>();
        for(SessaoTematica e: this.listaSessoesTematicas){
            if(e.isInCameraReadyState()|| e.isInEmSubmissaoCameraReadyState()){
                ls.add(e);
            }
        }
        return ls;
    }
    
    @Override
    public String showData() {
        return "";
    }
}
