/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Licitacao;
import model.Revisao;

/**
 * A class that represents a list of bids.
 * This class has all the necessary methods to deal with the manipulation of the list.
 * @author jbraga
 */
public class ListaLicitacoes implements iTOCS{

    private List<Licitacao> listaLicitacoes;
    /**
     * Creates an instance of object type {@link listas.ListaLicitacoes} with null parameters.
     */
    public ListaLicitacoes()
    {
        this(new ArrayList());
    }
    /**
     * Creates an instance of object {@link listas.ListaLicitacoes} with the 
     * specified parameters.
     * @param lc (List&lt;{@link model.Licitacao}&gt;) A revision list to populate this
     * list.
     */
    public ListaLicitacoes(List<Licitacao> lc)
    {
        setListLicitacoes(lc);
    }
    /**
     * Sets the bid ({@link model.Licitacao}) list of this list.
     * @param lc (List&lt;{@link model.Licitacao}&gt;) This list's new bid list.
     */
    public void setListLicitacoes(List<Licitacao> lc)
    {
        listaLicitacoes=lc;
    }
    /**
     * Returns the list of bids
     * @return List &gt;{@link model.Licitacao}&lt; 
     */
    public List<Licitacao> getLicitacoes()
    {
        return listaLicitacoes;
    }
    /**
     * Returns a {@link model.Licitacao} of a {@link model.Revisor} with the specified
     * id.
     * @param id (String) The revisor's id.
     * @return ({@link model.Licitacao}) The bid.
     */
    public Licitacao getLicitacaoDe(String id)
    {
        Licitacao result = null;
        for (Licitacao focus:listaLicitacoes)
        {
            if (focus.temRevisor(id))
            {
                result=focus;
                break;
            }
        }
        return result;
    }
    /**
     * Registers the {@link model.Licitacao} in the submission.
     * @param l ({@link model.Licitacao}) The bid to register. 
     * @return (boolean) True if successfully registered.
     */
    public boolean registaLicitacao(Licitacao l)
    {
        boolean result = validaLicitacao(l);
        if(l!=null)
        {
            if (!result)
            {
                result = listaLicitacoes.add(l);
            }
            else
            {
                result = listaLicitacoes.set(listaLicitacoes.indexOf(l), l) != null;
            }
        }
        else
        {
            result=false;
        }
        return result;
    }
    /**
     * Checks if the specified {@code model.Licitacao} exists in this list.
     * @param l ({@link model.Licitacao}) The bid to check.
     * @return (boolean) True if bid already exists.
     */
    public boolean validaLicitacao(Licitacao l)
    {
        boolean result=false;
        for (int i=0;i<listaLicitacoes.size();i++)
        {
            if (listaLicitacoes.get(i).equals(l))
            {
                result = true;
                break;
            }
        }
        return result;
    }
  
    public String showData() {
        return "";
    }
    
}
