/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controllers.DefinirTipoConflitoController;
import model.Empresa;

/**
 *
 * @author Diogo
 */
public class DefinirTipoConflitoUI {
    private Empresa empresa;
    private DefinirTipoConflitoController controller;
    public DefinirTipoConflitoUI(Empresa empresa)
    {
        this.empresa=empresa;
        controller= new DefinirTipoConflitoController(empresa);
        controller.getListaTipoConflitos();
    }
}
