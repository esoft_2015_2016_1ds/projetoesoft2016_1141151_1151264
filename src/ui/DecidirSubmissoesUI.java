/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controllers.DecidirSubmissoesController;
import model.Empresa;

/**
 *
 * @author Diogo
 */
public class DecidirSubmissoesUI {
    private Empresa empresa;
    private DecidirSubmissoesController controller;
    public DecidirSubmissoesUI(Empresa e)
    {
        empresa=e;
        controller = new DecidirSubmissoesController(e);
    }
}
