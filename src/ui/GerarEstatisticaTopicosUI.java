/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controllers.GerarEstatisticasTopicosController;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import model.Empresa;

/**
 *
 * @author Diogo
 */
public class GerarEstatisticaTopicosUI extends javax.swing.JDialog {
    private Empresa empresa;
    private GerarEstatisticasTopicosController controller;
    /**
     * Creates new form GerarEstatisticaTopicosUI
     */
    public GerarEstatisticaTopicosUI(java.awt.Frame parent, boolean modal,Empresa e) {
        super(parent, modal);
        initComponents();
        this.empresa = e;
        controller = new GerarEstatisticasTopicosController(empresa);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (empresa.isAdmin(empresa.getUtilizadorAutenticado()))
                {
                    try
                    {
                        controller.gerarStatsTopics();
                        JOptionPane.showMessageDialog(rootPane,"Foram geradas estatísticas dos tópicos de todos os artigos existentes que se encontram em camera ready ou"
                                + "em submissão camera ready.");
                        fileField.setText(controller.getFilePath());
                    }
                    catch (FileNotFoundException e)
                    {
                        JOptionPane.showMessageDialog(rootPane, "Erro: ficheiro não encontrado ("+e.getMessage()+").");
                        dispose();
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(rootPane,"Esta funcionalidade só está disponível para administradores!");
                    dispose();
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        fileField = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        sairButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Gerar Estatísticas Tópicos (UC19B)");

        jLabel2.setText("Ficheiro Estatístico:");

        fileField.setEditable(false);

        jButton1.setText("Abrir Ficheiro");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        sairButton.setText("Sair");
        sairButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(sairButton)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fileField, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel1)
                .addGap(116, 116, 116)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(fileField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                .addComponent(sairButton)
                .addGap(25, 25, 25))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try 
        {
            if (Desktop.isDesktopSupported()) 
            {
                Desktop.getDesktop().open(new File(fileField.getText()));
            }
        } 
        catch (IOException e) 
        {
            JOptionPane.showMessageDialog(this, "Erro ao abrir o ficheiro: "+e.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void sairButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairButtonActionPerformed
        // TODO add your handling code here:
        int op = JOptionPane.showConfirmDialog(this, "Deseja mesmo sair?", "Sair", JOptionPane.INFORMATION_MESSAGE);
        switch (op)
        {
            case JOptionPane.OK_OPTION:
            dispose();
            break;

            case JOptionPane.CANCEL_OPTION:
            case JOptionPane.CLOSED_OPTION:
            case JOptionPane.NO_OPTION:
            break;
        }
    }//GEN-LAST:event_sairButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField fileField;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton sairButton;
    // End of variables declaration//GEN-END:variables
}
