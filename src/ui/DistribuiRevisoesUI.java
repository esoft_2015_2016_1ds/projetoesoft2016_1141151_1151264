/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controllers.DistribuiRevisoesController;
import model.Empresa;

/**
 *
 * @author Diogo
 */
public class DistribuiRevisoesUI {
    private DistribuiRevisoesController distribuiController;
    private Empresa empresa;
            
    public DistribuiRevisoesUI(Empresa empresa)
    {
        this.empresa = empresa;
        distribuiController = new DistribuiRevisoesController(empresa);
    }
}
