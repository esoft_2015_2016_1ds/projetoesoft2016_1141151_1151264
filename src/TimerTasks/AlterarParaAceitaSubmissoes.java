/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TimerTasks;

import java.util.TimerTask;
import interfaces.Submissivel;

/**
 *
 * @author Gonçalo
 */
public class AlterarParaAceitaSubmissoes extends TimerTask {
    private Submissivel ev;
    
    /**
     * AlterarParaEmSubmissao constructor
     * @param e the submissible to associate to this task
     */
    public AlterarParaAceitaSubmissoes(Submissivel e){
        this.ev = e;
    }
    /**
     * Sets the state of the event ev to in submission
     */
    @Override
    public void run(){
        this.ev.setAceitaSubmissoes();
    }
}
