/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TimerTasks;

import interfaces.Distribuivel;
import java.util.TimerTask;
import model.Evento;

/**
 *
 * @author Gonçalo
 */
public class AlterarParaEmDistribuicao extends TimerTask{
    private Distribuivel ev;
    
    /**
     * Alterar para em distribuicao constructor
     * @param e the event to set
     */
    public AlterarParaEmDistribuicao(Distribuivel e){
        this.ev =e;
    }
    
    /**
     * Changes the state of the event to in distribuiton
     */
    @Override
    public void run(){
        this.ev.setEmDistribuicao();
    }
}
