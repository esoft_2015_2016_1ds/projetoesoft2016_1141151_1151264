/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TimerTasks;

import interfaces.Submissivel;
import java.util.TimerTask;

/**
 *
 * @author Gonçalo
 */
public class AlterarParaEmDecisao extends TimerTask{
    private Submissivel sub;
    
    /**
     * AlterarParaEmDecisao Constructor
     * @param sub  the submissible to associate this task to
     */
    public AlterarParaEmDecisao(Submissivel sub){
        this.sub = sub;
    }
    
    /**
     * This method sets the state of the associated submissible to EmDecisaoState
     */
    @Override
    public void run(){
        this.sub.setEmDecisao();
    }
}
