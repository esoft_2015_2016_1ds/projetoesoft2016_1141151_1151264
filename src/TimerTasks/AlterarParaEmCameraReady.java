/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TimerTasks;

import interfaces.Submissivel;
import java.util.TimerTask;

/**
 *
 * @author Gonçalo
 */
public class AlterarParaEmCameraReady extends TimerTask{
    private Submissivel sub;
    
    /**
     * AlterarParaEmCameraReady constructor
     * @param sub the submissible to associate to this timer task
     */
    public AlterarParaEmCameraReady(Submissivel sub){
        this.sub = sub;
    }
    
    /**
     * This method sets the state of the associated submissible to EmCameraReadyState.
     */
    @Override
    public void run(){
        this.sub.setEmCameraReady();
    }
}
