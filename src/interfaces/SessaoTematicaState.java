/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Diogo
 */
public interface SessaoTematicaState
{
    /**
     * This method checks it the {@link model.SessaoTematica} is valid
     * @return (boolean) Returns true if it is valid
     */
    public boolean valida();
    /**
     * This methods sets the state of the {@link model.SessaoTematica} to "CriadoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCriado();
        /**
     * This methods sets the state of the {@link model.SessaoTematica} to "RegistadoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRegistado();
        /**
     * This methods sets the state of the {@link model.SessaoTematica} to "CPDefinidaState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCPDefinida();
        /**
     * This methods sets the state of the {@link model.SessaoTematica} to "AceitaSubmissoesState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setAceitaSubmissoes();
        /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmDetecaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmDetecao();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmLicitacaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmLicitacao();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmDistribuicaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmDistribuicao();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmRevisaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmRevisao();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmDecisaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmDecisao();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmSubmissaoCameraReadyState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmSubmissaoCameraReady();
            /**
     * This methods sets the state of the {@link model.SessaoTematica} to "EmCameraReadyState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCameraReady();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaCriadoState} (created state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCriadoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaRegistadoState} (registered state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInRegistadoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaCPDefinidaState} (CP defined state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCPDefinidaState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaAceitaSubmissoesState} (accepts submissions state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInAceitaSubmissoesState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmDetecaoState} (detecting conflicts state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDetecaoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmLicitacaoState} (bidding state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmLicitacaoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmDistribuicaoState} (distributing revisions state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDistribuicaoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmRevisaoState} (in revision state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmRevisaoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmDecisaoState} (in decision state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDecisaoState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaEmSubmissaoCameraReadyState} (in 
     * camera ready submission state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmSubmissaoCameraReadyState();
    /**
     * Checks if the thematic session is in the {@link states.sessaotematica.SessaoTematicaCameraReadyState} (camera ready state).
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCameraReadyState();
}
