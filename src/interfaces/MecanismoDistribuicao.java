/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;
import model.Revisao;
import model.Submissao;

/**
 *
 * @author Diogo
 */
public interface MecanismoDistribuicao 
{
    /**
     * This methods distributes the {@link model.Revisao} for a list of {@link model.Submissao}
     * @param ls (List&lt;{@link model.Submissao}&gt;)
     * @return (List&lt;{@link model.Revisao}%gt;)
     */
    public List<Revisao> distribui(List<Submissao> ls);
}
