/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import model.Artigo;
import model.ProcessoDistribuicao;
import model.Submissao;
import model.ProcessoDecisao;
import java.util.List;
import model.Utilizador;
import utils.Data;

/**
 *
 * @author jbraga
 */
public interface Submissivel
{
    /**
     * This method returns a newly generated {@link model.ProcessoDistribuicao}
     * @return {@link model.ProcessoDistribuicao}
     */
    public ProcessoDistribuicao novoProcessoDistribuicao();
    /**
     * This method returns a newly generated {@link model.Submissao}
     * @return {@link model.Submissao}
     */
    public Submissao novaSubmissao();
    /**
     * This method checks if adding a {@link model.Submissao} is valid
     * @param submissao {@link model.Submissao}
     * @param a {@link model.Artigo}
     * @param u {@link model.Utilizador}
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean addSubmissao(Submissao submissao,Artigo a,Utilizador u);
        /**
     * This method returns a newly generated {@link model.ProcessoDecisao}
     * @return {@link model.ProcessoDecisao}
     */
    public ProcessoDecisao novoProcessoDecisao();
        /**
     * This method sets the {@link model.ProcessoDecisao}
     */
    public void setProcessoDecisao(ProcessoDecisao pd);
    /**
     * This method returns a list of removed {@link model.Submissao}
     * @return (List&lt;{@link model.Submissao}gt;)
     */
    public List<Submissao> getSubmissoesRemovidas();
    /**
     * This method returns a list of {@link model.Submissao}
     * @return (List&lt;{@link model.Submissao}gt;)
     */
    public List<Submissao> getListaSubmissoes();
    /**
     * This method registers a change to a {@link model.Submissao}
     * @param sub ({@link model.Submissao})
     * @return (boolean) 
     */
    public boolean registaAlteracao(Submissao sub);
    /**
     * This method changes the state to "EmCameraReadyState"
     * @return (boolean) returns true if the operation is valid
     */
    public boolean setEmCameraReady();
    /**
     * This method returns the limit date to the final submission
     * @return (Data)
     */
    public Data getDataLimiteSubmissaoFinal();
    /**
     * Returns a list of {@link model.Submissao} that are in the "Aceite" state
     * and that belong to the author with the specified id.
     * @param id (String) The user's id.
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getListaSubmissoesAceites(String id);
    /**
     * Returns a list of {@link model.Submissao} that are not in the "Camera Ready" state
     * and that belong to the author with the specified id.
     * @param id (String) The user's id.
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getSubmissoesNaoCameraReadyDe(String id);
    /**
     * This method sets the state of the {@link interfaces.Submissivel} to "CriadoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCriado();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "RegistaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRegistado();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "CPdefinidaState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCPDefinida();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "AceitaSubmissioesState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setAceitaSubmissoes();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "EmDistribuicaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmDistribuicao();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "EmRevisaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmRevisao();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "EmDecisaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmDecisao();
        /**
     * This method sets the state of the {@link interfaces.Submissivel} to "SubmissoesDecidasState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setSubmissoesDecididas();
    public ArrayList<String> getAcceptedTopics(ArrayList<String> ls);
    public ArrayList<String> getRejectedTopics(ArrayList<String> ls);
    public ArrayList<String[]> getStatsTopics(ArrayList<String[]> ls);
}
