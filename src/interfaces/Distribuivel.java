/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;
import model.Licitacao;
import model.ProcessoDistribuicao;
import model.Submissao;

/**
 *
 * @author Luis
 */
public interface Distribuivel 
{
    /**
     * This method creates a {@link model.ProcessoDistribuicao}
     * @return {@link model.ProcessoDistribuicao}
     */
    public ProcessoDistribuicao novoProcessoDistribuicao();
    /**
     * This method sets the state of the {@link interfaces.Distribuivel} to "EmDistribuicao"
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setEmDistribuicao();
    /**
     * This method procures {@link model.Submissao} of the {@link interfaces.Distribuivel}
     * @return List&lt;{@link model.Submissao}&gt; 
     */
    public List<Submissao> getSubmissoes();
        /**
     * This method procures {@link model.Licitacao} of the {@link interfaces.Distribuivel}
     * @return List&lt;{@link model.Licitacao}&gt; 
     */
    public List<Licitacao> getLicitacoes();
    /**
     * This method sets the {@link model.ProcessoDecisao} of the {@link interfaces.Distribuivel}
     * @param pd ({@link model.ProcessoDecisao})
     */
    public void setProcessoDistribuicao(ProcessoDistribuicao pd);
    /**
     * This method sets the state of the {@link interfaces.Distribuivel} to "EmRevisao"
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setEmRevisao();
    /**
     * This method sets the state of the {@link interfaces.Distribuivel} to "EmSubmissoesDecididas"
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setSubmissoesDecididas();
}
