/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import java.util.List;
import model.Revisao;

/**
 *
 * @author jbraga
 */
public interface Revisivel 
{
    /**
     * This method returns the list of {@link model.Revisao} made by a user within the {@link interfaces.Revisivel}
     * @param id (String) The user's ID
     * @return (List{&lt;{@link model.Revisao}&gt;})
     */
    public List<Revisao> getRevisoes(String id);
    /**
     * This method saves a {@link model.Revisao} within the list of the {@link interfaces.Revisivel} if it does noes exist already
     * @param rev ({@link model.Revisao}) 
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean saveRevisao(Revisao rev);
    /**
     * This method sets the state of the {@link interfaces.Revisivel} into "EmDecisaoState"
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setEmDecisao();
 
}
