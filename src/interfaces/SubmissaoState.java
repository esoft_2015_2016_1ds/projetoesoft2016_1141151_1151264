/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author jbraga
 */
public interface SubmissaoState 
{
    /**
     * This method checks if it is valid to remove the {@link model.Submissao}
     * @return (boolean) Returns true if the operaion is valid
     */
    public boolean validaRemover();
    /**
     * This method checks if the {@link model.Submissao} is valid
     * @return (boolean) Returns true if it is valid
     */
    public boolean valida();
            /**
     * This methods sets the state of the {@link model.Submissao} to "CriadoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setCriado();
                /**
     * This methods sets the state of the {@link model.Submissao} to "RegistadoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRegistado();
                /**
     * This methods sets the state of the {@link model.Submissao} to "EmLicitacaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmLicitacao();
                /**
     * This methods sets the state of the {@link model.Submissao} to "EmRevisaoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmRevisao();
                /**
     * This methods sets the state of the {@link model.Submissao} to "PreRevistaState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setPreRevista();
                /**
     * This methods sets the state of the {@link model.Submissao} to "NaoRevistoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setNaoRevisto();
                /**
     * This methods sets the state of the {@link model.Submissao} to "RemovidaState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRemovida();
                /**
     * This methods sets the state of the {@link model.Submissao} to "RejeitadaState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRejeitada();
                /**
     * This methods sets the state of the {@link model.Submissao} to "RevistoState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setRevisto();
                /**
     * This methods sets the state of the {@link model.Submissao} to "AceiteState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setAceite();
                /**
     * This methods sets the state of the {@link model.Submissao} to "AceiteNaoFinalState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setAceiteNaoFinal();
                /**
     * This methods sets the state of the {@link model.Submissao} to "CameraReadyState"
     * @return (boolean) Returns true if the operation is valid
     */
    public boolean setEmCameraReady();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoCriadoState} (created state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInCriadoState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoRegistadoState} (registered state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRegistadoState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoRemovidaState} (removed state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRemovidaState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoAceiteState} (accepted state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInAceiteState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoEmCameraReadyState} (in
     * camera ready state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmCameraReadyState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoEmLicitacaoState} (bidding state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmLicitacaoState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoNaoRevistoState} (not revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInNaoRevistoState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoPreRevistaState} (pre-revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInPreRevistaState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoEmRevisaoState} (in revision state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmRevisaoState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoRejeitadaState} (rejected state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRejeitadaState();
    /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoRevistoState} (revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRevistoState();
     /**
     * Checks if the submission is in the {@link states.submissao.SubmissaoAceiteNaoFinalState} (revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInAceiteNaoFinalState();
}
