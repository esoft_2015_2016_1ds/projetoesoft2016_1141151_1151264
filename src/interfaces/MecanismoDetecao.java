/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import model.Revisor;
import model.Submissao;

/**
 *
 * @author Diogo
 */
public interface MecanismoDetecao 
{
    /**
     * This method detets conflits of interest between a {@link model.Revisao} and a {@link model.Submissao} 
     * @param r ({@link model.Revisao})
     * @param s {@link model.Submissao}
     * @return (boolean) Returns true if a conflitct is detected
     */
    public boolean deteta(Revisor r,Submissao s);
}
