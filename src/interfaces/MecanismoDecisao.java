/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import model.Decisao;
import java.util.List;

/**
 *
 * @author Diogo
 */
public interface MecanismoDecisao 
{
    /**
     * This method decides the veridict of {@link model.Submissao}
     * @return (List&lt;{@link model.Decisao}&gt;)
     */
    public List<Decisao> decide();
}
