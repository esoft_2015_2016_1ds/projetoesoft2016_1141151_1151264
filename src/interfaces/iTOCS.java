/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 * Testing interface.
 * @author jbraga
 */
public interface iTOCS {
    /**
     * Used for junit result comparison.
     * @return (String) The returned data to use.
     */
    public String showData();
}
