/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;
import model.Revisor;
import model.Submissao;
import model.TipoConflito;

/**
 *
 * @author jbraga
 */
public interface Detetavel {
    /**
     * Detects conflitcs.
     * @param lmc (List&lt;{@link model.TipoConflito}&gt;) A list with the type of conflicts.
     */
    public void detetarConflitos(List<TipoConflito> lmc);
    /**
     * Sets the state of the detectable to the "EmLicitacao" state.
     * @return (boolean) True if state succesfully set.
     */
    public boolean setEmLicitacao();
    /**
     * Sets the state of the detectable to the "EmDetecao" state.
     * @return (boolean) True if state succesfully set.
     */
    public boolean setEmDetecao();
    /**
     * Retrieves the list of revisores of the detectable's CP.
     * @return (List&lt;{@link model.Revisor}&gt;) The list of revisors.
     */
    public List<Revisor> getListaRevisoresCP();
    /**
     * Returns a list of the submissions of the detectable.
     * @return (List&lt;{@link model.Revisor}&gt;) The list of submissions.
     */
    public List<Submissao> getListaSubmissoes();
}
