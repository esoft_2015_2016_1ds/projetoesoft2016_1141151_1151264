/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;
import model.ProcessoDecisao;
import model.Revisao;

/**
 *
 * @author jbraga
 */
public interface Decidivel 
{
    /**
     * Sets the decisable's state to the corresponding "EmDecisao" state.
     * @return (boolean) True if successfully changed states.
     */
    public boolean setEmDecisao();
    /**
     * Creates a new decision process for the target decisable.
     * @return ({@link model.ProcessoDecisao}) The new decision process.
     */
    public ProcessoDecisao novoProcessoDecisao();
    /**
     * Gets the list of all revisions of this decisable.
     * @return (List&lt;{@link model.Revisao}&gt;) The list of revision.
     */
    public List<Revisao> getRevisoes();
    /**
     * Sets the decisable's state to the corresponding "SubmissoesDecididas" state.
     * @return (boolean) True if successfully changed states.
     */
    public boolean setSubmissoesDecididas();
    /**
     * Sets this decisable's decision process.
     * @param pd ({@link model.ProcessoDecisao}) The decisable's new decision process.
     */
    public void setProcessoDecisao(ProcessoDecisao pd);
}
