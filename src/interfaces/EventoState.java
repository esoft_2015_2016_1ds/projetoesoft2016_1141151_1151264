/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Diogo
 */
public interface EventoState{
    public boolean valida();
    public boolean setCriado();
    public boolean setRegistado();
    public boolean setCPDefinida();
    public boolean setAceitaSubmissoes();
    public boolean setEmDetecao();
    public boolean setEmLicitacao();
    public boolean setEmDistribuicao();
    public boolean setEmRevisao();
    public boolean setEmDecisao();
    public boolean setEmSubmissaoCameraReady();
    public boolean setSessaoTematicaDefinida();
    public boolean setCameraReady();
    /**
     * Checks if this event is in the {@link states.evento.EventoCriadoState} (created state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCriadoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoRegistadoState} (registered state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInRegistadoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoSessaoTematicaDefinidaState} (thematic
     * session defined state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInSessaoTematicaDefinidaState();
    /**
     * Checks if this event is in the {@link states.evento.EventoCPDefinidaState} (CP defined state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCPDefinidaState();
    /**
     * Checks if this event is in the {@link states.evento.EventoAceitaSubmissoesState} (accepts submissions state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInAceitaSubmissoesState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmDetecaoState} (detecting conflicts state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDetecaoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmLicitacaoState} (bidding state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmLicitacaoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmDistribuicaoState} (distributing revisions state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDistribuicaoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmRevisaoState} (in revision state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmRevisaoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmDecisaoState} (in decision state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDecisaoState();
    /**
     * Checks if this event is in the {@link states.evento.EventoEmSubmissaoCameraReadyState} (in 
     * camera ready submission state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmSubmissaoCameraReadyState();
    /**
     * Checks if this event is in the {@link states.evento.EventoCameraReadyState} (camera ready state).
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCameraReadyState();
}
