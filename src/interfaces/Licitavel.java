/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;
import model.Revisor;
import model.Submissao;

/**
 *
 * @author jbraga
 */
public interface Licitavel 
{
    /**
     * This method returns a {@link model.Revisor} based in his ID
     * @param id (String)
     * @return {@link model.Revisor} The matching {@link model.Revisor}
     */
    public Revisor getRevisor(String id);
    /**
     * This method returns a list of {@link model.Submissao} that are in {@link states.submissao.SubmissaoEmLicitacaoState}
     * @return (List&lt;{@link model.Submissao}&gt;) 
     */
    public List<Submissao> getSubmissoesEmLicitacao();
}
