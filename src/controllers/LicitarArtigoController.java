/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Licitavel;
import interfaces.iTOCS;
import java.util.ArrayList;
import model.Licitacao;
import java.util.List;
import model.Revisor;
import model.Submissao;
import model.TipoConflito;
import registos.RegistoEventos;
import registos.RegistoTiposConflito;

/**
 *
 * @author Diogo
 */
public class LicitarArtigoController implements iTOCS{
    private RegistoEventos registoEventos;
    private RegistoTiposConflito regTC;
    private Licitacao l;
    private Licitavel sub;
    private Submissao s;
    public LicitarArtigoController(RegistoEventos registoEventos,RegistoTiposConflito regTC)
    {
        this.registoEventos=registoEventos;
        this.regTC = regTC;
    }
    /**
     * Returns a list of {@link interfaces.Licitavel} that are currently accepting
     * licitations (@link model.Licitacao} from a user with the specified id.
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Licitavel}&gt;) The list of submissiveis.
     */
    public List<Licitavel> getListaLicitaveisEmLicitacaoDe(String id)
    {
        return registoEventos.getListaLicitaveisEmLicitacaoDe(id);
    }
    /**
     * Selects the chosen licitavel for the controller.
     * @param sub ({@link interfaces.Licitavel}) The chosen submissivel.
     */
    public void selecionaLicitavel(Licitavel sub)
    {
        this.sub=sub;
    }
    /**
     * Selects the chosen submission for the controller.
     * @param s ({@link model.Submissao}) The chosen submission.
     */
    public void selectSubmissao(Submissao s)
    {
        this.s=s;
    }
    /**
     * Returns a list of submissions of the submissivel that are in 
     * {@link states.submissao.SubmissaoEmLicitacaoState} state.
     * @return (List&lt;{@link model.Submissao}&gt;)List containing the submissions.
     */
    public List<Submissao> getSubmissoesEmLicitacao()
    {
        return sub.getSubmissoesEmLicitacao();
    }
    /**
     * Returns a list containing all of the types of conflicts available in the company.
     * @return (List&lt;{@link model.TipoConflito}&gt;) List containing the types of conflict.
     */
    public List<TipoConflito> getListaTiposConflito()
    {
        return regTC.getListaTiposConflito();
    }
    /**
     * Gets the information of the specified submission.
     * @param s ({@link model.Submissao}) The submission to obtain information from.
     * @return (String) Textual information of the submission.
     */
    public String getInfo(Submissao s)
    {
        return s.getInfo();
    }
    /**
     * Gets the specified revisor's bid of the submission.
     * If the revisor hasn't yet made any bids, a new one will be created instead.
     * @param id (String) The specified revisor.
     * @return (@link model.Licitacao) The new bid.
     */
    public Licitacao novaLicitacao(String id)
    {
        Licitacao lc = s.getLicitacaoDe(id);
        if (lc==null)
        {
            List<TipoConflito> tcp = s.getTiposConflito(id);
            if (tcp==null)
            {
                tcp = new ArrayList();
            }
            Revisor r =sub.getRevisor(id);
            lc = s.novaLicitacao(r,tcp);
        }
        l=lc;
        return lc;
    }
    public void setInteresse(int interesse)
    {
        l.setInteresse(interesse);
    }
    /**
     * Adds a new type of conflict to the bid.
     * @param tc ({@link model.TipoConflito}) The new type of conflict to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addTipoConflito(TipoConflito tc)
    {
        return l.addTipoConflito(tc);
    }
    /**
     * Checks if a {@link model.Licitacao} is valid.
     * @return (boolean) True if valid.
     */
    public boolean validaLicitacao()
    {
        return l.valida();
    }
    /**
     * Registers the {@link model.Licitacao} in the submission.
     * @return (boolean) True if successfully registered.
     */
    public boolean registaLicitacao()
    {
        return s.registaLicitacao(l);
    }
        @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(sub))+"\n"+String.valueOf(System.identityHashCode(s))+"\n";
    }
}
