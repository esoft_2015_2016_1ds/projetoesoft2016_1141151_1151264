/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import interfaces.iTOCS;
import model.Artigo;
import model.Autor;
import model.Submissao;
import java.util.List;
import listas.ListaSubmissoes;
import model.Evento;
import model.SessaoTematica;
import registos.RegistoEventos;
import static utils.Utils.checkUsername;

/**
 *
 * @author Diogo
 */
public class AlterarSubmissaoController implements iTOCS
{
    /**
     * The event registry
     */
    private RegistoEventos registoEventos;
    /**
     * An object of type {@link model.Submissao} used to hold the possible changes to the user's Submissao
     */
    private Submissao sClone;
    /**
     * The current user's submission
     */
    private Submissao sub;
    /**
     * The Event/Thematic Session were the submission was made
     */
    private Submissivel subm;
    
    private Artigo aClone;
    /**
     * Creates an instance of object {@link controllers.AlterarSubmissaoController} with only the event registry.
     * @param registoEventos the event registry
     */ 
    public AlterarSubmissaoController(RegistoEventos registoEventos)
    {
        this.registoEventos = registoEventos;
    }
    /**
     * Creates an instance of object {@link controllers.AlterarSubmissaoController} all parameters filled.
     * This constructor should only be used for testing purposes.
     * @param registoEventos The event registry
     * @param sClone An object of type {@link model.Submissao} used to hold the possible changes to the user's Submissao
     * @param sub The current user's submission
     * @param a The current article clone
     */
    public AlterarSubmissaoController(RegistoEventos registoEventos, Submissao sClone, Submissao sub, Submissivel subm,Artigo a)
    {
        this.registoEventos=registoEventos;
        this.sClone=sClone;
        this.sub=sub;
        this.subm=subm;
        this.aClone=a;
    }
    /**
     * This method returns a list of objects type {@link model.Submissao} that are in {@link states.submissao.SubmissaoRegistadoState}.
     * @param id (String) The ID of the user. 
     * @return (List&lt;{@link model.Submissao}&gt;) the list of {@link model.Submissao} that are in Registado State
     */
    public List<Submissao> getListaSubmissoesRegistadasUtilizador(String id)
    {
 
        if(checkUsername(id))
        {
         List<Submissao> result = registoEventos.getSubmissoesRegistadasUtilizador(id);
         return result;
        }
        else
        {
          throw new IllegalArgumentException("ID inválido!");   
        }
    }
    /**
     * This method returns a list of {@link interfaces.Submissivel} that are accepting submissions.
     * @return (List&lt;Submissivel&gt;) 
     */
    public List<Submissivel> getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador(String id)
    {
        List<Submissivel> result = registoEventos.getListaSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
        if(result.isEmpty())
        {
            throw new IllegalArgumentException("Não há eventos nem sessões temáticas que aceitem mudanças de submissões.");
        }
        return result;
    }
    /**
     * This method returns the data of a submission
     * @param sub the user's submission
     * @return (String) the data of the user's submission
     */
    public String getDadosSubmissao(Submissao sub)
    {
        return sub.toString();    
    }
    /**
     * This method selects an Event/Thematic Session for the use case
     * @param submiss ({@link interfaces.Submissivel}) The Event/Thematic Session chosen by the user
     */
    public void selecionaSubmissivel(Submissivel submiss)
    {
        subm=submiss;
    }
    /**
     * This method selects an user's submission
     * @param submission the submission that may be changed
     */
    public void selecionaSubmissao(Submissao submission)
    {
            sub=submission;
        
    }
    /**
     * This method creates a copy of a submission
     * @return ({@link model.Submissao}) The created clone 
     */
    public Submissao criarClone()
    {
        sClone  = new Submissao(sub);
        return sClone;
    }
    /**
     * This method changes the parameters of an article
     * @param titulo (String)The title of the article
     * @param resumo (String)The abstract of the article
     * @param ficheiro (String) The path of the file
     */
    public void alteraDados(String titulo,String resumo, String ficheiro)
    {
        sClone.alteraDados(titulo, resumo, ficheiro);
    }
    /**
     * This method records a change made to a submission(changes the initial article's parameters)  
     * @return (boolean) true if the operation is sucessfull
     */
    public boolean registaAlteracao()
    {
        boolean result=false;
        if(sClone.validaInicial())
        {
            result=subm.registaAlteracao(sClone);
        }
        else
        {
            throw new IllegalArgumentException("Submissao inválida!");
        }
        return result;
    }
    /**
     * This method returs the current instance clone of {@link model.Submissao}
     * @return {@link model.Submissao} 
     * This method is used only for testing purposes.
     */
    public Submissao getsClone()
    {
        return this.sClone;
    }
    /**
     * This method returns the current instance clone of {@link interfaces.Submissivel}
     * @return {@link interfaces.Submissivel}
     * This method is used only for testing purposes
     */
    public Submissivel getSubmissivel()
    {
        return this.subm;
    }
    /**
     * This method creates a new {@link model.Autor}  
     * @param username (String) The username of the {@link model.Autor}  
     * @param email (String) The email adress of the {@link model.Autor}  
     * @param nome (String) The name of the{@link model.Autor}  
     * @param afiliacao (String) The afiliation {@link model.Autor}  
     * @return {@link model.Autor}  The created {@link model.Autor}  
     */
    public Autor novoAutor(String username, String email ,String nome, String afiliacao)
    {
          return sClone.novoAutor(username,email,nome,afiliacao);    
    }

    /**
     * This method adds an ({@link model.Autor}) to the initial {@link model.Artigo} of this {@link model.Submissao}
     * @param autor ({@link model.Autor})
     * @return (boolean) Returns true if the operation is Sucessful
     */
    public boolean addAutor(Autor autor)
    {
        return aClone.addAutor(autor);
    }
    
       /**
     * Removes an author from the article's author list.
     * @param author ({@link model.Autor}) The target author to remove.
     */
    public void removeAutor(Autor author)
    {
        aClone.removeAutor(author);
    }
    
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(sub))+"\n";
    }
}
