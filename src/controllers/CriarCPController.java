package controllers;

import interfaces.iTOCS;
import model.CP;
import model.Utilizador;
import model.Evento;
import model.Revisor;
import java.util.List;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 * Classe que é responsável por controlar a criação de uma comissão de programa
 *
 * @author André Vieira
 */
public class CriarCPController implements iTOCS {

    public RegistoEventos registoEventos;
    public RegistoUtilizadores registoUtilizadores;
    public Evento evento = null;
    public Utilizador u;
    public CP cp;
    public Revisor r;

    /**
     * Cria a instancia {@link controllers.CriarCPController} com os parametros:
     *
     * @param registoEventos {@link registos.RegistoEventos} o registo de todos
     * os Eventos
     * @param registoUtilizadores {@link registos.RegistoUtilizadores} o registo
     * de todos os Utilizadores
     */
    public CriarCPController(RegistoEventos registoEventos, RegistoUtilizadores registoUtilizadores) {
        this.registoEventos = registoEventos;
        this.registoUtilizadores = registoUtilizadores;
        this.evento=new Evento();
        this.u=new Utilizador();
        this.cp= new CP();
        this.r=new Revisor();
    }

    /**
     * Método que procura todos os eventos de um organizador no estado de {@link states.evento.EventoSessaoTematicaDefinidaState}
     *
     * @param strId(String) id do organizador
     * @return List&lt;{@link model.Evento}&gt; a lista de todos os eventos do
     * organizador
     */
    public List<Evento> getEventosOrganizadorEmEstadoSessaoTematicaDefinida(String strId) {

        return registoEventos.getEventosOrganizadorEmEstadoSessaoTematicaDefinida(strId);

    }

    /**
     * Método que guarda a seleção do evento e criar uma nova comissão de
     * programa
     *
     * @param e{@link model.Evento} o evento a ser guardado
     */
    public void selectEvento(Evento e) {
        evento = e;

        cp = evento.novaCP();
    }

    /**
     * Método que adiciona um Organizador a uma CP
     *
     * @param strId(String) do organizador
     * @return{@link model.Revisor} membro adicionado
     */
    public boolean addMembroCP(String strId) {
        Utilizador u = registoUtilizadores.getUtilizador(strId);

        if (u != null) {
            return cp.addMembroCP(u);
        } else {
            return false;
        }
    }

    /**
     * Método que adiciona um Revisor a uma CP
     *
     * @param r{@link model.Revisor} revisor a adicionar
     * @return {@link model.Revisor} membro adicionado
     */
    public boolean addMembroCP(Revisor r) {
        return cp.addMembroCP(r);
    }

    /**
     * Método que regista um Revisor a uma CP
     *
     * @param r {@link model.Revisor}
     * @return(boolean) membro adicionado
     */
    public boolean registaMembroCP(Revisor r) {
        return cp.registaMembroCP(r);
    }

    /**
     * Método irá acrescentar um organizador a uma CP
     *
     * @param id(String) do organizador
     */
    public Revisor novoMembroCP(String id) {
        u = registoUtilizadores.getUtilizador(id);
        if (u!=null)
        {
            return r = cp.novoMembroCP(u);
        }
        else
        {
            throw new IllegalArgumentException("O ID que introduziu não corresponde a um utilizador registado no sistema.");
        }
    }

    /**
     * Atribui o novo valor da CP do Evento
     */
    public boolean setCP() {
        return evento.setCP(cp);
    }

    /**
     * @see #showData() 
     * @return
     */
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(evento))+"\n";
    }

}
