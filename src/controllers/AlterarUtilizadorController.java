/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import model.Empresa;
import model.Utilizador;
import registos.RegistoUtilizadores;
import static utils.Utils.checkEmail;
import static utils.Utils.checkName;
import static utils.Utils.checkPassword;
import static utils.Utils.checkUsername;

/**
 *
 * @author Diogo
 */
public class AlterarUtilizadorController implements iTOCS{
    private Empresa empresa;
    /**
     * The user registry
     */
    private RegistoUtilizadores registoUtilizadores;
    /**
     * An object of type {@link model.Utilizador} used to hold the possible changes to user's profile
     */
    private Utilizador uClone;
    /**
     * The current user's profile
     */
    private Utilizador u;
    /**
     * Creates an instance of {@link controllers.AlterarUtilizadorController} with only the user registry
     * @param empresa ({@link model.Empresa}) The company.
     */
    public AlterarUtilizadorController(Empresa empresa)
    {
        this.registoUtilizadores = empresa.getRegistoUtilizadores();
        this.empresa=empresa;
    }
    /**
     * Gets the authenticated user of the system to change its data.
     */
    public void getUtilizadorAutenticado()
    {
        u=empresa.getUtilizadorAutenticado();
    }
    /**
     * Duplicates a user.
     * @return Object type {@link model.Utilizador} that is a copy of the parameter
     */
    public boolean criarClone()
    {
       boolean result = false; 
       if(u.valida())
       {
        uClone = new Utilizador(u);
        result = true;
       }
       else
       {
           throw new IllegalArgumentException("Utilizador inválido!");
       }
       return result;
       
    }
    /**
     * Change user's data
     * @param username 
     * @param password
     * @param email
     * @param nome
     * @return true if operation is sucessfull
     */
    public boolean alteraDados(String username,String password,String email,String nome)
    {
        if(checkEmail(email))
        {
            uClone.setEmail(email);
            if(checkName(nome))
            {
                uClone.setName(nome);
                if(checkPassword(password))
                {
                    uClone.setPassword(password);
                    if(checkUsername(username))
                    {
                        uClone.setUsername(username);
                        return true;
                    }
                    else
                    {
                    
                    throw new IllegalArgumentException("Username Inválido!");
                    
                    }
                    
                }
                else
                {
                    throw new IllegalArgumentException("Password Inválida!");
                   
                }
            }
            else
            {
                throw new IllegalArgumentException("Nome Inválido!");
              
            }
        }
        else
        {
            throw new IllegalArgumentException("Email Inválido!");
            
        }
    }
    /**
     * Changes user's data using a clone(type {@link model.Utilizador}) containing the new atributes if valid
     * @return true if change is valid
     */
    public boolean registaAlteracao()
    {
        boolean result=false;
        if (uClone.valida() && registoUtilizadores.validaAlteracaoUtilizador(uClone, registoUtilizadores.getPosition(u)))
        {
            result=true;
            u.setDados(uClone);
        }
        return result;
    }
    /**
     * Gets the info of an user
     * @return String - User's info
     */
    public String getInfo()
    {
        return u.toString();
    }
    
    
    @Override
    public String showData() {
        return registoUtilizadores.toString()+"\n"+u.toString()+"\n"+uClone.toString();
    }
}
