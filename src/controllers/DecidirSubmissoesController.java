/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Decidivel;
import interfaces.MecanismoDecisao;
import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import model.Empresa;
import model.ProcessoDecisao;
import java.util.List;
import model.Autor;
import model.Decisao;
import model.Evento;
import model.Revisao;
import model.SessaoTematica;
import registos.RegistoEventos;

/**
 *
 * @author Diogo
 */
public class DecidirSubmissoesController implements iTOCS
{
    private Empresa empresa;
    private RegistoEventos registoEventos;
    private Evento e;
    private Decidivel dec;
    private ProcessoDecisao pd;
    private List<Decisao> ld;
     /**
     * Constructor creates an instance of {@link controllers.DecidirSubmissoesController} with the specified parameters
     * @param e {@link model.Empresa}
     */
    public DecidirSubmissoesController(Empresa e)
    {
        empresa=e;
        registoEventos=empresa.getRegistoEventos();
    }
     /**
     * This method procures {{@link model.Evento}} that have have the "Main Event" or at least one {@link model.SessaoTematica} in "EmDecisaoState" whose {@link model.Proponente} is the user
     * @param id (String) The ID of the user
     * @return List (&gt;{@link model.Evento}&lt;) A list of {{@link model.Evento}} that have have the "Main Event" or at least one {@link model.SessaoTematica} in "EmDecisaoState" whose {@link model.Proponente} is the user
     */
    public List<Evento> getListaEventosEmDecisao(String id)
    {
        return registoEventos.getListaEventosEmDecisao(id);
    }
         /**
     * This method procures {@link interfaces.Submissivel} that are in "EmDecisaoState" and whose {@link model.Proponente} or {@link model.Organizador} is the user
     * @param id (String) The ID of the user
     * @return List (&gt;{@link model.Evento}&lt;) A list of {@link interfaces.Submissivel} that are in "EmDecisaoState" and whose {@link model.Proponente} or {@link model.Organizador} is the user
     */
    public List<Submissivel> getListaSubmissiveisEmDecisao(String id)
    {
        List<Submissivel> result = new ArrayList();
        if(e.isInEmDecisaoState() && e.temOrganizador(id))
        {
            result.add(e);
        }
        List<SessaoTematica> temp = e.getListaSessoesEmDecisao(id);
        if(!temp.isEmpty())
        {
            result.addAll(temp);
        }
        return result;
    }
    /**
     * This method returns the a list of {@link model.Revisao} of a {@link interfaces.Decidivel}
     * @return List (&gt;{@link model.Revisao}&lt;) list of {@link model.Revisao} of a {@link interfaces.Decidivel}
     */
    public List<Revisao> getRevisoes()
    {
        return dec.getRevisoes();
    }
    /**
     * This method sets the {@link model.Evento} chosen by the user
     * @param ev ({@link model.Evento}) The {@link model.Evento} chosen by the user
     */
    public void setEvento(Evento ev)
    {
        e=ev;
    }
    /**
     * This method sets the {@link interfaces.Decidivel} chosen by the user
     * @param d ({@link interfaces.Decidivel})
     */
    public void setDecidivel(Decidivel d)
    {
        dec=d;
        dec.setEmDecisao();
        pd=dec.novoProcessoDecisao();
    }
     /**
     * This method generates a list of {@link model.Decisao} within the {@link model.ProcessoDecisao}
     * @return List &gt;Decisao&lt; created by the {@link interfaces.MecanismoDecisao}
     * @param lr List(&lt;{@link model.Revisao}&gt;)
     */
    public List<Decisao> decide(List<Revisao> lr)
    {
        ld=pd.decide(lr);
       return ld;
    }
        /**
     * This method procures the {@link model.Empresa} for {@link interfaces.MecanismoDistribuicao}
     * @return (List &gt;{@link interfaces.MecanismoDistribuicao}&lt;)
     */
    public List<MecanismoDecisao> getMecanismosDecisao()
    {
        return empresa.getMecanismosDecisao();
    }
        /**
     * This method sets the {@link interfaces.MecanismoDistribuicao} chosen by the user
     * @param mech {@link interfaces.MecanismoDistribuicao}
     */
    public void setMecanismoDecisao(MecanismoDecisao mech)
    {
        pd.setMecanismoDecisao(mech);
    }
     /**
     * This method registers the generated {@link model.ProcessoDecisao} and sets all the 
     * {@link interfaces.Decidivel} and the {@link interfaces.Distribuivel} to "EmSubmissoesDecididasState".
     */
    public void registaDecisao()
    {
        dec.setProcessoDecisao(pd);
        
        for(Decisao focus : ld)
        {
            if(focus.getVeredito())
            {
                focus.getSubmissao().setAceite();
                for(Autor now :(focus.getSubmissao()).getListaAutores())
                {
                    now.enviarNotificacao("A submissão em que trabalhou foi aceite.");
                }
            }
            else
            {
                focus.getSubmissao().setRejeitada();
                for(Autor now :(focus.getSubmissao()).getListaAutores())
                {
                    now.enviarNotificacao("A submissão em que trabalhou foi rejeitada.");
                }
            }
        }
        dec.setSubmissoesDecididas();

    }
    /**
     * @see #showData() 
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
}
