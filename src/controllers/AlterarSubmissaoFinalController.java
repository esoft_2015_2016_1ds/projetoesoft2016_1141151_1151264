/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import java.util.List;
import listas.ListaSubmissoes;
import model.Submissao;
import registos.RegistoEventos;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class AlterarSubmissaoFinalController implements iTOCS{
    private RegistoEventos registoEventos;
    private Submissao sClone;
    

    public List<Submissao> getListaSubmissoesUtilizadorEmCameraReady(String id){
        return registoEventos.getListaSubmissoesUtilizadorEmCameraReady(id);
    }
    public void selectSubmissao(Submissao sub){
         ListaSubmissoes ls=registoEventos.getListaSubmissao(sub);
    }
    public String getDadosSubmissao(Submissao sub){
        return sub.toString();
    }
    public boolean criarClone(){
        Submissao sClone=new Submissao();
        return true;
    }

      @Override
    public String showData() {
        return "";
    }
}
