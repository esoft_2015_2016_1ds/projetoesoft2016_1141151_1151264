/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Revisivel;
import interfaces.iTOCS;
import model.Revisao;
import java.util.ArrayList;
import java.util.List;
import registos.RegistoEventos;

/**
 *
 * @author Gabriel
 */
public class ReverArtigoController implements iTOCS {

    private RegistoEventos registoEventos;
    private Revisao revisao;
    private ArrayList<Revisao> listaRevisoes;
    private Revisivel revisivel;

    /**
     * Creates an instance of object {@link controllers.ReverArtigoController} with the
     * specified parameters.
     * @param reg ({@link registos.RegistoEventos}) The event registry.
     */
    public ReverArtigoController(RegistoEventos reg)
    {
        registoEventos=reg;
    }
    /**
     * Método que busca a Lista de Revisiveis de um dado utilizador que se
     * encontre no estado em Revisao
     *
     * @param id (String) id do utilizador
     * @return a lista revisivel
     */
    public List<Revisivel> getListaRevisivelUtilizadorEmEstadoRevisao(String id) {
        return registoEventos.getListaRevisivelUtilizadorEmEstadoRevisao(id);
    }

    /**
     * Método que guarda o revisivel escolhido pelo utilizador
     *
     * @param r {@link interfaces.Revisivel} o revisivel escolhido
     *
     */
    public void selectRevisivel(Revisivel r) {
        revisivel = r;
    }

    /**
     * Método que busca as revisoes de um dado utilizador
     *
     * @param id (String) id do utilizador
     * @return a lista de revisoes
     */
    public List<Revisao> getRevisoes(String id) {
        return revisivel.getRevisoes(id);
    }

    /**
     * Método que guarda a revisao selecionada pelo utilizador
     *
     * @param rev {@link model.Revisao} a revisão selecionada
     */
    public void selectRevisao(Revisao rev) {
        revisao = rev;
    }

  

    /**
     * Método que irá alterar a classificação da confiança
     *
     * @param classificacao(int) classificacao a atribuir
     */
    public void setClassificacaoConfianca(int classificacao) {
        revisao.setClassificacaoConfianca(classificacao);
    }

    /**
     * Método que irá alterar a classificação da adequação
     *
     * @param classificacao(int) classificacao a atribuir
     */
    public void setClassificacaoAdequacao(int classificacao) {
        revisao.setClassificacaoAdequacao(classificacao);
    }

    /**
     * Método que irá alterar a classificação da originalidade
     *
     * @param classificacao(int) classificacao a atribuir
     */
    public void setClassificacaoOriginalidade(int classificacao) {
        revisao.setClassificacaoOriginalidade(classificacao);
    }

    /**
     * Método que irá alterar a classificação da apresentacao
     *
     * @param classificacao(int) classificacao a atribuir
     */
    public void setClassificacaoApresentacao(int classificacao) {
        revisao.setClassificacaoApresentacao(classificacao);
    }

    /**
     * Método que irá alterar a classificação da recomendacao
     *
     * @param classificacao(int) classificacao a atribuir
     */
    public void setClassificacaoRecomendacao(int classificacao) {
        revisao.setClassificacaoRecomendacao(classificacao);
    }

    /**
     * Método que irá alterar o texto explicativo
     *
     * @param texto(String) texto explicativo
     */
    public void setTextoExplicativo(String texto) {
        revisao.setTextoExplicativo(texto);
    }
    /**
     * Método que irá guarda a revisão
     * @return 
     */
    public boolean saveRevisao(){
        boolean result = revisivel.saveRevisao(revisao) && revisao.setPreRevistoState();
        revisivel.setEmDecisao();
        return  result;
    }
    /**
     * Método que irá alterar o estado
     * @return 
     */
    public boolean setPreRevistoState(){
        return revisao.setPreRevistoState();
    }
     /**
     * Método que irá alterar o estado
     * @return 
     */
    public boolean setEmDecisaoState(){
        return revisivel.setEmDecisao();
    }
    /**
     * @see #showData() 
     * @return 
     */
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(revisivel))+"\n"+String.valueOf(System.identityHashCode(revisao));
    }
}
