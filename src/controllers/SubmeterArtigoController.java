/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import interfaces.Submissivel;
import interfaces.iTOCS;
import model.Autor;
import model.Submissao;
import model.Utilizador;
import java.util.List;
import model.Artigo;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import states.evento.EventoAceitaSubmissoesState;

/**
 * A class that is responsible for controlling every action related to the 
 * submission of an article.
 * @author jbraga
 */
public class SubmeterArtigoController implements iTOCS
{
    private RegistoEventos registoEventos;
    private RegistoUtilizadores registoUtilizadores;
    private Submissao submission;
    private Submissivel sub;
    private Artigo a;
    private Utilizador u;
    /**
     * Creates a new instance of object SubmeterArtigoController with the specified parameters.
     * @param reg ({@link registos.RegistoEventos}) The registry of events.
     * @param actorUser ({@link model.Utilizador}) The user that started this use case.
     * @param regU ({@link registos.RegistoUtilizadores}) The registry of users.
     */
    public SubmeterArtigoController(RegistoEventos reg,Utilizador actorUser,RegistoUtilizadores regU)
    {
        registoEventos=reg;
        u=actorUser;
        registoUtilizadores = regU;
    }
    /**
     * Sets the corresponding author of the article.
     * @param author (Autor) The author to assign the role of corresponding author.
     */
    public void setAutorCorrespondente(Autor author)
    {
        Utilizador user = registoUtilizadores.getUtilizador(author.getEmail());
        if (user == null)
        {
            user = registoUtilizadores.getUtilizador(author.getUsername());
        }
        a.setAutorCorrespondente(author, user);
    }
    /**
     * Selects the {@link interfaces.Submissivel} that the user wants to submit to.
     * This method is also responsible for creating a new {@link model.Submissao} for the selected {@link interfaces.Submissivel}
     * and a new {@link model.Artigo} for the {@link model.Submissao} created.
     * @param sub ({@link interfaces.Submissivel}) The selected submissivel.
     */
    public void selectSubmissivel(Submissivel sub)
    {
        this.sub=sub;
        submission = sub.novaSubmissao();
        a=submission.novoArtigo();
    }
    /**
     * Sets the article's title.
     * @param title (String) The title's new value.
     */
    public void setArticleTitle(String title)
    {
        a.setTitulo(title);
    }
    /**
     * Sets the article's summary.
     * @param summary (String) The summary's new value.
     */
    public void setArticleSummary(String summary)
    {
        a.setResumo(summary);
    }
    /**
     * Sets the article's data with the specified parameters.
     * @param title (String) The title's new value.
     * @param summary (String) The summary's new value.
     * @param palavrasChave  (List&gt;String&lt;) The new set of keywords 
     * @param instituicao (String) The user's institution.
     */
    public void setArtigoData(String title,String summary,List<String> palavrasChave,String instituicao)
    {
        a.setTitulo(title);
        a.setResumo(summary);
        a.setPalavrasChave(palavrasChave);
        a.addUtilizadorComoAutor(u,instituicao);
    }
    /**
     * Sets the article's file.
     * @param file (String) The article's new file.
     */
    public void setFile(String file) //throws IllegalArgumentException
    {
        a.setFicheiro(file);
    }
    /**
     * Returns a list of all of the empresa's {@link interfaces.Submissivel}.
     * @return (List&lt;Submissivel&gt;) The list of submissiveis in the empresa.
     */
    public List<Submissivel> getListaSubmissiveisEmEstadoAceitaSubmissoes()
    {
        return registoEventos.getListaSubmissiveisEmEstadoAceitaSubmissoes();//empresa.getListaSubmissiveis();
    }
    /**
     * Returns a list of all the authors that can meet the criteria to be the
     * article's corresponding author.
     * @return (List&lt;Autor&gt;) The list of possible corresponding authors.
     */
    public List<Autor> getPossiveisAutorCorrespondentes()
    {
        return a.getPossiveisAutoresCorrespondentes(registoUtilizadores);
    }
    /**
     * Returns the summarized information of an article in a text-format.
     * @return (String) The text-formatted information of the article.
     */
    public String getInfoResumo()
    {
        return sub.toString();
    }
    /**
     * Registers the submission.
     * @return (boolean) True if successfully registered.
     */
    public boolean registerSubmissao()
    {
        return sub.addSubmissao(submission,a,u);
    }
    /**
     * Creates a new author for the article with the specified parameters.
     * @param name (String) The author's name.
     * @param email (String) The author's email.
     * @param institution (String) The author's institution.
     * @param username (String) The author's username.
     * @return (Autor) The newly created author.
     */
    public Autor novoAutor(String name,String email,String institution,String username)
    {
        return a.novoAutor(name, email, institution,username);
    }
    /**
     * Adds an author to the article's author list.
     * @param author (Autor) The target author to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addAutor(Autor author)
    {
        return a.addAutor(author);
    }
    /**
     * Removes an author from the article's author list.
     * @param author ({@link model.Autor}) The target author to remove.
     */
    public void removeAutor(Autor author)
    {
        a.removeAutor(author);
    }
    /**
     * Checks if the article is valid.
     * @return (boolean) True if article is valid.
     */
    public boolean validateArticle()
    {
        return a.valida();
    }
    
    @Override
    public String showData() {
        String result=a.getFicheiro()+"\n"+a.getResumo()+"\n"+a.getTitulo()+"\n"+System.identityHashCode(sub)+"\n";
        result+=((a.showData().split("\n").length!=0) ? a.showData().split("\n")[0]+"\n" : "\n");
        return result;
    }
}
