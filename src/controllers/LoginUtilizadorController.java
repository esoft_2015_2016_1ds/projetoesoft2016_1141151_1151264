/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import model.Utilizador;
import registos.RegistoUtilizadores;

/**
 *
 * @author Diogo
 */
public class LoginUtilizadorController implements iTOCS{

    private RegistoUtilizadores reg;
    
    public LoginUtilizadorController(RegistoUtilizadores regU)
    {
        this.reg=regU;
    }
    /**
     * Attempts to login the user with the specified credentials.
     * @param username (String) The user's username.
     * @param password (String)The user«s password.
     * @return ({@link model.Utilizador}) The user that was logged in. null
     * if the user was not found.
     */
    public Utilizador login(String username,String password)
    {
        return reg.login(username,password);
    }
    
   
    @Override
    public String showData() {
        return "";
    }
    
}
