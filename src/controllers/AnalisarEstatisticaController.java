/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import java.util.List;
import listas.ListaRevisoes;
import model.Evento;
import model.Revisao;
import model.Revisor;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 * A class that is responsible for controlling every action related to the 
 * statistical analysis of a revisor.
 * @author Diogo
 */
public class AnalisarEstatisticaController implements iTOCS{
    public static final double UNI_SIGN_5_PERCENT=1.645;
    private RegistoEventos reg;
    private RegistoUtilizadores regU;
    private List<Revisor> lrt;
    private Revisor r;
    public AnalisarEstatisticaController(RegistoEventos registoEventos,RegistoUtilizadores regU)
    {
        reg=registoEventos;
        this.regU = regU;
        
    }
    /**
     * Returns a list of all revisores in the system.
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all revisors.
     */
    public List<Revisor> getListaTodosRevisores()
    {
        lrt = regU.getListaTodosRevisores(reg);
        return lrt;
    }
    /**
     * Selects the revisor for this use case.
     * @param r ({@link model.Revisor}) The targer revisor.
     */
    public void selectRevisor(Revisor r)
    {
        this.r=r;
    }
    /**
     * Returns the statistical analysis of the selected revisor.
     * @return (double) The value of the test.
     */
    public double getAnaliseResults()
    {
        double result=0;
        List<Revisao> lrTemp = reg.getListaTodasRevisoes();
        if (lrTemp.size()>=30)
        {
            ListaRevisoes lr = new ListaRevisoes(lrTemp);
            double mediaTodas = getMediaDeRevisoes(lr);
            List<Revisao> lrRev = lr.getRevisoes(r.getUtilizador().getEmail());
            double[] diff = new double[lrRev.size()];
            for (int i=0;i<lrRev.size();i++)
            {
                Revisao nextRevisao = lrRev.get(i);
                diff[i]=getDiferencaAbsMedias(mediaTodas,nextRevisao.getClassificacaoRecomendacao());
            }
            double media = getMedia(diff,diff.length);
            double variencia = getVariencia(diff,media,diff.length);
            result = getResultadoHipotese(variencia,media,0.05,diff.length);
        }
        else
        {
            throw new IllegalArgumentException("Não existem revisões suficientes para analisar o revisor (são necessárias pelo menos 30).");
        }
        return result;
    }
    /**
     * Gets the mean value of the revisions in a list of revisions.
     * @param lr ({@link listas.ListaRevisoes}) The list with all revisions.
     * @return (double) The mean value.
     */
    private double getMediaDeRevisoes(ListaRevisoes lr)
    {
        return lr.getMediaDeRevisoes();
    }
    /**
     * Returns the absolute difference between two values.
     * In this context, the difference will be the difference
     * between the mean values a and b.
     * @param a (double) First value.
     * @param b (double) Second value.
     * @return (double) The absolute difference between both values.
     */
    private double getDiferencaAbsMedias(double a, double b)
    {
        return Math.abs(a-b);
    }
    /**
     * Gets the mean value of an array of doubles.
     * @param arr (double[]) The array containing the sample values.
     * @param size (int) The size of the sample.
     * @return (double) The mean value.
     */
    private double getMedia(double[] arr, int size)
    {
        double result=0;
        for (int i=0;i<arr.length;i++)
        {
            result+=arr[i];
        }
        result/=(double)size;
        return result;
    }
    /**
     * Returns the variencia given the differences, size of the array and
     * the mean value.
     * The returned value is an aproximation of sigma~s.
     * @param arr (double[]) The array containing each difference of each article. 
     * @param u (double) The mean value of the differences.
     * @param size (int) The size of the sample.
     * @return (double) Variencia.
     */
    private double getVariencia(double[] arr,double u,int size)
    {
        double result=0;
        for (int i=0;i<arr.length;i++)
        {
            result+=Math.pow(arr[i]-u,2);
        }
        result/=(double)(size-1);
        return result;
    }
    /**
     * Returns the result of an hypothesis test.
     * @param variencia (double) The varying value of the sample.
     * @param u (double) The mean value of the sample
     * @param significancia (double) The significancy degree. Lies between 0 and 1.
     * @param n (int) The sample size.
     * @return (double) The result.
     */
    private double getResultadoHipotese(double variencia,double u,double significancia, int n)
    {
        double result=0;
        if (significancia>1 || significancia<0)
        {
            throw new IllegalArgumentException("Valor do grau da significancia tem de estar compreendido entre 0 e 1!");
        }
        else
        {
            result = (u-1d)/(variencia/Math.sqrt(n));
        }
        return result;
    }
    /**
     * Sends an alert message to all the organizadores that have the
     * corresponding revisor. 
     * @return (boolean) True if all the alerts were sent.
     */
    public boolean enviarAlerta()
    {
        boolean result=true;
        List<Evento> le = reg.getListaEventosComRevisor(r);
        for (Evento focus:le)
        {
            focus.alertaOrganizadores(r,"O revisor deverá ser removido segundo os resultados recentes da análise estatística.");
        }
        return result;
    }

    /**
     * @see #showData() 
     * @return 
     */
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(r))+"\n";
    }
}
