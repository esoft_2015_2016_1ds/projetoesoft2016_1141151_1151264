/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import model.Empresa;
import model.Evento;
import model.SessaoTematica;
import registos.RegistoEventos;

/**
 *
 * @author Gonçalo
 */
public class GerarEstatisticasTopicosController {
    private Empresa emp;
    private static String filename = "estatisticaTopicos.csv";
    private File file;
    private RegistoEventos reg;
    public GerarEstatisticasTopicosController(Empresa emp){
        this.emp = emp;
        this.reg = emp.getRegistoEventos();
    }
    
    /**
     * This method generates a file containing statistics pertaining keywords in articles
     * @throws FileNotFoundException 
     */
    public void gerarStatsTopics() throws FileNotFoundException{
        file = new File(filename);
        Formatter out = new Formatter(file);
        ArrayList<Evento> eventos = reg.getListaEventosDecididos();
        ArrayList<SessaoTematica> sessoes = new ArrayList<SessaoTematica>();
        for(Evento e : eventos){
            sessoes.addAll(e.getListaSessoesDecididas());
        }
        ArrayList<String> acceptedTopics = new ArrayList<String>();
        ArrayList<String> rejectedTopics = new ArrayList<String>();
        
        for(Evento e: eventos){
            acceptedTopics = e.getAcceptedTopics(acceptedTopics);
            rejectedTopics = e.getRejectedTopics(rejectedTopics);
        }
        
        for(SessaoTematica st : sessoes){
            acceptedTopics = st.getAcceptedTopics(acceptedTopics);
            rejectedTopics = st.getRejectedTopics(rejectedTopics);
        }
        
        ArrayList<String[]> acceptedTopicsStats = new ArrayList<String[]>();
        ArrayList<String[]> rejectedTopicsStats = new ArrayList<String[]>();
        for(String s : acceptedTopics){
            String[] t = new String[3];
            t[0] = s;
            t[1] = 0+"";
            t[2] = 0+"";
            acceptedTopicsStats.add(t);
        }
        
        for(String s : rejectedTopics){
            String[] t = new String[3];
            t[0] = s;
            t[1] = 0+"";
            t[2] = 0+"";
            rejectedTopicsStats.add(t);
        }
        
        for(Evento e : eventos){
            acceptedTopicsStats  = e.getStatsTopics(acceptedTopicsStats);
            rejectedTopicsStats  = e.getStatsTopics(rejectedTopicsStats);
        }
        
        for(SessaoTematica e : sessoes){
            acceptedTopicsStats  = e.getStatsTopics(acceptedTopicsStats);
            rejectedTopicsStats  = e.getStatsTopics(rejectedTopicsStats);
        }
        
        out.format("Palavras-chave aceites\n");
        for(String[] s : acceptedTopicsStats){
            out.format(s[0]);
            out.format(";");
            double average = Integer.parseInt(s[1])/Double.parseDouble(s[2]);
            out.format(average +"\n");
        }
        out.format("\n");
        out.format("Palavras-chave rejeitadas\n");
        for(String[] s : rejectedTopicsStats){
            out.format(s[0]);
            out.format(";");
            double average = Integer.parseInt(s[1])/Double.parseDouble(s[2]);
            out.format(average +"\n");
        }
    }

    /**
     * @return the absolute path of of the file containing the topic statistics 
     */
    public String getFilePath(){
        return this.file.getAbsolutePath();
    }
}
