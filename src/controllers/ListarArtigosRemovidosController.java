package controllers;

import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Evento;
import model.Submissao;
import registos.RegistoEventos;

/**
 *
 * @author Gonçalo
 */
public class ListarArtigosRemovidosController implements iTOCS{
    private Evento escolhaEvento;
    private Submissivel escolhaSubmissivel;
    private RegistoEventos registo;
    
    /**
     * ListarArtigosRemovidosController constructor
     * @param registo the RegistoEventos to associate to the controller
     */
    public ListarArtigosRemovidosController(RegistoEventos registo){
        this.registo = registo;
    }

    /**
     * @param escolhaEvento the escolhaEvento to set
     */
    public void setEscolhaEvento(Evento escolhaEvento) {
        this.escolhaEvento = escolhaEvento;
    }
    
    /**
     * @param escolhaSubmissivel the escolhaSubmissivel to set
     */
    public void setEscolhaSubmissivel(Submissivel escolhaSubmissivel) {
        this.escolhaSubmissivel = escolhaSubmissivel;
    }
    
    /**
     * Needs testing
     * @param id the user to search for
     * @return a List containing an event and all its thematic sessions that have the user passed as parameter as a proponent
     */
    public List<Submissivel> getSessoesTematicas(String id){
        ArrayList<Submissivel> ls = new ArrayList<Submissivel>();
        ls.add(escolhaEvento);
        ls.addAll(escolhaEvento.getSessoesTematicasComProponenteRegistadoState(id));
        return ls;
    }
    
    /**
     * This method returns all the events of which the user is an organizer or that contain thematic sessions of which the user is a proponent
     * @param id  the identification(Email/username) of the user
     * @return all the events of which the user is an organizer or that contain thematic sessions of which the user is a proponent
     */
    public List<Evento> getEventos(String id){
        List<Evento> le = new ArrayList();
        le.addAll(registo.getEventosDeOrganizador(id));
        le.addAll(registo.getEventosComSTDeProponente(id));
        return le;
    }
    
    /**
     * needs testing
     * @return a list containin all the withdrawn submissions from a submissible
     */
    public List<Submissao> getSubmissoesRemovidas(){
        return escolhaSubmissivel.getSubmissoesRemovidas();
    }
   
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * Get of escolhaEvento for testing purposes
     * @return the eevent associated with this controller;
     */
    public Evento getEscolhaEvento(){
        return this.escolhaEvento;
    }
    
    /**
     * get of esccolhaSubmissivel for testing purposes
     * @return the submissible associated with this controller;
     */
    public Submissivel getEscolhaSubmissivel(){
        return this.escolhaSubmissivel;
    }
}
