/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Detetavel;
import interfaces.MecanismoDetecao;
import interfaces.iTOCS;
import model.Empresa;
import java.util.List;
import java.util.TimerTask;
import model.TipoConflito;
import registos.RegistoEventos;

/**
 *
 * @author Diogo
 */
public class DetetarConflitosController extends TimerTask implements iTOCS{
    private Empresa empresa;
    private RegistoEventos registoEventos;
    private Detetavel sub;
    private List<MecanismoDetecao> lmc;
    public DetetarConflitosController(Empresa empresa,Detetavel sub)
    {
        this.empresa=empresa;
        this.sub=sub;
        registoEventos=empresa.getRegistoEventos();
    }
    @Override
    public void run() {
        if (sub.setEmDetecao())
        {
            List<TipoConflito> rtc = empresa.getListaTipoConflitos();
            sub.detetarConflitos(rtc);
            if (!sub.setEmLicitacao())
            {
                throw new RuntimeException("Não foi possível iniciar a fase de licitação.");
            }
        }
        else
        {
            throw new RuntimeException("Não foi possível iniciar o processo de deteção.");
        }
    }
    /**
     * @see #showData() 
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
    
}
