/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import TimerTasks.AlterarParaAceitaSubmissoes;
import TimerTasks.AlterarParaEmCameraReady;
import TimerTasks.AlterarParaEmDecisao;
import TimerTasks.AlterarParaEmDistribuicao;
import interfaces.iTOCS;
import java.util.List;
import java.util.TimerTask;
import model.Empresa;
import model.Evento;
import model.SessaoTematica;
import model.Utilizador;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.Data;
import static utils.Utils.checkUsername;

/**
 *
 * @author jbraga
 */
public class DefinirSTController implements iTOCS {

    private RegistoUtilizadores registoUtilizadores;
    private RegistoEventos registoEventos;
    private Empresa empresa;
    private SessaoTematica st;
    private Evento e;

    /**
     * Constructor creates an instance of object {@link controllers.DefinirSTController
     * } with the company as parameter.
     *
     * @param emp {@link model.Empresa}
     */
    public DefinirSTController(Empresa emp) {
        empresa = emp;
        registoUtilizadores = empresa.getRegistoUtilizadores();
        registoEventos = empresa.getRegistoEventos();
    }

    /**
     * This method is used only for testing purposes. Constructor creates an
     * instance of object {@link controllers.DefinirSTController } with the user
     * registry, event registry and company.
     *
     * @param regU (@link registos.RegistoUtilizadores) the user registry
     * @param regE (@link registos.RegistoEventos) the event registry
     */
    public DefinirSTController(RegistoUtilizadores regU, RegistoEventos regE) {
        registoUtilizadores = regU;
        registoEventos = regE;

    }
    /**
     * This method selects the {@link model.Evento} chosen by the user.
     *
     * @param ev ({link model.Evento}) the Event chosen by the user.
     */
    public void escolheEvento(Evento ev) {
        e = ev;
    }

    public List<Evento> getListaEventosRegistadosOrganizador(String id) {
        return registoEventos.getListaEventosRegistadosOrganizador(id);
    }

    /**
     * This method creates a new {@link model.SessaoTematica} in
     * {@link states.sessaotematica.SessaoTematicaCriadoState}
     */
    public void novaSessaoTematica() {
        st = new SessaoTematica();
    }

    /**
     * This method sets the unique system code for the
     * {@link model.SessaoTematica}
     *
     * @param codigo (String) The unique system code of the
     * {@link model.SessaoTematica}
     */
    public void setCodigo(String codigo) {
        st.setCodigo(codigo);
    }

    /**
     * This method sets the description for the {@link model.SessaoTematica}
     *
     * @param descricao (String) The description of the
     * {@link model.SessaoTematica}
     */
    public void setDescricao(String descricao) {
        st.setDescricao(descricao);
    }

    /**
     * This method sets the initial date of the {@link model.SessaoTematica}
     *
     * @param di (Data) the initial date of the {@link model.SessaoTematica}
     */
    public void setDataInicio(Data di) {
        if(e.validaDataInicioST(di))
        {
            st.setDataInicio(di);
        }
    }

    /**
     * This method sets the final date of the {@link model.SessaoTematica}
     *
     * @param df (Data) the final date of the {link model.SessaoTematica}
     */
    public void setDataFim(Data df) {
        if(e.validaDataFimST(df) && df.isMaior(st.getDataInicio()))
        {
            st.setDataFim(df);
        }
        else
        {
            throw new IllegalArgumentException("Data de Fim da sessão deve ser posterior à sua de início");
        }
    }

    /**
     * This method sets the initial submisssion date of the
     * {@link model.SessaoTematica}
     *
     * @param dis (Data) the initial submission date of the {@link model.SessaoTematica}
     */
    public void setDataInicioSubmissao(Data dis) {
        if (e.validaDataInicioSubmissaoST(dis)) 
        {
            st.setDataInicioSubmissao(dis);
        }

    }

    /**
     * This method sets the final submisssion date of the
     * {@link model.SessaoTematica}
     *
     * @param dif (Data) the final submission date of the
     * {@link model.SessaoTematica}
     */
    public void setDataFimSubmissao(Data dif)  
    {
        if (e.validaDataFimSubmissaoST(dif) && dif.isMaior(st.getDataInicioSubmissao())) 
        {
            st.setDataFimSubmissao(dif);
        } else {
            throw new IllegalArgumentException("Data de Fim de Submissao da Sessão deve ser posterior à sua data de Inicio de Submissao");
        }
    }

    /**
     * This method sets the distribution date of the
     * {@link model.SessaoTematica}
     *
     * @param dd (Data) the distribution date of the
     * {@link model.SessaoTematica}
     */
    public void setDataInicioDistribuicao(Data dd) {
        if (e.validaDataInicioDistribuicaoST(dd)) {
            st.setDataInicioDistribuicao(dd);
        } else {
            throw new IllegalArgumentException("Data Invalida");
        }
    }

    /**
     * This method sets the borderline date of the final submisssion period of
     * the {@link model.SessaoTematica}
     *
     * @param dlsf (Data) the borderline date of the final submisssion period of
     * the {@link model.SessaoTematica}
     */
    public void setDataLimiteSubmissaoFinal(Data dlsf) {
        if (e.validaDataLimiteSubmissaoFinalST(dlsf)) 
        {
            st.setDataLimiteSubmissaoFinal(dlsf);
        }
    }

    /**
     * This method sets the borderline date of the final submissions period of
     * the {@link model.SessaoTematica}
     *
     * @param dlr (Data) the borderline date of the final submissions period of
     * the {@link model.SessaoTematica}
     */
    public void setDataLimiteRevisao(Data dlr) 
    {
        if (e.validaDataLimiteRevisaoST(dlr) && dlr.isMaior(st.getDataInicioDistribuicao())) {
            st.setDataLimiteRevisao(dlr);
        } 
        else 
        {
            throw new IllegalArgumentException("Data Limite de Revisão da Sessão deve ser posterior à sua data de Início de Distribuição !");
        }
    }

    /**
     * This method adds a {link model.Proponente} obtained by it's ID through
     * the user registry.
     *
     * @param id (String) The ID of the {@link model.Proponente}
     * @return (boolean) Returns true if the operation is sucessfull
     */
    public boolean addProponente(String id) {
        boolean result = false;
        if (registoUtilizadores.temUtilizadorEmail(id) || registoUtilizadores.temUtilizadorUsername(id)) {
            Utilizador u = registoUtilizadores.getUtilizador(id);
            if (st.validaProponente(u)) {
                result = st.addProponente(u);
            } else {
                throw new IllegalArgumentException("Proponente já existe na Sessão Temática!");
            }

        } else {
            throw new IllegalArgumentException("ID inválido!");
        }
        return result;
    }

    /**
     * This method validates a {@link model.SessaoTematica} and adds it in the
     * list of {@link model.SessaoTematica} that belongs to the
     * {@link model.Evento}
     *
     * @return (boolean) Returns true if the operation is sucessfull
     */
    public boolean registaSessaoTematica() {
        return e.registaSessaoTematica(st);
    }

    /**
     * This method returns a String containing the information of a
     * {@link model.SessaoTematica}
     *
     * @return (String) The information of the {@link model.SessaoTematica}
     */
    public String toString() {
        return st.toString();
    }

    /**
     * @see #showData()
     * @return
     */
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(st)) + "\n"
                +((st!=null) ? "SessaoTematica não nula" : "SessaoTematica nula") +"\n"
                +((st!=null) ? st.getCodigo() : "" )+"\n"
                +((st!=null) ? st.getDescricao(): "" )+"\n"
                +((st!=null) ? st.getDataInicio(): new Data())+"\n"
                +((st!=null) ? st.getDataFim(): new Data())+"\n"
                +((st!=null) ? st.getDataInicioSubmissao(): new Data())+"\n"
                +((st!=null) ? st.getDataFimSubmissao(): new Data())+"\n"
                +((st!=null) ? st.getDataInicioDistribuicao(): new Data())+"\n"
                +((st!=null) ? st.getDataLimiteSubmissaoFinal(): new Data())+"\n"
                +((st!=null) ? st.getDataLimiteRevisao(): new Data())+"\n";
                
    }

    /**
     * This method schedules a task to be executed on a certain date
     */
    public void createTimers(){
        AlterarParaAceitaSubmissoes paraAceitaSubmissoes = new AlterarParaAceitaSubmissoes(this.st);
        DetetarConflitosController uc14cntrl = new DetetarConflitosController(this.empresa,this.st ); 
        AlterarParaEmDistribuicao paraEmDistribuicao = new AlterarParaEmDistribuicao(this.st);
        AlterarParaEmDecisao paraEmDecisao = new AlterarParaEmDecisao(this.st);
        AlterarParaEmCameraReady paraEmCameraReady = new AlterarParaEmCameraReady(this.st);
        
        this.empresa.schedule(paraAceitaSubmissoes, this.st.getDataInicioSubmissao());
        this.empresa.schedule(uc14cntrl, this.st.getDataFimSubmissao());
        this.empresa.schedule(paraEmDistribuicao, this.st.getDataInicioDistribuicao());
        this.empresa.schedule(paraEmDecisao, this.st.getDataLimiteRevisao());
        this.empresa.schedule(paraEmCameraReady, this.st.getDataLimiteSubmissaoFinal());
    }
}
