/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Distribuivel;
import interfaces.MecanismoDistribuicao;
import interfaces.iTOCS;
import java.util.ArrayList;
import model.Empresa;
import model.ProcessoDistribuicao;
import java.util.List;
import model.Evento;
import model.Revisao;
import model.SessaoTematica;
import model.Submissao;
import registos.RegistoEventos;

/**
 *
 * @author Diogo
 */
public class DistribuiRevisoesController implements iTOCS{
    private Empresa empresa;
    private Evento ev;
    private RegistoEventos registoEventos;
    private Distribuivel distribuivel;
    private ProcessoDistribuicao pd;
    private List<Submissao> ls;
  
    
    /**
     * Constructor creates an instance of {@link controllers.DistribuiRevisoesController} with {@link model.Empresa} as parameter
     * @param empresa ({@link model.Empresa})
     */
    public DistribuiRevisoesController(Empresa empresa)
    {
        this.empresa=empresa;
    }
    /**
     * This method selects the {@link model.Evento} chosen by the user
     * @param event ({@link model.Evento}) the chosen event
     */
    public void selectEvento(Evento event)
    {
        ev=event;
    }
    /**
     * This method returns the selected Event
     * Used only for testing
     * @return {@link model.Evento}
     */
    public Evento getEvento()
    {
        return ev;
    }
        /**
     * This method selects the {@link interfaces.Distribuivel} chosen by the user
     * @param distribuive ({@link interfaces.Distribuivel}) the chosen Event/Thematic Session
     */
    public void selectDistribuivel(Distribuivel distribuive)
    {
        distribuivel=distribuive;
    }
    /**
     * This method returns the selected {@link interfaces.Distribuivel}
     * Used only for testing
     * @return {@link interfaces.Distribuivel}
     */
    public Distribuivel getDistribuivel()
    {
        return distribuivel;
    }
    /**
     * This method procures {@link model.Evento} within the {@link registos.RegistoEventos} that have the main event or the {@link model.SessaoTematica} in EmLicitacaoState and are associated to the user
     * @param id (String) The user ID
     * @return List &gt;Evento&lt; List of valid {@link model.Evento}
     */
    public List<Evento> getEventosDistribuiveis(String id)
    {
        return registoEventos.getEventosDistribuiveis(id);
    }
    /**
     * This method checks for the main event and {@link model.SessaoTematica} that are in  
     * @param id
     * @return 
     */
    public List<Distribuivel> getListaDistribuiveisEvento(String id)
    {
        List<Distribuivel> result = new ArrayList();
        List<SessaoTematica> ls = ev.getSessoesTematicasEmLicitacao(id);
        result.addAll(ls);
        if(ev.isInEmLicitacaoState())
        {
            result.add(ev);
        }
        return result;
                 
    }
    /**
     * This method procures the {@link model.Empresa} for {@link interfaces.MecanismoDistribuicao}
     * @return (List &gt;{@link interfaces.MecanismoDistribuicao}&lt;)
     */
    public List<MecanismoDistribuicao> getMecanismosDistribuicao()
    {
        return empresa.getMecanismosDistribuicao();
    }
    /**
     * This method instances a new {@link model.ProcessoDistribuicao}
     * @return ProcessoDistribuicao
     */
    public ProcessoDistribuicao novoProcessoDistribuicao()
    {
        pd = new ProcessoDistribuicao();
        return pd;
    }
    /**
     * This method returns the currently selected {@link model.ProcessoDecisao}
     * Only used for testing 
     * @return (@link model.ProcessoDecisao)
     */
    public ProcessoDistribuicao getProcessoDistribuicao()
    {
        return pd;
    }
    
    /**
     * This method procures the {@link model.SessaoTematica} within a {@link interfaces.Distribuivel}
     */
    public void getSubmissoes()
    {
        ls=distribuivel.getSubmissoes();
    }

    /**
     * This method sets the list of {@link model.Submissao}
     * Used only for testing
     * @param l (List&lt;{@link model.Submissao}&gt;)
     */
    public void setListaSubmissoes(List<Submissao> l)
    {
        ls=l;
    }
    /**
     * This method returns the current selected List of {@link model.Submissao}
     * Used only for testing
     * @return (List&lt;{@link model.Submissao}&gt;) 
     */
    public List<Submissao> getListaSubmissoes()
    {
        return ls;
    }
    /**
     * This method generates a list of {@link model.Revisao} within the {@link model.ProcessoDistribuicao}
     * @return List &gt;Revisao&lt; created by the {@link interfaces.MecanismoDistribuicao}
     */
    public List<Revisao> distribui()
    {
        return pd.distribui(ls);
    }
    /**
     * This method sets the {@link interfaces.MecanismoDistribuicao} chosen by the user
     * @param m {@link interfaces.MecanismoDistribuicao}
     */
    public void setMecanismoDistribuicao(MecanismoDistribuicao m)
    {
        pd.setMecanismoDistribuicao(m);
    }
    /**
     * This method registers the generated {@link model.ProcessoDistribuicao} and sets all the {@link model.Submissao} and the {@link interfaces.Distribuivel} to EmRevisaoState
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean registaDistribuicao()
    {        
        distribuivel.setProcessoDistribuicao(pd);
        distribuivel.setEmRevisao();
        for(Submissao focus : ls)
        {
           focus.setEmRevisao();
        }
        return true;       
    }
    
    @Override
    public String showData() {
        return "";
    }
}
