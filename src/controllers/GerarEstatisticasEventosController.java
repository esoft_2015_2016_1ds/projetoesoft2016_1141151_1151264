/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import java.util.List;
import model.Evento;
import registos.RegistoEventos;

/**
 *
 * @author AndreFilipeRamosViei
 */
public class GerarEstatisticasEventosController implements iTOCS {
    private RegistoEventos registoEventos;
    private Evento evento;
    private double taxaAceitacao;
    private double[] valoresMedios=new double[5];
    
    /**
     * Cria a instancia {@link controllers.GerarEstatisticasEventosController} com os parametros
     * especificados.
     * @param registo {@link registos.RegistoEventos} o registo de todos
     * os Eventos
     */
    public GerarEstatisticasEventosController(RegistoEventos registo){
        this.registoEventos=registo;
        this.evento=new Evento();
        
    }
    /**
     * Método que procura todos os Eventos de um Organizador que se encontram no estado de EmSubmissaoState 
     * @param id(String) o id do Utilizador
     * @return List&lt;{@link model.Evento}&gt; a lista de todos os eventos do
     * organizador
     */
    public List<Evento> getListaEventosOrganizadorEmSubmissaoCameraReady(String id){
        return registoEventos.getListaEventosOrganizadorEmSubmissaoCameraReady(id);
    }
    /**
     * Método que guarda a seleção do evento e que irá calcular a Taxa de Aceitação 
     * e os valores Médios atribuidos a cada parâmetro de avaliação
     * @param e {@link model.Evento} o evento selecionado
     */
    public void selectEvento(Evento e){
        evento=e;
        taxaAceitacao=evento.calcularTaxaAceitacao();
        valoresMedios=evento.calcularValoresMedios();
    }
    /**
     * Returns the controller's acceptance rate.
     * @return (int) The acceptance rate.
     */
    public double getTaxaAceitacao()
    {
        return taxaAceitacao;
    }
    /**
     * Returns the event's mean values of evaluation.
     * @return (double[]) The event's mean values.
     */
    public double[] getValoresMedios()
    {
        return valoresMedios;
    }
    
    public void setTaxaAceitacao(double taxa){
        taxaAceitacao=taxa;
    }
    
    public void setValoresMedios(double[] valores){
        valoresMedios=valores;
    }
 
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(evento))+"\n";
    }
}
