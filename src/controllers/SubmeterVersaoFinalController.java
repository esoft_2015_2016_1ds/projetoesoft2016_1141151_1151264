/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Artigo;
import model.Autor;
import model.Evento;
import model.Submissao;
import model.Utilizador;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 *  A class that is responsible for controlling every action related to the 
 * finalsubmission of an article.
 * 
 * @author Luis
 */

public class SubmeterVersaoFinalController implements iTOCS
{
    private RegistoEventos regE;
    private RegistoUtilizadores regU;
    private Evento e;
    private Submissao sub;
    private Submissivel subm;
    private Artigo artigoFinal;
    private Utilizador u;
    
    /**
     * This constructor creates an instance of object {@link controllers.SubmeterVersaoFinalController} with the specified parameters
     * @param rE {@link registos.RegistoEventos} The event registry
     * @param ator {@link model.Utilizador} The user of the use case
     * @param rU {@link registos.RegistoUtilizadores} The user registry
     */
    public SubmeterVersaoFinalController(RegistoEventos rE, RegistoUtilizadores rU, Utilizador ator)
    {
        regE = rE;
        regU = rU;
        u=ator;
    }
    /**
     * This constructor creates an instance of objetc {@link controllers.SubmeterVersaoFinalController} with all parameters filled
     * This method is used only for testing purposes
     * @param rE {@link registos.RegistoEventos} The event registry
     * @param rU {@link registos.RegistoUtilizadores} The user registry
     * @param ator {@link model.Utilizador} The user of the use case
     * @param ev {@link model.Evento} The Event chosen by the user
     * @param submissao {@link model.Submissao} The submissions chosen by the user
     * @param submissivel {@link interfaces.Submissivel} The Event/Thematic Session chosen by the user
     * @param aFinal {@link model.Artigo} The final article
     */
    public SubmeterVersaoFinalController(RegistoEventos rE, RegistoUtilizadores rU, Utilizador ator,Evento ev,Submissao submissao,Submissivel submissivel, Artigo aFinal)
    {
        regE=rE;
        regU=rU;
        u=ator;
        e=ev;
        sub=submissao;
        subm=submissivel;
        artigoFinal=aFinal;
        
    }
    /**
     * This method procures {@link model.Evento} that have {@link model.Submissao} that are in {@link states.submissao.SubmissaoAceiteState} made by the user
     * @param id (String) The ID if the user
     * @return List gt;{@link model.Evento}lt; A list of eligible {@link model.Evento}
     */
    public List<Evento> getListaEventosEmSubmissaoCR(String id)
    {
        return regE.getListaEventosEmSubmissaoCR(id);
    }
    
    /**
     * This method procures {@link interfaces.Submissivel} within the {@link model.Evento} conducted by the user that are in "EmSubmissaoCRState"
     * @param id The user's ID
     * @return List gt;{@link interfaces.Submissivel}lt; 
     */
    public List<Submissivel> getListaSubmissiveisEmSubmissaoCR(String id)
    {
       
        List<Submissivel> result = new ArrayList();
        if(e.temSubmissoesAceitesUtilizadorEventoPrincipal(id))
        {
            result.add(e);
        }
        result.addAll(e.getListaSessoesEmSubmissaoCR(id));
        return result;
    }
    /**
     * This method procures {@link model.Submissao} made by the ser in the {@link interfaces.Submissivel} that are in {@link states.submissao.SubmissaoAceiteState}
     * @param id (String) he user 
     * @return List (gt;{@link model.Submissao}lt;) A list of {@link model.Submissao} made by the ser in the {@link interfaces.Submissivel} that are in {@link states.submissao.SubmissaoAceiteState}
     */
    public List<Submissao> getListaSubmissoesAceites(String id)
    {
        return subm.getListaSubmissoesAceites(id);
    }
            
               
            /**
             * Selects the {@link model.Evento} chosen by the user
             * @param ev ({@link model.Evento}) 
             */
    public void selecionaEvento(Evento ev)
    {
        e=ev;
    }
    /**
     * Selects the {@link interfaces.Submissivel} chosen by the user
     * @param s {@link interfaces.Submissivel}
     */
    public void selecionaSubmissivel(Submissivel s)
    {
        subm=s;

    }
    /**
     * Selects the {@link model.Submissao} chosen by the user
     * @param s ({@link model.Submissao})
     */
    public void selecionaSubmissao(Submissao s)
    {
        sub=s;
        artigoFinal = sub.novoArtigo();
    }
    
    /**
     * Sets the article's data with the specified parameters.
     * @param title (String) The title's new value.
     * @param summary (String) The summary's new value.
     * @param palavrasChave (List gt;Stringlt;) The article's new set of keywords.
     * @param instituicao (String) The user's institution.
     */
    public void setArtigoData(String title,String summary,List<String> palavrasChave,String instituicao)
    {
        artigoFinal.setTitulo(title);
        artigoFinal.setResumo(summary);
        artigoFinal.setPalavrasChave(palavrasChave);
        artigoFinal.addUtilizadorComoAutor(u,instituicao);
    }
    
        /**
     * Creates a new author for the article with the specified parameters.
     * @param name (String) The author's name.
     * @param email (String) The author's email.
     * @param institution (String) The author's institution.
     * @param username (String) The author's username.
     * @return (Autor) The newly created author.
     */
    public Autor novoAutor(String name,String email,String institution,String username)
    {
        return artigoFinal.novoAutor(name, email, institution,username);
    }
    
    /**
     * Returns a list of all the authors that can meet the criteria to be the
     * article's corresponding author.
     * @return (List&lt;Autor&gt;) The list of possible corresponding authors.
     */
    public List<Autor> getPossiveisAutorCorrespondentes()
    {
        return artigoFinal.getPossiveisAutoresCorrespondentes(regU);
    }
    
     /**
     * Sets the corresponding author of the article.
     * @param author (Autor) The author to assign the role of corresponding author.
     */
    public void setAutorCorrespondente(Autor author)
    {
        Utilizador user = regU.getUtilizador(author.getEmail());
        if (user == null)
        {
            user = regU.getUtilizador(author.getUsername());
        }
        artigoFinal.setAutorCorrespondente(author, user);
    }
    
        /**
     * Sets the article's title.
     * @param title (String) The title's new value.
     */
    public void setArticleTitle(String title)
    {
        artigoFinal.setTitulo(title);
    }
    /**
     * Sets the article's summary.
     * @param summary (String) The summary's new value.
     */
    public void setArticleSummary(String summary)
    {
        artigoFinal.setResumo(summary);
    }
    
        /**
     * Returns the summarized information of an article in a text-format.
     * @return (String) The text-formatted information of the article.
     */
    public String getInfoResumo()
    {
        return artigoFinal.getInfo();
    }
    
     /**
     * Sets the article's file.
     * @param file (String) The article's new file.
     */
    public void setFile(String file)
    {
        artigoFinal.setFicheiro(file);
    }
    /**
     *  This method records the final article, sets the new state of both the {@link model.Submissao} and the {@link interfaces.Submissivel}
     */
    public boolean registaVersaoFinal()
    {
        boolean result = false;
           
            artigoFinal.setDataCriado(utils.Data.getDataAtual());
            artigoFinal.setAutorCriador(u);
            sub.setArtigoFinal(artigoFinal);
            result =  sub.setEmCameraReady();
            subm.setEmCameraReady();
        return result;
    }
    /**
     * 
     * @param autor ({@link model.Submissao}) 
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean addAutor(Autor autor)
    {
        return artigoFinal.addAutor(autor);
    }
        /**
     * @see #showData
     * @return 
     */
    @Override
    public String showData() {
        List<String> temp = artigoFinal.getPalavrasChave();
        String temp1 = "\n";
        for(String focus : temp)
        {
            temp1+=focus+"\n";
        }
        String result=temp1+"\n"+artigoFinal.getResumo()+"\n"+artigoFinal.getTitulo()+"\n"+System.identityHashCode(sub)+"\n";
        result+=((artigoFinal.showData().split("\n").length!=0) ? artigoFinal.showData().split("\n")[0]+"\n" : "\n");
        return result;
    }
   
    
}
