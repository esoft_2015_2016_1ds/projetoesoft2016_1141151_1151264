/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import model.CP;
import model.Empresa;
import model.Revisor;
import model.SessaoTematica;
import java.util.List;
import model.Utilizador;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 *
 * @author jbraga
 */
public class DefinirCPController implements iTOCS{

    private RegistoEventos registoEventos;
    private RegistoUtilizadores registoUtilizadores;
    private CP cp;
    private SessaoTematica sessaoTematica;
    private Revisor r;
    private Utilizador u;

    
    public DefinirCPController(Empresa empresa){
        registoEventos=empresa.getRegistoEventos();
    }
    
    /**
     * Cria a instancia {@link controllers.DefinirCPController} com os parametros:
     *
     * @param registoEventos {@link registos.RegistoEventos} o registo de todos
     * os Eventos
     * @param registoUtilizadores {@link registos.RegistoUtilizadores} o registo
     * de todos os Utilizadores
     */
    public DefinirCPController(RegistoEventos registoEventos, RegistoUtilizadores registoUtilizadores) {
        this.registoEventos = registoEventos;
        this.registoUtilizadores = registoUtilizadores;
        this.cp=new CP();
        this.sessaoTematica=new SessaoTematica();
        this.r=new Revisor();
        this.u=new Utilizador();
    }

    /**
     * Método que guarda a Sessão Temática selecionada e cria uma nova comissão
     * de programa
     *
     * @param st{@link model.SessaoTematica} selecionada
     */
    public void selectSessaoTematica(SessaoTematica st) {
        sessaoTematica = st;
        cp = sessaoTematica.novaCP();
    }

    /**
     * Método que cria uma nova CP
     */
    public void novaCP() {
        cp = new CP();
    }

    /**
     * Método que adiciona um Organizador a uma CP
     *
     * @param id(String) do organizador
     * @return {@link model.Revisor} se o utilizador com o dado id existir este é adicionado á CP passando a ser um Revisor, senão retorna null
     */
    public boolean addMembroCP(String id) {
        Utilizador u = registoUtilizadores.getUtilizador(id);

        if (u != null) {
            return cp.addMembroCP(u);
        } else {
            return false;
        }
    }

    /**
     * Método que adiciona um Revisor a uma CP
     *
     * @param r{@link model.Revisor}
     * @return {@link model.Revisor} membro adicionado
     */
    public boolean addMembroCP(Revisor r) {
        return cp.addMembroCP(r);
    }

    /**
     * Método que regista um Revisor a uma CP
     *
     * @param r{@link model.Revisor}
     * @return(Boolean) membro adicionado
     */
    public boolean registaMembroCP(Revisor r) {
        return cp.registaMembroCP(r);
    }

    /**
     * Atribui o novo valor da CP do Evento
     */
    public boolean setCP() {
        return sessaoTematica.setCP(cp);
    }

    /**
     * Metodo que busca os Eventos de um dado Utilizador no seu registo
     *
     * @param id(String) do Utilizador
     * @return List&lt;{@link SessaoTematica}&gt; 
     */
    public List<SessaoTematica> getSessoesTematicasProponenteEmEstadoRegistado(String id) {
        return registoEventos.getSessoesTematicasProponenteEmEstadoRegistado(id);
    }

    /**
     * Creates a new revisor to the CP.
     * @param id (String) Organizador's id.
     * @return ({@link model.Revisor}) The newly created revisor.
     */
    public Revisor novoMembroCP(String id) {
        u = registoUtilizadores.getUtilizador(id);
        if (u!=null)
        {
            r = cp.novoMembroCP(u);
            return r;
        }
        return null;
    }
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(sessaoTematica))+"\n"+((cp!=null) ? "CP não nula" : "CP nula") + "\n";
    }
}
