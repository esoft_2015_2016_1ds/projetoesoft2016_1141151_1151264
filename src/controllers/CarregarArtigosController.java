/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import model.Evento;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 *
 * @author Diogo
 */
public class CarregarArtigosController {
    private RegistoEventos reg;
    private RegistoUtilizadores regU;
    private Evento ev;
    private File file;
    private File erroFich;
    /**
     * Creates an instance of this object with the specified parameters.
     * @param regE ({@link registos.RegistoEventos}) The company's event registry.
     * @param regU({@link registos.RegistoUtilizadores}) The company's user registry.
     */
    public CarregarArtigosController(RegistoEventos regE,RegistoUtilizadores regU)
    {
        reg=regE;
        this.regU=regU;
    }
    /**
     * Returns a list of events, in the company's event registry, that are accepting
     * submissions.
     * @return (List&lt;{@link model.Evento}&gt;) The list of events that are accepting
     * submissions.
     */
    public List<Evento> getEventosAceitaSubmissoes()
    {
        return reg.getEventosAceitaSubmissoes();
    }
    /**
     * Selects the event to be used for the rest of the use case.
     * @param ev ({@link model.Evento}) The target event.
     */
    public void selectEvento(Evento ev)
    {
        this.ev=ev;
    }
    /**
     * Selects the file to be used for the rest of the use case.
     * @param file (File) The target file.
     */
    public void selectFicheiro(File file)
    {
        this.file=file;
    }
    /**
     * Generates the submissions based on the chosen file.
     * @return (boolean) True if no errors were found whilst loading.
     */
    public boolean geraSubmissoes() throws IOException
    {
        erroFich = new File("errosSubmissoesGeradas.txt");
        return ev.geraSubmissoesDeFicheiro(file,erroFich,regU);
    }
    /**
     * Returns the location of the error file.
     * @return (String) The absolute path of the file.
     */
    public String getLocalizacaoFicheiroErros()
    {
        return erroFich.getAbsolutePath();
    }
}
