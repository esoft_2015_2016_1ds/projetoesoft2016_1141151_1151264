/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.iTOCS;
import model.Empresa;
import model.TipoConflito;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class DefinirTipoConflitoController implements iTOCS{
    private Empresa empresa;
    private TipoConflito tipoConflito;
    public DefinirTipoConflitoController(Empresa empresa)
    {
        this.empresa=empresa;
    }
    public void setDados(String name,String descricao)
    {
        tipoConflito.setName(name);
        tipoConflito.setDescricao(descricao);
    }
    public void novoTipoConflito()
    {
        tipoConflito = empresa.novoTipoConflito();
    }
    public boolean registarTipoConflito()
    {
        if (tipoConflito.valida() && 
        empresa.valida(tipoConflito))
        {
            return empresa.addTipoConflito(tipoConflito);
        }
        return false;
    }
    /**
     * Returns the company's list of type of conflicts.
     * @return (List&lt;{@link model.TipoConflito}&gt;) The company's list of
     * type of conflicts.
     */
    public List<TipoConflito> getListaTipoConflitos()
    {
        return empresa.getListaTipoConflitos();
    }
    /**
     * @see #showData() 
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * Get of conflito
     * @return the associated conflict Type
     */
    public TipoConflito getConflito(){
        return this.tipoConflito;
    }
}
