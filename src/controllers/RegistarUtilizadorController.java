package controllers;

import interfaces.iTOCS;
import model.Empresa;
import model.Utilizador;
import registos.RegistoUtilizadores;
import utils.Coder;

/**
 *
 * @author Nuno Silva
 */

public class RegistarUtilizadorController implements iTOCS
{
    private RegistoUtilizadores registoUtilizadores;
    private Empresa e;
    private Utilizador m_utilizador;

    public RegistarUtilizadorController(Empresa e)
    {
        this.e = e;
    }
    
    /**
     * This method creates a new utilizador and sets the registoUtilizadores
     */
    public void novoUtilizador()
    {
        registoUtilizadores = e.getRegistoUtilizadores();
        m_utilizador = registoUtilizadores.novoUtilizador();
    }
    
    /**
     * This method sets all the atributes of the Utilizador Object
     * @param strUsername the username of the user
     * @param strPassword the password of the user
     * @param strNome the name of the user
     * @param strEmail the email of the user
     */
    public void setDados(String strUsername, String strPassword, String strNome, String strEmail)
    {   
        Coder tf = e.getRandomCoder();
        m_utilizador.setTabela(tf);
        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setName(strNome);
        m_utilizador.setEmail(strEmail);
        
    }
    
    /**
     * Tries to add a user to the company's user registry
     * @return true or false depending on the sucess of the operation(true if added)
     */
    public boolean adicionarUtilizador(){
        return this.registoUtilizadores.registaUtilizador(this.m_utilizador);
    }

    @Override
    public String showData() {
        return "";
    }
}

