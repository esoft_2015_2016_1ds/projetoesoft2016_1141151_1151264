package controllers;

import TimerTasks.*;
import interfaces.iTOCS;
import model.Empresa;
import model.Evento;
import model.Local;
import model.Utilizador;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;
import utils.Data;

/**
 *
 * @author Nuno Silva
 */
public class CriarEventoCientificoController implements iTOCS{
    
    private Empresa emp;
    private RegistoUtilizadores registoUtilizadores;
    private RegistoEventos registoEventos;
    private Evento m_evento;
    
    /**
     * The controller's constructor
     * @param e ({@link model.Empresa}) The company.
     */
    public CriarEventoCientificoController(Empresa e) 
    {
        emp=e;
        this.registoUtilizadores = emp.getRegistoUtilizadores();
        this.registoEventos = emp.getRegistoEventos();
    }
    
    /**
     * This method creates a new event
     */
    public void novoEvento() {
        m_evento = registoEventos.novoEvento();
    }
    
    /**
     * This method returns the toString of an event
     * @return the toString of an event
     */
    public String getEventoString() {
        return m_evento.toString();
    }
    
    /**
     * This method sets the event title
     * @param strTitulo 
     */
    public void setTitulo(String strTitulo) {
        m_evento.setTitulo(strTitulo);
    }
    
    /**
     * 
     * @param strDescricao the description to set in the event
     */
    public void setDescricao(String strDescricao) {
        m_evento.setDescricao(strDescricao);
    }
    
    /**
     * 
     * @param strLocal the local to set in the event
     */
    public void setLocal(String strLocal) {
        Local local = new Local();
        local.setLocal(strLocal);
        local.setId(("-1"));
        m_evento.setLocal(local);
    }
    
    /**
     * 
     * @param strDataInicio the start date to set in the event
     */
       public void setDataInicio(Data strDataInicio) {
        
        m_evento.setdataInicio(strDataInicio);
    }
       
    /**
     * 
     * @param strDataFim the end date to set in the event
     */
    public void setDataFim(Data strDataFim) {
        
        m_evento.setdataFim(strDataFim);
    }
    
    /**
     * 
     * @param dataInicioSubmissao the start date of the submission period to set in the event
     */
    public void setDataInicioSubmissao(Data dataInicioSubmissao) {
        
       m_evento.setdataInicioSubmissao(dataInicioSubmissao);
    }
    
    /**
     * 
     * @param dataFimSubmissao the end date of the submission period to set in the event
     */
    public void setDataFimSubmissao(Data dataFimSubmissao) {
        
        m_evento.setdataFimSubmissao(dataFimSubmissao);
        
    }
    
    /**
     * 
     * @param dataInicioDistribuicao the start date of the distribuiton periodo to set in the event
     */
    public void setDataInicioDistribuicao(Data dataInicioDistribuicao) {
        
        m_evento.setdataInicioDistribuicao(dataInicioDistribuicao);
    }
    
    /**
     * 
     * @param dataLimiteRevisao the end date of the distribuitoon period to set in the event
     */
    public void setDataLimiteRevisao(Data dataLimiteRevisao){
        
        m_evento.setdataLimiteRevisao(dataLimiteRevisao);
    }
    
    /**
     * 
     * @param dataSubmissaoFinal the end date of the final submission period of the event
     */
    public void setDataLimiteSubmissaoFinal(Data dataSubmissaoFinal){
        
        m_evento.setDataLimiteSubmissaoFinal(dataSubmissaoFinal);
    }
    
    /**
     * This method adds an organizer to the event
     * @param strId the email or username of the user to associate to the organizer
     * @return true or false depending on the sucess of the operation(true = positive)
     */
    public boolean addOrganizador(String strId) {
        Utilizador u = registoUtilizadores.getUtilizador(strId);

        if (u != null) {
            return m_evento.addOrganizador(u);
        } else {
            return false;
        }
    }
    
    /**
     * This method adds an event to the registry
     * @return true or false depending on the sucess of the operation(true = positive)
     */
    public boolean registaEvento() {
        if(registoEventos.registaEvento(m_evento)){
            this.m_evento.setRegistado();
            return true;
        }
        return false;
    }
    
    /**
     * This method shows the information of the class
     * @return a string containing the information of the class
     */
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * This method schedules a task to be executed on a certain date
     */
    public void createTimers(){
        AlterarParaAceitaSubmissoes paraAceitaSubmissoes = new AlterarParaAceitaSubmissoes(this.m_evento);
        DetetarConflitosController uc14cntrl = new DetetarConflitosController(this.emp,this.m_evento ); 
        AlterarParaEmDistribuicao paraEmDistribuicao = new AlterarParaEmDistribuicao(this.m_evento);
        AlterarParaEmDecisao paraEmDecisao = new AlterarParaEmDecisao(this.m_evento);
        AlterarParaEmCameraReady paraEmCameraReady = new AlterarParaEmCameraReady(this.m_evento);
        
        this.emp.schedule(paraAceitaSubmissoes, this.m_evento.getDataInicioSubmissao());
        this.emp.schedule(uc14cntrl, this.m_evento.getDataFimSubmissao());
        this.emp.schedule(paraEmDistribuicao, this.m_evento.getDataInicioDistribuicao());
        this.emp.schedule(paraEmDecisao, this.m_evento.getDataLimiteRevisao());
        this.emp.schedule(paraEmCameraReady, this.m_evento.getDataLimiteSubmissaoFinal());
    }
    
    /**
     * For Testing Purposes
     * @return the event associate to this controller
     */
    public Evento getEvento(){
        return this.m_evento;
    }
}
