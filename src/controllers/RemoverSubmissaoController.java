/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import model.Evento;
import model.Submissao;
import registos.RegistoEventos;
import registos.RegistoUtilizadores;

/**
 * Controller that coordinates the removal of a submission.
 * @author jbraga
 */
public class RemoverSubmissaoController implements iTOCS{
    private RegistoEventos registoEventos;
    private RegistoUtilizadores registoUtilizadores;
    private Submissivel sub;
    private Evento e;
    private Submissao s;
    /**
     * Creates a new instance of object {@link controllers.RemoverSubmissaoController} 
     * with the specified parameters.
     * @param regE ({@link registos.RegistoEventos}) The registry of events.
     * @param regU ({@link registos.RegistoUtilizadores}) The registry of users.
     */
    public RemoverSubmissaoController(RegistoEventos regE,RegistoUtilizadores regU)
    {
        registoEventos=regE;
        registoUtilizadores=regU;
    }
    /**
     * Returns a list of {@link model.Evento} that are not yet on the "Camera Ready"
     * state of a specified user.
     * @param id (String) The user's id.
     * @return (List&lt;{@link model.Evento}&gt;) The list of events.
     */
    public List<Evento> getListaEventosNaoCameraReadyDe(String id)
    {
        return registoEventos.getListaEventosNaoCameraReadyDe(id);
    }
    /**
     * Returns a list of {@link interfaces.Submissivel} that are not in "Camera Ready" state.
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of submissiveis.
     */
    public List<Submissivel> getListaSubmissiveisNaoCameraReadyDe(String id)
    {
        List<Submissivel> result = new ArrayList();
        if (e.temSubmissoesAutorExclusivo(id))
        {
            result.add(e);
        }
        List<Submissivel> content=e.getListaSubmissiveisNaoCameraReadyDe(id);
        if (content!=null && content.isEmpty())
        {
            result.addAll(content);
        }
        return result;
    }
    /**
     * Returns a list of submissions that are not in "Camera Ready" state 
     * from a user with the specified id.
     * @param id (String) The id of the user (either username or email).
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getSubmissoesNaoCameraReadyDe(String id)
    {
        return sub.getSubmissoesNaoCameraReadyDe(id);
    }
    public boolean setRemovida()
    {
        return s.setRemovida();
    }
    /**
     * Selects the chosen submission for the controller.
     * @param s ({@link model.Submissao}) The chosen submission.
     */
    public void selectSubmissao(Submissao s)
    {
        this.s=s;
    }
    /**
     * Selects the chosen event for the controller.
     * @param e ({@link model.Evento}) The chosen event.
     */
    public void selectEvento(Evento e)
    {
        this.e=e;
    }
    /**
     * Selects the chosen submissivel for the controller.
     * @param sub ({@link interfaces.Submissivel}) The chosen submissivel.
     */
    public void selectSubmissivel(Submissivel sub)
    {
        this.sub=sub;
    }
   
    @Override
    public String showData() {
        return String.valueOf(System.identityHashCode(s))+"\n"+
                String.valueOf(System.identityHashCode(e))+"\n"+
                String.valueOf(System.identityHashCode(sub))+"\n";
    }
}
