/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDecisao;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class ProcessoDecisao implements iTOCS{
    private MecanismoDecisao md;
    private List<Decisao> listaDecisoes;
    /**
     * Contructor creates an instance of {@link model.ProcessoDecisao} with no parameters
     */
    public ProcessoDecisao()
    {
        listaDecisoes=new ArrayList();
    }
    /**
     * This method returns a newly generated {@link model.Decisao}
     * @return ({@link model.Decisao})
     */
    public Decisao novaDecisao()
    {
        return new Decisao();
    }
    /**
     * Adds a new decision to this decision process.
     * This method should only be used when loading from a file.
     * @return ({@link model.Decisao}) The newly added decision.
     */
    public boolean addDecisaoUnchecked(Decisao d)
    {
        return listaDecisoes.add(d);
    }
    /**
     * This method sets the {@link interfaces.MecanismoDecisao} chosen by the user
     * @param md ({@link interfaces.MecanismoDecisao})
     */
    public void setMecanismoDecisao(MecanismoDecisao md)
    {
        this.md=md;
    }
    /**
     * This method returns the {@link interfaces.MecanismoDecisao}
     * @return {@link interfaces.MecanismoDecisao}
     */
    public MecanismoDecisao getMecanismoDecisao()
    {
        return md;
    }
    /**
     * The info of this {@link model.ProcessoDecisao}
     * @return (String)
     */
    public String getInfo()
    {
        return "";
    }
    /**
     * This method generates a list of {@link model.Decisao} using the selected {@link interfaces.MecanismoDecisao} and a (&gt;{@link model.Revisao}&lt;)
     * @param lr (&gt;{@link model.Revisao}&lt;)
     * @return (&gt;{@link model.Decisao}&lt;)
     */
    public List<Decisao> decide(List<Revisao> lr)
    {
        return md.decide();
        
    }

    /**
     * Checks if this decision process has any accepted submissions.
     * @return (boolean) True if at least one accepted submission has been found.
     */
    public boolean temSubmissoesAceites()
    {
        boolean result = false;
        for (Decisao focus:listaDecisoes)
        {
            if (focus.getVeredito())
            {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * @see #showData
     * @return 
     */
    @Override
    public String showData() 
    {
        return "";
    }
}
