package model;


import interfaces.MecanismoDecisao;
import interfaces.MecanismoDistribuicao;
import interfaces.iTOCS;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBException;
import registos.RegistoEventos;
import registos.RegistoTiposConflito;
import registos.RegistoUtilizadores;
import utils.Coder;
//import utils.CodificacaoAritmetica;
import utils.Data;
import utils.EventoXMLHandler;
import utils.xmlFigures.XMLFigureFactory;
import utils.xmlFigures.figures.LocalFigure;
import utils.xmlFigures.figures.UtilizadorFigure;
import utils.xmlFigures.loaders.LocalLoader;
import utils.xmlFigures.loaders.UtilizadorLoader;
import utils.xmlFigures.wrappers.LocalWrapper;
import utils.xmlFigures.wrappers.UtilizadorWrapper;

/**
 *
 * @author Nuno Silva
 */
public class Empresa implements iTOCS{

    private RegistoUtilizadores registoUtilizadores;
    private RegistoEventos registoEventos;
    private List<MecanismoDistribuicao> listaMecanismosDistribuicao;
    private RegistoTiposConflito listaTipoConflitos;
    private List<MecanismoDecisao> listaMecanismosDecisao;
    private List<Local> listaLocais;
    private Timer timer;
    private Utilizador loggedUser;
    private List<Administrador> adminList;

    /**
     * A lista de mecanismos de codificacao.
     */
    private List<Coder> mecanismosCodificacao;

    public Empresa() {
        mecanismosCodificacao = new ArrayList();
        adminList = new ArrayList();
        listaLocais=new ArrayList();
        List<Utilizador> uList=loadUtilizadores();
        List<Local> result = loadLocais();
        if (result!=null)
        {
            listaLocais.addAll(result);
        }
        loadMecanismosCodificacao();
        registoUtilizadores = new RegistoUtilizadores(uList);
        adminList.add(new Administrador(registoUtilizadores.getUtilizador("admin")));
        List<Evento> le = new ArrayList();
        try 
        {
            le.addAll(EventoXMLHandler.generateEventDataFromXML(new File("Eventos.xml"), registoUtilizadores, listaLocais));
        } 
        catch (Exception ex) 
        {
            JOptionPane.showMessageDialog(null,"Aconteceu um erro fatal inesperado ao ler os eventos ("+ex.getMessage()+").");
            ex.printStackTrace();
            System.exit(-1);
        }
        registoEventos = new RegistoEventos(registoUtilizadores,le);
        listaTipoConflitos = new RegistoTiposConflito();
        timer = new Timer();
        listaMecanismosDecisao = new ArrayList();
        listaMecanismosDistribuicao =new ArrayList();
    }
    /**
     * Loads the places of this company.
     */
    private List<Local> loadLocais()
    {
        try
        {
            LocalWrapper lw = new LocalWrapper();
            File file = new File("Locais.xml");
            LocalLoader loader = new LocalLoader();
            XMLFigureFactory.addClassLoader(loader);
            XMLFigureFactory.convertXMLtoFigure(lw, file);
            lw = (LocalWrapper)loader.getWrapper();
            return lw.convertFromXMLFigure();
        }
        catch (JAXBException e)
        {
            JOptionPane.showMessageDialog(null,"Aconteceu um erro fatal inesperado ao ler os utilizadores ("+e.getMessage()+").");
            System.exit(-1);
        }
        return null;
    }
    /**
     * This method sets the {@link registos.RegistoEventos}
     * Used for testing only
     * @param regE {@link registos.RegistoEventos}
     */
    public void setRegistoEventos(RegistoEventos regE)
    {
        registoEventos = regE;
    }
    /**
     * Testing purposes sets the user registry
     */
    public void setRegistoUtilizadores(RegistoUtilizadores reg){
        this.registoUtilizadores = reg;
    }
    /**
     * Loads the users of this company.
     */
    private List<Utilizador> loadUtilizadores()
    {
        try
        {
            UtilizadorWrapper uw = new UtilizadorWrapper(this);
            File file = new File("Utilizadores.xml");
            UtilizadorLoader loader = new UtilizadorLoader();
            XMLFigureFactory.addClassLoader(loader);
            XMLFigureFactory.convertXMLtoFigure(uw, file);
            uw = (UtilizadorWrapper)loader.getWrapper();
            uw.setEmpresa(this);
            return uw.convertFromXMLFigure();
        }
        catch (JAXBException e)
        {
            JOptionPane.showMessageDialog(null,"Aconteceu um erro fatal inesperado ao ler os utilizadores ("+e.getMessage()+").");
            System.exit(-1);
        }
        return null;
    }
    /**
     * Loads all the encoding mecanisms.
     */
    private void loadMecanismosCodificacao()
    {
        try
        {
            loadTabelas();
        }
        catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Aconteceu um erro fatal inesperado ao ler os mecanismos de codificacao ("+e.getMessage()+").");
            System.exit(0);
        }
    }
    /**
     * Loads the arithmetic coding tables.
     * @throws IOException In case there was a reading error.
     */
    private void loadTabelas() throws IOException
    {
        File file = new File("tabelas_CA.csv");
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        boolean ignoreHead=true;
        String data="";
        String temp;
        while ((temp=buffer.readLine())!=null)
        {
            if (!ignoreHead)
            {
                data+=temp+"\n";
            }
            else
            {
                ignoreHead=false;
            }
        }
        buffer.close();
        String[] tempChunk = data.split("\n");
        String[] chunk = new String[tempChunk.length-1];
        System.arraycopy(tempChunk, 0, chunk, 0, chunk.length);
        String[][] cache = new String[chunk.length][];
        for (int i=0;i<chunk.length;i++)
        {
            cache[i] = chunk[i].split(";");
        }
        double[][] tabelaFreq=new double[cache[0].length-2][257];
        for (int i=2;i<cache[0].length;i++)
        {
            for (int j=0;j<cache.length;j++)
            {
                if (cache[j].length>0)
                {
                    int symbol = cache[j][0].charAt(0);
                    tabelaFreq[i-2][symbol]=Double.parseDouble(cache[j][i].replace(",", "."));
                }
            }
        }
        int[][] finalFreqs = arrangeFrequencyTables(tabelaFreq);
        for (int i=0;i<finalFreqs.length;i++)
        {
            finalFreqs[i][256]++;
            //FrequencyTable freq = new SimpleFrequencyTable(finalFreqs[i]);
          //  CodificacaoAritmetica ac = new CodificacaoAritmetica(freq);
           // mecanismosCodificacao.add(ac);
        }
    }
    /**
     * Saves the data in this company to a file.
     */
    public void saveData()
    {
        saveUsers();
        saveLocais();
        saveEvents();
    }
    /**
     * Saves the events of this company.
     */
    private void saveEvents()
    {
        try
        {
            EventoXMLHandler.writeEventDataToXML(new File("Eventos.xml"), registoEventos.getListaEventos());
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,"An error occured whilst saving the events (data might have been lost). Error: "+e.getMessage()+"/"+e.getStackTrace()[2]);
            e.printStackTrace();
        }
    }
    /**
     * Saves the places of this company.
     */
    private void saveLocais()
    {
        try 
        {
            LocalWrapper uw = new LocalWrapper();
            List<LocalFigure> luf = new ArrayList();
            for (Local focus:listaLocais)
            {
                luf.add(new LocalFigure(focus));
            }
            uw.setListaLocais(luf);
            uw.setNumeroLocais(luf.size());
            XMLFigureFactory.convertFigureToXML(uw, new File("Locais.xml"));
        } 
        catch (JAXBException ex) 
        {
            JOptionPane.showMessageDialog(null,"Erro ao guardar os locais: "+ex.getMessage());
        }
    }
    /**
     * Saves the users in this company.
     */
    private void saveUsers()
    {
        try 
        {
            UtilizadorWrapper uw = new UtilizadorWrapper(this);
            List<UtilizadorFigure> luf = new ArrayList();
            for (Utilizador focus:registoUtilizadores.getLista())
            {
                luf.add(new UtilizadorFigure(focus,this));
            }
            uw.setListaUtilizadores(luf);
            uw.setNumeroUtilizadores(luf.size());
            XMLFigureFactory.convertFigureToXML(uw, new File("Utilizadores.xml"));
        } 
        catch (JAXBException ex) 
        {
            JOptionPane.showMessageDialog(null,"Erro ao guardar os utilizadores: "+ex.getMessage());
        }
    }
    /**
     * Cleans the frequency tables to be based upon frequency and not probability.
     * @param tabelaFreq (double[][]) A matrix with all the frequency tables that hold probabilities.
     * @return (int[][]) All the probability tables converted into frequency tables.
     */
    private int[][] arrangeFrequencyTables(double[][] tabelaFreq)
    {
        for (int i=0;i<tabelaFreq.length;i++)
        {
            while (hasDecimal(tabelaFreq[i]))
            {
                for (int j=0;j<tabelaFreq[i].length;j++)
                {
                    tabelaFreq[i][j]*=10;
                }
            }
        }
        int[][] result = convertDoubleToInt(tabelaFreq);
        return result;
    }
    /**
     * Converts a double matrix to an integer matrix.
     * @param arr (double[][]) The target matrix to convert.
     * @return (int[][]) The target matrix as int.
     */
    private int[][] convertDoubleToInt(double[][] arr)
    {
        int[][] result = new int[arr.length][];
        for (int i=0;i<result.length;i++)
        {
            result[i]=new int[arr[i].length];
            for (int j=0;j<result[i].length;j++)
            {
                result[i][j]=(int) arr[i][j];
            }
        }
        return result;
    }
    /**
     * Checks if an of doubles array still has decimal part.
     * @param arr (double[]) The array to check.
     * @return (boolean) True if there is a double with a decimal part.
     */
    private boolean hasDecimal(double[] arr)
    {
        boolean result=false;
        for (int i=0;i<arr.length && !result;i++)
        {
            result=(arr[i] % 1 != 0);
        }
        return result;
    }
    public TipoConflito novoTipoConflito() {
        return new TipoConflito();
    }

    public List<MecanismoDecisao> getMecanismosDecisao() {
        return listaMecanismosDecisao;
    }

    /**
     * Returns the list of distribution mecanisms of this company.
     * @return (List&lt;{@link interfaces.MecanismoDistribuicao}&gt;) The list
     * of distribution mecanisms.
     */
    public List<MecanismoDistribuicao> getMecanismosDistribuicao() {
        return new ArrayList(listaMecanismosDistribuicao);
    }

    /**
     * Returns the user registry of this company.
     *
     * @return a copy of Registo de Utilizadores
     */
    public RegistoUtilizadores getRegistoUtilizadores() {
        return registoUtilizadores;
    }
    
    /**
     * Returns the event registry of this company.
     * @return a copy of the event registry
     */
    public RegistoEventos getRegistoEventos() {
        return registoEventos;
    }
    /**
     * Checks if the target user is this admin.
     * @param u (Utilizador) The target user to check.
     * @return (boolean) True if user is the admin.
     */
    public boolean isAdmin(Utilizador u)
    {
        boolean result = false;
        for (Administrador a:adminList)
        {
            if (a.getUtilizador().getUsername().equals(u.getUsername()) || a.getUtilizador().getEmail().equals(u.getEmail()))
            {
                result = true;
                return result;
            }
        }
        return result;
    }
    /**
     * Sets the user that logged in.
     * @param u ({@link model.Utilizador})
     */
    public void setUtilizadorLogged(Utilizador u)
    {
        if (u!=null)
        {
            loggedUser=u;
        }
        else
        {
            throw new IllegalArgumentException("Utilizador não existe!");
        }
    }

    /**
     * Returns the user that has logged in.
     * @return (Utilizador) The logged in user.
     */
    public Utilizador getUtilizadorAutenticado()
    {
        return loggedUser;
    }
    /**
     * Adds a new type of conflict to this company's registry.
     * @param tipoConflito ({@link model.TipoConflito}) The new type of conflict to be added.
     * @return (boolean) True if successfully added.
     */
    public boolean addTipoConflito(TipoConflito tipoConflito) {
        return listaTipoConflitos.addTipoConflito(tipoConflito);
    }
    /**
     * Checks if the specified type of conflict already exists or not in this
     * registry.
     * @param tipoConflito ({@link model.TipoConflito}) The target type of conflict.
     * @return 
     */
    public boolean valida(TipoConflito tipoConflito) {
        return listaTipoConflitos.valida(tipoConflito);
    }
    /**
     * Returns the company's list of type of conflicts.
     * @return (List&lt;{@link model.TipoConflito}&gt;) The company's list of
     * type of conflicts.
     */
    public List<TipoConflito> getListaTipoConflitos()
    {
        return listaTipoConflitos.getListaTiposConflito();
    }
    /**
     * @return uma copia de mecanismosCodificacao
     */
    public List<Coder> getTabelasFreq() {
        List<Coder> tabelas = this.mecanismosCodificacao;
        return tabelas;
    }

    /**
     * @param mecanismosCodificacao the mecanismosCodificacao to set
     */
    public void setTabelasFreq(List<Coder> mecanismosCodificacao) {
        this.mecanismosCodificacao = mecanismosCodificacao;
    }

    /**
     * Retorna uma tabela de frequencia aleatoria da lista de tabelas da empresa
     *
     * @return a tabela de frequencia.
     */
    public Coder getRandomCoder() {
        Random ran = new Random();
        int i = ran.nextInt(this.mecanismosCodificacao.size());
        Coder tf = this.mecanismosCodificacao.get(i);
        return tf;
    }

    @Override
    public String showData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * This method schedules a task tt to be executed in the date d
     * @param tt teh task
     * @param d the date
     */
    public void schedule(TimerTask tt, Data d){
        this.timer.schedule(tt, d.toDate());
    }
}
