package model;

import interfaces.iTOCS;
import java.io.Serializable;
import utils.Coder;
import utils.Utils;
import static utils.Utils.checkEmail;
import static utils.Utils.checkName;
import static utils.Utils.checkPassword;
import static utils.Utils.checkUsername;

/**
 * A class that represents a user with an email, username, name and a password.
 *
 * @author jbraga
 */
public class Utilizador implements Serializable, iTOCS {

    private String nome;
    private String username;
    private String password;
    private String email;
    private Coder tabela;

    /**
     * Creates an instance of object {@link model.Utilizador} with null
     * parameters.
     */
    public Utilizador() {
    }

    /**
     * Creates an instance of object {@link model.Utilizador} with the specified
     * parameters.
     *
     * @param name (String) The name of the user.
     * @param userName (String) The username of the user.
     * @param password (String) The password of the user.
     * @param email (String) The email of the user.
     * @param tabela
     */
    public Utilizador(String name, String userName, String password, String email, Coder tabela) {
        this.tabela = tabela;
        setName(name);
        setUsername(userName);
        setPassword(password);
        setEmail(email);
    }

    /**
     * Creates a copy of an instance of object {@link model.Utilizador}.
     *
     * @param origin ({@link model.Utilizador}) The target user to copy.
     */
    public Utilizador(Utilizador origin) {
        this(origin.getName(), origin.getUsername(), origin.getPassword(), origin.getEmail(), origin.getTabela());
    }

    /**
     * Returns the user's name.
     *
     * @return (String) The user's name.
     */
    public String getName() {
        return nome;
    }

    /**
     * Returns the user's username.
     *
     * @return (String) The user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the user's password.
     *
     * @return (String) The user's password.
     */
    public String getPassword() {
        if (this.password == null) {
            return null;
        } else {
            return this.tabela.decode(password);
        }
    }

    /**
     * Returns the user's email.
     *
     * @return (String) The user's email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the name of the user. Throws an IllegalArgumentException in case the
     * data to be assigned is invalid.
     *
     * @param strNome (String) The new name of the user.
     */
    public void setName(String strNome) {
        if (strNome.length() < 2) {
            throw new IllegalArgumentException("Nome precisa de ter pelo menos 2 caracteres", new Throwable("Invalid length"));
        } else if (Utils.countElements(strNome, Utils.COUNT_LETTERS) == 0) {
            throw new IllegalArgumentException("Nome tem de conter letras!", new Throwable("Invalid"));
        } else {
            nome = strNome;
        }
    }

    /**
     * Sets the username of the user. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     *
     * @param strUtilizadorname (String) The new username of the user.
     */
    public void setUsername(String strUtilizadorname) {
        if (strUtilizadorname.length() <= 2) {
            throw new IllegalArgumentException("Utilizadorname precisa de ter pelo menos 3 caracteres", new Throwable("Invalid length."));
        } else {
            username = strUtilizadorname;
        }
    }

    /**
     * Sets the password of the user. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     *
     * @param strPassword (String) The new password of the user.
     */
    public void setPassword(String strPassword) {
        if (strPassword.length() > 3) {
            if (Utils.countElements(strPassword, Utils.COUNT_LETTERS) >= 3) {
                this.password = this.tabela.encode(strPassword);
            } else {
                throw new IllegalArgumentException("Quantidade de letras insuficientes. Tem de ter pelo menos 3 letras!");
            }
        } else {
            throw new IllegalArgumentException("A password tem de conter pelo menos 6 caracteres!");
        }
    }

    /**
     * Sets the email of the user. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     *
     * @param strEmail (String) The new email of the user.
     */
    public void setEmail(String strEmail) {
        if (checkEmail(strEmail)) {
            email = strEmail;
        } else {
            throw new IllegalArgumentException("Email inválido!", new Throwable("Invalid Email"));
        }
    }

    /**
     * Sets the data of this user based off of another user's data.
     *
     * @param u ({@link model.Utilizador}) The target user to copy the data
     * from.
     */
    public void setDados(Utilizador u) {
        setTabela(u.getTabela());
        setUsername(u.getUsername());
        setName(u.getName());
        setPassword(u.getPassword());
        setEmail(u.getEmail());

    }

    /**
     * Validates this user's data.
     *
     * @return (boolean) True if valid.
     */
    public boolean valida() {
        return (checkEmail(email)) && (checkName(nome)) && (checkUsername(username)) && (checkPassword(getPassword()));
    }

    /**
     * Returns a text representation of the user.
     *
     * @return (String) Text form of user.
     */
    @Override
    public String toString() {
        return nome + "/" + username + "/" + email + "/" + password;
    }

    /**
     * Checks if this user is equal to another user or object.
     *
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other) {
        boolean result = (other != null) && (getClass() == other.getClass());
        if (result) {
            Utilizador temp = (Utilizador) other;
            if (this.email != null && this.nome != null && this.password != null && this.username != null
                    && temp.getEmail() != null && temp.getName() != null && temp.getPassword() != null
                    && temp.getUsername() != null) {
                result = true;
            } else {
                result = false;
            }

            if (result) {
                result = result && ((this == other) || (nome.equalsIgnoreCase(temp.getName())
                        && email.equals(temp.getEmail()) && username.equals(temp.getUsername())
                        && tabela.decode(password).equals(temp.getPassword())));
            } else {
                result = this.getName() == temp.getName() && this.getEmail() == temp.getEmail()
                        && this.getPassword() == temp.getPassword() && this.getUsername() == temp.getUsername();

            }
        }
        return result;
    }

    /**
     * @return the tabela
     */
    public Coder getTabela() {
        return tabela;
    }

    /**
     * @param tabela the tabela to set
     */
    public void setTabela(Coder tabela) {
        this.tabela = tabela;
    }

    @Override
    public String showData() {
        return "Nome " + getName() + " Mail " + getEmail() + " Username " + getUsername() + " Password " + getPassword();
    }
}
