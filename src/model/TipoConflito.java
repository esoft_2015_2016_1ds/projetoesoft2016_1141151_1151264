/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.MecanismoDetecao;
import interfaces.iTOCS;

/**
 *
 * @author Diogo
 */
public class TipoConflito implements iTOCS{
    private String nome;
    private String descricao;
    private MecanismoDetecao mcd;
    /**
     * Creates an instance of object {@link model.TipoConflito} with null parameters.
     */
    public TipoConflito()
    {
        this(null,null);
    }
    /**
     * Creates an instance of object {@link model.TipoConflito} with the specified parameters.
     * @param nome (String) The name of the type of conflict.
     * @param descricao (String) The description of the type of conflict.
     */
    public TipoConflito(String nome,String descricao)
    {
        setName(nome);
        setDescricao(descricao);
    }
    /**
     * Creates an instance of objec {@link model.TipoConflito} with the specified parameters.
     * @param nome (String) The name of the type of conflict.
     * @param descricao (String) The description of the type of conflict.
     * @param md ({@link interfaces.MecanismoDetecao}) The detection mecanism for this
     * type of conflict.
     */
    public TipoConflito(String nome,String descricao,MecanismoDetecao md)
    {
        setName(nome);
        setDescricao(descricao);
        setMecanismoDetecao(md);
    }
    public void setName(String value)
    {
        nome=value;
    }
    /**
     * Sets the detection mecanism for this type of conflict.
     * @param md ({@link interfaces.MecanismoDetecao}) The new detection mecanism.
     */
    public void setMecanismoDetecao(MecanismoDetecao md)
    {
        this.mcd=md;
    }
    public void setDescricao(String value)
    {
        descricao=value;
    }
    /**
     * Detects if the target {@link model.Revisor} has this specified type of conflict
     * with the target {@link model.Submissao}.
     * @param r ({@link model.Revisor}) The target revisor.
     * @param s ({@link model.Submissao}) The target submission.
     * @return (boolean) True if conflict detected.
     */
    public boolean detetarConflitosCom(Revisor r,Submissao s)
    {
        return mcd.deteta(r, s);
    }
    /**
     * This method validates the interest conflict type
     * @return true or false depending on whether or not the object passes the test.
     */
    public boolean valida()
    {
        return nome != null && descricao != null;
    }
    
    /**
     * This method checks if this object is equal to another object.
     * @param other (Object) The object to compare with.
     * @return (boolean) True if both objects are equal.
     */
    @Override
    public boolean equals(Object other){
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result && mcd!=null)
        {
            TipoConflito temp = (TipoConflito)other;
            result=(this==other) || nome.equals(temp.nome) 
                    && descricao.equals(temp.descricao) && mcd.equals(temp.mcd);
        }
        return result;
    }
    
    /**
     * Returns the name of this type of conflict.
     * @return (String) The name of the type of conflict.
     */
    public String getNome(){
        return nome;
    }
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * returns the description of the conflict type
     * @return the description of the conflict type
     */
    public String getDescricao(){
        return descricao;
    }
}
