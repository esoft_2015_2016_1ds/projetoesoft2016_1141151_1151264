/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;

/**
 *
 * @author Nuno Silva
 */

public class Local implements iTOCS
{
    /**
     * ID do local no sistema.
     */
    private String id;
    /**
     * String com o local a associar ao objecto Local
     */
    private String m_strLocal;
    
    /**
     * Creates an instance of object {@link model.Local} with null parameters..
     */
    public Local(){}
    
    /**
     * Creates an instance of object {@link model.Local} with the specified parameters.
     * @param local (String) The real place that this class represents.
     */
    public Local(String local){
        this.setLocal(local);
    }
    /**
     * Creates an instance of object {@link model.Local} with the specified parameters.
     * @param id (String) The unique id of this place in the system.
     * @param local (String) The real place that this class represents.
     */
    public Local(String id,String local)
    {
        this.id=id;
        setLocal(local);
    }
    /**
     * set local
     * @param strLocal a String com o local a definir
     */
    public void setLocal(String strLocal)
    {
        m_strLocal = strLocal;
    }
    
    /**
     * método toString de local
     * @return String descritiva de local
     */
    @Override
    public String toString()
    {
        return id+"/"+m_strLocal;
    }
    
    /**
     * get Local 
     * @return String com do local;
     */
    public String getLocal(){
        String str = m_strLocal;
        return str;
    }
    
    /**
     * Método para validar local
     * @return true ou false consoante o objecto passe ou nao no teste
     */
    public boolean valida(){
        return m_strLocal != null;
    }

    @Override
    public String showData() {
        return this.toString();
    }

    /**
     * Returns the id of this local.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id of this local.
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * Checks if this object is equal to another one.
     * @return (boolean) True if equal.
     */
    @Override
    public boolean equals(Object other)
    {
        boolean result = (other != null) && (getClass() == other.getClass());
        if (result) {
            Local temp = (Local) other;
            if (id != null && m_strLocal != null && temp.getId()!=null && temp.getLocal()!=null) {
                result = true;
            } else {
                result = false;
            }

            if (result) {
                result = result && ((this == other) || (id.equals(temp.getId()) && m_strLocal.equals(temp.getLocal())));
            } else {
                result = id==temp.getId() && id==temp.getLocal();

            }
        }
        return result;
    }
}
