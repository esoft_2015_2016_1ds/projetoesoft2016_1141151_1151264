/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;
import java.io.Serializable;

/**
 * A class that represents a corresponding author. The corresponding author
 * is a relation between a user and an author, therefore this class as an user and
 * an author.
 * @author Diogo
 */
public class AutorCorrespondente implements Serializable,iTOCS{
    private Utilizador utilizador;
    private Autor author;
    /**
     * Creates an instance of object {@link model.AutorCorrespondente} with null parameters.
     */
    public AutorCorrespondente()
    {
        utilizador=null;
        author=null;
    }
    /**
     * Creates an instance of object AutorCorrespondente with the specified parameters.
     * @param u ({@link model.Utilizador}) The user of this corresponding author.
     */
    public AutorCorrespondente(Utilizador u)
    {
        setUtilizador(u);
        author=null;
    }
    /**
     * Creates a copy of an instance of object {@link model.AutorCorrespondente}.
     * @param authorC ({@link model.AutorCorrespondente}) The target author corresponding to copy.
     */
    public AutorCorrespondente(AutorCorrespondente authorC)
    {
        this(authorC.getAutor(),authorC.getUtilizador());
    }
    /**
     * Creates an instance of object {@link model.AutorCorrespondente} with the specified parameters.
     * @param author ({@link model.Autor}) The author that this author corresponding represents.
     * @param u ({@link model.Utilizador}) The user that this author correspondign represents.
     */
    public AutorCorrespondente(Autor author,Utilizador u)
    {
        setAutor(author);
        setUtilizador(u);
    }
    /**
     * Sets the user of this corresponding author.
     * @param u ({@link model.Utilizador}) The new user.
     */
    public void setUtilizador(Utilizador u)
    {
        utilizador=u;
    }
    /**
     * Sets the author of this corresponding author.
     * @param author ({@link model.AutorCorrespondente}) The new author.
     */
    private void setAutor(Autor author)
    {
        this.author=author;
    }
    /**
     * Returns the author of this corresponding author.
     * @return ({@link model.Autor}) The author that this corresponding author represents.
     */
    public Autor getAutor()
    {
        return author;
    }
    /**
     * Returns a copy of the user of this corresponding author.
     * @return ({@link model.Utilizador}) The user that this corresponding author represents.
     */
    public Utilizador getUtilizador()
    {
        return (utilizador==null) ? null : new Utilizador(utilizador);
    }
    
    @Override
    public boolean equals(Object other)
    {
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            AutorCorrespondente temp = (AutorCorrespondente)other;
            if (temp.getUtilizador()==null || temp.getAutor()==null || utilizador==null || author==null)
            {
                result=false;
            }
            else
            {
                result=(this==other) || (temp.getUtilizador().equals(utilizador) && temp.getAutor().equals(author));
            }
        }
        return result;
    }
    
    @Override
    public String toString()
    {
        return "Autor: "+author+";Utilizador: "+utilizador;
    }

    @Override
    public String showData() {
        return "";
    }
}
