/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;

/**
 *
 * @author Diogo
 */
public class Proponente implements iTOCS{
    private String nome;
    private Utilizador utilizador;
    public Proponente(Utilizador utilizador)
    {
        nome=utilizador.getName();
        setUtilizador(utilizador);
    }
    public void setUtilizador(Utilizador utilizador)
    {
        this.utilizador=utilizador;
    }
    public boolean valida()
    {
        boolean result = true;
        if( (utilizador.getEmail()==null) || (utilizador.getName()==null) || (utilizador.getPassword()==null) || (utilizador.getUsername()==null))
        {
            result = false;
        }
        return result;
    }
    public String getNome()
    {
        return nome;
    }
    
    public Utilizador getUtilizador()
    {
        return utilizador;
    }

    @Override
    public String toString()
    {
        return utilizador.toString();
    }
    
    /**
     * 
     * @return the email of the associated user
     */
    public String getEmailUser(){
        return utilizador.getEmail();
    }
    
    /**
     * 
     * @return the username of the associated user 
     */
    public String getUsernameUser(){
        return utilizador.getUsername();
    }

    @Override
    public String showData() {
       return "";
    }
}
