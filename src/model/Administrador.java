/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Represents a administrator of the company.
 * @author jbraga
 */
public class Administrador {
    /**
     * The user that the admin corresponds to.
     */
    private Utilizador utilizador;
    /**
     * Creates an instance of object {@link model.Administrador} with the
     * specified parameters.
     * @param u (@link model.Utilizador) The user that this admin is associated with.
     */
    public Administrador(Utilizador u)
    {
        utilizador=u;
    }
    /**
     * Returns the user with this admin role.
     * @return ({@link model.Utilizador}) The admin's user account.
     */
    public Utilizador getUtilizador()
    {
        return utilizador;
    }
}
