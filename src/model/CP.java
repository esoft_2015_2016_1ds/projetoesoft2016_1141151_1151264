package model;

import interfaces.iTOCS;
import java.util.*;

/**
 * Classe que representa uma CP
 *
 * @author Nuno Silva
 */
public class CP implements iTOCS {

    private List<Revisor> listaRevisores;

    /**
     * Cria a instancia do objeto(@link model.CP) com os parametros nulos
     */
    public CP() {
        listaRevisores = new ArrayList();
    }
    /**
     * Cria a instancia do objeto(@link model.CP) com os parametros:
     * @param lista(List&lt;{@link model.Revisor}&gt;)  lista de revisores de uma CP
     */
    public CP(List<Revisor> lista)
    {
        listaRevisores=new ArrayList(lista);
    }
    /**
     * Returns the {@link model.Revisor} with the specified id.
     * @param id (String) The revisor's id.
     * @return (@link model.Revisor) The specified revisor (null if was found).
     */
    public Revisor getRevisor(String id) 
    {
        Revisor result = null;
        for (Revisor focus:listaRevisores)
        {
            if (focus.getUtilizador().getEmail().equals(id) || 
                    focus.getUtilizador().getUsername().equals(id))
            {
                result = focus;
                break;
            }
        }
        return result;
    }
    /**
     * Returns all the revisores from this {@link model.CP}.
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all the revisors.
     */
    public List<Revisor> getRevisores()
    {
        return new ArrayList(listaRevisores);
    }
    /**
     * Método que adiciona um Utilizador a uma CP
     *
     * @param u instacia de objeto Utilizador a adicionar
     * @return o novo revisor
     */
    public boolean addMembroCP(Utilizador u) {
        Revisor r = new Revisor(u);

        if (r.valida() && validaMembroCP(r)) {
            return listaRevisores.add(r);
        } else {
            return false;
        }
    }

    /**
     * Método que adiciona um Revisor a uma CP
     *
     * @param r ({@link model.Revisor}) a adicionar
     * @return o novo revisor
     */
    public boolean addMembroCP(Revisor r) {
        if (r.valida() && validaMembroCP(r)) {
            return listaRevisores.add(r);
        } else {
            return false;
        }
    }

    /**
     * Metodo que adiciona um revisor à lista de revisores
     *
     * @param r(Revisor) revisor a adicionar
     * @return
     */
    public boolean addRevisor(Revisor r) {
        return listaRevisores.add(r);
    }

    /**
     * Validação de um revisor
     *
     * @param r (Revisor) a validar
     * @return true se for validado, false se nao ficar validado
     */
    private boolean validaMembroCP(Revisor r) {
        boolean result = true;
        for (Revisor elemento : listaRevisores) {
            if (elemento.equals(r)) {
                result = false;
                break;
            }
        }
        return result;
    }
    /**
     * This methods verifies if this {@link model.CP} is empty.
     * @return(boolean) true se existirem menbros adicionados,false se não existirem
     */
    public boolean valida() 
    {
        return !listaRevisores.isEmpty();
    }
    /**
     * Método que retorna o tamanho da lista
     * @return(int) tamanho da lista
     */
    public int tamanhoListaRevisores(){
        return listaRevisores.size();
    }

    /**
    * Método que regista um Organizador a uma CP
    *
    * @param r (Revisor)
    * @return membro adicionado
    */
    public boolean registaMembroCP(Revisor r) {
        return listaRevisores.add(r);
    }
    /**
     * Metodo que verifica se um revisor pertence ou não à CP
     * @param id(String) a verificar
     * @return true se o membro pertence,false se membro não pertence
     */
    public boolean temMembro(String id) {
        boolean result = false;
        for (Revisor element : listaRevisores) {
            Utilizador u = element.getUtilizador();
            result = (u.getEmail().equals(id) || u.getUsername().equals(id));
            if (result) {
                break;
            }
        }
        return result;
    }
    /**
     *Versão textual desta classe  
    * @return 
    */
    @Override
        public String toString() {
        return "Membros CP: "+listaRevisores;
    }
    /**
    * Método que acrescenta um Utilizador à CP
    * @param u(Utilizador) a adicionar
    * @return o Utilizador adicionado
    */
    public Revisor novoMembroCP(Utilizador u) {
        return new Revisor(u);
    }

 
    @Override
    public String showData() {
        return "";
    }
    
}
