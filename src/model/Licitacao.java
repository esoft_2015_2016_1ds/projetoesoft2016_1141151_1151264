/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class Licitacao implements iTOCS{
    private Submissao artigo;
    private Revisor r;
    private int interesse;
    private List<TipoConflito> listaTiposConflito;
    /**
     * Creates an instance of object {@link model.Licitacao} with null parameters.
     */
    public Licitacao()
    {
        r=null;
        artigo=null;
        listaTiposConflito = new ArrayList();
    }
    /**
     * Creates an instance of object {@link model.Licitacao} with the specified parameters.
     * @param s ({@link model.Submissao}) The submission that has this bid's article.
     * @param r ({@link model.Revisor}) The revisor of this bid.
     * @param ltc (List&lt;{@link model.TipoConflito}&gt;) The bid's list of types of conflict.
     */
    public Licitacao(Submissao s,Revisor r,List<TipoConflito> ltc)
    {
        this.r=r;
        artigo = s;
        listaTiposConflito = ltc;
    }
    /**
     * Sets the interest of this {@link model.Licitacao}.
     * @param interesse (int) The new interest value.
     */
    public void setInteresse(int interesse)
    {
        if (interesse>=0 && interesse<=3)
        {
            this.interesse=interesse;
        }
        else
        {
            throw new IllegalArgumentException("O valor do interesse tem de estar compreendido entre 0 e 3 inclusive!");
        }
    }
    /**
     * Returns this bid's interest.
     * @return (int) The bid's interest.
     */
    public int getInteresse()
    {
        return interesse;
    }
    /**
     * Adds a new {@link model.TipoConflito} to the bid.
     * @param tc ({@link model.TipoConflito}) The conflict type to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addTipoConflito(TipoConflito tc)
    {
        boolean result = tc!=null;
        if (result)
        for (TipoConflito focus:listaTiposConflito)
        {
            if (focus.equals(tc))
            {
                result=false;
                break;
            }
        }
        if (result)
        {
            result=listaTiposConflito.add(tc);
        }
        return result;
    }
    /**
     * Checks if this bid is valid.
     * @return (boolean) True if valid.
     */
    public boolean valida()
    {
        return artigo!=null && r!=null && (interesse>=0) && (interesse<=3) && listaTiposConflito!=null;
    }
    /**
     * Checks if this {@link model.Licitacao} has the specified
     * revisor.
     * @param id (String) The id of the revisor.
     * @return (boolean) True if the revisor is present.
     */
    public boolean temRevisor(String id)
    {
        return r.getUtilizador().getEmail().equals(id) || 
                r.getUtilizador().getUsername().equals(id);
    }
 
    @Override
    public String showData() {
        return interesse+"\n";
    }
}
