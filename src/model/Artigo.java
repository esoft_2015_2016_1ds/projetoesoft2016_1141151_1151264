
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import listas.ListaAutores;
import registos.RegistoUtilizadores;
import utils.Data;
import utils.Utils;

/**
 * A class that represents an article with a title, summary (brief description of
 * what the article is about), a list of the authors of the article, the corresponding author
 * and the corresponding file that has the article.
 * @author jbraga
 */
public class Artigo implements iTOCS
{
    private String titulo;
    private String resumo;
    private ListaAutores listaAutores;
    private AutorCorrespondente autorCorrespondente;
    private Utilizador autorCriador;
    private String ficheiro;
    private Data dataSubmissao;
    private List<String> palavrasChave;
    public Artigo()
    {
        listaAutores = new ListaAutores();
    }
    /**
     * Creates a copy of an instance of object Artigo.
     * @param a (Artigo) The article to clone.
     */
    public Artigo(Artigo a)
    {
        listaAutores=new ListaAutores(a.getListaAutores());
        setResumo(a.getResumo());
        setTitulo(a.getTitulo());
        setFicheiro(a.getFicheiro());
        setAutorCorrespondente(a.getAutorCorrespondente());
        setAutorCriador(a.getAutorCriador());
        setDataCriado(a.getDataCriado());
        setPalavrasChave(a.getPalavrasChave());
    }
    
     /**
     * Creates an instance of object type {@code model.Artigo} with the specified 
     * parameters.
     * @param la ({@code listas.ListaAutores}) The authors list of this article.
     * @param titulo (String) The title of the article.
     * @param resumo (String) The summary of the article.
     * @param ficheiro (String) The file of the article.
     */
    public Artigo(ListaAutores la,String titulo,String resumo,String ficheiro)
    {
        listaAutores=new ListaAutores(la.getListaAutores());
        setResumo(resumo);
        setTitulo(titulo);
        setFicheiro(ficheiro);
    }
    
    /**
     * Creates an instance of object type {@code model.Artigo} with the specified 
     * parameters.
     * @param la ({@code listas.ListaAutores}) The authors list of this article.
     * @param titulo (String) The title of the article.
     * @param resumo (String) The summary of the article.
     * @param ficheiro (String) The file of the article.
     * @param autorC ({@code model.AutorCorrespondente}) The corresponding author
     * of this article.
     */
    public Artigo(ListaAutores la,String titulo,String resumo,String ficheiro,AutorCorrespondente autorC)
    {
        listaAutores=new ListaAutores(la.getListaAutores());
        setResumo(resumo);
        setTitulo(titulo);
        setFicheiro(ficheiro);
        setAutorCorrespondente(autorC);
    }
        /**
     * Creates an instance of object type {@code model.Artigo} with the specified 
     * parameters.
     * @param la ({@code listas.ListaAutores}) The authors list of this article.
     * @param titulo (String) The title of the article.
     * @param resumo (String) The summary of the article.
     * @param ficheiro (String) The file of the article.
     * @param autorC ({@code model.AutorCorrespondente}) The corresponding author
     * of this article.
     * @param pch (List&lt;String&gt;)  The new set of keywords
     */
    public Artigo(ListaAutores la,String titulo,String resumo,String ficheiro,AutorCorrespondente autorC,List<String> pch)
    {
        listaAutores=new ListaAutores(la.getListaAutores());
        setResumo(resumo);
        setTitulo(titulo);
        setFicheiro(ficheiro);
        setAutorCorrespondente(autorC);
        setPalavrasChave(pch);
    }
    /**
     * Sets the title of the article. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param strTitulo (String) The new title value.
     */
    public void setTitulo(String strTitulo)
    {
        if(strTitulo.length()>2)
        {
            if(Utils.countElements(strTitulo, Utils.COUNT_LETTERS)==0)
            {
                throw new IllegalArgumentException("Título tem de conter letras!", new Throwable("Invalid"));
            }
            else
            {
                titulo=strTitulo;
            }
        }
        else
        {
            throw new IllegalArgumentException("Título tem de conter pelo menos 3 caracteres", new Throwable("Length Invalid"));
        }
    }
    /**
     * Sets the summary of the article. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param strResumo (String) The new summary value. 
     */
    public void setResumo(String strResumo)
    {
        if(strResumo.length()>2)
        {
            if(Utils.countElements(strResumo, Utils.COUNT_LETTERS)==0)
            {
                throw new IllegalArgumentException("Resumo tem de conter letras!", new Throwable("Invalid"));
            }
            else
            {
                resumo=strResumo;
            }
        }
        else
        {
            throw new IllegalArgumentException("Resumo tem de conter pelo menos 3 caracteres", new Throwable("Length Invalid"));
        }
    }
    /**
     * Sets the authors' list of the article by copying the specified authors list.
     * Throws an IllegalArgumentException in case the data to be assigned is invalid.
     * @param autores (List&lt;Autor&gt;) The new authors' list.
     */
    public void setAutores(List<Autor> autores)
    {
        if(autores.isEmpty())
        {
            throw new IllegalArgumentException("Lista de autores importada está vazia!", new Throwable("Invalid Autor List"));
        }
        else
        {
            listaAutores=new ListaAutores(autores);
        }
    }
    /**
     * Sets the corresponding author of the article by copying the specified corresponding author. 
     * Throws an IllegalArgumentException in case the data to be assigned is invalid.
     * @param autor (AutorCorrespondente) The new corresponding author. 
     */
    public void setAutorCorrespondente(AutorCorrespondente autor)
    {
        if (listaAutores.temAutor(autor.getAutor().getEmail()))
        {
            autorCorrespondente=new AutorCorrespondente(autor);
        }
        else
        {
            throw new IllegalArgumentException("O autor correspondente não consta da lista de autores!");
        }
    }
    /**
     * This method returns the set of keywords of this article
     * @return List &gt;String&lt; The set of keywords
     */
    public List<String> getPalavrasChave()
    {
        return palavrasChave;
    }
        /**
     * This method changes the aricles set of keywords
     * @param plc List gt;Stringlt;  the new set of the articles keywords
     */
    public void setPalavrasChave(List<String> plc)
    {
        boolean found=false;
        for (String focus:plc)
        {
            
            if (focus!=null)
            {
                
                if (!focus.isEmpty())
                {
                    found=true;
                    break;
                }
            }
        }
        if (found)
        {
            if (plc.size()<=5)
            {
                palavrasChave=plc;
            }
            else
            {
                throw new IllegalArgumentException("Não pode ter mais do que 5 palavras chave!");
            }
        }
        else
        {
            throw new IllegalArgumentException("Tem de introduzir pelo menos uma palavra chave!");
        }
    }

    /**
     * Sets the corresponding author of the article. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param author (Autor) The author to be set as the corresponding author.
     * @param u (Utilizador) The user of the company that represents the author.
     */
    public void setAutorCorrespondente(Autor author,Utilizador u)
    {
        if (validaAutor(author))
        {
            if (u.getEmail().equals(author.getEmail()) || u.getUsername().equals(author.getUsername()))
            {
                autorCorrespondente=new AutorCorrespondente(author,u);
            }
            else
            {
                throw new IllegalArgumentException("O autor inserido não corresponde ao utilizador inserido!");
            }
        }
        else
        {
            throw new IllegalArgumentException("O autor selecionado como autor correspondente não consta da lista.");
        }
    }
    /**
     * Sets the file of the article. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param file (String) The new file of the article.
     */
    public void setFicheiro(String file)
    {
        if (Utils.getExtension(file).equals(Utils.pdf))
        {
            ficheiro = file;
        }
        else
        {
            throw new IllegalArgumentException("O ficheiro escolhido não se encontra no formato PDF");
        }
    }
    /**
     * Sets the user corresponding to the author that created this article.
     * @param u (Utilizador) The user that is creating the article's submission.
     */
    public void setAutorCriador(Utilizador u)
    {
        autorCriador=u;
    }
    /**
     * Returns the user that submitted this article.
     * @return ({@link model.Utilizador}) The user that submitted this article.
     */
    public Utilizador getAutorCriador()
    {
        return autorCriador;
    }
    /**
     * Sets the article's creation date.
     * @param value (Data) The new date value;
     */
    public void setDataCriado(Data value)
    {
        dataSubmissao=value;
    }
    /**
     * Returns the data when this article was submitted.
     * @return ({@link utils.Data}) The specified date.
     */
    public Data getDataCriado()
    {
        return dataSubmissao;
    }
    /**
     * Returns the article's title.
     * @return (String) The article's title.
     */
    public String getTitulo()
    {
        return titulo;
    }
    /**
     * Returns the article's summary.
     * @return (String) The article's summary.
     */
    public String getResumo()
    {
        return resumo;
    }
    /**
     * Returns a copy of this article's author list.
     * @return (List&lt;Autor&gt;) The article's author list.
     */
    public ArrayList<Autor> getListaAutores()
    {
        return listaAutores.getListaAutores();
    }
    /**
     * Returns the article's file.
     * @return (String) The article's file.
     */
    public String getFicheiro()
    {
        return ficheiro;
    }
    /**
     * Returns a copy of the article's corresponding author.
     * @return (AutorCorrespondente) The article's corresponding author.
     */
    public AutorCorrespondente getAutorCorrespondente()
    {
        return new AutorCorrespondente(autorCorrespondente);
    }
    /**
     * Creates a new author in the article with the specified parameters.
     * @param strNome (String) The author's name.
     * @param email (String) The author's email.
     * @param strAfiliacao (String) The author's institution.
     * @param username (String) The author's username.
     * @return (Autor) The newly created author.
     */
    public Autor novoAutor(String strNome, String email,String strAfiliacao,String username)
    {
        Autor autor = new Autor(strNome,email,strAfiliacao,username);
        return autor;
    }
    /**
     * Adds the specified author to the article's author list.
     * @param autor (Autor) The author to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addAutor(Autor autor)
    {
        return listaAutores.addAutor(autor);
    }
    /**
     * Adds the specified {@link model.Utilizador} as an author of this article's
     * author list.
     * @param u ({@link model.Utilizador}) The specified user to add. Usually, this user
     * is the user that is performing the submission of a new article.
     * @param instituicao (String) The user's institution.
     * @return (boolean) True if successfully added the new user.
     */
    public boolean addUtilizadorComoAutor(Utilizador u,String instituicao)
    {
        Autor temp = new Autor(u,instituicao);
        return listaAutores.addAutor(temp);
    }
    /**
     * Removes an author from the article's author list.
     * @param author ({@link model.Autor}) The target author to remove.
     */
    public void removeAutor(Autor author)
    {
        listaAutores.removeAutor(author);
    }
    /**
     * Checks to see if the author exists in the article's author list.
     * @param author (Autor) The author to check.
     * @return (boolean) True if exists, false if it doesn't.
     */
    private boolean validaAutor(Autor author) 
    {
        return listaAutores.validaAutor(author);
    }
    /**
     * Returns a list of the authors of the article that can be the corresponding
     * author.
     * @param userList ({@link registos.RegistoUtilizadores}) {@link model.Empresa}'s registry of users.
     * @return (List&lt;Autor&gt;) The list of authors that can be the corresponding author.
     */
    public List<Autor> getPossiveisAutoresCorrespondentes(RegistoUtilizadores userList)
    {
        return listaAutores.getPossiveisAutoresCorrespondentes(userList);
    }        
    /**
     * Returns the information of this article in a text format.
     * @return (String) The article's information.
     */        
    public String getInfo()
    {
        return this.toString();
    }
    /**
     * Returns the article's author list in a text format.
     * @return (String) The author list's textual form.
     */
    public String getListaAutorsAsString()
    {
        return listaAutores.toString();
    }
    /**
     * Checks if this article is a valid article.
     * @return (boolean) True if valid.
     */
    public boolean valida()
    {
       boolean result =(titulo!=null && resumo!=null && ficheiro!=null && autorCorrespondente!=null
               && listaAutores!=null);
       return result && (!titulo.isEmpty() && !resumo.isEmpty() 
                && !ficheiro.isEmpty()
                && !listaAutores.isEmpty());
    }
    /**
     * Checks if this article has the author with the specified
     * id.
     * @param id (String) The id of the author.
     * @return (boolean) True if article has the author.
     */
    public boolean temAutor(String id)
    {
        return listaAutores.temAutor(id);
    }
    /**
     * Returns the textual format of this article.
     * @return (String) Artigo's text form.
     */
    @Override
    public String toString()
    {
        return "Título: "+titulo+";Resumo: "+resumo+";Autor Correspondente: "+autorCorrespondente+";Ficheiro: "+ficheiro+";Lista de autores:"+getListaAutorsAsString();
    }
    /**
     * Checks if this article is equal to another article or object.
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other)
    {   
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            Artigo temp = (Artigo)other;
            
            result=(this==other) || (titulo.equalsIgnoreCase(temp.getTitulo()) 
                    && resumo.equalsIgnoreCase(temp.getResumo()) && listaAutores.equals(temp.getListaAutores())
                    && ficheiro.equals(temp.getFicheiro()));
        }
        return result;
    }

    @Override
    public String showData() {
        String result="";
        if (autorCorrespondente!=null && autorCorrespondente.getAutor()!=null)
        {
            result+=String.valueOf(System.identityHashCode(autorCorrespondente.getAutor()));
        }
        return result+"\n";
    }
    
    /**
     * This method returns all keywords associated with the article
     * @return all keywords associated with the article
     */
    public ArrayList<String> getTopicos(){
       ArrayList<String> topicos = new ArrayList<String>();
       topicos.addAll(this.palavrasChave);
       return topicos;
    }
}
