/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * This class represents an alert of a revisor with a certain message.
 * @author Diogo
 */
public class Alerta {
    private Revisor rev;
    private String message;
    
    /**
     * Creates an instance of object type {@link model.Alerta} with the specified
     * parameters.
     * @param r ({@link model.Revisor}) The target revisor.
     * @param message (String) The message about the target revisor.
     */
    public Alerta(Revisor r,String message)
    {
        this.rev=rev;
        this.message=message;
    }
    /**
     * Textual format of an alert.
     * @return (String) The text formatted message from the alert.
     */
    @Override
    public String toString()
    {
        return message;
    }
}
