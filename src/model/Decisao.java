/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;

/**
 *
 * @author Diogo
 */
public class Decisao implements iTOCS {

    private int classificacao;
    private boolean veredito;
    private Submissao submissao;

    /**
     * Creates an instance of object {@link model.Decisao} with null parameters.
     */
    public Decisao() {
        classificacao = 0;
    }

    /**
     * Creates an instance of object {@link model.Decisao} with the specified
     * parameters.
     *
     * @param classificacao (int) The classification of this decision.
     */
    public Decisao(int classificacao) 
    {
        setClassificacao(classificacao);
    }

    /**
     * Creates an instance of object {@link model.Decisao} with the specified
     * parameters.
     *
     * @param classificacao (int) The classification of this decision.
     * @param ver (boolean) The Veredict of the {@link model.Revisao}
     * @param sub ({@link model.Submissao}) The {@link model.Submissao} that was
     * decided
     */
    public Decisao(int classificacao, boolean ver, Submissao sub) {
        setClassificacao(classificacao);
        setVeredito(ver);
        setSubmissao(sub);
    }

    /**
     * Sets the {@link model.Submissao} of the decision
     *
     * @param sub ({@link model.Submissao}) The {@link model.Submissao} that was
     * decided
     */
    public void setSubmissao(Submissao sub) {
        submissao = sub;
    }

    /**
     * Sets the veredict of the decision.
     *
     * @param ver (boolean) True if it was accepted
     */
    public void setVeredito(boolean ver) {
        veredito = ver;
    }

    /**
     * Returns the veredict of the decision
     *
     * @return (boolean) Returns true if it was accepted
     */
    public boolean getVeredito() {
        return veredito;
    }

    /**
     * Returns the {@link model.Submissao} that was chosen
     *
     * @return ({@link model.Submissao}) The {@link model.Submissao} that was
     * decided
     */
    public Submissao getSubmissao() {
        return submissao;
    }

    /**
     * Sets the classification of the decision.
     *
     * @param value (int) The new value for the decision's classification.
     */
    public void setClassificacao(int value) {
        classificacao = value;
    }

    /**
     * Returns the classification of the decision.
     *
     * @return (int) The classification of the decision.
     */
    public int getClassificacao() {
        return classificacao;
    }

    @Override
    public String showData() {
        return "";
    }
}
