/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.Decidivel;
import interfaces.Detetavel;
import interfaces.Distribuivel;
import interfaces.EventoState;
import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.Submissivel;
import interfaces.iTOCS;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import listas.ListaAutores;
import listas.ListaSessoesTematicas;
import listas.ListaSubmissoes;
import registos.RegistoUtilizadores;
import states.evento.*;
import utils.Data;

/**
 * A class that represents an event. An event has a title, description, an
 * inicial date of submission, an end date of submission and a list of
 * submissions.
 *
 * @author jbraga
 */
public class Evento implements Submissivel, Licitavel, Revisivel, Decidivel, Detetavel,Distribuivel, iTOCS {

    private String titulo;
    private String descricao;
    private Local local;
    private Data dataInicio;
    private Data dataFim;
    private Data dataInicioSubmissao, dataFimSubmissao, dataInicioDistribuicao;
    private Data dataLimiteSubmissaoFinal;
    private Data dataLimiteRevisao;
    private List<Organizador> listaOrganizadores;
    private CP cp;
    private ProcessoDistribuicao processoDistribuicao;
    private ProcessoDecisao processoDecisao;
    private EventoState state;
    private ListaSessoesTematicas listaSessoesTematicas;
    private ListaSubmissoes listaSubmissoes;

    /**
     * Creates an instance of object {@link model.Evento} with
     * null parameters.
     */
    public Evento() {
        //this(new ArrayList(),null,null,null,null);
        local = new Local();
        listaOrganizadores = new ArrayList<Organizador>();
        listaSubmissoes = new ListaSubmissoes();
        listaSessoesTematicas = new ListaSessoesTematicas();
        processoDistribuicao = new ProcessoDistribuicao();
        state = new EventoCriadoState(this);
    }

    /**
     * Creates an instance of object {@link model.Evento} with
     * the specified parameters.
     *
     * @param lista (List&lt;{@link model.Organizador}&gt;) The event's
     * organization list containing all the people who are organizing this
     * event.
     * @param lst (List&lt;{@link model.SessaoTematica}&gt;) The event's
     * thematic session list containing all thematic sessions
     */
    public Evento(List<Organizador> lista, List<SessaoTematica> lst) {
        local = new Local();
        listaOrganizadores = new ArrayList(lista);
        listaSubmissoes = new ListaSubmissoes();
        listaSessoesTematicas = new ListaSessoesTematicas(lst);
        state = new EventoCriadoState(this);
    }

    /**
     * Creates an instance of object {@link model.Evento} with
     * the specified parameters.
     *
     * @param title (String) The event's title.
     * @param description (String) The event's description
     * @param di ({@link utils.Data}) The event's starting date.
     * @param df ({@link utils.Data}) The event's end date.
     * @param dIS ({@link utils.Data}) The event's starting date for submitting
     * articles.
     * @param dFS ({@link utils.Data}) The event's end date for submitting
     * articles.
     * @param dID ({@link utils.Data}) The event's starting date for article
     * distribution process to begin.
     * @param cp ({@link model.CP}) The event's CP (Comission Program/Comissão
     * de Programa).
     * @param organizadores (List&lt;{@link model.Organizador}&gt;) The event's
     * organization list containing all the people who are organizing this
     * event.
     * @param listSub (List&lt;{@link model.Submissao}&gt;) The event's list of
     * submissions.
     */
    public Evento(String title, String description, Data di, Data df, Data dIS, Data dFS, Data dID, CP cp, List<Organizador> organizadores, List<Submissao> listSub) {
        this(new ArrayList(), description, title, di, df, dIS, dFS, dID, cp, organizadores, listSub);
    }

    /**
     * Creates an instance of object {@link model.Evento} with
     * the specified parameters.
     *
     * @param reg (List&lt;SessaoTematica&gt;) A list containing the thematic
     * sessions of the event.
     * @param description (String) The event's title.
     * @param title (String) The event's title.
     * @param di ({@link utils.Data}) The event's starting date for submitting
     * articles.
     * @param df ({@link utils.Data}) The event's starting date for submitting
     * articles.
     * @param dIS ({@link utils.Data}) The event's starting date for submitting
     * articles.
     * @param dFS ({@link utils.Data}) The event's end date for submitting
     * articles.
     * @param dID ({@link utils.Data}) The event's starting date for article
     * distribution process to begin.
     * @param cp ({@link model.CP}) The event's CP (Comission Program/Comissão
     * de Programa).
     * @param organizadores (List&lt;{@link model.Organizador}&gt;) The event's
     * organization list containing all the people who are organizing this
     * event.
     * @param listSub (List&lt;{@link model.Submissao}&gt;) The event's list of
     * submissions.
     */
    public Evento(List<SessaoTematica> reg, String description, String title, Data di, Data df, Data dIS, Data dFS, Data dID, CP cp, List<Organizador> organizadores, List<Submissao> listSub) {
        //submissionList = new ArrayList();
        //thematicSessionList= new ArrayList(list);
        descricao = description;
        listaSessoesTematicas = new ListaSessoesTematicas(reg);
        setTitulo(title);
        setdataInicio(di);
        setdataInicioSubmissao(dIS);
        setdataFimSubmissao(dFS);
        
        setdataFim(df);
        setdataInicioDistribuicao(dID);
        this.cp = cp;
        listaOrganizadores = organizadores;
        listaSubmissoes = new ListaSubmissoes(listSub);
    }

    /**
     * Returns the description of the event.
     *
     * @return (String) The event's description.
     */
    public String getDescription() {
        return descricao;
    }

    /**
     * Returns the title of the event.
     *
     * @return (String) The event's title.
     */
    public String getTitle() {
        return titulo;
    }

    /**
     * Returns the starting date of submission. This date represents the date
     * from which a user can start submitting an article to this event.
     *
     * @return (Data) The starting date.
     */
    public Data getDataInicioSubmissao() {
        return new Data(dataInicioSubmissao);
    }

    /**
     * Returns the end date of submission. This date represents the date from
     * which a user can no longer submit an article to this event.
     *
     * @return (Data) The end date.
     */
    public Data getDataFimSubmissao() {
        return new Data(dataFimSubmissao);
    }
    /**
     * This method returns the limit date for the final {@link model.Submissao}
     * @return (Data) The limit date for the final {@link model.Submissao} 
     */
    @Override
    public Data getDataLimiteSubmissaoFinal()
    {
        return dataLimiteSubmissaoFinal;
    }
    /**
     * Returns the localization where this event will occur.
     * @return ({@link model.Local}) The localization of the event.
     */
    public Local getLocal()
    {
        return local;
    }
    /**
     * Returns a list of thematic sessions of this event.
     * @return (List&lt;{@link model.SessaoTematica}&gt;) The list of thematic
     * sessions of this event.
     */
    public List<SessaoTematica> getListaSessoesTematicas()
    {
        return listaSessoesTematicas.getListaSessoesTematicas();
    }
     /**
     * This methods verifies if a begin date of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param di ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the begin date {@link model.SessaoTematica} is valid
     */
    public boolean validaDataInicioST(Data di)
    {
        boolean result = false;
        if((di).isMaior(dataInicio) && dataFim.isMaior(di))
        {
            result=true;
        }
        else
        {
            throw new IllegalArgumentException("Data de Início da Sessão deve ser posterior à do Evento e anterior à sua data de fim(Evento)");
        }
        return result;
    }
        /**
     * This methods verifies if a end date of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param df ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the end date {@link model.SessaoTematica} is valid
     */
    public boolean validaDataFimST(Data df)
    {
        boolean result = false;
        if(dataFim.isMaior(df) && df.isMaior(dataInicio))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("Data de Fim da Sessão deve ser anterior à do Evento e posterior à de Início(do Evento)");
        }
        return result;
        
    }
            /**
     * This methods verifies if a distribution date of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param dd ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the distribution date {@link model.SessaoTematica} is valid
     */
    public boolean validaDataInicioDistribuicaoST(Data dd)
    {
        boolean result = false;
        if(dataInicioDistribuicao.isMaior(dd) && dataLimiteSubmissaoFinal.isMaior(dd))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("A data de Início de Distribuição da Sessão deve ser posterior à do evento e anterior à data Limite da Submissao Final");
        }
        return result;
        
    }
               /**
     * This methods verifies if a initial submission date of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param dis ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the beging date of {@link model.SessaoTematica} is valid
     */
    public boolean validaDataInicioSubmissaoST(Data dis)
    {
        boolean result = false;
        if(dis.isMaior(dataInicioSubmissao) && dataFimSubmissao.isMaior(dis))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("A data de Início de Submissões da Sessão deve ser posterior à do Evento e anterior à sua data de fim");
        }
        return result;
        
    }
                   /**
     * This methods verifies if a final submission date of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param dis ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the final submission date of {@link model.SessaoTematica} is valid
     */
    public boolean validaDataFimSubmissaoST(Data dis)
    {
        boolean result = false;
        if(dis.isMaior(dataInicioSubmissao))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("A data de Fim de Submissões da Sessão deve ser anterior à do Evento.");
        }
        return result;
        
    }
                      /**
     * This methods verifies if a limit date for the final submission of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param dis ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the beging date {@link model.SessaoTematica} is valid
     */
    public boolean validaDataLimiteSubmissaoFinalST(Data dis)
    {
        boolean result = false;
        if(dataLimiteSubmissaoFinal.isMaior(dis) && dis.isMaior(dataLimiteRevisao))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("A data Limite de Submissão final da Sessão deve ser anterior à do Evento e posterior à data Limite de Revisao(do Eventon)");
        }
        return result;
        
    }
    /**
     * This methods verifies if a limit date for the final submission of a {@link model.SessaoTematica} is valid using a date passed by parameter
     * @param dis ({@link utils.Data}) the Date to be validated
     * @return (boolean) Returns true if the limit date for the final submission of {@link model.SessaoTematica} is valid
     */
    public boolean validaDataLimiteRevisaoST(Data dis)
    {
        boolean result = false;
        if(dataLimiteRevisao.isMaior(dis) && dis.isMaior(dataInicioDistribuicao))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("Data de Limite de Revisão deve ser anterior à do evento e posterior à data de Início de Distribuição(do Evento)");
        }
        return result;
        
    }
    /**
     * Returns the start date of the event
     * @return the start date of the event
     */
    public Data getDataInicio(){
        return (dataInicio==null) ? null :  new Data(dataInicio);
    }
    
    /**
     * Returns the end date of the event
     * @return the end date of the event
     */
    public Data getDataFim(){
        return (dataFim==null) ? null :  new Data(dataFim);
    }
    
    /**
     * Returns the start date of the distribuiton period of the event
     * @return the start date of the distribuiton period of the event
     */
    public Data getDataInicioDistribuicao(){
        return (dataInicioDistribuicao==null) ? null :  new Data(dataInicioDistribuicao);
    }
    /**
     * Returns the end date of the revision period of the event
     * @return the end date of the revision period of the event
     */
    public Data getDataLimiteRevisao(){
        return (dataLimiteRevisao==null) ? null : new Data(dataLimiteRevisao);
    }

    /**
     * Method that seeks list of Submissives in SessoesTematicas of Evento
     * @param id (String) The target id.
     */
    public List<Revisivel> getRevisiveisEmEstadoDeRevisaoDe(String id) {
        List<Revisivel> result = new ArrayList();
        result.addAll(listaSessoesTematicas.getRevisiveisEmEstadoDeRevisaoDe(id));
        result.add(this);
        return result;
    }

    public List<Submissivel> getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes() {
        return listaSessoesTematicas.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoes();
    }

    /**
     *  This method returns a list of registered submission made by an user in the whole Event
     * @param id (String)the user's ID
     * @return List &gt;{@link model.Submissao}&lt;  The List of the user's registered submissions
     */
    public List<Submissao> getSubmissoesRegistadasUtilizador(String id)
    {
        ArrayList<Submissao> result = new ArrayList();
        List<Submissao> est = listaSubmissoes.getListaSubmissoesRegistadasUtilizador(id);

        result.addAll(est);
        result.addAll(listaSessoesTematicas.getListaSubmissoesRegistadasUtilizador(id));
        return result;
                
            
        
    }

    public List<Submissivel> getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(String id)
    {
        return listaSessoesTematicas.getListaDeSessoesSubmissiveisEmEstadoAceitaSubmissoesUtilizador(id);
    }

     public ListaSubmissoes getListaSubmissoesEvento()
    {
        return listaSubmissoes;
    }
     

    /**
     * Returns a list of {@link interfaces.Licitavel} that are currently
     * accepting licitations (@link model.Licitacao} from a user with the
     * specified id.
     *
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Licitavel}&gt;) The list of
     * submissiveis.
     */
    public List<Licitavel> getListaLicitaveisEmLicitacaoDe(String id) {
        return listaSessoesTematicas.getListaLicitaveisEmLicitacaoDe(id);
    }

    public List<Submissao> getSubmissoesUtilizador(String id) {
        return listaSubmissoes.getSubmissoesUtilizador(id);
    }
    /*public RegistoSubmissoes getRegistoSubmissoes(Submissao sub)
     {
     return registoSubmissoes;
     }*/

    /**
     * Método que pesquisa todas as sessoes tematicas de id
     *
     * @param id(String) do Utilizador
     * @return lista das sessões temáticas
     */
    public List<SessaoTematica> getSessoesTematicasProponenteEmEstadoRegistado(String id) {
        return listaSessoesTematicas.getSessoesTematicasProponenteEmEstadoRegistado(id);
    }
    /**
     * Returns a list of all revisores in this event and thematic sessions.
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all revisors.
     */
    public List<Revisor> getListaTodosRevisores()
    {
        List<Revisor> result = new ArrayList();
        result.addAll(cp.getRevisores());
        result.addAll(listaSessoesTematicas.getListaTodosRevisores());
        return result;
    }
    /**
     * Returns a list of all revisions in this event.
     * @return (List&lt;{@link model.Revisao}&gt;) A list containing all revisions.
     */
    public List<Revisao> getListaTodasRevisoes()
    {
        List<Revisao> result = new ArrayList();
        if (processoDistribuicao!=null)
        {
            result.addAll(processoDistribuicao.getListaTodasRevisoes());
        }
        result.addAll(listaSessoesTematicas.getListaTodasRevisoes());
        return result;
    }
    /**
     * Checks if this event's decision process has any accepted submissions.
     * @return (boolean) True if at least one submission accepted has been found.
     */
    public boolean temSubmissoesAceites()
    {
        return processoDecisao.temSubmissoesAceites();
    }
    /**
     * Checks if this event's distribution process has any revisions.
     * @return (boolean) True if at least one revision has been found.
     */
    public boolean temRevisoes()
    {
        return processoDistribuicao.temRevisoes();
    }

    /**
     * Set de titulo
     *
     * @param strtitulo o titulo a a definir para o Evento
     */
    public void setTitulo(String strtitulo) {
        this.titulo = strtitulo;
    }

    /**
     * set de descricao
     *
     * @param strdescricao a descricao a definir para o Evento
     */
    public void setDescricao(String strdescricao) {
        this.descricao = strdescricao;
    }

    /**
     * Set of dataLimiteSubmissaoFinal
     * @param sf the  date to set
     */
    public void setDataLimiteSubmissaoFinal(Data sf){
        if(sf.isMaior(this.dataFim)){
            throw new IllegalArgumentException ("A data de limite de submissao final deve ser anterior à data de fim do evento");
        } else
        this.dataLimiteSubmissaoFinal = sf;
    }

 /**
     * Set de data de inicio
     *
     * @param strdataInicio a data de inicio do evento
     */
    public void setdataInicio(Data strdataInicio) {
        if(Data.getDataAtual().isMaior(strdataInicio)){
            throw new IllegalArgumentException("Data de Inicio tem de ser posterior à data atual.");
        } else
        this.dataInicio = strdataInicio;
    }

    /**
     * Set de data de fim
     *
     * @param strdataFim a data de fim do evento
     */
    public void setdataFim(Data strdataFim) {
        if(this.getDataInicio().isMaior(strdataFim)){
            throw new IllegalArgumentException("A data de fim do evento deve ser posterior à data de inicio");
        } else
        this.dataFim = strdataFim;
    }

    /**
     * Set da data de inicio de submissao
     *
     * @param data a data de inicio do periodo de submissao
     */
    public void setdataInicioSubmissao(Data data) {
        if(data.isMaior(this.getDataInicio())){
           throw new IllegalArgumentException("A data de inicio de submissao do evento deve ser anterior à data de inicio");
       } else
        dataInicioSubmissao = data;
    }

    /**
     * set da data de fim de submissao
     *
     * @param data a data do fim do periodo de submissao
     */
    public void setdataFimSubmissao(Data data) {
        if(data.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de fim de submissão do evento deve ser anterior à data de inicio de");
        } else if (this.getDataInicioSubmissao().isMaior(data)){
            throw new IllegalArgumentException("A data de fim de submissão do evento tem de ser posterior à data de inicio de submissão");
        } else
        dataFimSubmissao = data;
    }

    /**
     * set da data de inicio da distribuicao
     *
     * @param data a data de inicio do periodo de distribuicao
     */
    public void setdataInicioDistribuicao(Data data) {
        if(data.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de inicio de distribuicao do evento deve ser anterior à data de inicio");
        } else if (this.getDataFimSubmissao().isMaior(data)){
            throw new IllegalArgumentException("A data de inicio de distribuicao do eveento deve ser posterior à data de fim de submissao");
        } else
        dataInicioDistribuicao = data;
    }
    
    /**
     * set da data de Fo, da distribuicao
     *
     * @param data a data de Fim do periodo de distribuicao
     */
    public void setdataLimiteRevisao(Data data){
        if(data.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de limite de revisao do evento deve ser anterior a data de inicio");
        } else if (this.getDataInicioDistribuicao().isMaior(data)){
            throw new IllegalArgumentException("A data de limite de revisao do evento deve ser posterior a data de inicio de distribuicao");
        } else
            dataLimiteRevisao = data;
    }
    
    /**
     * set de local do evento
     *
     * @param strLocal o local de realização do evento
     */
    public void setLocal(Local strLocal) {
        this.local=strLocal;
    }

    public void setState(EventoState state) {
        this.state = state;
    }

    public boolean setCP(CP cp) {
        this.cp = cp;
        return this.setCPDefinida();
    }

    /**
     * Returns the state of this event in a text format.
     * @return (String) The text format of the state.
     */
    public String getState() {
        return state.toString();
    }

    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = listaOrganizadores.listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }
        /** This method procures {@link model.Submissao} made by the ser in the {@link model.Evento} that are in {@link states.submissao.SubmissaoAceiteState}
     * @param id (String) he user 
     * @return List (gt;{@link model.Submissao}lt;) A list of {@link model.Submissao} made by the ser in the {@link model.Evento} that are in {@link states.submissao.SubmissaoAceiteState}
     */
    public List<Submissao> getListaSubmissoesAceites(String id)
    {
        return listaSubmissoes.getListaSubmissoesAceites(id);
    }

/**
 * This method procures for {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
 * @param id String) The user's ID
 * @return (&gt;{@link model.SessaoTematica}&lt;) A list of {@link model.SessaoTematica} that are in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user 
 */
    public List<SessaoTematica> getListaSessoesEmDecisao(String id)
    {
        return listaSessoesTematicas.getListaSessoesEmDecisao(id);
    }
    /**
     * This method generates a list of {@link model.Decisao} within the {@link model.ProcessoDecisao}
     * @param lr (List&lt;{@link model.Revisao}&gt;) The list with revisions.
     * @return List &gt;Decisao&lt; created by the {@link interfaces.MecanismoDecisao}
     */
    public List<Decisao> decide(List<Revisao> lr)
    {
       return processoDecisao.decide(lr);
    }
            /**
     * This method procures all {@link model.Revisao}
     * @return List(&lt;{@link model.Revisor}&gt;) 
     */
    @Override
    public List<Revisao> getRevisoes()
    {
        return processoDistribuicao.getListaTodasRevisoes();
    }
    /**
     * Returns a list containing all the submissions of this event's thematic
     * sessions that belong to a user with the specified id.
     *
     * @param id (String) The user's id.
     * @return (List&lt;{@link model.Submissao}&gt;) The list of submissions.
     */
    public List<Submissao> getSubmissoesSessoesUtilizador(String id) {
        return listaSessoesTematicas.getSubmissoesSessoesUtilizador(id);
    }

    /**
     * Returns a list of {@link interfaces.Submissivel} that are not in "Camera
     * Ready" state.
     *
     * @param id (String) The user's id.
     * @return (List&lt;{@link interfaces.Submissivel}&gt;) The list of
     * submissiveis.
     */
    public List<Submissivel> getListaSubmissiveisNaoCameraReadyDe(String id) {
        return listaSessoesTematicas.getListaSubmissiveisNaoCameraReadyDe(id);
    }

    /**
     * This method checks if there is at least one {@link model.SessaoTematica} that is in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
     * @param id (String) The user's ID
     * @return (boolean) Returns true if there is at least one {@link model.SessaoTematica} that is in {@link states.sessaotematica.SessaoTematicaEmDecisaoState} whose {@link model.Proponente} is the user
     */
    public boolean temSessoesEmDecisao(String id)
    {
        return listaSessoesTematicas.temSessoesEmDecisao(id);
    }
    
    /**
     * This method checks the if the main event has at least one {@link model.Submissao} in {@link states.submissao.SubmissaoAceiteState} made by the user
     * @param id (String) The user ID
     * @return (boolean) Returns true if the main event has at least one {@link model.Submissao} in {@link states.submissao.SubmissaoAceiteState}
     */
    public boolean temSubmissoesAceitesUtilizadorEventoPrincipal(String id)
    {
        return listaSubmissoes.temSubmissoesAceitesUtilizador(id);
    }
    
    public List<SessaoTematica> getListaSessoesEmSubmissaoCR(String id)
    {
        return listaSessoesTematicas.getListaSessoesEmSubmissaoCR(id);
    }
    /**
     * method that return list of Submissives from listaSessoesTematicas(List)
     * correspondent to this Revisor
     */
    public List<Revisivel> getListaSubmissiveisEmEstadoDeRevisaoDe(String id) {
        return listaSessoesTematicas.getRevisiveisEmEstadoDeRevisaoDe(id);
    }

    /**
     * Alerts the {@link model.Organizador}s whether it is advised to kick out a 
     * revisor.
     * @param r ({@link model.Revisor}) The revisor to warn about.
     * @param message (String) The message about the revisor to send.
     * @return (boolean) True if all organizadores were warned.
     */
    public boolean alertaOrganizadores(Revisor r,String message)
    {
        boolean result = true;
        for (Organizador focus:listaOrganizadores)
        {
            result = result && focus.alertaOrganizador(r,message);
        }
        return result;
    }
    /**
     * Checks if this event has any thematic sessions.
     * @return (boolean) True if successful.
     */
    public boolean temSessaoTematica() {
        return listaSessoesTematicas.temSessaoTematica();
    }
  
    /**
     * This method validates a {@link model.SessaoTematica} and adds it in the list of {@link model.SessaoTematica} that belongs to the {@link model.Evento}
     * @param st ({@link model.SessaoTematica}) The event where the {@link model.SessaoTematica} shall be registered
     * @return (boolean) Returns true if the operation is sucessfull
     */
    public boolean registaSessaoTematica(SessaoTematica st) 
    {
        boolean result = listaSessoesTematicas.registaSessaoTematica(st);
        if (result)
        {
            setSessaoTematicaDefinida();
        }
        return result;
    }
    /**
     * Adds all the submissions present in the target list to this event's submission list.
     * @param ls (List&lt;{@link model.Submissao}&gt;) The target list of submissions to add.
     */
    public void addAllSubmissoes(List<Submissao> ls)
    {
        listaSubmissoes.addAllSubmissoes(ls);
    }
    /**
     * Directly adds a thematic session without validation.
     * Should only be used for testing purposes.
     * @param st ({@link model.SessaoTematica}) The thematic session to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addSessaoTematica(SessaoTematica st) {
        return listaSessoesTematicas.add(st);
    }
    /**
     * Checks if this event already has all revisions done.
     * @return 
     */
    public boolean temTodasRevisoesFeitas(){
        return processoDistribuicao.temTodasRevisoesFeitas();
    }

    /**
     * This searches the list for thematic sessions that have a proponent passed
     * as parameter Needs testing
     *
     * @param id the id(username or email) of the user to search for
     * @return a list of all the thematic sessions that have the user as a
     * proponent
     */
    public List<SessaoTematica> getSessoesTematicasComProponenteRegistadoState(String id) {
        return listaSessoesTematicas.getSessoesTematicasComProponenteRegistadoState(id);
    }
    /**
     * This method checks in the list if there are any thematic sessions that
     * have a proponent passed as parameter. Needs testing
     *
     * @param id (String) The identification(Username or email) of the user to
     * search for
     * @return (boolean) True or false depending on whether there is a thematic
     * session with the user as a proponent
     */
    public boolean temSessaoComProponente(String id) {
        return listaSessoesTematicas.temSessaoComProponente(id);
    }

    /**
     * Adiciona um organizador ao evento
     *
     * @param u o utilizador a associar a esse organizador
     * @return true ou falso dependendo do sucesso da operação(true se bem
     * sucedida)
     */
    public boolean addOrganizador(Utilizador u) {
        Organizador o = new Organizador(u);

        if (o.valida() && validaOrganizador(o)) {
            return addOrganizador(o);
        }
        return false;
    }
    /**
     * Adds an organizer to this event.
     * @param o ({@link model.Organizador}) The organizer to add.
     * @return (boolean) True if successfully added.
     */
    private boolean addOrganizador(Organizador o) {
        return listaOrganizadores.add(o);
    }

    /**
     * Método para validar o Organizador no evento
     *
     * @param o o Organizador a validar
     * @return true ou falso dependendo do sucesso da operação(true se bem
     * sucedida)
     */
    public boolean validaOrganizador(Organizador o){
        if(!listaOrganizadores.isEmpty()){
            for(Organizador org : listaOrganizadores){
                if(org.getUserEmail().equalsIgnoreCase(o.getUserEmail()) 
                        || org.getUserUsername().equalsIgnoreCase(o.getUserUsername())){
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * Sets this event's thematic session list to a new one.
     * @param lst (List&lt;{@link model.SessaoTematica}&gt;) The new thematic session
     * list.
     */
    public void setListaSessoesTematicas(List<SessaoTematica> lst)
    {
        listaSessoesTematicas = new ListaSessoesTematicas(lst);
    }
    /**
     * Adds all the thematic sessions the provided list to this event's thematic
     * session list.
     * @param st (List&lt;{@link model.SessaoTematica}&gt;) The list of thematic
     * sessions to add to this event.
     */
    public void addAllSessoesTematicas(List<SessaoTematica> st)
    {
        listaSessoesTematicas.addAllSessoesTematicas(st);
    }
    /**
     * Método que cria uma nova instância de CP
     *
     * @return ({@link model.CP}) nova CP
     */
    public CP novaCP() {
        cp = new CP();

        return cp;
    }
    /**
     * Checks if this event is valid.
     * @return (boolean) True if valid.
     */
    public boolean valida() {
        return titulo != null && descricao != null && local != null && dataInicio!= null && dataFim != null && dataInicioSubmissao != null && dataFimSubmissao != null && dataInicioDistribuicao != null && dataLimiteSubmissaoFinal != null && dataLimiteRevisao != null && listaOrganizadores != null && cp != null && processoDistribuicao != null && processoDecisao != null && state != null && listaSessoesTematicas != null && listaSubmissoes != null;
    }

    /**
     * Checks if this event's CP (Comission Program) has the specified revisor.
     * @param id (String) The id of the revisor.
     * @return (boolean) True if the revisor is present.
     */
    public boolean temRevisor(String id) {
        return cp.temMembro(id);
    }

    /**
     * Método que verifica se um evento tem organizadores
     *
     * @return
     */
    public boolean temOrganizadores() 
    {
        boolean result = false;
        for (Organizador elemento : listaOrganizadores) {
            if (elemento != null) {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * Checks if this event has any submissions.
     * @return (boolean) True if at least one submission was found.
     */
    public boolean temSubmissoes()
    {
        return listaSubmissoes.temSubmissoes();
    }
    /**
     * Generates submissions based on data provided from a .csv file.
     * @param stdIn (File) The file to read the data from.
     * @param stdOut (File) The output file to write any errors that occur
     * whilst loading the submissions.
     * @param regU ({@link registos.RegistoUtilizadores}) The company's user registry.
     * @return (boolean) True if no errors were generated.
     * @throws java.io.IOException In case there was a reading/writing error.
     */
    public boolean geraSubmissoesDeFicheiro(File stdIn,File stdOut,RegistoUtilizadores regU) throws IOException
    {
        boolean result = true;
        String dataIn = utils.Utils.getDataFromFile(stdIn);
        BufferedWriter buffer = new BufferedWriter(new FileWriter(stdOut));
        String[] chunkedData = dataIn.split("\n");
        for (int i=0;i<chunkedData.length;i++)
        {
            String[] cache = chunkedData[i].split(";");
            if (cache.length<10)
            {
                result=false;
                buffer.write("Número de parcelas mínimas insuficientes (falta um campo)");
                buffer.newLine();
            }
            else
            {
                Submissao s = new Submissao();
                Artigo artigoInicial = new Artigo();
                String stCode = cache[0];
                String titulo=cache[1];
                String autorC = cache[2];
                String dataSub = cache[3];
                String[] palavrasChave = cache[4].split(",");
                List<String> pc = new ArrayList();
                for (String focus:palavrasChave)
                {
                    pc.add(focus);
                }
                String resumo=cache[5];
                String ficheiro = cache[6];
                ListaAutores autores = new ListaAutores();
                for (int j=7;j<cache.length;j+=3)
                {
                    try
                    {
                        String autNome = cache[j];
                        String filiacao = cache[j+1];
                        String email = cache[j+2];
                        Autor autor = new Autor();
                        autor.setNome(autNome);
                        autor.setEmail(email);
                        autor.setAfiliacao(filiacao);
                        autores.addAutor(autor);
                    }
                    catch (IllegalArgumentException |ArrayIndexOutOfBoundsException ex)
                    {
                        result=false;
                        if (ex instanceof ArrayIndexOutOfBoundsException)
                        {
                            buffer.write("Um dos autores contém campos a menos!");
                        }
                        else
                        {
                            buffer.write(ex.getMessage());
                        }
                        buffer.newLine();
                    }
                }
                try
                {
                    artigoInicial.setTitulo(titulo);
                    artigoInicial.setAutores(autores.getListaAutores());
                    int[] data = utils.Utils.convertStringDateToInteger(dataSub, "-");
                    artigoInicial.setDataCriado(new Data(data[2],data[1],data[0]));
                    artigoInicial.setResumo(resumo);
                    artigoInicial.setFicheiro(ficheiro);
                    artigoInicial.setPalavrasChave(pc);
                    if (artigoInicial.temAutor(autorC) && regU.temUtilizadorUsername(autorC))
                    {
                        artigoInicial.setAutorCorrespondente(autores.getAutor(autorC), regU.getUtilizador(autorC));
                    }
                    else
                    {
                        throw new IllegalArgumentException("O autor correspondente não consta da lista de autores ou não é um utilizador registado.");
                    }
                }
                catch (IllegalArgumentException e)
                {
                    result=false;
                    buffer.write(e.getMessage());
                    buffer.newLine();
                }
                if (!stCode.equals(""))
                {
                    s.setArtigoInicial(artigoInicial);
                    if (!listaSubmissoes.addSubmissao(s))
                    {
                        buffer.write("A submissão já existe...");
                        buffer.newLine();
                        result=false;
                    }
                }
                else
                {
                    SessaoTematica st = listaSessoesTematicas.getSessaoTematicaCodigo(stCode);
                    if (st==null)
                    {
                        buffer.write("A sessão temática não existe...");
                        result=false;
                        buffer.newLine();
                    }
                    else
                    {
                        if (!st.addGeneratedSubmission(s))
                        {
                            buffer.write("A submissão já existe...");
                            buffer.newLine();
                            result=false;
                        }
                    }
                }
            }
        }
        buffer.close();
        return result;
    }
     /**
     * This methods checks for {@link model.SessaoTematica} that are registered in this {@link model.Evento} that are in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState}
     * @param id (String) The user's ID
     * @return (boolean) Returns true if the the list of {@link model.SessaoTematica} of this {@link model.Evento} has at least one in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState} 
     */
    public boolean temSessoesDistribuiveis(String id)
    {
        return listaSessoesTematicas.temSessoesDistribuiveis(id);
    }
    /**
     * This method procures {@link model.SessaoTematica} within the {@link model.Evento} that have the user as {@link model.Proponente} and that are in {@link states.sessaotematica.SessaoTematicaEmLicitacaoState}
     * @param id (String) The user's ID
     * @return List &gt;{@link model.SessaoTematica}&lt; 
     */
    public List<SessaoTematica> getSessoesTematicasEmLicitacao(String id)
    {
        return listaSessoesTematicas.getSessoesTematicasEmLicitacao(id);
    }

    /**
     * This method returns the index of a Submission within the Event's list of Submissions
     * @param sub {@link model.Submissao}
     * @return (int) The position of the Submission within the Event's list of Submissions
     */
    public int getPosition(Submissao sub)
    {
        return listaSubmissoes.getPosition(sub);
    }
    /**
     * This method verifies a change made to a submission that belong in the submission list of the Event
     * @param sub ({@link model.Submissao}) The change(clone) to be verified
     * @param i (int) The position of the Submission in the Event's list of Submissions
     * @return (boolean) Returns true if the change is valid.
     */
    public boolean valida(Submissao sub, int i)
    {
        boolean result=true;
        for(Submissao focus:this.getListaSubmissoes())
        {
            if(focus.equals(sub) && (this.listaSubmissoes.getPosition(sub)!=i))
            {
                result=false;
            }
        }
        return result;
    }
    /**
     * Checks if this event has a {@link model.CP} created.
     *
     * @return (boolean) True if the event has a commision program created.
     */
    public boolean temCP() {
        return cp != null;
    }

        /**
     * This methods check if there are {@link model.Submissao} in {@link states.submissao.SubmissaoAceiteState} submitted anywhere in the {@link model.Evento}
     * @param id (String) The user ID
     * @return result (boolean) Returns true if there are valid {@link model.Submissao}
     */
    public boolean temSubmissoesAceitesUtilizador(String id)
    {
       boolean result = false; 
       if(listaSubmissoes.temSubmissoesAceitesUtilizador(id) || listaSessoesTematicas.temSubmissoesAceitesUtilizador(id))
       {
           result = true;
       }
       return result;
    }

    /**
     * This methods checks if all the {@link model.Submissao} are in {@link states.submissao.SubmissaoEmCameraReadyState}
     * @return (boolean)  Returns true if all the {@link model.Submissao} are in {@link states.submissao.SubmissaoEmCameraReadyState}
     */
    public boolean temTodasSubmissoesCameraReady()
    {
        return listaSubmissoes.temTodasSubmissoesCameraReady();
    }
    /**
     * Checks if this event and its thematic sessions have any articles
     * submitted by an author with the specified id.
     *
     * @param id (String) The author's id (either email or username).
     * @return (boolean) True if it has.
     */
    public boolean temSubmissoesAutor(String id) {
        return listaSessoesTematicas.temSubmissoesAutor(id) || listaSubmissoes.temSubmissoesAutor(id);
    }

    /**
     * Método que irá calcular a Taxa de Aceitação de um evento
     *
     * @return valor atribuido à Taxa de Aceitação
     */
    public double calcularTaxaAceitacao() {
        int nTotal = listaSubmissoes.getNumeroTotalSubmissoes();
        nTotal += listaSessoesTematicas.getNumeroTotalSubmissoes();
        int nAceites = listaSubmissoes.getNumeroSubmissoesAceites();
        nAceites += listaSessoesTematicas.getNumeroSubmissoesAceites();
        return (double) nAceites / (double) nTotal;
    }

    /**
     * Método que irá calcular os Valores Médios de um evento
     *
     * @return(double[]) vetor com os valores médios de um evento
     */
    public double[] calcularValoresMedios() {
        double[] valoresMedios = new double[5];

        int nTotal = listaSubmissoes.getNumeroTotalSubmissoes();
        nTotal += listaSessoesTematicas.getNumeroTotalSubmissoes();

        int nConfianca = processoDistribuicao.getConfianca();
        nConfianca += listaSessoesTematicas.getConfianca();

        valoresMedios[0] = (double) nConfianca / (double) nTotal;

        int nAdequacao = processoDistribuicao.getAdequacao();
        nAdequacao += listaSessoesTematicas.getAdequacao();

        valoresMedios[1] = (double) nAdequacao / (double) nTotal;

        int nOriginalidade = processoDistribuicao.getOriginalidade();
        nOriginalidade += listaSessoesTematicas.getOriginalidade();

        valoresMedios[2] = (double) nOriginalidade / (double) nTotal;

        int nQualidade = processoDistribuicao.getQualidade();
        nQualidade += listaSessoesTematicas.getQualidade();

        valoresMedios[3] = (double) nQualidade / (double) nTotal;

        int nRecomendacao = processoDistribuicao.getRecomendacao();
        nRecomendacao += listaSessoesTematicas.getRecomendacao();

        valoresMedios[4] = (double) nRecomendacao / (double) nTotal;

        return valoresMedios;
    }
    
     /**
     * Método que busca a Lista de Revisiveis de um dado utilizador que se
     * encontre no estado em Revisao
     *
     * @param id (String) id do utilizador
     * @return a lista revisivel
     */
    public List<Revisivel> getListaRevisivelUtilizadorEmEstadoRevisao(String id) {
        return listaSessoesTematicas.getListaRevisivelUtilizadorEmEstadoRevisao(id);        
        }
    

    /**
     * Checks if only this event has any articles submitted by an author with
     * the specified id, ignoring if the thematic sessions have.
     *
     * @param id (String) The author's id (either email or username).
     * @return (boolean) True if it has.
     */
    public boolean temSubmissoesAutorExclusivo(String id) {
        return listaSubmissoes.temSubmissoesAutor(id);
    }

    /**
     * Checks if this event is in the {@link states.evento.EventoCriadoState}
     * (created state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCriadoState() {
        return state.isInCriadoState();
    }

    /**
     * Checks if this event is in the {@link states.evento.EventoRegistadoState}
     * (registered state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInRegistadoState() {
        return state.isInRegistadoState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoSessaoTematicaDefinidaState} (thematic session
     * defined state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInSessaoTematicaDefinidaState() {
        return state.isInSessaoTematicaDefinidaState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoCPDefinidaState} (CP defined state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCPDefinidaState() {
        return state.isInCPDefinidaState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoAceitaSubmissoesState} (accepts submissions
     * state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInAceitaSubmissoesState() {
        return state.isInAceitaSubmissoesState();
    }

    /**
     * Checks if this event is in the {@link states.evento.EventoEmDetecaoState}
     * (detecting conflicts state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDetecaoState() {
        return state.isInEmDetecaoState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoEmLicitacaoState} (bidding state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmLicitacaoState() {
        return state.isInEmLicitacaoState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoEmDistribuicaoState} (distributing revisions
     * state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDistribuicaoState() {
        return state.isInEmDistribuicaoState();
    }

    /**
     * Checks if this event is in the {@link states.evento.EventoEmRevisaoState}
     * (in revision state).
     * 
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmRevisao() {
        return state.isInEmRevisaoState();
    }

    /**
     * Checks if this event is in the {@link states.evento.EventoEmDecisaoState}
     * (in decision state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmDecisaoState() {
        return state.isInEmDecisaoState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoEmSubmissaoCameraReadyState} (in camera ready
     * submission state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInEmSubmissaoCameraReadyState() {
        return state.isInEmSubmissaoCameraReadyState();
    }

    /**
     * Checks if this event is in the
     * {@link states.evento.EventoCameraReadyState} (camera ready state).
     *
     * @return (boolean) True if the event is in the desired state.
     */
    public boolean isInCameraReadyState() {
        return state.isInCameraReadyState();
    }

    @Override
    public String toString() {
        return "Evento: "+this.titulo + "/"+this.descricao+"\n";
    }

    /**
     * Creates a new submission for this event.
     *
     * @return (Submissao) The created submission.
     */
    @Override
    public Submissao novaSubmissao() {
        return listaSubmissoes.novaSubmissao();
    }

    /**
     * Adds a submission to this event's submission list.
     *
     * @param submissao (Submissao) The submission to add.
     * @param a (Artigo) The submission's article.
     * @param u (Utilizador) The user that submitted the article.
     * @return (boolean) True if successfully added.
     */
    @Override
    public boolean addSubmissao(Submissao submissao, Artigo a, Utilizador u) {
        boolean result;
        a.setAutorCriador(u);
        a.setDataCriado(Data.getDataAtual());
        result = listaSubmissoes.addSubmissao(submissao);
        if (result) {
            result = submissao.setSubmissaoRegistadaState();
        }
        return result;
    }

    @Override
    public List<Revisao> getRevisoes(String id) {
        return processoDistribuicao.getRevisoes(id);
    }
    /**
    * Método que irá guardar a revisão
    * @param rev {@link model.Revisao} revisão a guardar
    * @return true se guardou a revisao, false se não guardou
    */
    @Override
    public boolean saveRevisao(Revisao rev){
        return processoDistribuicao.saveRevisao(rev);
    }
    
    /**
     * Returns the {@link model.Revisor} with the specified id.
     * @param id (String) The revisor's id.
     * @return (Revisor) The specified revisor (null if was found).
     */
    @Override
    public Revisor getRevisor(String id) {
        return cp.getRevisor(id);
    }
    /**
     * Returns the revisor that corresponds to the specified user.
     * If the revisor does not exist anywhere on the event or its thematic
     * sessions, null is returned.
     * @param u ({@link model.Utilizador}) The target user.
     * @return ({@link model.Revisor}) The revisor.
     */
    public Revisor getRevisor(Utilizador u)
    {
        Revisor result;
        result = cp.getRevisor(u.getEmail());
        if (result==null)
        {
            result = listaSessoesTematicas.getRevisor(u);
        }
        return result;
    }
   
    @Override
    public List<Submissao> getSubmissoesNaoCameraReadyDe(String id) {
        return listaSubmissoes.getSubmissoesNaoCameraReadyDe(id);
    }

    @Override
    public ProcessoDistribuicao novoProcessoDistribuicao() {
        return new ProcessoDistribuicao();
    }

    @Override
    public List<Licitacao> getLicitacoes()
    {
        return listaSubmissoes.getLicitacoes();
    }
    

    /**
     * Sets this thematic session's distribution process.
     * @param pd ({@link model.ProcessoDistribuicao}) The new distribution process.
     */
    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao pd) 
    {
        processoDistribuicao = pd;
    }

    @Override
    public boolean setCriado() {
        return state.setCriado();
    }

    @Override
    public boolean setRegistado() {
        return state.setRegistado();
    }

    @Override
    public boolean setCPDefinida() {
        return state.setCPDefinida();
    }

    @Override
    public boolean setAceitaSubmissoes() {
        return state.setAceitaSubmissoes();
    }

    @Override
    public boolean setEmDetecao() {
        return state.setEmDetecao();
    }

    @Override
    public boolean setEmLicitacao() {
        return state.setEmLicitacao();
    }

    @Override
    public boolean setEmDistribuicao() {
        return state.setEmDistribuicao();
    }

    @Override
    public boolean setEmRevisao() {
        return state.setEmRevisao();
    }

    @Override
    public boolean setEmDecisao() {
        return state.setEmDecisao();
    }

    @Override
    public boolean setSubmissoesDecididas() {
        return state.setEmSubmissaoCameraReady();
    }
    /**
     * This method changes the state of the {@link model.Evento} to {@link states.evento.EventoSessaoTematicaDefinidaState}
     * @return (boolean) Returns true if the operation is sucessfull
     */
    public boolean setSessaoTematicaDefinida()
    {
        return state.setSessaoTematicaDefinida();
    }
        /**
     * This method changes the state of the {@link model.Evento} to {@link states.evento.EventoCameraReadyState}
     * @return (boolean) Returns true if the operation is sucessfull
     */
    @Override
    public boolean setEmCameraReady()
    {
        return state.setCameraReady();
    }
    /**
     * The method verifies a change made to the submission of the Event
     * @param sub ({@link model.Submissao})
     * @return (boolean) if the change is valid in the submission list of the event
     */
    @Override
    public boolean registaAlteracao(Submissao sub)
    {
        boolean result=false;
        if(sub.validaInicial() && this.valida(sub,getPosition(sub)))
        {
            result = true;
        }
        else
        {
            throw new IllegalArgumentException("Submissao Inválida");
        }
        return result;
    }
    /**
     * Makes the detection process of this event detect any conflicts
     * between revisors and its submissions.
     * @param lmc (List&lt;{@link model.TipoConflito}&gt;) The list of types
     * of conflict to check whilst detecting.
     */
    @Override
    public void detetarConflitos(List<TipoConflito> lmc) {
        ProcessoDetecao pd = new ProcessoDetecao(this,lmc);
    }
    @Override
    public List<Revisor> getListaRevisoresCP() {
        return cp.getRevisores();
    }
    @Override
    public List<Submissao> getListaSubmissoes() {
        return listaSubmissoes.getSubmissoes();
    }
    
    @Override
    public List<Submissao> getSubmissoes()
    {
        return listaSubmissoes.getSubmissoes();
    }

    @Override
    public ProcessoDecisao novoProcessoDecisao() {
        return new ProcessoDecisao();
    }

    @Override
    public void setProcessoDecisao(ProcessoDecisao pd) {
        this.processoDecisao=pd;
    }
    /**
     * Checks if this event is equal to another object
     *
     * @param other (Object) The object to compare with.
     * @return (boolean) True if both objects are equal.
     */
    @Override
    public boolean equals(Object other) {
        boolean result = (other != null) && (getClass() == other.getClass());
        if (result) {
            Evento temp = (Evento) other;
            result= (dataInicio!=null && dataFim!=null && titulo!=null);
            if (result)
            {
                result = (this == other) || dataInicio.equals(temp.getDataInicio())
                    && dataFim.equals(temp.getDataFim()) && titulo.equals(temp.getTitle());
            }else{
                result=dataFim==temp.getDataFim() && dataInicio==temp.getDataInicio() && titulo==temp.getTitle();
            }
        }
        return result;
    }

    /**
     * This searches a list of submissions for withdrawn submissions needs
     * tests.
     *
     * @return a list containing all the withdrawn submissions found
     */
    @Override
    public List<Submissao> getSubmissoesRemovidas() {
        return listaSubmissoes.getSubmissoesRemovidas();
    }

    /**
     * Returns a list of submissions that are in
     * {@link states.submissao.SubmissaoEmLicitacaoState} state.
     *
     * @return (List&lt;{@link model.Submissao}&gt;)List containing the
     * submissions.
     */
    @Override
    public List<Submissao> getSubmissoesEmLicitacao() {
        return listaSubmissoes.getSubmissoesEmLicitacao();
    }

    /**
     * This method checks if the event has a certain user as it's organizer
     * Needs testing
     *
     * @param id the idetification(username email) of the user to check
     * @return true or false depending the event has the user as an organizer or
     * not, respectively
     */
    public boolean temOrganizador(String id) {
        for (Organizador o : listaOrganizadores) {
            if (o.getUserEmail().equals(id) || o.getUserUsername().equals(id)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * This method updates the accepted topics list with the topics of each accepted submission
     * @param ls the topics list
     * @return  the topics list
     */
    @Override
    public ArrayList<String> getAcceptedTopics(ArrayList<String> ls){
        return this.listaSubmissoes.getAcceptedTopics(ls);
    }
    
    /**
     * This method updates the rejected topics list with the topics of each rejected submission
     * @param ls the topics list
     * @return  the topics list
     */
    @Override
    public ArrayList<String> getRejectedTopics(ArrayList<String> ls){
        return this.listaSubmissoes.getRejectedTopics(ls);
    }
    
    /**
     * This method checks each revision of this submissible's distribuition Process to see if there is an ocurrance of a keyword existing in the arraylist passed as parameter in it. When it finds a match it adds it's global recomendation to it and increments one to the frequency of the keyword
     * @param ls the array list with each keyword with it's global recomendation sum and frequency
     * @return the array list with each keyword with it's global recomendation sum and frequency updated with each revision
     */
    @Override
    public ArrayList<String[]> getStatsTopics(ArrayList<String[]> ls) {
        return this.processoDistribuicao.getStatsTopicos(ls);
    }
    
    /**
     * This method returns all Sessions that have gone through with the decision process in this registry
     * @return all sessions that have gone through with the decision process in this registry
     */
    public ArrayList<SessaoTematica> getListaSessoesDecididas(){
        return this.listaSessoesTematicas.getListaSessoesDecididas();
    }

    /**
     * @see #showData() 
     * @return
     */
    @Override
    public String showData() {
        return ((cp != null) ? "CP não nula" : "CP nula") + "\n"+String.valueOf(System.identityHashCode(this));
    }
    
}
