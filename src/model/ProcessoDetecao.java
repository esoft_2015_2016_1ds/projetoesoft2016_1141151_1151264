/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.Detetavel;
import interfaces.MecanismoDetecao;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import listas.ListaConflitosDetetados;

/**
 *
 * @author Diogo
 */
public class ProcessoDetecao implements iTOCS{
    private Detetavel sub;
    private List<Revisor> lr;
    private List<Submissao> ls;
    private List<TipoConflito> rtc;
    public ProcessoDetecao(Detetavel sub,List<TipoConflito> rtc)
    {
        this.sub=sub;
        lr=sub.getListaRevisoresCP();
        ls=sub.getListaSubmissoes();
        this.rtc=rtc;
        run();
    }
    private void run()
    {
        for (Submissao focus:ls)
        {
            ListaConflitosDetetados lcd = focus.novaListaConflitos();
            for (Revisor element:lr)
            {
                ConflitoDetetado con = new ConflitoDetetado(element,focus);
                List<TipoConflito> ltc = new ArrayList();
                for (TipoConflito target:rtc)
                {
                    if (target.detetarConflitosCom(element,focus))
                    {
                        ltc.add(target);
                    }
                }
                con.addTiposConflito(ltc);
                lcd.addConflitoDetetado(con);
            }
        }
    }

  
    @Override
    public String showData() {
        return "";
    }
}
