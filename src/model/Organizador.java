/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author André Vieira
 */

public class Organizador implements iTOCS
{
    /**
     * O utilizador associado ao Organizador
     */
    private Utilizador m_utilizador;
 
    private List<Alerta> listaAlertas;
     
    /**
     * Creates an instance of object {@link model.Organizador} with the specified 
     * parameters.
     * @param u ({@link model.Utilizador}) The user that the organizador corresponds to.
     */
    public Organizador(Utilizador u)
    {
        this(u,new ArrayList());
    }
    /**
     * Creates an instance of object {@link model.Organizador} with the specified 
     * parameters.
     * @param u ({@link model.Utilizador}) The user that the organizador corresponds to.
     * @param la ({List&lt;{@link model.Alerta}&gt;}) A list containing all the alert
     * messages for this organizador.
     */
    public Organizador(Utilizador u, List<Alerta> la)
    {
        this.setUtilizador(u);
        listaAlertas = new ArrayList(la);
    }
    
    /**
     * Construtor vazio de organizador
     */
    public Organizador()
    {
        this(new Utilizador(),new ArrayList());
    }
    
    /**
     * Construtor copia de organizador
     * @param o o organizador a copiar
     */
    public Organizador(Organizador o){
        this(o.getUtilizador(),o.listaAlertas);
    }
    
    /**
     * Set de utilizador
     * @param u o utilizador a associar ao ogranizador
     */
    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    /**
     * Método para validar Organizador
     * @return true ou false consoante passe ou falhe no teste respectivamente.
     */
    public boolean valida()
    {
        return this.getUtilizador()!= null;
    }
    
    /**
     * Método get do atributo utilizador
     * @return cópia do utilizador do objecto
     */
    public Utilizador getUtilizador()
    {
        return new Utilizador(this.m_utilizador);
    }
    /**
     * Adds a new alert to this organizador's alert list about a target revisor
     * with a specific message..
     * @param r ({@link model.Revisor}) The target revisor.
     * @param message (String) The message about the target revisor.
     * @return (boolean) True if successfully added.
     */
    public boolean alertaOrganizador(Revisor r,String message)
    {
        return listaAlertas.add(new Alerta(r,message));
    }
    
    /**
     * Método toString()
     * @return uma string representativa do Organizador
     */
    @Override
    public String toString()
    {
        return m_utilizador.toString();
    }
    
    /**
     * Set da lista alertas
     * @param lAlertas the alert list to associate to this organizer
     */
    public void setListaAlertas(List<Alerta> lAlertas){
        this.listaAlertas = lAlertas;
    }
    
    /**
     * Get of user email
     * @return the email adress of the associated user
     */
    public String getUserEmail(){
        String str = m_utilizador.getEmail();
        return str;
    }
    
    /**
     * get of user username
     * @return the username of the associated user
     */
    public String getUserUsername(){
        String str = m_utilizador.getUsername();
        return str;
    }
  
    @Override
    public String showData() {
        return "";
    }
}
