package model;


import interfaces.iTOCS;
import java.io.Serializable;
import registos.RegistoUtilizadores;
import static utils.Utils.checkEmail;

/**
 * A class that represents an author of an article with a name, email and institution.
 * @author jbraga
 */
public class Autor implements Serializable,iTOCS{
    private String m_strNome;
    private String email;
    private String m_strAfiliacao;
    private String username;
    /**
     * Creates an instance of object Autor with null parameters.
     */
    public Autor()
    {
        m_strNome="";
        email="";
        m_strAfiliacao="";
        username="";
    }
    /**
     * Creates a copy of an instance of object {@link model.Autor}.
     * @param target (Autor) The target author to copy.
     */
    public Autor(Autor target)
    {
        this(target.getName(),target.getEmail(),target.getInstituicaoAfiliacao(),target.getUsername());
    }
    /**
     * Creates an instance of object {@link model.Autor} with the specified parameters.
     * @param u ({@link model.Utilizador}) The registered user to add as an author.
     * @param afiliacao (String) The institution of the user.
     */
    public Autor(Utilizador u,String afiliacao)
    {
        setNome(u.getName());
        setEmail(u.getEmail());
        setUsername(u.getUsername());
        setAfiliacao(afiliacao);
    }
    /**
     * Creates an instance of object Autor with the specified parameters.
     * @param name (String) The name of the author.
     * @param email (String) The email of the author.
     * @param instituicao (String) The institution where the author resides.
     * @param username (String) The username of the author.
     */
    public Autor(String name,String email,String instituicao,String username)
    {
        setNome(name);
        setEmail(email);
        setUsername(username);
        setAfiliacao(instituicao);
    }
    /**
     * Sets the name of the author. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param strNome (String) The new name of the author.
     */
    public void setNome(String strNome)
    {
        if (strNome.length()>2)
        {
               this.m_strNome = strNome;
        }
        else
        {
            throw new IllegalArgumentException("O comprimento do nome tem de ser superior a 2!",new Throwable("INVALID_NAME"));
        }
    }
    /**
     * Sends a notification to the {@link model.Autor} via email.
     * @param message (String) The message to send.
     */
    public void enviarNotificacao(String message)
    {
        
    }
    /**
     * Sets the username of the author. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param strNome (String) The new username of the author.
     */
    public void setUsername(String strNome)
    {
        /*if (strNome.length()>2)
        {
               username = strNome;
        }
        else
        {
            throw new IllegalArgumentException("O comprimento do nome tem de ser superior a 2!");
        }*/
        username=strNome;
    }
    /**
     * Sets the email of the author. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param value (String) The new email of the author.
     */
    public void setEmail(String value)
    {
        if(checkEmail(value))
        {
            email=value;
        }
        else
        {
            throw new IllegalArgumentException("Email inválido!", new Throwable("INVALID_EMAIL"));
        }
    }
    /**
     * Sets the institution of the author. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param strAfiliacao (String) The new value for the institution.
     */
    public void setAfiliacao(String strAfiliacao)
    {
        if (strAfiliacao.length()>=3)
        {
            m_strAfiliacao = strAfiliacao;
        }
        else
        {
            throw new IllegalArgumentException("Instituicao de afiliacao tem de conter pelo menos 3 caratéres!",new Throwable("INVALID_AFILIACAO"));
        }
    }
    /**
     * Returns the username of the author.
     * @return (String) The author's username.
     */
    public String getUsername()
    {
        return username;
    }
    /**
     * Returns the name of the author.
     * @return (String) The author's name.
     */
    public String getName()
    {
        return m_strNome;
    }
    /**
     * Returns the email of the author.
     * @return (String) The author's email.
     */
    public String getEmail()
    {
        return email;
    }
    /**
     * Returns the institution of the author.
     * @return (String) The author's institution.
     */
    public String getInstituicaoAfiliacao()
    {
        return m_strAfiliacao;
    }
    /**
     * This method validates this instance's data.
     * It is not used since all the attributions of the data are validated (every
     * setXYZ() only accepts valid data). Although, it still checks if the data is empty
     * or not.
     * @return (boolean) 
     */
    public boolean valida()
    {
        boolean result = false;
        result = m_strNome!=null && email!=null && m_strAfiliacao!=null;
        if (result)
        {
            result=!m_strNome.isEmpty() && !email.isEmpty() && !m_strAfiliacao.isEmpty();
        }
        return result;
    }
    /**
     * Checks if the author can be a corresponding author.
     * @param regU ({@link registos.RegistoUtilizadores}) {@link model.Empresa}'s registry of users.
     * @return (boolean) True if author can be a corresponding author.
     */
    public boolean podeSerCorrespondente(RegistoUtilizadores regU) 
    {
        return regU.temUtilizadorEmail(email) || regU.temUtilizadorUsername(username);
    }

    /**
     * Returns a text representation of the author.
     * @return (String) Text form of author.
     */
    @Override
    public String toString()
    {
        return m_strNome + " - " +email+" - "+ m_strAfiliacao+ " - "+username; 
    }
    /**
     * Checks if this author is equal to another author or object.
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other)
    {
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            Autor temp = (Autor)other;
            result=(this==other) || (m_strNome.equalsIgnoreCase(temp.getName()) 
                    && email.equals(temp.getEmail()) && m_strAfiliacao.equalsIgnoreCase(temp.getInstituicaoAfiliacao()));
        }
        return result;
    }

    @Override
    public String showData() {
        return m_strNome+"\n"+email+"\n"+m_strAfiliacao+"\n"+username+"\n";
    }
}

