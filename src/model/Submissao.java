/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import interfaces.SubmissaoState;
import interfaces.iTOCS;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import listas.ListaConflitosDetetados;
import listas.ListaLicitacoes;
import states.submissao.*;

/**
 * This class represents a submission that has an article.
 * @author jbraga
 */
public class Submissao implements Serializable,iTOCS{
    private Artigo artigoInicial;
    private Artigo artigoFinal;
    private SubmissaoState state;
    private ListaLicitacoes listaLicitacoes;
    private ListaConflitosDetetados listaConflitos;
    /**
     * Creates an instance of object {@link model.Submissao} with null parameters.
     */
    public Submissao()
    {
        artigoInicial=new Artigo();
        artigoFinal = new Artigo();
        listaConflitos=new ListaConflitosDetetados();
        state=new SubmissaoCriadoState(this);
        listaLicitacoes = new ListaLicitacoes();
    }
    /**
     * Creates a copy of an instance of object {@link model.Submissao}.
     * @param sub ({@link model.Submissao}) The target submission to copy.
     */
    public Submissao(Submissao sub)
    {
        artigoInicial=new Artigo(sub.getArtigoInicial());
        state=new SubmissaoCriadoState(this);
        listaLicitacoes = new ListaLicitacoes();
    }
    /**
     * Creates an instance of object {@link model.Submissao} with the specified
     * parameters.
     * @param artigoInicial ({@link model.Artigo}) The initial article of this submission.
     * @param artigoFinal ({@link model.Artigo}) The final article of this submission.
     * @param state ({@link interfaces.SubmissaoState}) This submission's state.
     * @param lcd ({@link listas.ListaConflitosDetetados}) This submission's list of detected conflicts.
     */
    public Submissao(Artigo artigoInicial,Artigo artigoFinal,SubmissaoState state,ListaConflitosDetetados lcd)
    {
        this.artigoInicial=new Artigo(artigoInicial);
        this.artigoFinal=new Artigo(artigoFinal);
        this.state=state;
        listaConflitos=lcd;
        listaLicitacoes = new ListaLicitacoes();
    }
    /**
     * Creates an instance of object {@link model.Submissao} with the specified
     * parameters.
     * @param artigoInicial ({@link model.Artigo}) The initial article of this submission.
     * @param artigoFinal ({@link model.Artigo}) The final article of this submission.
     * @param state ({@link interfaces.SubmissaoState}) This submission's state.
     * @param lcd ({@link listas.ListaConflitosDetetados}) This submission's list of detected conflicts.
     * @param lsl ({@link listas.ListaLicitacoes}) This submission's list of bids.
     */
    public Submissao(Artigo artigoInicial,Artigo artigoFinal,SubmissaoState state,ListaConflitosDetetados lcd,ListaLicitacoes lsl)
    {
        this.artigoInicial=new Artigo(artigoInicial);
        this.artigoFinal=new Artigo(artigoFinal);
        this.state=state;
        listaConflitos=lcd;
        listaLicitacoes = lsl;
    }
    
    /**
     * Creates an instance of object {@link model.Submissao} with the specified
     * parameters.
     * @param artigoInicial ({@link model.Artigo}) The initial article of this submission.
     * @param artigoFinal ({@link model.Artigo}) The final article of this submission.
     * @param state ({@link interfaces.SubmissaoState}) This submission's state.
     * 
     */
     public Submissao(Artigo artigoInicial,Artigo artigoFinal,SubmissaoState state){
         this.artigoInicial=new Artigo(artigoInicial);
        this.artigoFinal=new Artigo(artigoFinal);
        this.state=state;
        this.listaConflitos=new ListaConflitosDetetados();
     }
    
    /**
     * Changes the data of the article of this {@link model.Submissao}
     * @param titulo (String) The title of the article.
     * @param resumo (String) The summary of the article.
     * @param autores (List&lt;Autors&gt;) The list of authors of the article.
     * @param ficheiro (String) The file of the article.
     */
    public void alteraDados(String titulo, String resumo,List<Autor> autores,String ficheiro)
    {
        artigoInicial.setTitulo(titulo);
        artigoInicial.setResumo(resumo);
        artigoInicial.setAutores(autores);
        artigoInicial.setFicheiro(ficheiro);
    }
    /**
     * Changes the data of the article of this {@link model.Submissao}
     * @param titulo (String) The title of the article.
     * @param resumo (String) The summary of the article.
     * @param ficheiro (String) The file of the article.
     */
    public void alteraDados(String titulo, String resumo, String ficheiro)
    {
        artigoInicial.setTitulo(titulo);
        artigoInicial.setResumo(resumo);
        artigoInicial.setFicheiro(ficheiro);
    }
    /**
     * Creates a new article on the submission.
     * @return ({@link model.Artigo}) The article created.
     */
    public Artigo novoArtigo()
    {
        return new Artigo();
    }
    /**
     * Returns the initial article of this submission.
     * @return ({@link model.Artigo}) The initial article of the submission.
     */
    public Artigo getArtigoInicial()
    {
        return artigoInicial;
    }
    /**
     * Returns the final article of this submission.
     * @return ({@link model.Artigo}) The final article of the submission.
     */
    public Artigo getArtigoFinal()
    {
        return artigoFinal;
    }
    /**
     * Returns the info of this submission in text form.
     * @return (String) The submission's info.
     */
    public String getInfo()
    {
        return this.toString();
    }
    /**
     * Returns the current submission's state.
     * @return (String) The current state of the submission.
     */
    public String getState()
    {
        return state.toString();
    }
    /**
     * Returns a {@link model.Licitacao} of a {@link model.Revisor} with the specified
     * id.
     * @param id (String) The revisor's id.
     * @return ({@link model.Licitacao}) The bid.
     */
    public Licitacao getLicitacaoDe(String id)
    {
        return listaLicitacoes.getLicitacaoDe(id);
    }
    /**
     * Returns a list containing all the types of conflicts of this submission with the
     * specified revisor.
     * @param id (String) The id of the revisor.
     * @return (List&lt;{@link model.TipoConflito}&gt;) The list of types of conflict.
     */
    public List<TipoConflito> getTiposConflito(String id)
    {
        return listaConflitos.getTiposConflito(id);
    }
    /**
     * Sets the initial article of this submission.
     * @param artigo ({@link model.Artigo}) The new initial article of this submission.
     */
    public void setArtigoInicial(Artigo artigo)
    {
        this.artigoInicial = new Artigo(artigo);
    }
    /**
     * Sets the initial article of this submission.
     * @param artigo ({@link model.Artigo}) The new initial article of this submission.
     */
    public void setArtigoFinal(Artigo artigo)
    {
        this.artigoFinal = new Artigo(artigo);
    }
    /**
     * Sets the title of the article.
     * @param title (String) The title's new value.
     */
    public void setArtigoTitle(String title)
    {
        artigoInicial.setTitulo(title);
    }
    /**
     * Sets the summary of the article.
     * @param summary (String) The summary's new value.
     */
    public void setArtigoSummary(String summary)
    {
        artigoInicial.setResumo(summary);
    }
    /**
     * Sets the article's data with the specified parameters.
     * @param title (String) The title's new value.
     * @param summary (String) The summary's new value.
     * @param file (File) The article's new file.
     */
    public void setArtigoData(String title,String summary,String file)
    {
        artigoInicial.setTitulo(title);
        artigoInicial.setResumo(summary);
        artigoInicial.setFicheiro(file);
    }
    /**
     * Sets the article's file.
     * @param file (String) The article's new file.
     */
    public void setFile(String file) 
    {
        artigoInicial.setFicheiro(file);
    }
    /**
     * Sets the corresponding author of the article. Throws an IllegalArgumentException in case
     * the data to be assigned is invalid.
     * @param author (Autor) The author to be set as the corresponding author.
     * @param user (User) The user of the company that represents the author.
     */
    public void setAutorCorrespondente(Autor author,Utilizador user)
    {
        if (user!=null)
        {
            artigoInicial.setAutorCorrespondente(author,user);
        }
        else
        {
            throw new IllegalArgumentException("O autor correspondente escolhido não é um utilizador registado no sistema.");
        }
    }
    /**
     * Returns a list of bids
     * @return List &gt;{@link model.Licitacao}&lt; 
     */
    public List<Licitacao> getLicitacoes()
    {
        return listaLicitacoes.getLicitacoes();
    }
    public boolean setSubmissaoRegistadaState()
    {
        return state.setRegistado();
    }
    public void setState(SubmissaoState state)
    {
        this.state=state;
    }
    /**
     * Creates a new author in the article with the specified parameters.
     * @param name (String) The author's name.
     * @param email (String) The author's email.
     * @param institution (String) The author's institution.
     * @param username (String) The author's username.
     * @return (Autor) The newly created author.
     */
    @Deprecated
    public Autor newAutor(String name,String email,String institution,String username)
    {
        return artigoInicial.novoAutor(name, email, institution,username);
    }
    /**
     * Creates a new bid for this submission.
     * @param r ({@link model.Revisor}) The revisor that is creating this bid.
     * @param ltc (List&lt;{@link model.TipoConflito}&gt;) The list of types of conflict.
     * @return (@link model.Licitacao) The newly created bid.
     */
    public Licitacao novaLicitacao(Revisor r,List<TipoConflito> ltc)
    {
        return new Licitacao(this,r,ltc);
    }
    public ListaConflitosDetetados novaListaConflitos()
    {
        listaConflitos=new ListaConflitosDetetados();
        return listaConflitos;
    }
    /**
     * Adds the specified author to the article's author list.
     * @param author (Autor) The author to add.
     * @return (boolean) True if successfully added.
     */
    @Deprecated
    public boolean addAutor(Autor author)
    {
        return artigoInicial.addAutor(author);
    }
    /**
     * Adds a new conflict to the conflict list.
     * @param c ({@link model.ConflitoDetetado}) The conflict to add.
     * @return (boolean) True if successful.
     */
    public boolean addConflitoDetetado(ConflitoDetetado c)
    {
        return listaConflitos.addConflitoDetetado(c);
    }
    /**
     * Registers the {@link model.Licitacao} in the submission.
     * @param l ({@link model.Licitacao}) The bid to register. 
     * @return (boolean) True if successfully registered.
     */
    public boolean registaLicitacao(Licitacao l)
    {
        return listaLicitacoes.registaLicitacao(l);
    }
    /**
     * Removes an author from the article's author list.
     * @param author ({@link model.Autor}) The target author to remove.
     */
    @Deprecated
    public void removeAutor(Autor author)
    {
        artigoInicial.removeAutor(author);
    }
    /**
     * This method returns a list of authors of the initial article of this submission
     * @return (List&lt;{@link model.Autor}&gt;)
     */
    public List<Autor> getListaAutores()
    {
        return artigoInicial.getListaAutores();
    }
    public Autor novoAutor(String username, String email ,String nome, String afiliacao)
    {
        return artigoInicial.novoAutor(username,email ,nome,afiliacao);
    }
        /**
     * Changes the state of the {@link model.Submissao} to {@link states.submissao.SubmissaoEmCameraReadyState}
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setEmCameraReady()
    {
        boolean result = false;
        if(state.isInAceiteState())
        {
            state.setEmCameraReady();
            result = true;
        }
        return result;
    }
    /**
     * Changes the state of the {@link model.Submissao} to {@link states.submissao.SubmissaoAceiteState}
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setAceite()
    {
        boolean result = false;
        if(state.isInRevistoState())
        {
            state.setAceite();
            result = true;
        }
        return result;
    }
        /**
     * Changes the state of the {@link model.Submissao} to {@link states.submissao.SubmissaoRejeitadaState}
     * @return (boolean) Returns true if the operation is sucessful
     */
    public boolean setRejeitada()
    {
        boolean result = false;
        if(state.isInRevistoState())
        {
            state.setRejeitada();
            result = true;
        }
        return result;
    }
     /**
     * Método que irá alterar o estado
     * @return 
     */
    public boolean setPreRevistoState(){
        return true;
    }
    
    /**
     * Checks if this submission's initial article is valid.
     * @return (boolean) True if valid.
     */
    public boolean validaInicial()
    {
        return artigoInicial.valida();
    }
    /**
     * Checks if this submission's initial article has the author with the specified
     * id.
     * @param id (String) The id of the author.
     * @return (boolean) True if article has the author.
     */
    public boolean temAutorArtigoInicial(String id)
    {
        return artigoInicial.temAutor(id);
    }
    /**
     * Returns a copy of an instance of object {@link model.Submissao}
     * @return ({@link model.Submissao})
     */
    public Submissao criarClone()
    {
        return new Submissao(this);
    }
    /**
     * Sets this submission's current state to {@link states.submissao.SubmissaoRemovidaState}.
     * @return (boolean) True if state successfully changed.
     */
    public boolean setRemovida()
    {
        return state.setRemovida();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoCriadoState} (created state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInCriadoState()
    {
        return state.isInCriadoState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoRegistadoState} (registered state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRegistadoState()
    {
        return state.isInRegistadoState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoRemovidaState} (removed state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRemovidaState()
    {
        return state.isInRemovidaState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoAceiteState} (accepted state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInAceiteState()
    {
        return state.isInAceiteState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoEmCameraReadyState} (in
     * camera ready state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmCameraReadyState()
    {
        return state.isInEmCameraReadyState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoEmLicitacaoState} (bidding state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmLicitacaoState()
    {
        return state.isInEmLicitacaoState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoNaoRevistoState} (not revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInNaoRevistoState()
    {
        return state.isInNaoRevistoState();
    }
    /**
     * This method sets the {@link model.Submissao} in {@link states.submissao.SubmissaoEmRevisaoState}
     * @return (boolean) Return true if the operation is sucessful
     */
    public boolean setEmRevisao()
    {
        return state.setEmRevisao();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoPreRevistaState} (pre-revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInPreRevistaState()
    {
        return state.isInPreRevistaState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoEmRevisaoState} (in revision state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInEmRevisaoState()
    {
        return state.isInEmRevisaoState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoRejeitadaState} (rejected state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRejeitadaState()
    {
        return state.isInRejeitadaState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoRevistoState} (revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInRevistoState()
    {
        return state.isInRevistoState();
    }
    /**
     * Checks if this submission is in the {@link states.submissao.SubmissaoAceiteNaoFinalState} (revised state).
     * @return (boolean) True if the submission is in the desired state.
     */
    public boolean isInAceiteNaoFinal()
    {
        return state.isInAceiteNaoFinalState();
    }
    /**
     * Returns the textual format of this submission.
     * @return (String) Submissao's text form.
     */
    @Override
    public String toString()
    {
        return "Submissão:\nArtigoInicial:"+artigoInicial.toString()+
                "\nArtigoFinal:"+artigoFinal.toString();
    }
    /**
     * Checks if this article is equal to another thematic session or object.
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other)
    {
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            Submissao temp = (Submissao)other;
            result=this==other || (artigoInicial.equals(temp.getArtigoInicial()));
        }
        return result;
    }
    @Override
    public String showData() {
        return "";
    }
    
    /**
     * This method updates the accepted topics list with the topics of an accepted submission
     * @param ls the topics list
     * @return  the topics list
     */
    public ArrayList<String> getAceptedTopics(ArrayList<String> ls) {
        if (this.isInAceiteState()) {
            ArrayList<String> lt =this.artigoInicial.getTopicos();
            for(String s:lt){
                if(!ls.contains(s)){
                    ls.add(s);
                }
            }
            return ls;
        } else if(this.isInEmCameraReadyState()){
            ArrayList<String> lt =this.artigoFinal.getTopicos();
            for(String s:lt){
                if(!ls.contains(s)){
                    ls.add(s);
                }
            }
            return ls;
        } else throw new IllegalArgumentException("Esta submissão não foi aceite");
            
    }
    
    /**
     * This method updates the accepted topics list with the topics of a rejected submission
     * @param ls the topics list
     * @return  the topics list
     */
    public ArrayList<String> getRejectedTopics(ArrayList<String> ls){
        if(this.isInRejeitadaState()){
            ArrayList<String> lt =this.artigoInicial.getTopicos();
            for(String s:lt){
                if(!ls.contains(s)){
                    ls.add(s);
                }
            }
            return ls;
        } else throw new IllegalArgumentException("Esta submissão não foi rejeitada");
    }
}
