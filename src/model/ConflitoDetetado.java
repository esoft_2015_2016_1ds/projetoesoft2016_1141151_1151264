/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class ConflitoDetetado implements iTOCS{
    private List<TipoConflito> listaTc;
    private Revisor r;
    private Submissao s;
    public ConflitoDetetado(Revisor r,Submissao s)
    {
        this.r = r;
        this.s = s;
        listaTc = new ArrayList();
    }
    /**
     * Returns the list of types of conflict of this {@link model.ConflitoDetetado}.
     * @return ({List&lt;{@link model.TipoConflito}&gt;}) The list of types of conflict.
     */
    public List<TipoConflito> getTiposConflito()
    {
        return new ArrayList(listaTc);
    }
    public boolean addTiposConflito(List<TipoConflito> list)
    {
        boolean result=false;
        if (!list.isEmpty())
        {
            result=listaTc.addAll(list);
        }
        return result;
    }
    /**
     * Checks if this detected conflict has the specified
     * revisor.
     * @param id (String) The id of the revisor.
     * @return (boolean) True if the revisor is present.
     */
    public boolean temRevisor(String id)
    {
        return r.getUtilizador().getUsername().equals(id) ||
                r.getUtilizador().getEmail().equals(id);
    }
    /**
     * @see interfaces.Submissivel
     * @return 
     */
    @Override
    public String showData() {
        return "";
    }
}
