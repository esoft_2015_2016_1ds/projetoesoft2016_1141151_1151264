/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.Decidivel;
import interfaces.Detetavel;
import interfaces.Distribuivel;
import interfaces.Licitavel;
import interfaces.Revisivel;
import interfaces.SessaoTematicaState;
import interfaces.Submissivel;
import interfaces.iTOCS;
import java.util.ArrayList;
import java.util.List;
import listas.ListaSessoesTematicas;
import listas.ListaSubmissoes;
import states.sessaotematica.*;
import utils.Data;

/**
 *
 * @author jbraga
 */
public class SessaoTematica implements Submissivel, Licitavel, Revisivel, Decidivel, Distribuivel,Detetavel, iTOCS {

    private String codigo, descricao;
    private CP cp;
    private List<Proponente> listaProponentes;
    private ListaSubmissoes listaSubmissoes;
    private ProcessoDistribuicao processoDistribuicao;
    private ProcessoDecisao processoDecisao;
    private SessaoTematicaState state;
    private ListaSessoesTematicas lst;
    private Data dataInicio;
    private Data dataFim;
    private Data dataInicioSubmissao, dataFimSubmissao, dataInicioDistribuicao, dataLimRevisao;
    private Data dataLimiteSubmissaoFinal;

    /**
     * Creates an instance of object
     * {@link model.SessaoTematica} with null parameters.
     */
    public SessaoTematica() {
        codigo = "N/A";
        descricao = "N/A";
        listaProponentes = new ArrayList();
        listaSubmissoes = new ListaSubmissoes();
        state = new SessaoTematicaCriadoState(this);
    }

    /**
     * Creates an instance of object
     * {@link model.SessaoTematica} with the specified
     * parameters.
     *
     * @param lista (List&lt;{@link model.Proponente}&gt;) The event's
     * organization list containing all the people who are organizing this
     * event.
     */
    public SessaoTematica(List<Proponente> lista) {
        codigo = "N/A";
        descricao = "N/A";
        listaProponentes = new ArrayList(lista);
        listaSubmissoes = new ListaSubmissoes();
        state = new SessaoTematicaCriadoState(this);
    }

    public SessaoTematica(ListaSubmissoes ls) {
        codigo = "N/A";
        descricao = "N/A";
        listaProponentes = new ArrayList();
        listaSubmissoes = ls;
        state = new SessaoTematicaCriadoState(this);
    }

    public String getCodigo() {
        return codigo;
    }

    /**
     * Returns the description.
     * @return (String)
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Returns a list of all revisions in this thematic session.
     *
     * @return (List&lt;{@link model.Revisao}&gt;) A list containing all
     * revisions.
     */
    public List<Revisao> getListaTodasRevisoes() {
        List<Revisao> result = new ArrayList();
        if (processoDistribuicao!=null)
        {
            result.addAll(processoDistribuicao.getListaTodasRevisoes());
        }
        return result;
    }

    /**
     * Returns a list of all revisores in this thematic session.
     *
     * @return (List&lt;{@link model.Revisor}&gt;) A list containing all
     * revisors.
     */
    public List<Revisor> getListaTodosRevisores() {
        return cp.getRevisores();
    }

    /**
     * Returns the begin date of the Thematic Session
     *
     * @return {@link utils.Data} the begin date
     */
    public Data getDataInicio() {
        return dataInicio;
    }

    /**
     * Returns the begin date of the Thematic Session
     *
     * @return {@link utils.Data} the begin date
     */
    public Data getDataFim() {
        return dataFim;
    }
    /**
     * Returns the list of proponents of this thematic session.
     * @return (List&lt;{@link model.Proponente}&gt;) The list of proponentes.
     */
    public List<Proponente> getListaProponentes()
    {
        return new ArrayList(listaProponentes);
    }
    /**
     * Changes the begin date of the Thematic Session
     *
     * @param di {@link utils.Data} the begin date
     */
    public void setDataInicio(Data di) {
        if(Data.getDataAtual().isMaior(di))
        {
            throw new IllegalArgumentException("Data de Inicio tem de ser posterior à data atual.");
        } 
        else
        {
        this.dataInicio = di;
        }
    }

    /**
     * Changes the begin date of the Thematic Session
     *
     * @param df {@link utils.Data} the begin date
     */
    public void setDataFim(Data df) {
        if(this.getDataInicio().isMaior(df) ){
            throw new IllegalArgumentException("A data de fim do evento deve ser posterior à data de inicio");
        } else
        this.dataFim = df;
    }

    /**
     * Returns the begin submission date of the Thematic Session
     *
     * @return {@link utils.Data} the begin date
     */
    public Data getDataInicioSubmissao() {
        return dataInicioSubmissao;
    }

    /**
     * Changes the begin submission date of the Thematic Session
     *
     * @param dis {@link utils.Data} the begin submission date
     */
    public void setDataInicioSubmissao(Data dis) {
        if(dis.isMaior(this.getDataInicio())){
           throw new IllegalArgumentException("A data de inicio de submissao do evento deve ser anterior à data de inicio");
       } else
        dataInicioSubmissao = dis;
    }

    /**
     * Returns the end Submission date of the Thematic Session
     *
     * @return {@link utils.Data} the end submission date
     */
    public Data getDataFimSubmissao() {
        return dataFimSubmissao;
    }

    /**
     * Changes the end submission date of the Thematic Session
     *
     * @param dif {@link utils.Data} the end submission date
     */
    public void setDataFimSubmissao(Data dif) {
        if(dif.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de fim de submissão do evento deve ser anterior à data de inicio de");
        } else if (this.getDataInicioSubmissao().isMaior(dif)){
            throw new IllegalArgumentException("A data de fim de submissão do evento tem de ser posterior à data de inicio de submissão");
        } else
        dataFimSubmissao = dif;
    }

    /**
     * Returns the distribution of the Thematic Session
     *
     * @return {@link utils.Data} the distribution date
     */
    public Data getDataInicioDistribuicao() {
        return dataInicioDistribuicao;
    }

                /**
     * This method procures {@link model.Submissao} made by the ser in the {@link model.SessaoTematica} that are in {@link states.submissao.SubmissaoAceiteState}
     * @param id (String) he user 
     * @return List (gt;{@link model.Submissao}lt;) A list of {@link model.Submissao} made by the ser in the {@link model.SessaoTematica} that are in {@link states.submissao.SubmissaoAceiteState}
     */
    public List<Submissao> getListaSubmissoesAceites(String id)
    {
        return listaSubmissoes.getListaSubmissoesAceites(id);
    }
    /**
     * Changes the begin date of the Thematic Session
     *
     * @param dd {@link utils.Data} the begin date
     */
    public void setDataInicioDistribuicao(Data dd) {
        if(dd.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de inicio de distribuicao do evento deve ser anterior à data de inicio");
        } else if (this.getDataFimSubmissao().isMaior(dd)){
            throw new IllegalArgumentException("A data de inicio de distribuicao do eveento deve ser posterior à data de fim de submissao");
        } else
        dataInicioDistribuicao = dd;
    }

    /**
     * Returns the begin date of the Thematic Session
     *
     * @return {@link utils.Data} the begin date
     */
    public Data getDataLimiteRevisao() {
        return dataLimRevisao;
    }

    /**
     * This method checks if there are {@link model.Submissao} in the
     * {@link model.SessaoTematica} list of Submissions
     *
     * @param id (String) The user ID
     * @return (boolean) Returns true if there are valid {@link model.Submissao}
     */
    public boolean temSubmissoesAceitesUtilizador(String id) {
        return listaSubmissoes.temSubmissoesAceitesUtilizador(id);
    }
    
    /**
     * Método que irá verificar se a as  revisões estão ou não todas feitas
     * @return 
     */
    public boolean temTodasRevisoesFeitas()
    {
        return processoDistribuicao.temTodasRevisoesFeitas();
    }
    
    /**
     * Changes the begin date of the Thematic Session
     *
     * @param dlr {@link utils.Data} the begin date
     */
    public void setDataLimiteRevisao(Data dlr) {
        if(dlr.isMaior(this.getDataInicio())){
            throw new IllegalArgumentException("A data de limite de revisao do evento deve ser anterior à data de inicio");
        } else if (this.getDataInicioDistribuicao().isMaior(dlr)){
            throw new IllegalArgumentException("A data de limite de revisao do evento deve ser posterior à data de inicio de distribuicao");
        } else
            dataLimRevisao = dlr;
    }

        /**
     * This method returns the limit date for the final {@link model.Submissao}
     * @return (Data) The limit date for the final {@link model.Submissao} 
     */
    @Override
    public Data getDataLimiteSubmissaoFinal()
    {
        return dataLimiteSubmissaoFinal;
    }

        /**
     * This method generates a list of {@link model.Decisao} within the {@link model.ProcessoDecisao}
     * @return List &gt;Decisao&lt; created by the {@link interfaces.MecanismoDecisao}
     */
    public List<Decisao> decide(List<Revisao> lr)
    {
       return processoDecisao.decide(lr);
    }
    /**
     * This method procures all {@link model.Revisao}
     * @return List(&gt;{@link model.Autor}&lt;) 
     */
    @Override
    public List<Revisao> getRevisoes()
    {
        return processoDistribuicao.getListaTodasRevisoes();
    }
    /**
     * This methods checks if all the {@link model.Submissao} are in {@link states.submissao.SubmissaoEmCameraReadyState}
     * @return (boolean)  Returns true if all the {@link model.Submissao} are in {@link states.submissao.SubmissaoEmCameraReadyState}
     */
    public boolean temTodasSubmissoesCameraReady()
    {
        return listaSubmissoes.temTodasSubmissoesCameraReady();
    }
    /**
     * Changes the begin date of the Thematic Session
     *
     * @param dlsf {@link utils.Data} the begin date
     */
    public void setDataLimiteSubmissaoFinal(Data dlsf) {
        if(dlsf.isMaior(this.dataFim)){
            throw new IllegalArgumentException ("A data de limite de submissao final deve ser anterior à data de fim do evento");
        } else if(this.dataLimRevisao.isMaior(dlsf)){
            throw new IllegalArgumentException("A data de limite de submissao final deve ser posterior à data de fim de revisao");
        } else
        this.dataLimiteSubmissaoFinal = dlsf;
    }

    public boolean validaProponente(Utilizador u) {
        boolean result = true;
        for (Proponente p : listaProponentes) {
            if (p.equals(new Proponente(u))) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * Metodo que verifica se um utillizador é proponente
     *
     * @param id(String)
     * @return
     */
    public boolean temProponente(String id) {
        boolean result = false;
        for (Proponente elemento : listaProponentes) {
            if (elemento.getEmailUser().equals(id) || elemento.getUsernameUser().equals(id)) {
                result = true;
            }
        }
        return result;
    }

    public boolean setCP(CP cp) {
        this.cp = cp;
        return setCPDefinida();
    }

    public List<Submissao> getListaSubmissoesSessao() {
        return listaSubmissoes.getSubmissoes();
    }

    @Override
    public List<Submissao> getSubmissoes()
    {
        return listaSubmissoes.getSubmissoes();
    }
    
    public void setCodigo(String value) {
        codigo = value;
    }

    public void setDescricao(String value) {
        descricao = value;
    }

    public void setState(SessaoTematicaState state) {
        this.state = state;
    }

    public boolean addProponente(Utilizador utilizador) {
        Proponente proponente = new Proponente(utilizador);
        proponente.valida();
        return addProponente(proponente);
    }

    public boolean addProponente(Proponente proponente) {
        return listaProponentes.add(proponente);
    }

    /**
     * Metodo que cria uma nova CP
     *
     * @return a CP criada
     */
    public CP novaCP() {
        cp = new CP();

        return cp;
    }

    public boolean temCP() {
        return cp != null;
    }

    public boolean valida() 
    {
        boolean result = false;
       if(cp.valida() && !listaProponentes.isEmpty())
       {
           result = true;
       }
       return result;
    }

    /**
     * Método que procura todas as submissões aceites
     *
     * @return(int) o número de submissões aceites
     */
    public int getNumeroSubmissoesAceites() {
        return listaSubmissoes.getNumeroSubmissoesAceites();
    }

    /**
     * Método que faz a soma de todas as submissões
     *
     * @return(int) numero total de submissoes
     */
    public int getNumeroTotalSubmissoes() {
        return listaSubmissoes.getNumeroTotalSubmissoes();
    }

    /**
     * Método que devolve a Confiança de um revisor nos tópicos de um evento
     *
     * @return (int) classificacao
     */
    public int getConfianca() {
        return processoDistribuicao.getConfianca();
    }

    /**
     * Método que devolve a classificacao da Adequacao de um evento
     *
     * @return (int) classificacao
     */
    public int getAdequacao() {
        return processoDistribuicao.getAdequacao();
    }

    /**
     * Método que devolve o valor da originalidade do evento
     *
     * @return (int) classificacao
     */
    public int getOriginalidade() {
        return processoDistribuicao.getOriginalidade();
    }

    /**
     * Método que devolve o valor da Qualidade de um evento
     *
     * @return (int) classificação
     */
    public int getQualidade() {
        return processoDistribuicao.getQualidade();
    }

    /**
     * Método que devolve o valor da Recomendação Global de um evento
     *
     * @return (int) classificação
     */
    public int getRecomendacao() {
        return processoDistribuicao.getRecomendacao();
    }

    /**
     * Método que irá guardar a revisão
     *
     * @param rev {@link model.Revisao} revisão a guardar
     * @return true se guardou a revisao, false se não guardou
     */
    @Override
    public boolean saveRevisao(Revisao rev) {
        return processoDistribuicao.saveRevisao(rev);
    }

    public List<Submissao> getSubmissoesUtilizador(String id) {
        return listaSubmissoes.getSubmissoesUtilizador(id);
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaCriadoState} (created state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCriadoState() {
        return state.isInCriadoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaRegistadoState} (registered
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInRegistadoState() {
        return state.isInRegistadoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaCPDefinidaState} (CP defined
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCPDefinidaState() {
        return state.isInCPDefinidaState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaAceitaSubmissoesState}
     * (accepts submissions state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInAceitaSubmissoesState() {
        return state.isInAceitaSubmissoesState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmDetecaoState} (detecting
     * conflicts state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDetecaoState() {
        return state.isInEmDetecaoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmLicitacaoState} (bidding
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmLicitacaoState() {
        return state.isInEmLicitacaoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmDistribuicaoState}
     * (distributing revisions state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDistribuicaoState() {
        return state.isInEmDistribuicaoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmRevisaoState} (in revision
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmRevisao() {
        return state.isInEmRevisaoState();
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmDecisaoState} (in decision
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmDecisaoState() {
        return state.isInEmDecisaoState();
    }

    /**
     * This method returns the index of a Submission within the Event's list of
     * Submissions
     *
     * @param sub {@link model.Submissao}
     * @return (int) The position of the Submission within the Event's list of
     * Submissions
     */
    public int getPosition(Submissao sub) {
        return listaSubmissoes.getPosition(sub);
    }

    /**
     * This method verifies a change made to a submission that belong in the
     * submission list of the Event
     *
     * @param sub ({@link model.Submissao}) The change(clone) to be verified
     * @param i (int) The position of the Submission in the Event's list of
     * Submissions
     * @return (boolean) Returns true if the change is valid.
     */
    public boolean valida(Submissao sub, int i) {
        boolean result = true;
        for (Submissao focus : this.getListaSubmissoes()) {
            if (focus.equals(sub) && (this.listaSubmissoes.getPosition(sub) != i)) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaEmSubmissaoCameraReadyState}
     * (in camera ready submission state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInEmSubmissaoCameraReadyState() {
        return state.isInEmSubmissaoCameraReadyState();
    }

    /**
     * This method returns a list of registered submissions
     *
     * @param id (String)the user's ID
     * @return List&lt;{@link model.Submissao}&gt; The List of the user's registered
     * submissions
     */
    public List<Submissao> getListaSubmissoesRegistadasUtilizador(String id) {
        List<Submissao> result = new ArrayList();
        result.addAll(listaSubmissoes.getListaSubmissoesRegistadasUtilizador(id));
        return result;
    }

    /**
     * Checks if this thematic session is in the
     * {@link states.sessaotematica.SessaoTematicaCameraReadyState} (camera ready
     * state).
     *
     * @return (boolean) True if the thematic session is in the desired state.
     */
    public boolean isInCameraReadyState() {
        return state.isInCameraReadyState();
    }
    /**
     * Returns this thematic session's state in a text format. 
     * Should only be used in case there is a need of saving.
     * @return (String) The thematic session's state.
     */
    public String getState()
    {
        return state.toString();
    }
    /**
     * Adds all the submissions present in the target list to this event's submission list.
     * @param ls (List&lt;{@link model.Submissao}&gt;) The target list of submissions to add.
     */
    public void addAllSubmissoes(List<Submissao> ls)
    {
        listaSubmissoes.addAllSubmissoes(ls);
    }
    /**
     * Adds a submission that was generated from a file (bypassing default creation data
     * set).
     * @param s ({@link model.Submissao}) The submission to add.
     * @return (boolean) True if successfully added.
     */
    public boolean addGeneratedSubmission(Submissao s)
    {
       return listaSubmissoes.addSubmissao(s);
    }
    /**
     * Returns a list of revisions of the specified revisor.
     * @param id (String) The revisor's id. Can be email or username.
     * @return (List&lt;{@link model.Revisao}&gt;) The list of revisions of the
     * target revisor.
     */
    @Override
    public List<Revisao> getRevisoes(String id) {

        return processoDistribuicao.getRevisoes(id);
    }

    @Override
    public String toString() {
        return "SessaoTematica: " + codigo + "/" + descricao;
    }
    /**
     * Creates a new distribution process for this thematic session.
     * @return ({@link model.ProcessoDistribuicao}) The new distribution process.
     */
    @Override
    public ProcessoDistribuicao novoProcessoDistribuicao() {
        return new ProcessoDistribuicao();
    }

    /**
     * Sets this thematic session's distribution process.
     * @param pd ({@link model.ProcessoDistribuicao}) The new distribution process.
     */
    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao pd) 
    {
        processoDistribuicao = pd;
    }

    /**
     * Checks if this thematic session's CP (Comission Program) has the
     * specified revisor.
     *
     * @param id (String) The id of the revisor.
     * @return (boolean) True if the revisor is present.
     */
    public boolean temRevisor(String id) {
        return cp.temMembro(id);
    }

    /**
     * Checks if this thematic session has any articles submitted by an author
     * with the specified id.
     *
     * @param id (String) The author's id (either email or username).
     * @return (boolean) True if it has.
     */
    public boolean temSubmissoesAutor(String id) {
        return listaSubmissoes.temSubmissoesAutor(id);
    }
    /**
     * Checks if this thematic session's distribution process has any revisions.
     * @return (boolean) True if at least one revision has been found.
     */
    public boolean temRevisoes()
    {
        return processoDistribuicao.temRevisoes();
    }
    /**
     * Checks if this thematic session's decision process has any accepted submissions.
     * @return (boolean) True if at least one submission accepted has been found.
     */
    public boolean temSubmissoesAceites()
    {
        return processoDecisao.temSubmissoesAceites();
    }
    /**
     * Checks if this thematic session has any submissions.
     * @return (boolean) True if at least one submission was found.
     */
    public boolean temSubmissoes()
    {
        return listaSubmissoes.temSubmissoes();
    }
    @Override
    public List<Submissao> getSubmissoesNaoCameraReadyDe(String id) {
        return listaSubmissoes.getSubmissoesNaoCameraReadyDe(id);
    }

    /**
     * Returns a list of submissions that are in
     * {@link states.submissao.SubmissaoEmLicitacaoState} state.
     *
     * @return (List&lt;{@link model.Submissao}&gt;)List containing the
     * submissions.
     */
    @Override
    public List<Submissao> getSubmissoesEmLicitacao() {
        return listaSubmissoes.getSubmissoesEmLicitacao();
    }

    /**
     * Returns the {@link model.Revisor} with the specified id.
     *
     * @param id (String) The revisor's id.
     * @return (Revisor) The specified revisor (null if was found).
     */
    @Override
    public Revisor getRevisor(String id) {
        return cp.getRevisor(id);
    }

    /**
     * Creates a new submission for this thematic session.
     *
     * @return (Submissao) The created submission.
     */
    @Override
    public Submissao novaSubmissao() {
        return listaSubmissoes.novaSubmissao();
    }

    /**
     * Adds a submission to this thematic session's submission list.
     *
     * @param submissao (Submissao) The submission to add.
     * @param a (Artigo) The submission's article.
     * @param u (Utilizador) The user that submitted the article.
     * @return (boolean) True if successfully added.
     */
    @Override
    public boolean addSubmissao(Submissao submissao, Artigo a, Utilizador u) {
        boolean result;
        a.setAutorCriador(u);
        a.setDataCriado(Data.getDataAtual());
        result = listaSubmissoes.addSubmissao(submissao);
        if (result) {
            submissao.setSubmissaoRegistadaState();
        }
        return result;
    }

    @Override
    public List<Licitacao> getLicitacoes()
    {
        return listaSubmissoes.getLicitacoes();
    }
    @Override
    public boolean setCriado() {
        return state.setCriado();
    }

    @Override
    public boolean setRegistado() {
        return state.setRegistado();
    }

    @Override
    public boolean setCPDefinida() {
        return state.setCPDefinida();
    }

    @Override
    public boolean setAceitaSubmissoes() {
        return state.setAceitaSubmissoes();
    }

    @Override
    public boolean setEmDetecao() {
        return state.setEmDetecao();
    }

    @Override
    public boolean setEmLicitacao() {
        return state.setEmLicitacao();
    }

    @Override
    public boolean setEmDistribuicao() {
        return state.setEmDistribuicao();
    }

    @Override
    public boolean setEmRevisao() {
        return state.setEmRevisao();
    }

    @Override
    public boolean setEmDecisao() {
        return state.setEmDecisao();
    }

    @Override
    public boolean setSubmissoesDecididas() {
        return state.setEmSubmissaoCameraReady();
    }

    /**
     * This method changes the state of the {@link model.Evento} to
     * {@link states.evento.EventoCameraReadyState}
     *
     * @return (boolean) Returns true if the operation is sucessfull
     */
    @Override
    public boolean setEmCameraReady() {
        return state.setCameraReady();
    }

    /**
     * The method verifies a change made to the submission of the
     * ThematicSession
     *
     * @param sub ({@link model.Submissao})
     * @return (boolean) if the change is valid in the submission list of the
     * ThematicSession
     */
    @Override
    public boolean registaAlteracao(Submissao sub) {
        boolean result = false;
        if (sub.validaInicial() && this.valida(sub, getPosition(sub))) {
            result = true;
        } else {
            throw new IllegalArgumentException("Submissao Inválida");
        }
        return result;
    }
    /**
     * Makes the detection process of this thematic session detect any conflicts
     * between revisors and its submissions.
     * @param lmc (List&lt;{@link model.TipoConflito}&gt;) The list of types
     * of conflict to check whilst detecting.
     */
    @Override
    public void detetarConflitos(List<TipoConflito> lmc) {
        ProcessoDetecao pd = new ProcessoDetecao(this, lmc);
    }

    @Override
    public List<Revisor> getListaRevisoresCP() {
        return cp.getRevisores();
    }

    /**
     * This method returns 
     * @return List (&lt;{@link model.Submissao}&gt;)
     */
    @Override
    public List<Submissao> getListaSubmissoes() {
        return listaSubmissoes.getSubmissoes();
    }

    /**
     * This method returns a new {link model.ProcessoDecisao}
     * @return {ProcessoDecisao} 
     */
    @Override
    public ProcessoDecisao novoProcessoDecisao() {
        return new ProcessoDecisao();
    }
    /**
    * This method sets the {@link model.ProcessoDecisao} for this {@link model.SessaoTematica}
    * @param pd ({@link model.ProcessoDecisao}) the new {@link model.ProcessoDecisao}
    */
    @Override
    public void setProcessoDecisao(ProcessoDecisao pd) {
        this.processoDecisao=pd;
    }


    /**
     * This searches a {@link listas.ListaSubmissoes} for withdrawn submissions
     * needs tests.
     *
     * @return a list containing all the withdrawn submissions found
     */
    @Override
    public List<Submissao> getSubmissoesRemovidas() {
        return listaSubmissoes.getSubmissoesRemovidas();
    }

    /**
     * This method updates the accepted topics list with the topics of each
     * accepted submission
     *
     * @param ls the topics list
     * @return the topics list
     */
    @Override
    public ArrayList<String> getAcceptedTopics(ArrayList<String> ls) {
        return this.listaSubmissoes.getAcceptedTopics(ls);
    }

    /**
     * This method updates the rejected topics list with the topics of each
     * rejected submission
     *
     * @param ls the topics list
     * @return the topics list
     */
    @Override
    public ArrayList<String> getRejectedTopics(ArrayList<String> ls) {
        return this.listaSubmissoes.getRejectedTopics(ls);
    }

    /**
     * This method checks each revision of this submissible's distribuition
     * Process to see if there is an ocurrance of a keyword existing in the
     * arraylist passed as parameter in it. When it finds a match it adds it's
     * global recomendation to it and increments one to the frequency of the
     * keyword
     *
     * @param ls the array list with each keyword with it's global recomendation
     * sum and frequency
     * @return the array list with each keyword with it's global recomendation
     * sum and frequency updated with each revision
     */
    @Override
    public ArrayList<String[]> getStatsTopics(ArrayList<String[]> ls) {
        return this.processoDistribuicao.getStatsTopicos(ls);
    }
    
    @Override
    public boolean equals(Object other) {
        boolean result = (other != null) && (getClass() == other.getClass());
        if (result) {
            SessaoTematica temp = (SessaoTematica) other;
            result = (codigo != null && descricao != null);
            if (result) {
                result = (this == other) || codigo.equals(temp.getCodigo()) && descricao.equals(temp.getDescricao());

            } else {
                result = codigo == temp.getCodigo() && descricao == temp.getDescricao();
            }
        }
        return result;
    }

    @Override
    public String showData() {
        return ((cp != null) ? "CP não nula" : "CP nula") + "\n"+ ((this!=null) ? this.getCodigo(): "") +
                "\n"+ ((this!=null) ?this.getDescricao() : "" )+"\n"+(String.valueOf(System.identityHashCode(processoDistribuicao)))+"\n";
    }

}
