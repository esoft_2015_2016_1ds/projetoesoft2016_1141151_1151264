/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;

/**
 *
 * @author Nuno Silva
 */
public class Revisor implements iTOCS
{
    private String strNome;
    private Utilizador utilizador;
    
    public Revisor(Utilizador u)
    {
        strNome = u.getName();
        utilizador = u;
    }
    public Revisor(){
        strNome="";
        utilizador=new Utilizador();
    }

    public boolean valida()
    {
        return utilizador.valida();
    }

    public String getNome()
    {
        return strNome;
    }
    
    public Utilizador getUtilizador()
    {
        return utilizador;
    }
    /**
     * Método que atribui um novo Utilizador ao Utilizador
     * @param utilizador(@link model.Utilizador) o novo Utilizador 
     */
    public void setUtilizador(Utilizador utilizador)
    {
        this.utilizador=new Utilizador(utilizador);
        
    }
    
    /**
     * Checks if this user is equal to another revisor or object.
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other)
    {
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            Revisor temp = (Revisor)other;
            if (this.utilizador!=null && temp.getUtilizador()!=null)
            result=(this==other) || (utilizador.equals(temp.getUtilizador()));
        }
        return result;
    }

    
    @Override
    public String toString()
    {
        return "Revisor "+utilizador.getName();
    }

    @Override
    public String showData() {
        return "Nome"+utilizador.getName()+"Mail"+utilizador.getEmail()+"Username"+utilizador.getUsername()+"Password"+utilizador.getPassword();
    }
}
