/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.iTOCS;
import java.util.ArrayList;

/**
 *
 * @author jbraga
 */
public class Revisao implements iTOCS {

    private String textoExplicativo;
    public int classificacaoConfianca=-5;
    public int classificacaoOriginalidade=-5;
    public int classificacaoAdequacao=-5;
    public int classificacaoApresentacao=-5;
    public int classificacaoRecomendacao=-5;
    private Submissao submissao;
    public Revisor revisor;

    public Revisao() {
        textoExplicativo="";
    }

    public Revisao(int classificacaoConfianca, int classificacaoOriginalidade, int classificacaoAdequacao, int classificacaoApresentacao, int classificacaoRecomendacao) {
        this.classificacaoAdequacao = classificacaoAdequacao;
        this.classificacaoApresentacao = classificacaoApresentacao;
        this.classificacaoConfianca = classificacaoConfianca;
        this.classificacaoOriginalidade = classificacaoOriginalidade;
        this.classificacaoRecomendacao = classificacaoRecomendacao;
    }

    public Revisao(Revisor r) {
        setRevisor(r);
    }

    public Revisao(Submissao submissao) {
        this.submissao = submissao;
    }


    public Submissao getSubmissao() {
        return submissao;
    }

    /**
     * Devolve a Classificação da Confianca de um Revisor nos tópicos do evento
     *
     * @return (int) classificação
     */
    public int getClassificacaoConfianca() {
        return classificacaoConfianca;
    }
    public Artigo getArtigo(){
        return submissao.getArtigoInicial();
    }
    /**
     * Devolve a Classificação quanto à originalidade de um evento
     *
     * @return (int) classificação
     */
    public int getClassificacaoOriginalidade() {
        return classificacaoOriginalidade;
    }

    /**
     * Devolve a Classificação quanto à Adequação a um evento
     *
     * @return (int) classificação
     */
    public int getClassificacaoAdequacao() {
        return classificacaoAdequacao;
    }

    /**
     * Devolve a Classificação quanto à Qualidade da Apresentação
     *
     * @return (int) classificação
     */
    public int getClassificacaoApresentacao() {
        return classificacaoApresentacao;
    }

    /**
     * Devolve a Classificação quanto à Recomendação Global
     *
     * @return
     */
    public int getClassificacaoRecomendacao() {
        return classificacaoRecomendacao;
    }

    /**
     * Método que irá alterar a classificação da confiança
     *
     * @param classificacaoConfianca(int) classificacao a atribuir
     */
    public void setClassificacaoConfianca(int classificacaoConfianca) {
        if (classificacaoConfianca < 0 || classificacaoConfianca > 5) {
            throw new IllegalArgumentException("A classificação atribuida à Confiança inválida");
        } else {
            this.classificacaoConfianca = classificacaoConfianca;
        }
    }

    /**
     * Método que irá alterar a classificação da originalidade
     *
     * @param classificacaoOriginalidade(int) classificacao a atribuir
     */
    public void setClassificacaoOriginalidade(int classificacaoOriginalidade) {
        if (classificacaoOriginalidade < 0 || classificacaoOriginalidade > 5) {
            throw new IllegalArgumentException("A classificação atribuida à Originalidade inválida");
        } else {
            this.classificacaoOriginalidade = classificacaoOriginalidade;
        }
    }

    /**
     * Método que irá alterar a classificação da adequação
     *
     * @param classificacaoAdequacao(int) classificacao a atribuir
     */
    public void setClassificacaoAdequacao(int classificacaoAdequacao) {
        if (classificacaoAdequacao < 0 || classificacaoAdequacao > 5) {
            throw new IllegalArgumentException("A classificação atribuida à Adequação inválida");
        } else {
            this.classificacaoAdequacao = classificacaoAdequacao;
        }
    }

    /**
     * Método que irá alterar a classificação da apresentacao
     *
     * @param classificacaoApresentacao(int) classificacao a atribuir
     */
    public void setClassificacaoApresentacao(int classificacaoApresentacao) {
        if (classificacaoApresentacao < 0 || classificacaoApresentacao > 5) {
            throw new IllegalArgumentException("A classificação atribuida à Apresentação inválida");
        } else {
            this.classificacaoApresentacao = classificacaoApresentacao;
        }
    }

    /**
     * Método que irá alterar a classificação da recomendacao
     *
     * @param classificacaoRecomendacao(int) classificacao a atribuir
     */
    public void setClassificacaoRecomendacao(int classificacaoRecomendacao) {
        if (classificacaoRecomendacao < -2 || classificacaoRecomendacao > 2) {
            throw new IllegalArgumentException("A classificação atribuida à Recomendação inválida");
        } else {
            this.classificacaoRecomendacao = classificacaoRecomendacao;
        }
    }

    /**
     * Método que irá alterar o texto explicativo
     *
     * @param textoExplicativo(String) texto explicativo
     */
    public void setTextoExplicativo(String textoExplicativo) {
        if (textoExplicativo.length() > 2) {
            this.textoExplicativo = textoExplicativo;
        } else {
            throw new IllegalArgumentException("Insira um texto explicativo válido");
        }
    }
    public void setSubmissao(Submissao s) {
        submissao = s;
    }

    public void setRevisor(Revisor r) {
        revisor = r;
    }
    
    public boolean setPreRevistoState(){
        return submissao.setPreRevistoState();
    }

    /**
     * This method checks the article of this submission to see if there is an
     * ocurrance of a keyword existing in the arraylist passed as parameter in
     * it. When it finds a match it adds it's global recomendation to it and
     * increments one to the frequency of the keyword
     *
     * @param ls the array list with each keyword with it's global recomendation
     * sum and frequency
     * @return the array list with each keyword with it's global recomendation
     * sum and frequency updated with this submission
     */
    public ArrayList<String[]> getStatsTopicos(ArrayList<String[]> ls) {
        ArrayList<String> lt = submissao.getArtigoInicial().getTopicos();
        for (int i = 0; i < ls.size(); i++) {
            for (int j = 0; j < lt.size(); j++) {
                if (ls.get(i)[0].equalsIgnoreCase(lt.get(j))) {
                    int a = Integer.parseInt(ls.get(i)[1]) + this.classificacaoRecomendacao;
                    ls.get(i)[1] = a + "";
                    int b = Integer.parseInt(ls.get(i)[2]) + 1;
                    ls.get(i)[2] = b + "";
                    break;
                }
            }
        }
        return ls;
    }
    /**
     * Returns the textual format of this revision.
     * @return (String) The textual form of this revision.
     */
    @Override
    public String toString()
    {
        return submissao.getArtigoInicial().getInfo();
        
    }
    @Override
    public String showData() {
        return "";
    }

    public String getTextoExplicativo() {
        return textoExplicativo;
    }

    public Revisor getRevisor() {
        return revisor;
    }
    /**
     * Checks if this user is equal to another user or object.
     * @param other (Object) The object to compare with.
     * @return (boolean) Result of the equality.
     */
    @Override
    public boolean equals(Object other)
    {
        boolean result=(other!=null) && (getClass()==other.getClass());
        if (result)
        {
            Revisao temp = (Revisao)other;
            if (revisor!=null && submissao!=null)
            {
                result=true;
            }
            if (result)
            {
           
                result = revisor.equals(temp.getRevisor()) && submissao.equals(temp.getSubmissao());
            }
            else
            {
                result= revisor==temp.getRevisor() && submissao==temp.getSubmissao();
            }
        }
        return result;
    }
}
